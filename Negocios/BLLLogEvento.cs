﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLLogEvento
    {
        private DALConexao conexao;

        public BLLLogEvento(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloLogEvento modelo)
        {
            int id = 0;

            /*if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();*/

            DALLogEvento dalLogEvento = new DALLogEvento(conexao);
            id = dalLogEvento.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloLogEvento modelo)
        {

            /*if (modelo.IdFornecedor <= 0)
            {
                throw new Exception("Código do fornecedor deve ser informado.");
            }

            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();

            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            dalFornecedor.Alterar(modelo);
            */
        }

        public void Excluir(int codigo)
        {
            DALLogEvento dalLogEvento = new DALLogEvento(conexao);
            dalLogEvento.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALLogEvento dalLogEvento = new DALLogEvento(conexao);
            return dalLogEvento.Localizar(valor);
        }
        public ModeloLogEvento CarregarModeloLogEvento(int codigo)
        {
            DALLogEvento dalLogEvento = new DALLogEvento(conexao);
            return dalLogEvento.CarregarModeloLogEvento(codigo);
        }
        public ModeloLogEvento CarregarModeloLogEventoPedido(int idPedido)
        {
            DALLogEvento dalLogEvento = new DALLogEvento(conexao);
            return dalLogEvento.CarregarModeloLogEventoPedido(idPedido);
        }
    }
}
