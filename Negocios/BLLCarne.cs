﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLCarne
    {
        private DALConexao conexao;

        public BLLCarne(DALConexao cx)
        {
            this.conexao = cx;
        }

        /*public int BaixarParcela(ModeloCarne modelo, string tipoRecebimento)
        {
            int id = 0;

            if (modelo.IdPedido.ToString().Length == 0)
            {
                throw new Exception("O ID Conta não pode esta em branco.");
            }

            DALCarne dalCarner = new DALCarne(conexao);
            id = dalCarner.baixarParcela(modelo, tipoRecebimento);
            return id;
        }*/


        /*public int CancelarContasAReceber(int codigo)
        {
            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            return dalContaReceber.CancelarContasAReceber(codigo);
        }*/



       /* public int EstornarContasAReceber(int codigo)
        {
            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            return dalContaReceber.EstornarContasAReceber(codigo);
        }

        public int ExcluirContasAReceber(int codigo)
        {
            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            return dalContaReceber.ExcluirContasAReceber(codigo);
        }*/

        public int Incluir(ModeloCarne modelo)
        {
            int id = 0;

            if (modelo.IdPedido.ToString().Length == 0)
            {
                throw new Exception("O ID Conta não pode esta em branco.");
            }

            if (modelo.IdCliente.ToString().Length == 0)
            {
                throw new Exception("O ID Cliente não pode esta em branco.");
            }

            //modelo.NM_nomeConjuge = modelo.NM_nomeConjuge.ToUpper();


            DALCarne dalCarne = new DALCarne(conexao);
            id = dalCarne.Incluir(modelo);
            return id;
        }
    }
}
