﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLItemPedidoVenda
    {
        private DALConexao conexao;

        public BLLItemPedidoVenda(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloItemPedidoVenda modelo)
        {
            int id = 0;

            /*if (modelo.NM_nome.Trim().Length == 0)
            {
                throw new Exception("O Nome do operdor não pode estar em branco.");
            }


            modelo.CD_operador = modelo.CD_operador.ToUpper();
            modelo.CD_login = modelo.CD_login.ToUpper();
            modelo.DE_email = modelo.DE_email.ToUpper();
            modelo.DE_senha = modelo.DE_senha.ToUpper();*/


            DALItemPedidoVenda dalItemPedidoVenda = new DALItemPedidoVenda(conexao);
            id = dalItemPedidoVenda.Incluir(modelo);
            return id;
        }

        public List<ModeloItemPedidoVenda> RecuperaItensPedido(int idPedido)
        {
            DALItemPedidoVenda dalItemPedidoVenda = new DALItemPedidoVenda(conexao);
            return dalItemPedidoVenda.RecuperaItensPedido(idPedido);
        }
    }
}
