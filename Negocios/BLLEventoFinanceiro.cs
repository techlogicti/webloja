﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLEventoFinanceiro
    {
        private DALConexao conexao;

        public BLLEventoFinanceiro(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int EstornarEventoFinanceiro(int idConta, int idEventoFinanceiro)
        {
            DALEventoFinanceiro dalEventoFinanceiro = new DALEventoFinanceiro(conexao);
            return dalEventoFinanceiro.EstornarEventoFinanceiro(idConta, idEventoFinanceiro);
        }

        public void atualizaValorRecebidoTabelaContasReceberAposRecebimento(ModeloEventoFinanceiro modelo)
        {
            DALEventoFinanceiro dEvFin = new DALEventoFinanceiro(conexao);
            
            dEvFin.atualizaValorRecebidoTabelaContasReceberAposRecebimento(modelo);
        }

        public int Incluir(ModeloEventoFinanceiro modelo)
        {
            int id = 0;

            if (modelo.Id.ToString().Length == 0)
            {
                throw new Exception("O ID não pode esta em branco.");
            }

            //modelo.NM_nomeConjuge = modelo.NM_nomeConjuge.ToUpper();


            DALEventoFinanceiro dalEventoFinanceiro = new DALEventoFinanceiro(conexao);
            id = dalEventoFinanceiro.Incluir(modelo);
            return id;
        }
    }
}
