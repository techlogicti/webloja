﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLNotaFiscalEntrada
    {

        private DALConexao conexao;

        public BLLNotaFiscalEntrada(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloNotaFiscalEntrada modelo)
        {
            int id = 0;

            /*if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();
            */
            DALNotaFiscalEntrada dalNotaFiscalEntrada = new DALNotaFiscalEntrada(conexao);
            id = dalNotaFiscalEntrada.Incluir(modelo);
            return id;
        }


        public void Alterar(ModeloNotaFiscalEntrada modelo)
        {

           /* if (modelo.IdFornecedor <= 0)
            {
                throw new Exception("Código do fornecedor deve ser informado.");
            }

            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();

            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            dalFornecedor.Alterar(modelo);*/

        }

        public void Excluir(int codigo)
        {
            DALNotaFiscalEntrada dalNotaFiscal = new DALNotaFiscalEntrada(conexao);
            dalNotaFiscal.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALNotaFiscalEntrada dalNotaFiscal = new DALNotaFiscalEntrada(conexao);
            return dalNotaFiscal.Localizar(valor);
        }

        public ModeloNotaFiscalEntrada LocalizarBetter(string chaveNfe)
        {
            DALNotaFiscalEntrada dalNotaFiscal = new DALNotaFiscalEntrada(conexao);
            return dalNotaFiscal.LocalizarBetter(chaveNfe);
        }

        

        public ModeloNotaFiscalEntrada CarregarModeloNotaFiscalEntrada(int codigo)
        {
            DALNotaFiscalEntrada dalNotaFiscal = new DALNotaFiscalEntrada(conexao);
            return dalNotaFiscal.CarregarModeloNotaFiscalEntrada(codigo);
        }

    }
}
