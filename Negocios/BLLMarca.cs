﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLMarca
    {
        private DALConexao conexao;

        public BLLMarca(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloMarca modelo)
        {
            int id = 0;
            /*
            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();*/

            DALMarca dalMarca = new DALMarca(conexao);
            id = dalMarca.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloMarca modelo)
        {
            /*
            if (modelo.IdFornecedor <= 0)
            {
                throw new Exception("Código do fornecedor deve ser informado.");
            }

            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();
            */
            DALMarca dalMarca = new DALMarca(conexao);
            dalMarca.Alterar(modelo);

        }

        public void Excluir(int codigo)
        {
            DALMarca dalMarca = new DALMarca(conexao);
            dalMarca.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALMarca dalMarca = new DALMarca(conexao);
            return dalMarca.Localizar(valor);
        }
        public ModeloMarca CarregarModeloMarca(int codigo)
        {
            DALMarca dalMarca = new DALMarca(conexao);
            return dalMarca.CarregarModeloMarca(codigo);
        }
    }
}
