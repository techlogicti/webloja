﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLCategoria
    {
        private DALConexao conexao;

        public BLLCategoria(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloCategoria modelo)
        {
            int id = 0;
            /*
            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();*/

            DALCategoria dalCategoria = new DALCategoria(conexao);
            id = dalCategoria.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloCategoria modelo)
        {
            /*
            if (modelo.IdFornecedor <= 0)
            {
                throw new Exception("Código do fornecedor deve ser informado.");
            }

            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();
            */
            DALCategoria dalCategoria = new DALCategoria(conexao);
            dalCategoria.Alterar(modelo);

        }

        public void Excluir(int codigo)
        {
            DALCategoria dalCategoria = new DALCategoria(conexao);
            dalCategoria.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALCategoria dalCategoria = new DALCategoria(conexao);
            return dalCategoria.Localizar(valor);
        }
        public ModeloCategoria CarregarModeloCategoria(int codigo)
        {
            DALCategoria dalCategoria = new DALCategoria(conexao);
            return dalCategoria.CarregarModeloCategoria(codigo);
        }
    }
}
