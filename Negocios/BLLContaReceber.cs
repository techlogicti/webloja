﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLContaReceber
    {
        private DALConexao conexao;

        public BLLContaReceber(DALConexao cx)
        {
            this.conexao = cx;
        }

        public string obtemSaldoAPagarDaPrestacao(string idConta, string parcela)
        {
            string saldo = "";
            DALContaReceber dALContaReceber = new DALContaReceber(conexao);
            saldo = dALContaReceber.obtemSaldoAPagarDaPrestacao(idConta, parcela);
            return saldo;
        }

        public int BaixarConta(ModeloContaReceber modelo,string tipoRecebimento)
        {
            int id = 0;

            if (modelo.IdPedido.ToString().Length == 0)
            {
                throw new Exception("O ID Conta não pode esta em branco.");
            }

            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            id = dalContaReceber.baixarConta(modelo,tipoRecebimento);
            return id;
        }


        public int CancelarContasAReceber(int codigo)
        {
            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            return dalContaReceber.CancelarContasAReceber(codigo);
        }

        

        public int EstornarContasAReceber(int codigo)
        {
            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            return dalContaReceber.EstornarContasAReceber(codigo);
        }

        public int ExcluirContasAReceber(int codigo)
        {
            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            return dalContaReceber.ExcluirContasAReceber(codigo);
        }

        public int Incluir(ModeloContaReceber modelo)
        {
            int id = 0;

            if (modelo.IdPedido.ToString().Length == 0)
            {
                throw new Exception("O ID Conta não pode esta em branco.");
            }

            if (modelo.IdCliente.ToString().Length == 0)
            {
                throw new Exception("O ID Cliente não pode esta em branco.");
            }

            //modelo.NM_nomeConjuge = modelo.NM_nomeConjuge.ToUpper();
            

            DALContaReceber dalContaReceber = new DALContaReceber(conexao);
            id = dalContaReceber.Incluir(modelo);
            return id;
        }

    }
}
