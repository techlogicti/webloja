﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
  public class BLLOperador
    {

        private DALConexao conexao;

        public BLLOperador(DALConexao cx)
        {
            this.conexao = cx;
        }

        public ModeloOperador recuperaIdLogin(string login)
        {
            DALOperador dalOperador = new DALOperador(conexao);
            return dalOperador.recuperaIdLogin(login);
        }

        public int Incluir(ModeloOperador modelo)
        {
            int id = 0;

            if (modelo.NM_nome.Trim().Length == 0)
            {
                throw new Exception("O Nome do operdor não pode estar em branco.");
            }

            
            modelo.CD_operador = modelo.CD_operador.ToUpper();
            modelo.CD_login = modelo.CD_login.ToUpper();
            modelo.DE_email = modelo.DE_email.ToUpper();
            modelo.DE_senha = modelo.DE_senha.ToUpper();
            

            DALOperador dalOperador = new DALOperador(conexao);
            id = dalOperador.Incluir(modelo);
            return id;
        }

        public void Excluir(int codigo)
        {
            DALOperador dalOperador = new DALOperador(conexao);
            dalOperador.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALOperador dalOperador = new DALOperador(conexao);
            return dalOperador.Localizar(valor);
        }

        public void Alterar(ModeloOperador modelo)
        {

            if (modelo.Id <= 0)
            {
                throw new Exception("Código do operador deve ser informado.");
            }

            if (modelo.NM_nome.Trim().Length == 0)
            {
                throw new Exception("O nome do operador não pode estar em branco.");
            }

            modelo.CD_operador = modelo.CD_operador.ToUpper();
            modelo.CD_login = modelo.CD_login.ToUpper();
            modelo.DE_email = modelo.DE_email.ToUpper();
            modelo.DE_senha = modelo.DE_senha.ToUpper();

            DALOperador dalOperador = new DALOperador(conexao);
            dalOperador.Alterar(modelo);

        }

    }
}
