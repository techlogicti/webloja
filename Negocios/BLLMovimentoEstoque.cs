﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLMovimentoEstoque
    {
        private DALConexao conexao;

        public BLLMovimentoEstoque(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int RetornaItensPedidoCancelado(ModeloMovimentoEstoque modelo)
        {
            int retorno = 0;
            DALMovimentoEstoque dalMovimentoEstoque = new DALMovimentoEstoque(conexao);
            retorno = dalMovimentoEstoque.RetornaItensPedidoCancelado(modelo);
            
            return retorno;
        }

        public int Incluir(ModeloMovimentoEstoque modelo)
        {
            int id = 0;

            /*if (modelo.NM_nome.Trim().Length == 0)
            {
                throw new Exception("O Nome do operdor não pode estar em branco.");
            }


            modelo.CD_operador = modelo.CD_operador.ToUpper();
            modelo.CD_login = modelo.CD_login.ToUpper();
            modelo.DE_email = modelo.DE_email.ToUpper();
            modelo.DE_senha = modelo.DE_senha.ToUpper();*/


            DALMovimentoEstoque dalMovimentoEstoque = new DALMovimentoEstoque(conexao);
            id = dalMovimentoEstoque.Incluir(modelo);
            return id;
        }

        public List<ModeloMovimentoEstoque> RecuperaItensMovimento(int idPedido)
        {
            DALMovimentoEstoque dalMovimentoEstoque = new DALMovimentoEstoque(conexao);
            return dalMovimentoEstoque.RecuperaItensMovimento(idPedido);
        }
    }
}
