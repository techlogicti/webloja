﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLPedidoVenda
    {
        private DALConexao conexao;

        public BLLPedidoVenda(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloPedidoVenda modelo)
        {
            int id = 0;
            /*
            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();
            */

            DALPedidoVenda dalPedidoVenda = new DALPedidoVenda(conexao);
            id = dalPedidoVenda.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloPedidoVenda modelo)
        {

            /* if (modelo.IdFornecedor <= 0)
             {
                 throw new Exception("Código do fornecedor deve ser informado.");
             }

             if (modelo.RazaoSocial.Trim().Length == 0)
             {
                 throw new Exception("A Razão Social não pode estar em branco.");
             }

             modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
             modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
             modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
             modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();*/

            DALPedidoVenda dalPedido = new DALPedidoVenda(conexao);
            dalPedido.Alterar(modelo);

        }

        public void Excluir(int codigo)
        {
            DALPedidoVenda dalPedido = new DALPedidoVenda(conexao);
            dalPedido.Excluir(codigo);
        }

        public int CancelarPedido(int codigo)
        {
            DALPedidoVenda dalPedido = new DALPedidoVenda(conexao);
            return dalPedido.CancelarPedido(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALPedidoVenda dalPedido = new DALPedidoVenda(conexao);
            return dalPedido.Localizar(valor);
        }
        public ModeloPedidoVenda CarregarModeloPedidoVenda(int codigo)
        {
            DALPedidoVenda dalPedido = new DALPedidoVenda(conexao);
            return dalPedido.CarregarModelo(codigo);
        }
    }
}
