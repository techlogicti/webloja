﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLProduto
    {

        private DALConexao conexao;

        public BLLProduto(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloProduto modelo)
        {
            int id = 0;

            if (modelo.nmNome.Trim().Length == 0)
            {
                throw new Exception("Nome do produto é obrigatório.");
            }

            //modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            //modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            //modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            //modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();

            DALProduto dallProduto = new DALProduto(conexao);
            id = dallProduto.Incluir(modelo);
            return id;
        }

        public int Alterar(ModeloProduto modelo)
        {

            int t = 0;

            if (modelo.id <= 0)
            {
                throw new Exception("Código do produto deve ser informado.");
            }

            if (modelo.nmNome.Trim().Length == 0)
            {
                throw new Exception("Nome do produto é obrigatório.");
            }

            //modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            //modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            //modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            //modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();

            DALProduto dallProduto = new DALProduto(conexao);
            t = dallProduto.Alterar(modelo);

            return 0;
        }
         
        public void Excluir(int codigo)
        {
            
            DALProduto dallProduto = new DALProduto(conexao);
            dallProduto.Excluir(codigo);

            
        }

        public DataTable Localizar(String valor)
        {
            DALProduto dallProduto = new DALProduto(conexao);
            return dallProduto.Localizar(valor);
        }
        public ModeloProduto CarregarModeloProduto(int codigo)
        {
            DALProduto dallProduto = new DALProduto(conexao);
            return dallProduto.CarregarModeloProduto(codigo);
        }

        public int ObtemSaldoAtualProdutoPorId(int id)
        {
            int saldo = 0;

            DALProduto dallProduto = new DALProduto(conexao);
            saldo = dallProduto.ObtemSaldoAtualProdutoPorId(id);

            return saldo;
        }

    }
}
