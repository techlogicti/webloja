﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLPedidoCompra
    {
        private DALConexao conexao;

        public BLLPedidoCompra(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloPedidoCompra modelo)
        {
            int id = 0;
            /*
            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();
            */

            DALPedidoCompra dalPedidoCompra = new DALPedidoCompra(conexao);
            id = dalPedidoCompra.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloPedidoCompra modelo)
        {

            /* if (modelo.IdFornecedor <= 0)
             {
                 throw new Exception("Código do fornecedor deve ser informado.");
             }

             if (modelo.RazaoSocial.Trim().Length == 0)
             {
                 throw new Exception("A Razão Social não pode estar em branco.");
             }

             modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
             modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
             modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
             modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();*/

            DALPedidoCompra dalPedidoCompra = new DALPedidoCompra(conexao);
            dalPedidoCompra.Alterar(modelo);

        }

        public void Excluir(int codigo)
        {
            DALPedidoCompra dalPedidoCompra = new DALPedidoCompra(conexao);
            dalPedidoCompra.Excluir(codigo);
        }

        public int CancelarPedido(int codigo)
        {
            DALPedidoCompra dalPedidoCompra = new DALPedidoCompra(conexao);
            return dalPedidoCompra.CancelarPedido(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALPedidoCompra dalPedidoCompra = new DALPedidoCompra(conexao);
            return dalPedidoCompra.Localizar(valor);
        }
        public ModeloPedidoCompra CarregarModeloPedidoVenda(int codigo)
        {
            DALPedidoCompra dalPedidoCompra = new DALPedidoCompra(conexao);
            return dalPedidoCompra.CarregarModelo(codigo);
        }

    }
}
