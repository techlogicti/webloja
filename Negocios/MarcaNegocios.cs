﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcessoBancoDados;
using ObjetoTransferencia;
using System.Data;

namespace Negocios
{
    public class MarcaNegocios
    {
        //Instanciar = criar um novo objeto baseado em um modelo
        AcessoDadosSqlServer acessoDadosSqlServer = new AcessoDadosSqlServer();

        //public string Inserir(string descricao_curta, string descricao_longa, int cd_operador, int dt_dataCriacao, int hr_horaCriacao, int dt_dataAtualizacao, int hr_horaAtualizacao, string st_situacaoRegistro){ }

        public string Inserir(ModeloMarca marca)
        {
            try
            {

                acessoDadosSqlServer.LimparParametros();
                acessoDadosSqlServer.AdicionarParametros("@DescricaoCurta", marca.Descricao_curta);
                acessoDadosSqlServer.AdicionarParametros("@DescricaoLonga", marca.Descricao_longa);
                acessoDadosSqlServer.AdicionarParametros("@CD_operador", marca.CD_operador);
                acessoDadosSqlServer.AdicionarParametros("@DT_dataCriacao", marca.DT_dataCriacao);
                acessoDadosSqlServer.AdicionarParametros("@HR_horaCriacao", marca.HR_horaCriacao);
                acessoDadosSqlServer.AdicionarParametros("@DT_dataAtualizacao", marca.DT_dataAtualizacao);
                acessoDadosSqlServer.AdicionarParametros("@HR_horaAtualizacao", marca.HR_horaAtualizacao);
                acessoDadosSqlServer.AdicionarParametros("@ST_situacaoRegistro", marca.ST_situacaoRegistro);

                string idMarca = acessoDadosSqlServer.ExecutarManipulacao(CommandType.StoredProcedure, "procMarcaInserir").ToString();

                return idMarca;

            }
            catch (Exception ex)
            {
                //return ex.Message;
                throw new Exception(ex.Message);
            }

        }

        public string Alterar(ModeloMarca marca)
        {
            try
            {
                acessoDadosSqlServer.LimparParametros();
                acessoDadosSqlServer.AdicionarParametros("@IDMarca", marca.IdMarca);
                acessoDadosSqlServer.AdicionarParametros("@DescricaoCurta", marca.Descricao_curta);
                acessoDadosSqlServer.AdicionarParametros("@DescricaoLonga", marca.Descricao_longa);
                acessoDadosSqlServer.AdicionarParametros("@CD_operador", marca.CD_operador);
                acessoDadosSqlServer.AdicionarParametros("@DT_dataCriacao", marca.DT_dataCriacao);
                acessoDadosSqlServer.AdicionarParametros("@HR_horaCriacao", marca.HR_horaCriacao);
                acessoDadosSqlServer.AdicionarParametros("@DT_dataAtualizacao", marca.DT_dataAtualizacao);
                acessoDadosSqlServer.AdicionarParametros("@HR_horaAtualizacao", marca.HR_horaAtualizacao);
                acessoDadosSqlServer.AdicionarParametros("@ST_situacaoRegistro", marca.ST_situacaoRegistro);

                string idMarca = acessoDadosSqlServer.ExecutarManipulacao(CommandType.StoredProcedure, "procMarcaAlterar").ToString();

                return idMarca;

            }
            catch (Exception ex)
            {
                //return ex.Message;
                throw new Exception(ex.Message);
            }
        }

        public string Excluir(ModeloMarca marca)
        {
            try
            {
                acessoDadosSqlServer.LimparParametros();
                acessoDadosSqlServer.AdicionarParametros("@IDMarca", marca.IdMarca);
                string idMarca = acessoDadosSqlServer.ExecutarManipulacao(CommandType.StoredProcedure, "procMarcaExcluir").ToString();

                return idMarca;
            }
            catch (Exception ex)
            {
                //return ex.Message;
                throw new Exception(ex.Message);
            }
        }

        public MarcaColecao ConsultarPorNome(string nome)
        {
            try
            {

                //Criar uma coleção nova de marcas (aqui ela está vazia)
                MarcaColecao marcaColecao = new MarcaColecao();
                acessoDadosSqlServer.LimparParametros();
                acessoDadosSqlServer.AdicionarParametros("@DescricaoCurta", nome);

                //Data=Dados e Table=Tabela
                DataTable dataTableMarca = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "procMarcaConsultaPorNome");

                //Percorrer o DataTable e transformar em coleção de marca
                //Cada linha do DataTable é uma marca
                //Data=Dados e Row=Linha
                //For=Para e Each=Cada
                foreach(DataRow linha in dataTableMarca.Rows)
                {
                    //Criar uma marca vazia
                    //colocar os dados da linha nela
                    //Adicionar a marca na colecao
                    ModeloMarca marca = new ModeloMarca();
                    marca.IdMarca = Convert.ToInt32(linha["IDMarca"]);
                    marca.Descricao_curta = linha["descricao_curta"].ToString();
                    marca.Descricao_longa = linha["descricao_longa"].ToString();
                    marca.CD_operador = Convert.ToInt32(linha["CD_operador"]);

                    
                    marcaColecao.Add(marca);
                }

                return marcaColecao;
            }
            catch (Exception ex)
            {
                //return ex.Message;
                throw new Exception("Não foi possível consulta o cliente por nome. Detalhes: "+ex.Message);
            }
        }

        public MarcaColecao ConsultarPorId(int id)
        {
            try
            {

                //Criar uma coleção nova de marcas (aqui ela está vazia)
                MarcaColecao marcaColecao = new MarcaColecao();
                acessoDadosSqlServer.LimparParametros();
                acessoDadosSqlServer.AdicionarParametros("@IDMarca", id);

                //Data=Dados e Table=Tabela
                DataTable dataTableMarca = acessoDadosSqlServer.ExecutarConsulta(CommandType.StoredProcedure, "procMarcaConsultaPorId");

                //Percorrer o DataTable e transformar em coleção de marca
                //Cada linha do DataTable é uma marca
                //Data=Dados e Row=Linha
                //For=Para e Each=Cada
                foreach (DataRow linha in dataTableMarca.Rows)
                {
                    //Criar uma marca vazia
                    //colocar os dados da linha nela
                    //Adicionar a marca na colecao
                    ModeloMarca marca = new ModeloMarca();
                    marca.IdMarca = Convert.ToInt32(linha["IDMarca"]);
                    marca.Descricao_curta = linha["descricao_curta"].ToString();
                    marca.Descricao_longa = linha["descricao_longa"].ToString();
                    marca.CD_operador = Convert.ToInt32(linha["CD_operador"]);


                    marcaColecao.Add(marca);
                }

                return marcaColecao;
            }
            catch (Exception ex)
            {
                //return ex.Message;
                throw new Exception("Não foi possível consulta o cliente por nome. Detalhes: " + ex.Message);
            }
        }
    }
}