﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLSpc
    {

        private DALConexao conexao;

        public BLLSpc(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloSpc modelo)
        {
            int id = 0;

            if (modelo.Id == 0)
            {
                throw new Exception("O ID do cliente não pode ser ZERO");
            }

            modelo.DE_Motivo = modelo.DE_Motivo.ToUpper();
            modelo.DE_observacao = modelo.DE_observacao.ToUpper();
            modelo.DT_dataRegistro = modelo.DT_dataRegistro;
            

            DALSpc dalSPC = new DALSpc(conexao);
            id = dalSPC.Incluir(modelo);
            return id;
        }

        public DataTable Localizar(String valor)
        {
            DALSpc dalSpc = new DALSpc(conexao);
            return dalSpc.Localizar(valor);
        }
        public ModeloSpc CarregarModeloSpc(int codigo)
        {
            DALSpc dalSpc = new DALSpc(conexao);
            return dalSpc.CarregarModeloSpc(codigo);
        }

    }
}
