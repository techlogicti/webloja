﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLFornecedor
    {
        private DALConexao conexao;

        public BLLFornecedor(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int  Incluir(ModeloFornecedor modelo)
        {
            int id = 0 ;

            if(modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();

            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            id = dalFornecedor.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloFornecedor modelo) {

            if (modelo.IdFornecedor <= 0)
            {
                throw new Exception("Código do fornecedor deve ser informado.");
            }

            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();

            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            dalFornecedor.Alterar(modelo);

        }

        public void Excluir(int codigo) {
            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            dalFornecedor.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            return dalFornecedor.Localizar(valor);
        }
        public ModeloFornecedor CarregarModeloFornecedor(int codigo)
        {
            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            return dalFornecedor.CarregarModeloFornecedor(codigo);
        }

    }
}
