﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLAutorizacaoCliente
    {
        private DALConexao conexao;

        public BLLAutorizacaoCliente(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloAutorizacaoCliente modelo)
        {
            int id = 0;

            /*if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }*/

            modelo.IdCliente = modelo.IdCliente;
            modelo.IdTipoPagamento = modelo.IdTipoPagamento;
            

            DALAutorizacaoCliente dalAutorizacaoCliente = new DALAutorizacaoCliente(conexao);
            id = dalAutorizacaoCliente.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloAutorizacaoCliente modelo)
        {

            /*if (modelo.IdFornecedor <= 0)
            {
                throw new Exception("Código do fornecedor deve ser informado.");
            }

            if (modelo.RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A Razão Social não pode estar em branco.");
            }

            modelo.RazaoSocial = modelo.RazaoSocial.ToUpper();
            modelo.NomeFantasia = modelo.NomeFantasia.ToUpper();
            modelo.CpfCnpj = modelo.CpfCnpj.ToUpper();
            modelo.TipoPessoa = modelo.TipoPessoa.ToUpper();

            DALFornecedor dalFornecedor = new DALFornecedor(conexao);
            dalFornecedor.Alterar(modelo);
            */
        }

        public void Excluir(int codigo)
        {
            DALAutorizacaoCliente dalAutorizacaoCliente = new DALAutorizacaoCliente(conexao);
            dalAutorizacaoCliente.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALAutorizacaoCliente dalAutorizacaoCliente = new DALAutorizacaoCliente(conexao);
            return dalAutorizacaoCliente.Localizar(valor);
        }
        public ModeloAutorizacaoCliente CarregarModeloAutorizacaoCliente(int codigo)
        {
            DALAutorizacaoCliente dalAutorizacaoCliente = new DALAutorizacaoCliente(conexao);
            return dalAutorizacaoCliente.CarregarModeloAutorizacaoCliente(codigo);
        }
    }
}
