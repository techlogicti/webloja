﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class BLLCliente
    {
        private DALConexao conexao;

        public BLLCliente(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloCliente modelo)
        {
            int id = 0;

            if (modelo.NM_nomeCompleto.Trim().Length == 0)
            {
                throw new Exception("O Nome do cliente não pode estar em branco.");
            }

            modelo.NM_nomeConjuge = modelo.NM_nomeConjuge.ToUpper();
            modelo.NM_apelido = modelo.NM_apelido.ToUpper();
            modelo.NU_cpfCnpj = modelo.NU_cpfCnpj.ToUpper();
            modelo.DE_email = modelo.DE_email.ToUpper();
            modelo.NU_celular1 = modelo.NU_celular1.ToUpper();
            modelo.NU_celular2 = modelo.NU_celular2.ToUpper();
            modelo.NU_celular3 = modelo.NU_celular3.ToUpper();
            modelo.NU_celular4 = modelo.NU_celular4.ToUpper();

            DALCliente dalCliente = new DALCliente(conexao);
            id = dalCliente.Incluir(modelo);
            return id;
        }

        public void Alterar(ModeloCliente modelo)
        {

            if (modelo.Id <= 0)
            {
                throw new Exception("Código do cliente deve ser informado.");
            }

            if (modelo.NM_nomeCompleto.Trim().Length == 0)
            {
                throw new Exception("O nome do cliente não pode estar em branco.");
            }

            modelo.NM_nomeConjuge = modelo.NM_nomeConjuge.ToUpper();
            modelo.NM_apelido = modelo.NM_apelido.ToUpper();
            modelo.NU_cpfCnpj = modelo.NU_cpfCnpj.ToUpper();
            modelo.DE_email = modelo.DE_email.ToUpper();
            modelo.NU_celular1 = modelo.NU_celular1.ToUpper();
            modelo.NU_celular2 = modelo.NU_celular2.ToUpper();
            modelo.NU_celular3 = modelo.NU_celular3.ToUpper();
            modelo.NU_celular4 = modelo.NU_celular4.ToUpper(); ;

            DALCliente dalCliente = new DALCliente(conexao);
            dalCliente.Alterar(modelo);

        }

        public void Excluir(int codigo)
        {
            DALCliente dalCliente = new DALCliente(conexao);
            dalCliente.Excluir(codigo);
        }

        public DataTable Localizar(String valor)
        {
            DALCliente dalCliente = new DALCliente(conexao);
            return dalCliente.Localizar(valor);
        }
        public ModeloCliente CarregarModeloCliente(int codigo)
        {
            DALCliente dalCliente = new DALCliente(conexao);
            return dalCliente.CarregarModeloCliente(codigo);
        }
    }
}
