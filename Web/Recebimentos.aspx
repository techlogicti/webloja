﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Recebimentos.aspx.cs" Inherits="Web.Recebimentos" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


<asp:UpdatePanel runat="server">
	<ContentTemplate>

	<asp:Panel ID="panelGrid" runat="server" Visible="false">Recebimentos
     
    <h3>Lista de Recebimentos</h3>
    <asp:GridView id="RecebimentoGridView" AllowSorting="True" AllowPaging="True" GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White" Runat="Server" PageSize="10000" 
		AutoGenerateColumns="False" >

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="DataVencimento" HeaderText="Data Vencimento" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="ValorConta" HeaderText="Valor Conta" />
			<asp:BoundField DataField="NumeroParcela" HeaderText="Numero Parcela" />
			<asp:BoundField DataField="QuantidadeParcela" HeaderText="Quantidade Parcela" />
            <asp:BoundField DataField="DataRecebimento" HeaderText="Data Recebimento" />
			<asp:BoundField DataField="ValorRecebido" HeaderText="Valor Recebido" />
			<asp:BoundField DataField="FormaPagamento" HeaderText="Forma de Pagamento" />
            <asp:CommandField ShowSelectButton="True" />
        </Columns>

	</asp:GridView>
          
   </asp:Panel>
   </ContentTemplate>
   </asp:UpdatePanel>
</asp:Content>
