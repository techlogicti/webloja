﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContasAPagar.aspx.cs" Inherits="Web.ContasAPagar" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
	<hr />
    <hr />
    <asp:Panel ID="panelBotoes" runat="server" Visible="false">
     <div class="container-fluid">
         <asp:Button ID="btnListar" class="btn btn-primary" runat="server" Text="Listar Contas" OnClick="btnListar_Click" />
         <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Nova Conta" OnClick="btnNovo_Click"  />
     </div>
    </asp:Panel>
     <hr />

    </ContentTemplate>
	</asp:UpdatePanel>

<!--Inicio Cadastro -->
	 <asp:Panel ID="panelCadastro" runat="server" Visible="false">
         <asp:UpdatePanel ID="UpdatePanel3" runat="server">
             <ContentTemplate>
			 </ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
<!--Fim Cadastro -->

<!-- Inicio Filtro -->
	 <asp:Panel ID="panelFiltro" runat="server" Visible="true" CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
			<!--Linha 1-->
			<div class="row">
                <div class="col-sm-3">
                    <label for="txtNumPedido">Número do Documento</label>
                    <asp:TextBox class="form-control input-sm" ID="txtNumeroNotaFiscal" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtNomeCliente">Fornecedor</label>
                    <asp:TextBox class="form-control input-sm" ID="txtNomeFornecedor" runat="server" ></asp:TextBox>
                   
                </div>

                <div class="col-sm-3">
                <label for="ddlFormaPagamento">Forma de Pagamento</label>
                    <asp:ListBox ID="listFormaPagamento" class="form-control input-sm" runat="server" selectionMode="Multiple"></asp:ListBox>
                 </div>

                <div class="col-sm-2">
                <asp:Button ID="btnPesquisar" class="btn btn-info btn-xs" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click"/>
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info btn-xs" runat="server" Text="Limpar Campos"   />
                    </div>

                 
            </div>

			 <div class="row">
                <div class="col-sm-3">
                    <label for="txtDataInicioEmissao">Data de Emissão (de)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataInicioEmissao" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimEmissao">Data de Emissão (até)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataFimEmissao" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>
                <div class="col-sm-3">
                    <label for="ddlStatusPagamento">Status Pagamento</label>
                    <!--<asp:DropDownList ID="ddlStatusPagamento" class="form-control" runat="server" />-->
                    <asp:ListBox ID="listStatusPagamento" class="form-control input-sm" runat="server" selectionMode="Multiple"></asp:ListBox>
                </div>
                <div class="col-sm-3">
                    <label for="ddlStatusRegistro">Status </label>
                    <asp:DropDownList ID="ddlStatusRegistro" class="form-control input-sm" runat="server" />
                </div>
            </div>
                                  <!-- -->

			 <div class="row">
                <div class="col-sm-3">
                    <label for="txtDataInicioVencimento">Data de Vencimento (de)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataInicioVencimento" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimVencimento">Data de Vencimento (até)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataFimVencimento" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>

				<div class="col-sm-3">
                    <label for="txtDataInicioRecebimento">Data de Pagamento(de)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataInicioPagamento" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimRecebimento">Data de Pagamento(até)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataFimPagamento" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>                

            </div>

       <br />
			
			<!--Fim -->
		</div>
		</div>
	</asp:Panel>
<!-- Fim Filtro -->

<!-- Inicio Grid -->
	 <asp:Panel ID="panelGrid" runat="server" Visible="true">
		 <div class="well well-sm"><b>Total de Contas ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

		 <h3>Contas à Pagar</h3>

		 <asp:GridView id="ContasGridView" AllowSorting="True" AllowPaging="True" CssClass="table table-hover table-striped table-sm"
			 BackColor="White"  GridLines="None" Runat="Server" PageSize="10000" AutoGenerateColumns="False" ShowFooter="true" OnRowDataBound="ContasGridView_RowDataBound"  >

          <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
			<asp:BoundField DataField="IdPedido" HeaderText="N.Documento" />
			<asp:BoundField DataField="Fornecedor" HeaderText="Fornecedor" />
			<asp:BoundField DataField="DataVencimento" HeaderText="DT.Vencimento" />
			<asp:BoundField DataField="ValorParcela" HeaderText="Valor Parcela" />
			<asp:BoundField DataField="NumeroParcela" HeaderText="Parcela" />
			<asp:BoundField DataField="QtdParcelas" HeaderText="Qtd.Parcelas" />
			<asp:BoundField DataField="ValorTotal" HeaderText="Valor Total" />
		  </Columns>
          
		 </asp:GridView>

	 </asp:Panel>
<!-- Fim Grid -->

</asp:Content>
