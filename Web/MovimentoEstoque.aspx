﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MovimentoEstoque.aspx.cs" Inherits="Web.MovimentoEstoque" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
      <hr />

       <asp:Panel ID="panelBotoes" runat="server" Visible="false">
          <div class="container-full">
            <asp:Button ID="btnListarNotas" class="btn btn-primary" runat="server" Text="Listar" />
            <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Novo Movimento" OnClick="btnNovo_Click"  />
         </div>
        </asp:Panel>

<!--FILTRO -->
<hr />
<asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-full">
    <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-2">
                    <label for="txtCodigo">Cod.Mov.</label>
                    <asp:TextBox class="form-control" ID="txtCodigo" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtDE_descricao">Nº Documento</label>
                    <asp:TextBox class="form-control" TextMode="Search" ID="txtNumeroDocumentoBusca" runat="server"></asp:TextBox>   
                </div>
                <div class="col-sm-7">
                    <label for="txtDE_descricao">Cliente/Fornecedor</label>
                    <asp:TextBox class="form-control" TextMode="Search" ID="txtClienteFornecedorBusca" runat="server"></asp:TextBox>   
                </div>
                
            </div>
            <!-- -->
            <div class="row">
                <div class="col-sm-2">
                    <label for="txtCodigo">Cod.Produto</label>
                    <asp:TextBox class="form-control" ID="txtCodProdutoBusca" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtDE_descricao">Categora Prod.</label>
                    <asp:TextBox class="form-control" TextMode="Search" ID="txtCategoriaProdutoBusca" runat="server"></asp:TextBox>   
                </div>
                <div class="col-sm-5">
                    <label for="txtDE_descricao">Referência Produto</label>
                    <asp:TextBox class="form-control" TextMode="Search" ID="txtReferenciaProdutoBusca" runat="server"></asp:TextBox>   
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="btnPesquisar" class="btn btn-info" runat="server" Text="Pesquisar"/>
                </div>
            </div>
            <!-- -->
        </div>
    </div>
    
 </asp:Panel>
<!-------------------------------------------------------->
<!-- INICIO CADASATRO ------------------------------------->
<asp:Panel ID="panelCadastro" runat="server" Visible="false">
        <div class="container-full">
            <asp:Button ID="btnVoltar" class="btn btn-warning" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
            <h2>Movimento</h2>
                    <p>Identificação do Movimento -   <asp:Label ID="lblMVC" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label></p>
            

        <!-- -->
        <!-- -->
        <!-- -->
             <!-- AÇÔES-->
                <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">Ações</div>
                      <div class="panel-body">
                          <div class="row">
                           <div class="col-sm-12">
                            <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />
                            <asp:Button ID="btnAtualizar" class="btn btn-primary" runat="server" Text="Atualizar" OnClick="btnAtualizar_Click" />
                            <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
                            <asp:Button ID="btnLimparCampos" class="btn btn-default" runat="server" Text="Limpar Campos" />
                            <asp:Label ID="lblMensagemAcoes" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
                        </div>  
                        </div>
                      </div>
                  </div>
                </div>
        <!-- -->

         </div>
</asp:Panel>

<!-- FIM CADASTRO ----------------------------------------->

<!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal hide fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

<!--INICIO GRIDVIEW ----------------------------------------------------->
 <asp:Panel ID="panelGrid" runat="server" Visible="true">
     <div class="well well-sm"><b>Total de Movimentos ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

    <h3>Lista de Movimento</h3>
    <asp:GridView id="MyGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" OnSelectedIndexChanged="MyGridView_SelectedIndexChanged"
         OnSelectedIndexChanging="MyGridView_SelectedIndexChanging" 
        AutoGenerateColumns="False" OnRowDataBound="MyGridView_RowDataBound" >
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="N.Doc" HeaderText="N.Documento" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="TipoMovimento" HeaderText="TP.Mov" />
            <asp:BoundField DataField="Produto" HeaderText="Produto" />
            <asp:BoundField DataField="Quantidade" HeaderText="Qtd." />
            <asp:BoundField DataField="Tabela" HeaderText="Origem" />
            <asp:BoundField DataField="DataMovimento" HeaderText="Dt.Mov" />  
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" OnClick="ImageButton1_Click" Width="20px" />
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                 <ItemTemplate>
                       <asp:Button  class="btn btn-primary btn-xs" ID="btnDetalharGrid" runat="server"  Text="Detalhes" OnClick="btnDetalharGrid_Click" />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                
                  <ItemTemplate>
                    <asp:Button  class="btn btn-warning btn-xs" ID="btnEditaGridr" runat="server" Text="Editar" OnClick="btnEditaGridr_Click" />
                </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField>
                
                  <ItemTemplate>
                    <asp:Button  class="btn btn-danger btn-xs" ID="btnCancelarGrid" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
   </asp:Panel>
<!--FIM    GRIDVIEW ----------------------------------------------------->
<!-- Trigger the modal with a button -->
<script type="text/javascript">
    function openModal() {
        $('#myModal').modal('show');
    }
</script>
<!-- -->

      </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>