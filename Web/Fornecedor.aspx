﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Fornecedor.aspx.cs" Inherits="Web.Fornecedor" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate>
     <hr />
                 <asp:Panel ID="panelBotoes" runat="server" Visible="false">
    <div class="container-fluid">
        <asp:Button ID="btnListarFornecedores" class="btn btn-primary" runat="server" Text="Listar Fornecedor" OnClick="btnListarFornecedores_Click" />
         <asp:Button ID="btnNovoFornecedor" class="btn btn-success" runat="server" Text="Novo Fornecedor" OnClick="btnNovoFornecedor_Click" />
     </div>
    </asp:Panel>

    <hr />

    <asp:Panel ID="panelCadastro" runat="server" Visible="false">
         <div class="container">
  <h2>Novo Fornecedor</h2>
  <p>Identificação do Fornecedor</p>
  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Dados Básicos</div>
          <div class="panel-body">
              <!-- Linha 1-->
               <div class="row">
                    <div class="col-lg-2">
                         <asp:Label ID="lblCpfCnpj" runat="server" Text="CPF/CNPJ: ">
                            <asp:TextBox  class="form-control" ID="txtCpfCnpjInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                     <div class="col-lg-2">
                         <asp:Label ID="lblTipoPessoaInput" runat="server" Text="Tipo Pessoa: ">
                             
                             
                        
                         <asp:RadioButtonList ID="radioButtonListTipoPessoa" runat="server">
                             <asp:ListItem Value="F">Física</asp:ListItem>
                             <asp:ListItem Selected="True" Value="F">Jurídica</asp:ListItem>
                         </asp:RadioButtonList>
                             </asp:Label>
                    </div>
                   <div class="col-lg-4">
                         <asp:Label ID="lblRazaoSocial" runat="server" Text="Razao Social: ">
                            <asp:TextBox  class="form-control" ID="txtRazaoSocialInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-lg-4">
                         <asp:Label ID="lblNomeFantasia" runat="server" Text="Nome Fantasia: ">
                            <asp:TextBox  class="form-control" ID="txtNomeFantasiaInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                </div>
             <!-- -->

             

          </div>
        <hr />
          <!-- AÇÔES-->
            <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Ações</div>
          <div class="panel-body">

              <div class="row">
               <div class="col-sm-3">
             <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click"/>
            <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"/>
            </div>
                
                  </div>


          </div>
      </div>
    </div>


            <!-- -->
      </div>
    </div>
        
    </asp:Panel>
                 

<!-- -->
                <asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">

        <div class="row">
             <div class="col-md-10">
                 <label for="txtDE_descricao">Código/Nome Fantasia/Razão Social/Cnpj/Cpf:
                      <div class="input-group">
                 <asp:TextBox ID="txtDE_descricao" TextMode="Search" class="form-control" runat="server" OnTextChanged="txtDE_descricao_TextChanged" Wrap="True"></asp:TextBox>
                     <span class="input-group-btn">
                         <asp:Button ID="btnPesquisar" class="btn btn-default" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click"   />
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info" runat="server" Text="Limpar Campos" OnClick="btnLimparFiltros_Click"  />
                      </span>
                          </div>
                 </label>
             </div>
            <div class="col-md-2">
                 <label for="txtDE_descricao">Ordenar por:
                <asp:DropDownList class="form-control" ID="ddlOrdemPesquisaCliente" runat="server"></asp:DropDownList>
                     </label>
            </div>
        </div>

        </div>
        </div>
    </asp:Panel>
<!-- -->

    

<!-------------------------------------------------------->
                <!-- Trigger the modal with a button -->

                <script type="text/javascript">


                  function openModalMensagem() {
                      $('#myModalMensagem').modal('show');
                  }

    function openModal() {
        $('#myModal').modal('show');
    }
</script>
                

                
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Detalhes do Fornecedor</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-3">
                                        <label for="txtModalId">Id</label>
                                        <asp:TextBox ID="txtModalId" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                    </div>
                                      <div class="col-sm-3">
                                        <label for="txtModalCodigo">Código</label>
                                        <asp:TextBox ID="txtModalCodigo" runat="server" placeholder="Código" class="form-control"></asp:TextBox><br />
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="txtModalCodigo">Departamento</label>
                                        <asp:TextBox ID="txtModalDepartamento" runat="server" placeholder="Departamento" class="form-control"></asp:TextBox><br />
                                    </div>
                                  </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalCategoria" runat="server" placeholder="Categoria" class="form-control"></asp:TextBox><br />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalSubcategoria" runat="server" placeholder="SubCategoria" class="form-control"></asp:TextBox><br />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalMarca" runat="server" placeholder="Marca" class="form-control"></asp:TextBox><br />
                                        </div>
                                     </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalProduto" runat="server" placeholder="Produto" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoVenda" runat="server" placeholder="Preço Etiqueta" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoCompra" runat="server" placeholder="Preço Compra" class="form-control"></asp:TextBox><br />
                                        </div>
                                    </div>

                                     <div class="row">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalCif" runat="server" placeholder="Preço CIF" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoMinimo" runat="server" placeholder="Preço Mínimo" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoAtacado" runat="server" placeholder="Preço Atacado" class="form-control"></asp:TextBox><br />
                                        </div>
                                    </div>
                                    
                                     <div class="row">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalQuantidade" runat="server" placeholder="Quantidade" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalAtacado" runat="server" placeholder="Atacado" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button Text="Salvar"  runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--------------------------------------------------------->
         <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->
               

                <div class="well well-sm"><b>Total de Fornecedores ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

               <asp:Panel ID="panelGrid" runat="server" Visible="false">Detalhe do Fornecedor
     
                 <h3>Lista de Fornecedores</h3>
    <asp:GridView id="FornecedorGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" AutoGenerateColumns="False" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="CpfCnpj" HeaderText="CPF/CNPJ" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="NomeFantasia" HeaderText="Nome Fantasia" />
            <asp:BoundField DataField="RazaoSocial" HeaderText="Razão Social" />
            

            <asp:CommandField ShowSelectButton="True" />
        </Columns>
                   </asp:GridView>
          
          

   </asp:Panel>
          </ContentTemplate>
        </asp:UpdatePanel>
     
    

    
</asp:Content>


