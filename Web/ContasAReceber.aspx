﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContasAReceber.aspx.cs" Inherits="Web.ContasAReceber" EnableEventValidation="false" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	  <asp:UpdatePanel ID="UpdatePanel1" runat="server">

    <ContentTemplate>
    <hr />
     <hr />
    <asp:Panel ID="panelBotoes" runat="server" Visible="false">
     <div class="container-fluid">
         <asp:Button ID="btnListar" class="btn btn-primary" runat="server" Text="Listar Contas" OnClick="btnListar_Click" />
         <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Nova Conta" OnClick="btnNovo_Click"  />
     </div>
    </asp:Panel>

     <hr /></ContentTemplate>
		</asp:UpdatePanel>
		 
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
             <ContentTemplate>
				 <asp:Panel ID="panelCadastro" runat="server" Visible="true">
                 <div class="container-full">
                    <asp:Button ID="btnVoltar" class="btn btn-warning" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
                    <h2>Conta à pagar</h2>
                    <p>Identificação da Conta -   <asp:Label ID="lblMVC" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label></p>

                     <div class="panel-group">
                           <div class="panel panel-default">
                               <div class="panel-heading">Dados da conta</div>
                               <div class="panel-body">
                                   <!-- Linha 1-->
                                   <div class="row">
                                       <div class="col-md-1">
                                           <asp:Label ID="lblIdCadastro" runat="server" Text="ID: ">
                                                <asp:TextBox  class="form-control" ID="txtIdCadastro" ReadOnly="true" runat="server"></asp:TextBox>
                                            </asp:Label>
                                        </div>
                                       <div class="col-md-1">
                                           <asp:Label ID="lblIdPedido" runat="server" Text="Pedido: ">
                                                <asp:TextBox  class="form-control" ID="txtIdPedido" ReadOnly="false" runat="server"></asp:TextBox>
                                            </asp:Label>
                                        </div>
                                       <div class="col-md-2">
                                           <asp:Label ID="Label2" runat="server" Text="Cpf/Cnpj: ">
                                               <asp:TextBox  class="form-control" ID="txtIdClienteCadastro"   Visible="false" runat="server"></asp:TextBox>
                                               <asp:TextBox  class="form-control" ID="txtCpfClienteCadastro"  runat="server"></asp:TextBox>
                                            </asp:Label>
                                        </div>
                                       <div class="col-md-6">
                                           <asp:Label ID="Label3" runat="server" Text="Cliente: ">
                                             <asp:TextBox  class="form-control" ID="txtNomeClienteCadastro" TextMode="Search" ReadOnly="true" runat="server"></asp:TextBox>
                                           </asp:Label>
                                       </div>
                                       <div class="col-md-1">
                                           <asp:Label ID="Label4" runat="server" Text="."></asp:Label>
                                             <asp:Button ID="btnBuscarClienteCadastro" class="btn btn-default"  runat="server" Text="Pesquisar" OnClick="btnBuscarClienteCadastro_Click"/>
                                               <asp:Button ID="btnCalc" class="btn btn-default"  runat="server" Text="Calc" OnClick="btnCalc_Click"/>
                                           
                                       </div>
                                   </div>
                                   <!-- -->
                                   <!-- Dados do Pagamento -->
                                  <div class="row">

                                      <div class="col-sm-2">
                                          <asp:Label ID="lblDataEmissaoCadastro" runat="server" Text="Data Emissão: ">
                                            <asp:TextBox  class="form-control" ID="txtDataEmissaoCadastro" text="%# DateTime.Now.ToShortDateString() %>" TextMode="Date"  runat="server"></asp:TextBox>
                                           </asp:Label>
                                      </div>

                                      <div class="col-sm-2">
                                         <asp:Label ID="Label5" runat="server" Text="Forma de Pagamento: ">
                                            <asp:DropDownList class="form-control" ID="ddlTipoPagamento" runat="server"></asp:DropDownList>
                                         </asp:Label>
                                     </div>

                                      <div class="col-sm-2">
                                          <asp:Label ID="Label6" runat="server" Text="Valor Conta: "></asp:Label>
                                            <asp:TextBox Text="0"  class="form-control" ID="txtValorContaCadastro"  runat="server" OnTextChanged="txtValorContaCadastro_TextChanged"></asp:TextBox>
                                           
                                      </div>

									 

                                      <div class="col-sm-2">
                                          <asp:Label ID="Label7" runat="server" Text="Nº Parcela(s): "></asp:Label>
                                            <asp:TextBox  class="form-control" ID="txtQuantidadeParcelaCadastro" text="1" TextMode="Number"  runat="server"></asp:TextBox>
                                           
                                      </div>

                                      <div class="col-sm-2">
                                          <asp:Label ID="Label8" runat="server" Text="Valor Parcela: ">
                                            <asp:TextBox  Text="0"  class="form-control" ID="txtValorParcelaCadastro" ReadOnly="true"  runat="server"></asp:TextBox>
                                           </asp:Label>
                                      </div>

                                       <div class="col-sm-2">
                                          <asp:Label ID="Label9" runat="server" Text="Data 1ª Parcela:">
                                            <asp:TextBox  class="form-control" ID="txtData1Parcela"  TextMode="Date"  runat="server"></asp:TextBox>
                                           </asp:Label>
                                      </div>

                                  </div>
                                  <!-- -->

								   <!-- LInha 3 -->
								<div class="row"> 
									 <div class="col-sm-2">
                                          <asp:Label ID="Label14" runat="server" Text="Nº Autorização / Contrato(s): "></asp:Label>
                                            <asp:TextBox  class="form-control" ID="txtNumeroContratoAutorizacao" text="1" TextMode="Number"  runat="server"></asp:TextBox>
                                           
                                      </div>

                                       <div class="col-md-10">
                                           <asp:Label ID="Label12" runat="server" Text="Observação: ">
                                                <asp:TextBox TextMode="MultiLine"  class="form-control" ID="txtObservacaoConta"  runat="server"></asp:TextBox>
                                            </asp:Label>
                                        </div>
								</div>
								   <!-- -->

                                   <!-- Linha da Mensagem de validação-->
                                    <div class="row">
                                        <asp:Label ID="lblMensagemDadosBasicos" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                                    </div>
                                   <!-- -->

                               </div>
                            </div>
                     </div>

                     <!-- Fim Panel Dados da Conta -->
                     <!-- AÇÔES-->
                <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">Ações</div>
                      <div class="panel-body">
                          <div class="row">
                           <div class="col-sm-12">
                            <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click"/>
                            <asp:Button ID="btnAtualizar" class="btn btn-primary" runat="server" Text="Atualizar" OnClick="btnAtualizar_Click" />
                            <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"/>
                            <asp:Button ID="btnLimparCampos" class="btn btn-default" runat="server" Text="Limpar Campos" OnClick="btnLimparCampos_Click"/>
                            <asp:Label ID="lblMensagemAcoes" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
                        </div>  
                        </div>
                      </div>
                  </div>
                </div>

                 </div>
					 </asp:Panel>
             </ContentTemplate>
			
         </asp:UpdatePanel>
    

    <asp:Panel ID="panelFiltro" runat="server" Visible="true" CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
                
            <div class="row">
                <div class="col-sm-3">
                    <label for="txtNumPedido">Núm.Pedido</label>
                    <asp:TextBox class="form-control input-sm" ID="txtNumPedido" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtNomeCliente">Cliente</label>
                    <asp:TextBox class="form-control input-sm" ID="txtNomeCliente" runat="server" ></asp:TextBox>
                   
                </div>

                <div class="col-sm-3">
                <label for="ddlFormaPagamento">Forma de Pagamento</label>
                    <!--<asp:DropDownList ID="ddlFormaPagamento" class="form-control" runat="server"  /> -->
                    <asp:ListBox ID="listFormaPagamento" class="form-control input-sm" runat="server" selectionMode="Multiple"></asp:ListBox>
                 </div>

                <div class="col-sm-2">
                <asp:Button ID="btnPesquisar" class="btn btn-info btn-xs" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click"/>
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info btn-xs" runat="server" Text="Limpar Campos"   />
                    </div>

                 
            </div>
       <br />
           
           

            <div class="row">
                <div class="col-sm-3">
                    <label for="txtDataInicioEmissao">Data de Emissão (de)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataInicioEmissao" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimEmissao">Data de Emissão (até)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataFimEmissao" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>
                <div class="col-sm-3">
                    <label for="ddlStatusPagamento">Status Pagamento</label>
                    <!--<asp:DropDownList ID="ddlStatusPagamento" class="form-control" runat="server" />-->
                    <asp:ListBox ID="listStatusPagamento" class="form-control input-sm" runat="server" selectionMode="Multiple"></asp:ListBox>
                </div>
                <div class="col-sm-3">
                    <label for="ddlStatusRegistro">Status </label>
                    <asp:DropDownList ID="ddlStatusRegistro" class="form-control input-sm" runat="server" />
                </div>
            </div>
        
            <div class="row">
                <div class="col-sm-3">
                    <label for="txtDataInicioVencimento">Data de Vencimento (de)</label>
                    <asp:TextBox class="form-control" ID="txtDataInicioVencimento" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimVencimento">Data de Vencimento (até)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataFimVencimento" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>

				<div class="col-sm-3">
                    <label for="txtDataInicioRecebimento">Data de Recebimento(de)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataInicioRecebimento" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimRecebimento">Data de Recebimento(até)</label>
                    <asp:TextBox class="form-control input-sm" ID="txtDataFimRecebimento" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>                

            </div>
        
        </div>
    </div>
    

    </asp:Panel>



          <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                        aria-hidden="true">
                        
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" aria-label="Close"  data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Detalhes - À Receber [ <asp:Label ID="lblModo" runat="server" Text=""></asp:Label> ] </h4>
                                </div>
                                <div class="modal-body">

                                    

                                  <div class="row">
                                    <div class="col-md-2">
                                        <label for="txtModalId">Id</label>
                                        <asp:TextBox ID="txtModalId" ReadOnly="true" runat="server" placeholder="Id" class="form-control input-sm"></asp:TextBox>
                                    </div>
                                      <div class="col-md-2">
                                        <label for="txtModalPedido">Pedido</label>
                                        <asp:TextBox ID="txtModalPedido"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="txtModalDataEmissao">Data da Emissão</label>
                                        <asp:TextBox ID="txtModalDataEmissao"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                    </div>
                                     <div class="col-md-6">
                                            <label for="ddlModalCliente">Cliente</label>
                                            <asp:DropDownList  ReadOnly="true"  class="form-control input-sm"  ID="ddlModalCliente" runat="server"></asp:DropDownList>
                                        </div>
                                  </div>
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-1">
                                            <label for="txtModalNumeroParcela">N.Par.</label>
                                            <asp:TextBox ID="txtModalNumeroParcela"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

                                        <div class="col-md-1">
                                            <label for="txtModalQuantidadeParcela">Qtd.P.</label>
                                            <asp:TextBox ID="txtModalQuantidadeParcela"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>
                                        
                                        <div class="col-md-2">
                                             <label for="txtModalStatusConta">Status Conta</label>
                                             <asp:TextBox ID="txtModalStatusConta"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

                                        <div class="col-md-2">
                                             <label for="txtModalDataRecebimento">Data Recebimento</label>
                                             <asp:TextBox ID="txtModalDataRecebimento" TextMode="Date" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

										<div class="col-md-6">
                                            <label for="ddlModalHistoricoFinanceiro">Histórico Financeiro</label>
                                            <asp:DropDownList  class="form-control input-sm"  ID="ddlModalHistoricoFinanceiro" runat="server"></asp:DropDownList>
                                        </div>
                                        

                                     </div>

                                    <div class="row">

                                        <div class="col-md-2">
                                             <label for="txtModalValorConta">Valor Conta</label>
                                             <asp:TextBox ID="txtModalValorConta"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

                                        <div class="col-md-2">
                                            <label for="txtModalValorMulta">Valor Multa</label>
                                            <asp:TextBox ID="txtModalValorMulta"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

                                        <div class="col-md-2">
                                             <label for="txtModalValorJuros">Valor Juros</label>
                                             <asp:TextBox ID="txtModalValorJuros"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

										<div class="col-md-2">
                                             <label for="txtModalValorTotal">Valor Total</label>
                                             <asp:TextBox ID="txtModalValorTotal"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

                                         <div class="col-md-2">
                                             <label for="txtModalValorPago">Valor Pago</label>
                                             <asp:TextBox ID="txtModalValorPago"  ReadOnly="true" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

										 <div class="col-md-2">
                                             <label for="txtModalValorRecebimento">Recebimento</label>
                                             <asp:TextBox ID="txtModalValorRecebimento"  ReadOnly="false" runat="server" placeholder="" class="form-control input-sm"></asp:TextBox><br />
                                        </div>

                                        
                                     </div>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <label for="ddlModalTipoPagamento">Tipo Pagamento</label>
                                            <asp:DropDownList  class="form-control input-sm"  ReadOnly="true"  ID="ddlModalTipoPagamento" runat="server"></asp:DropDownList>
                                        </div>
										<div class="col-md-6">
                                            <label for="ddlModalImpressora">Impressora</label>
                                            <asp:DropDownList  class="form-control input-sm"   ID="ddlModalImpressora" runat="server"></asp:DropDownList>
                                        </div>

                                         
                                    </div>
                                    

                                     <div class="row">
                                        <div class="col-md-12">
                                            <label for="txtModalObservacoes">Observações</label>
                                            <asp:TextBox ID="txtModalObservacoes" TextMode="MultiLine" runat="server" placeholder="Quantidade" class="form-control input-sm"></asp:TextBox><br />
                                        </div>
                                    </div>

									
									<div class="row">	
											
										<div class="panel panel-default">
										  <div class="panel-heading">Histórico Financeiro</div>
										  <div class="panel-body">
												
											
<asp:GridView ID="GridViewHistoricoFinanceiro" AutoGenerateColumns="False" ShowFooter="true" Runat="Server"
												AllowSorting="True" AllowPaging="True" GridLines="None" CssClass="table table-hover table-striped table-sm" BackColor="White">
												<Columns>
													<asp:BoundField DataField="ID" HeaderText="ID" />
													<asp:BoundField DataField="dt_dataevento" HeaderText="Data" />
													<asp:BoundField DataField="vl_valorevento" HeaderText="Valor" />
												</Columns>
											</asp:GridView>

										  </div>
										</div>
										
									</div>
                                              
                                    <div class="row">
                                    <div class="col-sm-4">
                                        <label for="ddlUsuarioAutorizacao">Usuário</label>
                                            <asp:DropDownList  class="form-control input-sm"  ID="ddlUsuarioAutorizacao" runat="server"></asp:DropDownList>
                                    </div>
                                      <div class="col-sm-4">
                                        <label for="txtSenhaAutorizacao">Senha</label>
                                        <asp:TextBox ID="txtSenhaAutorizacao" AutoPostBack="false"  runat="server" placeholder="Id" class="form-control  input-sm"></asp:TextBox>
                                    </div>
                                      
                                  </div>

                                
                                    
                                
                                </div> <!-- FIm modal body -->
                                <div class="modal-footer">
                                    <button type="button" id="btnCloseModal" class="btn btn-warning" > Fechar</button>
                                    <button type="button" id="btnTeste" onclick="Teste(this)" class="btn btn-warning" > Teste</button>
                                    <asp:Button ID="btnSalvarModal" Text="Salvar" Visible="false" class="btn btn-default"   runat="server" OnClick="btnSalvarModal_Click" />
                                    <asp:Button ID="btnConfirmarRecebiment" Text="Confirmar recebimento" class="btn btn-danger"    runat="server" AutoPostBack="false" OnClick="btnConfirmarRecebiment_Click" />                 
                                    
                                    <asp:Button ID="Button1" runat="server" Text="Login" onclik="Teste(this)" AutoPostBack="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--------------------------------------------------------->
<!--INICIO MODAL - CONFIRMAÇÃO -->
         
        


                                  
                                
                 
<!--FIM MODAL - CONFIRMAÇÃO -->
<!-------------------------------------------------------->
                <!-- Trigger the modal with a button -->

<script type="text/javascript">

    function closeModalMensagem() {
        $('#myModalMensagem').modal('hide');
    }

    function openModalMensagem() {
        $('#myModalMensagem').modal('show');
    }

    function openModalConfirmacaoExclusaoConta() {
        $('#myModalConfirmacaoExclusaoConta').modal('show');
    }

    function openModalConfirmacaoEstornoConta() {
        $('#myModalConfirmacaoEstornoConta').modal('show');
    }


    function Teste(Src) {
        PageMethods.cripMD5();
    }

   function openModal() {
        $("#myModal").modal({ backdrop: "static" });
   }

                  
</script>


                    
<!-- ------------------------------------------------------------------------------------>  
<!--INICIO MODAL - CONFIRMAÇÃO ESTORNO CONTA-->
    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label10" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalConfirmacaoEstornoConta" tabindex="-1" role="dialog" aria-labelledby="myModalLabelConfirmacaoEstornoConta"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelConfirmacaoEstornoConta">Confirmação - Estorno de Conta</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="txtModalIDConfirmacaoEstornoConta">ID</label>
                                            <asp:TextBox ReadOnly="true" ID="txtModalIDConfirmacaoEstornoConta" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                        </div>
										 <div class="col-sm-10">
                                         <asp:Label ID="Label13" runat="server">Evento à estornar</asp:Label>
                                            <asp:DropDownList class="form-control" ID="ddlEventoFinanceiro" runat="server"></asp:DropDownList>
                                         
                                     </div>
                                    </div>

                                  <div class="row">
                                    <div class="col-sm-12">
                                        <label for="txtJustificativaEstornoConta">Justificativa</label>
                                        <asp:TextBox ID="txtJustificativaEstornoConta" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                      
                                  </div>

                                 <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
                                    </div>
                                 </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button ID="btnConfirmarEstornoConta"  Text="Confirmar" class="btn btn-danger"   runat="server" OnClick="btnConfirmarEstornoConta_Click"  />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - CONFIRMAÇÃO EXCLUSAO CONTA -->     
<!-- --------------------------------------------------------->
              
<!-- ------------------------------------------------------------------------------------>  
<!--INICIO MODAL - CONFIRMAÇÃO EXCLUSAO CONTA-->
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label15" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalConfirmacaoExclusaoConta" tabindex="-1" role="dialog" aria-labelledby="myModalLabelConfirmacaoExclusaoConta"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelConfirmacaoExclusaoConta">Confirmação - Exclusão de Conta</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="txtModalIDConfirmacaoExclusaoConta">ID</label>
                                            <asp:TextBox ReadOnly="true" ID="txtModalIDConfirmacaoExclusaoConta" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                  <div class="row">
                                    <div class="col-sm-12">
                                        <label for="txtJustificativaExclusaoConta">Justificativa</label>
                                        <asp:TextBox ID="txtJustificativaExclusaoConta" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                      
                                  </div>

                                 <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagemModalConfirmacaoExclusaoConta" runat="server" Text=""></asp:Label>
                                    </div>
                                 </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button ID="btnConfirmarExclusaoConta"  Text="Confirmar" class="btn btn-danger"   runat="server" OnClick="btnConfirmarExclusaoConta_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - CONFIRMAÇÃO EXCLUSAO CONTA -->     
<!-- --------------------------------------------------------->

        <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal hide fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->
               

     

      <asp:Panel ID="panelGrid" runat="server" Visible="true">

     <div class="well well-sm"><b>Total de Contas ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

                 <h3>Contas à Receber
                     <asp:Button ID="btnExport" CssClass="btn-xs" Visible="false" runat="server" Text="Extrair para Excel" OnClick = "ExportToExcel" />
                 </h3>

            <asp:GridView id="ContasGridView" 
    AllowSorting="True" AllowPaging="True" 
         GridLines="None"
         CssClass="table table-hover table-striped table-sm" BackColor="White"
Runat="Server" PageSize="10000" AutoGenerateColumns="False" ShowFooter="true" OnRowDataBound="ContasGridView_RowDataBound" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="NumeroPedido" HeaderText="Pedido" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Cliente" HeaderText="Cliente"  />
            <asp:BoundField DataField="DataEmissao" HeaderText="DT.Emissão" />
            <asp:BoundField DataField="NumeroParcela" HeaderText="Nº" />
            <asp:BoundField DataField="QtdParcelas" HeaderText="Qtd" />
            <asp:BoundField DataField="ValorParcela" HeaderText="Vlr.Parcela" />
            <asp:BoundField DataField="DataVencimento" HeaderText="Vencimento" />
            <asp:BoundField DataField="Multa" HeaderText="Multa" />
            <asp:BoundField DataField="Juros" HeaderText="Juros" />
            <asp:BoundField DataField="ValorTotal" HeaderText="Vlr.Total" />
            <asp:BoundField DataField="DataRecebimento" HeaderText="DT.Receb." />
            <asp:BoundField DataField="ValorRecebido" HeaderText="Vlr.Recebido" />
            <asp:BoundField DataField="StatusPagamento" HeaderText="Situação" />
            <asp:BoundField DataField="DescricaoFormaPagamento" HeaderText="F.Pag" />

			<asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" OnClick="ImageButton1_Click" Width="20px" />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField> 
                 <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnReceber" runat="server"  Text="Receber" OnClick="btnReceber_Click" />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField> 
                 <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnEstornar" runat="server"  Text="Estornar" OnClick="btnEstornar_Click"  />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField> 
                 <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnExcluir" runat="server"  Text="Excluir"  OnClick="btnExcluir_Click"  />
                      </ItemTemplate>
            </asp:TemplateField>

           
        </Columns>
                   </asp:GridView>

      </asp:Panel>

     </ContentTemplate>
        </asp:UpdatePanel>

</asp:Content>