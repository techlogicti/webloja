﻿using AcessoBancoDados;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Web
{
    public class Util
    {

        string ambiente = "";

        public bool validarChaveNFE(string chaveNFE)
        {
            bool retorno = false;

            if (chaveNFE.Length.Equals(44)) retorno = true;

            return retorno;
        }

        public string carregaConfigs()
        {
            ambiente = ConfigurationManager.AppSettings["ambiente"];

            return ambiente;
        }

        public string obtemAmbiente()
        {
            ambiente = ConfigurationManager.AppSettings["ambiente"];

            return ambiente;
        }

        public string conexao()
        {
            string connectionString = "";
            connectionString = DadosDaConexao.StringDeConexao(obtemAmbiente());

            //connectionString = ConfigurationManager.ConnectionStrings["Prod"].ConnectionString;
            //connectionString = ConfigurationManager.ConnectionStrings["Desenv"].ConnectionString;

            return connectionString;
        }

     

        public void preencheComboOrdemPesquisaCliente(DropDownList comboBox)
        {
            ListItem l = new ListItem("-", "-", true);
            l.Selected = true;
            comboBox.Items.Clear();
            comboBox.Items.Add(new ListItem("Nome", "NM_nomeCompleto"));
            comboBox.Items.Add(new ListItem("Apelido", "NM_apelido"));
            comboBox.Items.Add(new ListItem("Cpf", "NU_cpfCnpj"));
        }


        public void preencheComboStatusEntrega(DropDownList comboBox)
        {
            ListItem l = new ListItem("-", "-", true);
            l.Selected = true;
            comboBox.Items.Clear();
            comboBox.Items.Add("-");
            comboBox.Items.Add(new ListItem("Aguardando", "A"));
            comboBox.Items.Add(new ListItem("Entregue", "E"));
        }

        public void preencheComboStatusPagamento(DropDownList comboBox)
        {
            ListItem l = new ListItem("-", "-", true);
            l.Selected = true;
            comboBox.Items.Clear();
            comboBox.Items.Add("-");
            comboBox.Items.Add("Atrasada");
            comboBox.Items.Add("Conferir");
            comboBox.Items.Add("Vencida");
            comboBox.Items.Add("Pendente");
            comboBox.Items.Add("Quitado");
        }

        public void preencheListStatusPagamento(ListBox listBox)
        {
            ListItem l = new ListItem("-", "-", true);
            l.Selected = true;
            listBox.Items.Clear();
            listBox.Items.Add("-");
            listBox.Items.Add("Atrasada");
            listBox.Items.Add("Conferir");
            listBox.Items.Add("Cancelado");
            listBox.Items.Add("Vencida");
            listBox.Items.Add("Parcial");
            listBox.Items.Add("Pendente");
            listBox.Items.Add("Quitado");
            foreach (ListItem li in listBox.Items)
                li.Selected = true;
        }

        public void preencheListBox(ListBox listBox, string queryString, string todos)
        {
            DataSet ds = GetData(queryString, conexao());
            if (ds.Tables.Count > 0)
            {
                listBox.Items.Clear();
                listBox.DataSource = ds;
                listBox.DataTextField = "NM_nome";
                listBox.DataValueField = "ID";
                listBox.DataBind();
                if (todos.Equals("S"))
                {
                    ListItem l = new ListItem("-", "0", true);
                    l.Selected = true;
                    listBox.Items.Add(l);
                }
            }
            else
            {
                //lblMensagem.Text = "Não foi possível conectar a base de dados.";
            }
        }

        public void preencheCombo(DropDownList comboBox, string queryString, string todos)
        {

            DataSet ds = GetData(queryString, conexao());
            if (ds.Tables.Count > 0)
            {
                comboBox.Items.Clear();
                comboBox.DataSource = ds;
                comboBox.DataTextField = "NM_nome";
                comboBox.DataValueField = "ID";
                comboBox.DataBind();
                if (todos.Equals("S"))
                {
                    ListItem l = new ListItem("-", "0", true);
                    l.Selected = true;
                    comboBox.Items.Add(l);
                }
            }
            else
            {
                //lblMensagem.Text = "Não foi possível conectar a base de dados.";
            }

        }

        DataSet  GetData(String queryString, String connectionString)
        {

            

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                //lblMensagem.Text = "Não foi possível conectar a base de dados.";

            }

            return ds;


        }

        public DataSet ObtemDados(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();
            string connectionString = "";
            try
            {
                // Connect to the database and run the query.
                Util u = new Util();

                connectionString = DadosDaConexao.StringDeConexao(u.obtemAmbiente());

                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                //lblMensagemAniverMes.Text = "Não foi possível conectar a base de dados.";

            }

            return ds;

        }

        public string transformaDataAAAAMMDD(string data) {
            string dt = "";
            string r = ""; ;
            dt = data.ToString().Trim();
            dt = dt.Replace("/", "");

            string a, m, d;

            //01022000
            a = dt.Substring(4,4);
            m = dt.Substring(2, 2);
            d = dt.Substring(0, 2);

            r = a + m + d;
            return r;
        }

        /// <summary>
        /// Calcula MD% Hash de uma determinada string passada como parametro
        /// </summary>
        /// <param name="Senha">String contendo a senha que deve ser criptografada para MD5 Hash</param>
        /// <returns>string com 32 caracteres com a senha criptografada</returns>
        public  string CalculaHash(string Senha)
        {
            try
            {
                System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(Senha);
                byte[] hash = md5.ComputeHash(inputBytes);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString(); // Retorna senha criptografada 
            }
            catch (Exception)
            {
                return null; // Caso encontre erro retorna nulo
            }
        }

    }
}