﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Carne : System.Web.UI.Page
    {

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            testCheckUserSoberana();
            Util u = new Util();
            connectionString = u.conexao();
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                panelBotoes.Visible = true;

                carregaGrid();
            }
        }

        public void carregaGrid()
        {

            String queryString = " select ID as ID, ";
            queryString = queryString + " ID_pedido as NumeroPedido, ";
            queryString = queryString + " (select c.NM_nomeCompleto from TB_cliente c where id_cliente = c.ID) as Cliente, ";
            queryString = queryString + " nu_numeroParcela as NumeroParcela, ";
            queryString = queryString + " vl_valorParcela as ValorParcela, ";
            queryString = queryString + " VL_valorCompra as ValorCompra, ";
            queryString = queryString + " VL_valorTotal as ValorTotal, ";
            queryString = queryString + " nu_quantidadeParcela as QtdParcelas, ";
            queryString = queryString + " Convert(varchar(10),CONVERT(date,convert(varchar,dt_dataCompra),106),103) as DataCompra, ";
            queryString = queryString + " Convert(varchar(10),CONVERT(date,convert(varchar,dt_dataVencimento),106),103) as DataVencimento, ";
            queryString = queryString + " ST_situacaoCarne as StatusCarne ";
            queryString = queryString + " from [TB_carne]; ";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);

            if (ds.Tables.Count > 0)
            {
                CarnesGridView.DataSource = ds;

                CarnesGridView.DataBind();

                lblTotal.Text = CarnesGridView.Rows.Count.ToString();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
            }

        }

        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";

            }

            return ds;


        }

        protected void btnListarCarne_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            panelCadastro.Visible = true;
            panelDetalhe.Visible = false;
            panelGrid.Visible = false;
            panelBotoes.Visible = false;
            panelFiltro.Visible = false;
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Server.Transfer("Carne.aspx", true);

        }

        protected void BtnDetalhar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;


            exibeMVC(gvr.Cells[0].Text.ToString(), "V", "1");
        }

        public void exibeMVC(string id, string modoMVC, string usuario)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelDetalhe.Visible = false;
            panelBotoes.Visible = false;
        }

    }
}