﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Relatorios
{
    public partial class RelatorioVendasMes : System.Web.UI.Page
    {


        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("~/Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            testCheckUserSoberana();

            //panelRelatorios.Visible = true;
            ReportViewer1.AsyncRendering = false;
            ReportViewer1.SizeToReportContent = true;
        }

        protected void btnAtualizar_Click(object sender, EventArgs e)
        {
            string relatorio = "";

            relatorio = ddlRelatorios.SelectedValue.ToString();

            ReportDataSource ReportDataSource1 = new ReportDataSource();

            ReportDataSource1.Name = "VendasPorMesTipoPagamento";
            ReportDataSource1.Value = "VendasPorMesTipoPagamento";
            ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1);
            ReportViewer1.LocalReport.ReportEmbeddedResource = "VendasPorMesTipoPagamento.rdlc";

            /*ReportDataSource1.Name = "RelVendasPorMes";
            ReportDataSource1.Value = "SqlDataSource1";
            ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1);
            ReportViewer1.LocalReport.ReportEmbeddedResource = "ReportSchedule.rdlc";*/



            /*ReportViewer1.Reset();
            ReportViewer1.LocalReport.ReportEmbeddedResource = MapPath("Report1.rdlc");
            
            ReportViewer1.LocalReport.Refresh();*/
        }
    }
}