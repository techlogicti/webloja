﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RelatorioComprasMes.aspx.cs" Inherits="Web.Relatorios.RelatorioComprasMes" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SoberanaLocalConnectionString %>" SelectCommand="SELECT * FROM [vw_relatorioComprasMes]"></asp:SqlDataSource>
    
     <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="661px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1184px">
         <LocalReport ReportPath="Relatorios\RelComprasMes.rdlc">
             <DataSources>
                 <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="RelComprasMes" />
             </DataSources>
         </LocalReport>
     </rsweb:ReportViewer>
    
     <asp:Button ID="btnAtualizar" runat="server" Text="Atualizar" OnClick="btnAtualizar_Click" />
</asp:Content>
