﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RelatorioVendasMes.aspx.cs" Inherits="Web.Relatorios.RelatorioVendasMes" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    
     <asp:Panel ID="panelRelatorios" runat="server" Visible="false">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
        <div class="container-full">
            <asp:Button ID="btnVoltar" class="btn-sm btn-warning" runat="server" Text="Voltar"  />
            <h2>Relatórios</h2>
            <p>Identificação do Relatório -   <asp:Label ID="lblModo" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label></p>
            <div class="panel-group">
            <div class="panel panel-default">
              <div class="panel-heading">
                  <asp:DropDownList ID="ddlRelatorios" runat="server">
                      <asp:ListItem Value="VendasPorMes.rdlc">Vendas Totais por Mês</asp:ListItem>
                      <asp:ListItem Value="VendasPorMesTipoPagamento.rdlc">Vendas Totais por Mês e Tipo de Pagamento</asp:ListItem>
                  </asp:DropDownList>
                  <asp:Button ID="btnAtualizar" runat="server" Text="Atualizar" OnClick="btnAtualizar_Click" />
                </div>
                  <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="867px" Height="589px">
        <LocalReport ReportPath="Relatorios\VendasPorMes.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="RelVendasPorMes" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SoberanaLocalConnectionString %>" SelectCommand="SELECT * FROM [vw_relatorioVendasMes]"></asp:SqlDataSource>
                            </div>
                        </div>
                  </div>
            </div>
            </div>
       </ContentTemplate>
    </asp:UpdatePanel>
  </asp:Panel>
</asp:Content>