﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RelatorioVendasTotaisMesVendedor.aspx.cs" Inherits="Web.Relatorios.RelatorioVendasTotaisMesVendedor" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="570px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1007px">
        <ServerReport ReportPath="RelatorioVendasTotaisMesVendedor" />
    <LocalReport ReportPath="Relatorios\RelatorioVendasTotaisMesVendedor.rdlc">
        <DataSources>
            <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="RelatorioVendasTotaisMesVendedor" />
        </DataSources>
    </LocalReport>
</rsweb:ReportViewer>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SoberanaLocalConnectionString %>" SelectCommand="SELECT * FROM [vw_relatorioVendasTotaisMesVendedor] ORDER BY [Mes], [Vendedor]"></asp:SqlDataSource>

</asp:Content>
