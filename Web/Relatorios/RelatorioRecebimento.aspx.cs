﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Relatorios
{
    public partial class RelatorioRecebimento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Relatorios/Recebimento.rdlc");
                //ReportViewer1.LocalReport.Refresh();
                ReportViewer1.AsyncRendering = false;
                ReportViewer1.SizeToReportContent = true;
            }
        }
    }
}