﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Logon.aspx.cs" Inherits="Web.Logon" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">




	<div align="center">
	<asp:Panel ID="panelLogin" runat="server" DefaultButton="btnLogin"  CssClass="container-full" Width="270px" HorizontalAlign="Center">
		<div class="well well-sm">Acesso ao Sistema</div>
		 <div class="row">
			<div class="col-sm-12">
				<asp:Image ID="Image1" runat="server" ImageUrl="~/Img/logo-MENOR.png" />
             </div>
			 
		</div>
		 <div class="row">
			<div class="col-sm-12">
               <label for="txtUsuario">Usuário</label>
               <asp:TextBox ID="txtUsuario" runat="server" placeholder="Usuário" class="form-control"></asp:TextBox>
             </div>
			 
		</div>
		<div class="row">
			 <div class="col-sm-12">
               <label for="txtSenha">Senha</label>
               <asp:TextBox textmode="Password" ID="txtSenha" runat="server" placeholder="Senha" class="form-control"></asp:TextBox>
             </div>
		</div>
		<div class="row">
			  <div class="col-sm-12">
               <br />
              <asp:Button ID="btnLogin" runat="server" placeholder="Senha" class="btn btn-danger"  OnClick="btnLogin_Click" Text="Entrar"></asp:Button>
             </div>
		</div>
		<div class="row">
			  <div class="col-sm-12">
               
              <asp:Label ID="lblMsg" runat="server" Text="Label" ForeColor="Red"></asp:Label>
             </div>
		  </div>
		
	</asp:Panel>
		</div>
		
	

</asp:Content>