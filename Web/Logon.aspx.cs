﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Logon : System.Web.UI.Page
    {

        Util util = new Util();

        // Verify a hash against a string.
        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

       
        public void testCheckUserSoberana()
        {
            if (!string.IsNullOrEmpty(Session["user"] as string))
            {
                //Response.Redirect("~/Default");
                Server.Transfer("Default.aspx", true);
            }
            else
            {
                //Response.Redirect("~/Logon");
                Server.Transfer("Logon.aspx", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
        }

        private bool ValidateUser(string userName, string passWord)
        {

            //
            using (MD5 md5Hash = MD5.Create())
            {
                passWord = GetMd5Hash(md5Hash, passWord);
            }
            //


            SqlConnection conn;
            SqlCommand cmd;
            string lookupPassword = null;

           
            try
            {
                // Consult with your SQL Server administrator for an appropriate connection
                // string to use to connect to your local SQL Server.
                conn = new SqlConnection(util.conexao());
                conn.Open();

                // Create SqlCommand to select pwd field from users table given supplied userName.
                string sql = "";
                sql = "Select de_senha from tb_operador where cd_login='"+ userName + "'";
                cmd = new SqlCommand(sql, conn);


                // Execute command and fetch pwd field into lookupPassword string.
                lookupPassword = (string)cmd.ExecuteScalar();

                

                

                // Cleanup command and connection objects.
                cmd.Dispose();
                conn.Dispose();
            }
            catch (Exception ex)
            {
                // Add error handling here for debugging.
                // This error message should not be sent back to the caller.
                System.Diagnostics.Trace.WriteLine("[ValidateUser] Exception " + ex.Message);
            }

            // If no password found, return false.
            if (null == lookupPassword)
            {
                // You could write failed login attempts here to event log for additional security.
                return false;
            }

            // Compare lookupPassword and input passWord, using a case-sensitive comparison.
            return (0 == string.Compare(lookupPassword, passWord, false));

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string usuario = "";
            string senha = "";

            usuario = txtUsuario.Text.Trim().ToString();
            senha = txtSenha.Text.Trim().ToString();

            bool userOk = false;

            userOk = ValidateUser(usuario, senha);

            if (userOk)
            {
                //lblMsg.Text = "Passou";
                Session["user"] = usuario.ToString();
                //Response.Redirect("~/Default.aspx");
                Server.Transfer("Default.aspx", true);

            }
            else
            {
                lblMsg.Text = "Usuário ou senha inválido(s)";
            }
        }
    }
}