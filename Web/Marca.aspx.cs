﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Marca : System.Web.UI.Page
    {

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        private DALConexao conexao;
        Util util = new Util();
        BLLOperador bllOperador = null;

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

            return ds;


        }

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            testCheckUserSoberana();
            connectionString = util.conexao();
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {


                panelBotoes.Visible = true;


                // Declare the query string.
                
                String queryString = " select [ID] as ID, [NM_nome] as nome, [DE_descricao] as descricao ";
                queryString = queryString + "  from [TB_marca] where 1=1  order by NM_nome asc ";

                // Run the query and bind the resulting DataSet
                // to the GridView control.
                DataSet ds = GetData(queryString);
                if (ds.Tables.Count > 0)
                {
                    MarcasGridView.DataSource = ds;

                    MarcasGridView.DataBind();

                    lblTotal.Text = MarcasGridView.Rows.Count.ToString();
                }
                else
                {
                    lblMensagem.Text = "Não foi possível conectar a base de dados.";
                    openModalMensagem();
                }
            }
        }

        protected void btnNovaMarca_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;
            

            limpaCamposTela();
        }

        public void limpaCamposTela()
        {

            txtNomeInput.Text = "";
            txtDescricaoInput.Text = "";

        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
            panelBotoes.Visible = true;
        }

        protected void btnListarMarcas_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            //
            int idOperador = 0;



            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            bllOperador = new BLLOperador(conexao);
            ModeloOperador modeloOperador = new ModeloOperador();
            if (!string.IsNullOrEmpty(Session["user"] as string))
            {
                modeloOperador = bllOperador.recuperaIdLogin(Session["user"].ToString());
                idOperador = Convert.ToInt32(modeloOperador.Id);
            }


            //

            //recupera valores tela

            string iNome, iDescricao;

            iNome = txtNomeInput.Text.ToString();
            iDescricao = txtDescricaoInput.Text.ToString();
            
            //valida dados
            string sqlInsertPedido = "";
            bool formOk = false;
            int count = 0;

            if (!iNome.Equals("")) count++;
            if (!iDescricao.Equals("")) count++;
            


            if (count == 2) { formOk = true; } else { formOk = false; }


            if (formOk)
            {

                ModeloMarca modelo = new ModeloMarca();
                modelo.Descricao_curta = iNome.ToString();
                modelo.Descricao_longa = iDescricao.ToString();
                
                modelo.CD_operador = Convert.ToInt32(idOperador.ToString());

                try
                {

                    int id = 0;
                    conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                    BLLMarca bll = new BLLMarca(conexao);
                    id = bll.Incluir(modelo);

                    //this.gravarLog("Execução do sql: " + sqlInsertPedido.ToString());

                    if (id > 0)
                    {
                        lblMensagem.Text = "Marca inserida com sucesso ! [ ID: " + id.ToString() + " ] ";
                        openModalMensagem();
                        limpaCamposTela();
                    }
                    else
                    {
                        lblMensagem.Text = "Verificar log de inclusão !";
                        openModalMensagem();
                    }



                }
                catch (Exception ex)
                {
                    //ex.ToString();
                    lblMensagem.Text = "Erro na gravação";
                    openModalMensagem();
                    //this.gravarLog(ex.ToString());
                }




            }
            else
            {
                lblMensagem.Text = "Preencha todos os campos antes de salvar os dados ";
                openModalMensagem();
            }








        }

        public void atualizaGrid(string filtro)
        {

            // Declare the query string.

            String queryString = " select [ID] as ID, [DE_descricao] as descricao, [NM_nome] as nome ";
            queryString = queryString + "  from [dbo].[TB_marca] where 1=1  ";



            if (!filtro.Equals(""))
            {
                queryString = queryString + " and nm_nome like '%" + filtro.ToString() + "%'";
                queryString = queryString + " or de_descricao like '%" + filtro.ToString() + "%'";
                queryString = queryString + " or ID like '%" + filtro.ToString() + "%'";
            }

            queryString = queryString + " order by [nm_nome] asc";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                MarcasGridView.DataSource = ds;
                MarcasGridView.DataBind();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        protected void pesquisar()
        {
            string strDE_descricao = "";

            if (!txtDE_descricao.Text.Trim().Equals(""))
            {
                strDE_descricao = txtDE_descricao.Text.Trim().ToString();
                atualizaGrid(strDE_descricao);
            }
        }
        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisar();
        }
    }
}