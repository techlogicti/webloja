﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Categoria.aspx.cs" Inherits="Web.Categoria" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

	<asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate>
     <hr />
                 <asp:Panel ID="panelBotoes" runat="server" Visible="false">
    <div class="container-fluid">
        <asp:Button ID="btnListarCategorias" class="btn btn-primary" runat="server" Text="Listar Categorias"/>
         <asp:Button ID="btnNovaCategoria" class="btn btn-success" runat="server" Text="Nova Categoria"  />
     </div>
    </asp:Panel>

    <hr />

 <asp:Panel ID="panelCadastro" runat="server" Visible="false">
         <div class="container">
  <h2>Nova Categoria</h2>
  <p>Identificação da Categoria</p>
  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Dados Básicos</div>
          <div class="panel-body">
              <!-- Linha 1-->
               <div class="row">
                    <div class="col-lg-2">
                         <asp:Label ID="lblCpfCnpj" runat="server" Text="CPF/CNPJ: ">
                            <asp:TextBox  class="form-control" ID="txtCpfCnpjInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                     <div class="col-lg-2">
                         <asp:Label ID="lblTipoPessoaInput" runat="server" Text="Tipo Pessoa: ">
                             
                             
                        
                         <asp:RadioButtonList ID="radioButtonListTipoPessoa" runat="server">
                             <asp:ListItem Value="F">Física</asp:ListItem>
                             <asp:ListItem Selected="True" Value="F">Jurídica</asp:ListItem>
                         </asp:RadioButtonList>
                             </asp:Label>
                    </div>
                   <div class="col-lg-4">
                         <asp:Label ID="lblRazaoSocial" runat="server" Text="Razao Social: ">
                            <asp:TextBox  class="form-control" ID="txtRazaoSocialInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-lg-4">
                         <asp:Label ID="lblNomeFantasia" runat="server" Text="Nome Fantasia: ">
                            <asp:TextBox  class="form-control" ID="txtNomeFantasiaInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                </div>
             <!-- -->

             

          </div>
        <hr />
          <!-- AÇÔES-->
            <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Ações</div>
          <div class="panel-body">

              <div class="row">
               <div class="col-sm-3">
             
            </div>
                
                  </div>


          </div>
      </div>
    </div>


            <!-- -->
      </div>
    </div>
        
    </asp:Panel>

				  <asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">

        <div class="row">
             <div class="col-md-10">
                 <label for="txtDE_descricao">Código/Categoria/SubCategoria:
                      <div class="input-group">
                 <asp:TextBox ID="txtDE_descricao" TextMode="Search" class="form-control" runat="server"  Wrap="True"></asp:TextBox>
                     <span class="input-group-btn">
                         <asp:Button ID="btnPesquisar" class="btn btn-default" runat="server" Text="Pesquisar"  />
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info" runat="server" Text="Limpar Campos"  />
                      </span>
                          </div>
                 </label>
             </div>
            <div class="col-md-2">
                 <label for="txtDE_descricao">Ordenar por:
                <asp:DropDownList class="form-control" ID="ddlOrdemPesquisaCategoria" runat="server"></asp:DropDownList>
                     </label>
            </div>
        </div>

        </div>
        </div>
    </asp:Panel>

  <script type="text/javascript">


                  function openModalMensagem() {
                      $('#myModalMensagem').modal('show');
                  }

    function openModal() {
        $('#myModal').modal('show');
    }
</script>

    <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

  <div class="well well-sm"><b>Total de Categoria/Subcategoria ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>               

  <asp:Panel ID="panelGrid" runat="server" Visible="false">Detalhe das Categorias
     
                 <h3>Lista de Categorias</h3>
    <asp:GridView id="CategoriaGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" AutoGenerateColumns="False" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="CpfCnpj" HeaderText="CPF/CNPJ" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="NomeFantasia" HeaderText="Nome Fantasia" />
            <asp:BoundField DataField="RazaoSocial" HeaderText="Razão Social" />
            

            <asp:CommandField ShowSelectButton="True" />
        </Columns>
                   </asp:GridView>
          
          

   </asp:Panel>
          </ContentTemplate>
        </asp:UpdatePanel>
     
    

    
</asp:Content>