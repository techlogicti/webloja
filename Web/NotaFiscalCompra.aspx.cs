﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class NotaFiscalCompra : System.Web.UI.Page
    {
        private DALConexao conexao;
        Util util = new Util();

        string connectionString = "";

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        string sqlFornecedor = "select IDFornecedor as ID, DE_razaoSocial as NM_NOME from tb_fornecedor where st_situacaoregistro='A' order by DE_razaoSocial asc";
        string sqlCfopEntrada = "select ID, CD_codigo + ' - ' + NM_NOME as NM_NOME from tb_cfop where st_situacaoregistro='A' order by nm_nome asc";
        string sqlFormaPagamento = "select ID, NM_NOME from tb_tipoPagamento where st_situacaoregistro='A' order by nm_nome asc";
        string sqlModeloNotaFiscal = "select ID, NM_NOME from tb_modeloNotaFiscal where st_situacaoregistro='A' order by nm_nome asc";
        string sqlTransportadora = "select IDFornecedor as ID, DE_razaoSocial as NM_NOME from tb_fornecedor where st_situacaoregistro='A' order by DE_razaoSocial asc";
        string sqlLocalEstoque = "select ID, NM_NOME from tb_localEstoque where st_situacaoregistro='A' order by nm_nome asc";
        

        

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            testCheckUserSoberana();

            btnCancelar.Attributes.Add("onClick", "javascript:history.back(); return false;");
            btnVoltar.Attributes.Add("onClick", "javascript:history.back(); return false;");

            panelBotoes.Visible = true;
            panelFiltro.Visible = true;

            connectionString = util.conexao();

            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                carregaGrid("");
            }
        }

        public void carregaGrid(String filtro)
        {

            String queryString = "select * from VW_notaFiscalEntrada ";



            if (!filtro.Equals(""))
            {
                queryString += filtro.ToString();
            }

            queryString += " order by id desc ";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);

            if (ds.Tables.Count > 0)
            {
                
                MyGridView.DataSource = ds;

                MyGridView.DataBind();

                lblTotal.Text = MyGridView.Rows.Count.ToString();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        DataSet GetData(String queryString)
        {


            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {
                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                //incluir textbox multline com o toString da ex
                //openModalMensagem();
            }

            return ds;


        }

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        protected void MyGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void MyGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the currently selected row using the SelectedRow property.
            GridViewRow row = MyGridView.SelectedRow;

            // Display the first name from the selected row.
            // In this example, the third column (index 2) contains
            // the first name.
            // lblMensagem.Text = "You selected " + row.Cells[2].Text + ".";

            //openModalFillData(Convert.ToInt32(row.Cells[0].Text));
        }

        protected void MyGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            // Get the currently selected row. Because the SelectedIndexChanging event
            // occurs before the select operation in the GridView control, the
            // SelectedRow property cannot be used. Instead, use the Rows collection
            // and the NewSelectedIndex property of the e argument passed to this 
            // event handler.
            GridViewRow row = MyGridView.Rows[e.NewSelectedIndex];

            // You can cancel the select operation by using the Cancel
            // property. For this example, if the user selects a customer with 
            // the ID "ANATR", the select operation is canceled and an error message
            // is displayed.
            if (row.Cells[1].Text == "ANATR")
            {
                e.Cancel = true;
                lblMensagem.Text = "You cannot select " + row.Cells[2].Text + ".";
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Server.Transfer("NotaFiscalCompra.aspx", true);
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            exibeMVC("", "I", "1");
        }

        public string recuperaProximoIdNotaFiscalCompra()
        {

            string sql = "select (max(id) + 1) as ID from TB_notaFiscalEntrada";
            string proximoId = "";


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "TB_notaFiscalEntrada");

            foreach (DataRow pRow in customerOrders.Tables["TB_notaFiscalEntrada"].Rows)
            {

                proximoId = pRow["ID"].ToString();


            }


            return proximoId;

        }

        public void exibeMVC(string id, string modoMVC, string usuario)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;

            if (modoMVC.Equals("I"))
            {
                lblMVC.Text = "Modo de inclusão";
                btnSalvar.Visible = true;
                btnAtualizar.Visible = false;
                btnLimparCampos.Visible = true;
                btnCancelar.Visible = true;

                string proximoIdNotaFiscalCompra = "";
                proximoIdNotaFiscalCompra = recuperaProximoIdNotaFiscalCompra();

                txtIdCadastro.Text = proximoIdNotaFiscalCompra.ToString();

                preencheCombo(ddlCfopEntradaCadastro, sqlCfopEntrada, "S");
                preencheCombo(ddlFornecedorCadastro, sqlFornecedor, "S");
                preencheCombo(ddlFormaPagamentoCadastro, sqlFormaPagamento, "S");
                preencheCombo(ddlModeloCadastro, sqlModeloNotaFiscal, "S");
                preencheCombo(ddlTransportadoraCadastro, sqlTransportadora, "S");
                preencheCombo(ddlLocalEstoque, sqlLocalEstoque, "N");

            }
            else if (modoMVC.Equals("A"))
            {
                lblMVC.Text = "Modo de edição";
                btnSalvar.Visible = false;
                btnAtualizar.Visible = true;
                btnLimparCampos.Visible = false;
                btnCancelar.Visible = true;
            }

       }

        public void preencheCombo(DropDownList comboBox, string queryString, string todos)
        {

            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                comboBox.Items.Clear();



                comboBox.DataSource = ds;
                comboBox.DataTextField = "nm_nome";
                comboBox.DataValueField = "id";
                comboBox.DataBind();

                if (todos.Equals("S"))
                {
                    ListItem l = new ListItem("Todos", "Todos", true);
                    l.Selected = true;
                    comboBox.Items.Add(l);
                }
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {

            /*Obtem dados da Tela*/
            string chaveAcessoCadastro = "";
            chaveAcessoCadastro = txtChaveNfeCadastro.Text.Trim().ToString();

            string numeroNotaCadastro = "";
            numeroNotaCadastro = txtNumeroNotaCadastro.Text.Trim().ToString();

            string dataEmissaoNotaCadastro = "";
            dataEmissaoNotaCadastro = txtDataEmissaoCadastro.Text.Trim().ToString();
            dataEmissaoNotaCadastro = dataEmissaoNotaCadastro.ToString().Replace("-", "");

            string dataEntradaNotaCadastro = "";
            dataEntradaNotaCadastro = txtDataEntradaCadastro.Text.Trim().ToString();
            dataEntradaNotaCadastro = dataEntradaNotaCadastro.ToString().Replace("-", "");

            string cfopEntradaCadastro = "";
            cfopEntradaCadastro = ddlCfopEntradaCadastro.SelectedValue.ToString();

            string serieEntradaCadastro = "";
            serieEntradaCadastro = txtSerieCadastro.Text.Trim().ToString();

            string modeloEntradaCadastro = "";
            modeloEntradaCadastro = ddlModeloCadastro.SelectedValue.ToString();

            string formaPagamentoEntradaCadastro = "";
            formaPagamentoEntradaCadastro = ddlFormaPagamentoCadastro.SelectedValue.ToString();

            string fornecedorEntradaCadastro = "";
            fornecedorEntradaCadastro = ddlFornecedorCadastro.SelectedValue.ToString();

            string baseCalculoICMSEntradaCadastro = "";
            baseCalculoICMSEntradaCadastro = txtBaseCalcICMSCadastro.Text.Trim().ToString().Replace(".", ","); ;
            

            string valorICMSEntradaCadastro = "";
            valorICMSEntradaCadastro = txtValorICMSCadastro.Text.Trim().ToString().Replace(".", ","); ;

            string baseCalculoICMSSubstituicaoEntradaCadastro = "";
            baseCalculoICMSSubstituicaoEntradaCadastro = txtBaseCalcICMSSubCadastro.Text.Trim().ToString().Replace(".", ","); ;

            string valorICMSSubstituicaoEntradaCadastro = "";
            valorICMSSubstituicaoEntradaCadastro = txtValorICMSSubCadastro.Text.Trim().ToString().Replace(".", ","); ;

            string transportadoraEntradaCadastro = "";
            transportadoraEntradaCadastro = ddlTransportadoraCadastro.SelectedValue.ToString();

            string valorFreteEntradaCadastro = "";
            valorFreteEntradaCadastro = txtValorFreteCadastro.Text.Trim().ToString().Replace(".", ","); ;

            string valorSeguroEntradaCadastro = "";
            valorSeguroEntradaCadastro = txtValorSeguroCadastro.Text.Trim().ToString().Replace(".", ","); ;

            string valorOutrasDespesasEntradaCadastro = "";
            valorOutrasDespesasEntradaCadastro = txtValorOutrasDespesas.Text.Trim().ToString().Replace(".", ","); ;

            string valorIPIEntradaCadastro = "";
            valorIPIEntradaCadastro = txtValorIPICadastro.Text.Trim().ToString().Replace(".", ","); ;

            string valorTotalProdutosEntradaCadastro = "";
            valorTotalProdutosEntradaCadastro = txtValorTotalProdutosCadastro.Text.Trim().ToString().Replace(".", ","); ;

            string valorTotalNotaEntradaCadastro = "";
            valorTotalNotaEntradaCadastro = txtValorTotalNotaCadastro.Text.Trim().ToString().Replace(".", ","); ;

            /*Verifica se a chave informada já está cadastrada no sistema, visto
            que o número da nota até pode ser o mesmo, uma vez que sua sequência
            é por fornecedor*/

            /*Instância um novo objeto ModeloNotaFiscalEntrada e realiza o parse dos dados*/
            ModeloNotaFiscalEntrada modeloNotaFiscalEntrada = new ModeloNotaFiscalEntrada();
            modeloNotaFiscalEntrada.ChaveAcesso = chaveAcessoCadastro.ToString();
            modeloNotaFiscalEntrada.NumeroNotaFiscal = numeroNotaCadastro.ToString();
            modeloNotaFiscalEntrada.DataEmissao = Convert.ToInt32(dataEmissaoNotaCadastro.ToString());
            modeloNotaFiscalEntrada.DataEntrada = Convert.ToInt32(dataEntradaNotaCadastro.ToString());

            modeloNotaFiscalEntrada.IdCfop = Convert.ToInt32(cfopEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.SerieNotaFiscal = serieEntradaCadastro.ToString();
            modeloNotaFiscalEntrada.ModeloNotaFiscal = modeloEntradaCadastro.ToString();
            modeloNotaFiscalEntrada.IdFormaPagamento = Convert.ToInt32(chaveAcessoCadastro.ToString());
            modeloNotaFiscalEntrada.IdFornecedor = Convert.ToInt32(fornecedorEntradaCadastro.ToString());

            modeloNotaFiscalEntrada.ValorBaseCalculoICMS = Convert.ToDecimal(baseCalculoICMSEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorICMS = Convert.ToDecimal(valorICMSEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorBaseCalculoICMSSubST = Convert.ToDecimal(baseCalculoICMSSubstituicaoEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorICMSSubST = Convert.ToDecimal(valorICMSSubstituicaoEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.IdTransportadora = Convert.ToInt32(transportadoraEntradaCadastro.ToString());

            modeloNotaFiscalEntrada.ValorFrete = Convert.ToDecimal(valorFreteEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorSeguro = Convert.ToDecimal(valorSeguroEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorOutrasDespesas = Convert.ToDecimal(valorOutrasDespesasEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorIPI = Convert.ToDecimal(valorIPIEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorTotalProdutos = Convert.ToDecimal(valorTotalProdutosEntradaCadastro.ToString());
            modeloNotaFiscalEntrada.ValorTotalNota = Convert.ToDecimal(valorTotalNotaEntradaCadastro.ToString());

            modeloNotaFiscalEntrada.Observacoes = "";

            //não presente na tela
            modeloNotaFiscalEntrada.CD_operador = 0;
            modeloNotaFiscalEntrada.DT_dataCriacao = Convert.ToInt32(dataHoje);
            modeloNotaFiscalEntrada.HR_horaCriacao = Convert.ToInt32(horaHoje);
            modeloNotaFiscalEntrada.SituacaoEntrega = "A";
            modeloNotaFiscalEntrada.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
            modeloNotaFiscalEntrada.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
            modeloNotaFiscalEntrada.SituacaoPedido = "A";
            modeloNotaFiscalEntrada.SituacaoRegistro = "A";
            //

            /*Instância a classe de negócio
             * Chama o método de inclusao e passa o objeto modelo como parâmetro*/
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLNotaFiscalEntrada bllNotaFiscalEntrada = new BLLNotaFiscalEntrada(conexao);

            int idGerado = 0;
            idGerado = bllNotaFiscalEntrada.Incluir(modeloNotaFiscalEntrada);

            if(idGerado > 0)
            {
                lblMensagem.Text = "Registro inserido !";
                
                openModalMensagem();
                panelFiltro.Visible = false;
            }
            else
            {
                lblMensagem.Text = "Registro não gravado. Favor consultar o log.";
                
                openModalMensagem();
                panelFiltro.Visible = false;
            }


        }

        protected void btnAtualizar_Click(object sender, EventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btnDetalharGrid_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //ViewState["idNotaFiscalEntrada"] = row.Cells[0].Text.ToString();
            
            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;



            int nfEntrada = 0;
            nfEntrada = Convert.ToInt32(gvr.Cells[0].Text);

            Response.Redirect("~/NotaFiscalCompraDetalhe.aspx?idNotaFiscalEntrada="+ nfEntrada);

        }

        protected void btnEditaGridr_Click(object sender, EventArgs e)
        {

        }

        protected void btnBuscarDadosBetter_Click(object sender, EventArgs e)
        {
            string chaveNFE = "";
            chaveNFE = txtChaveNfeCadastro.Text.Trim().ToString();

            if (!chaveNFE.Equals(""))
            {
                if (util.validarChaveNFE(chaveNFE))
                {
                    Console.WriteLine("Chave OK");
                    lblMensagem.Text = "Chave OK";

                    ModeloNotaFiscalEntrada modeloNFeEntrada = new ModeloNotaFiscalEntrada();
                    

                    conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                    BLLNotaFiscalEntrada nfeEntradaBLL = new BLLNotaFiscalEntrada(conexao);

                    modeloNFeEntrada = nfeEntradaBLL.LocalizarBetter(chaveNFE);

                    if (modeloNFeEntrada.Equals(null))
                    {
                        lblMensagem.Text = "OBJETO NULO";
                    }
                    else
                    {
                        lblMensagem.Text = "DADOS RECUPERADOS. CONFIRA ANTES DE PROSSEGUIR !";
                        txtNumeroNotaCadastro.Text = modeloNFeEntrada.NumeroNotaFiscal.ToString();
                        txtSerieCadastro.Text = modeloNFeEntrada.NumeroNotaFiscal.ToString();
                        txtValorSeguroCadastro.Text = modeloNFeEntrada.ValorSeguro.ToString();
                        txtValorFreteCadastro.Text = modeloNFeEntrada.ValorFrete.ToString();
                        txtValorTotalNotaCadastro.Text = modeloNFeEntrada.ValorTotalNota.ToString();
                        txtValorTotalProdutosCadastro.Text = modeloNFeEntrada.ValorTotalProdutos.ToString();
                        ddlModeloCadastro.SelectedValue = modeloNFeEntrada.ModeloNotaFiscal;

                    }

                    openModalMensagem();
                    panelFiltro.Visible = false;
                }
                else
                {
                    Console.WriteLine("Chave Incorreta");
                    lblMensagem.Text = "Chave Não Encontrada";
                    openModalMensagem();
                    panelFiltro.Visible = false;
                }
            }
            else
            {
                Console.WriteLine("Chave Vazia");
                lblMensagem.Text = "Favor preencher o campo Chave NFe";
                openModalMensagem();
                panelFiltro.Visible = false;
            }

            
            
        }
    }
}