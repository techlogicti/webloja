﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Marca.aspx.cs" Inherits="Web.Marca" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate>
     <hr />
                 <asp:Panel ID="panelBotoes" runat="server" Visible="false">
    <div class="container-fluid">
        <asp:Button ID="btnListarMarcas" class="btn btn-primary" runat="server" Text="Listar Marcas" OnClick="btnListarMarcas_Click" />
         <asp:Button ID="btnNovaMarca" class="btn btn-success" runat="server" Text="Nova Marca" OnClick="btnNovaMarca_Click" />
     </div>
    </asp:Panel>

    <hr />

    <asp:Panel ID="panelCadastro" runat="server" Visible="false">
         <div class="container">
  <h2>Nova Marca</h2>
  <p>Identificação da Marca</p>
  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Dados Básicos</div>
          <div class="panel-body">
              <!-- Linha 1-->
               <div class="row">
                   <div class="col-lg-6">
                         <asp:Label ID="lblNomeInput" runat="server" Text="Nome: ">
                            <asp:TextBox  class="form-control" ID="txtNomeInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-lg-6">
                         <asp:Label ID="lblDescricaoInput" runat="server" Text="Descrição: ">
                            <asp:TextBox  class="form-control" ID="txtDescricaoInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                </div>
             <!-- -->

             

          </div>
        <hr />
          <!-- AÇÔES-->
            <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Ações</div>
          <div class="panel-body">

              <div class="row">
               <div class="col-sm-3">
             <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />
            <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"/>
            </div>
                
                  </div>


          </div>
      </div>
    </div>


            <!-- -->
      </div>
    </div>
        
    </asp:Panel>
                 

<!-- -->
                <asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">

        <div class="row">
             <div class="col-md-10">
                 <label for="txtDE_descricao">Código/Nome/Descrição:
                      <div class="input-group">
                 <asp:TextBox ID="txtDE_descricao" TextMode="Search" class="form-control" runat="server"  Wrap="True"></asp:TextBox>
                     <span class="input-group-btn">
                         <asp:Button ID="btnPesquisar" class="btn btn-default" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click" />
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info" runat="server" Text="Limpar Campos" />
                      </span>
                          </div>
                 </label>
             </div>
            <div class="col-md-2">
                 <label for="txtDE_descricao">Ordenar por:
                <asp:DropDownList class="form-control" ID="ddlOrdemPesquisaCliente" runat="server"></asp:DropDownList>
                     </label>
            </div>
        </div>

        </div>
        </div>
    </asp:Panel>
<!-- -->

    

<!-------------------------------------------------------->
                <!-- Trigger the modal with a button -->

                <script type="text/javascript">


                  function openModalMensagem() {
                      $('#myModalMensagem').modal('show');
                  }

    function openModal() {
        $('#myModal').modal('show');
    }
</script>
                

                
             
         <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->
               

                

               <asp:Panel ID="panelGrid" runat="server" Visible="true">Detalhe da Marca
     <div class="well well-sm"><b>Total de Marcas ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>
                 <h3>Lista de Marcas</h3>
    <asp:GridView id="MarcasGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" AutoGenerateColumns="False" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" />
            <asp:BoundField DataField="Descricao" HeaderText="Descrição" />
            

            <asp:CommandField ShowSelectButton="True" />
        </Columns>
                   </asp:GridView>
          
          

   </asp:Panel>
          </ContentTemplate>
        </asp:UpdatePanel>
     
    

    
</asp:Content>


