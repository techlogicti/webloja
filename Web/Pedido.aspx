﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pedido.aspx.cs" Inherits="Web.Pedido" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <ContentTemplate>
     <hr />


    <asp:Panel ID="panelBotoes" runat="server" Visible="false">

    <div class="container-full">
        <asp:Button ID="btnListarPedidos" class="btn btn-primary" runat="server" Text="Listar Pedidos" OnClick="btnListarPedidos_Click" />
         <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Novo Pedido" OnClick="btnNovo_Click" />
     </div>
    </asp:Panel>

    <hr />

    <asp:Panel ID="panelCadastro" runat="server" Visible="false">
        
        <div class="container-full">
  <h2>Cadastro de Pedido</h2>
  <p>Preencha o formulário abaixo para registrar o Pedido de Venda no Sistema. Somente após todo o preenchimento a venda será realizada.</p>
  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Identificação do Pedido</div>
      <div class="panel-body">

           <div class="row">
                <div class="col-sm-3">
                    <asp:Label ID="lblNumeroPedidoInput" runat="server" Text="Núm.Pedido: ">
                    <asp:TextBox  ReadOnly="false" class="form-control input-sm" ID="txtNumeroPedidoInput" runat="server"></asp:TextBox>
                    </asp:Label>
                </div>

                 <div class="col-sm-3">
                      <asp:Label ID="lblDataPedidoInput" runat="server" Text="Data Pedido: "></asp:Label>
                        <asp:TextBox  class="form-control input-sm" ID="txtDataPedidoInput" text="%# DateTime.Now.ToShortDateString() %>" TextMode="Date"  runat="server" OnTextChanged="txtDataPedidoInput_TextChanged"></asp:TextBox>

                    
                 </div>

                   <div class="col-sm-3">
                     <asp:Label ID="lblVendedorInput" runat="server" Text="Vendedor: ">
                         <asp:DropDownList   class="form-control input-sm"  ID="ddlVendedorInput" runat="server"></asp:DropDownList>
                    </asp:Label>
                    </div>

                    <div class="col-sm-3">
                     <asp:Label ID="lblLocalVendaInput" runat="server" Text="Local da Venda: ">
                         <asp:DropDownList   class="form-control input-sm"  ID="ddlLocalVendaInput" runat="server"></asp:DropDownList>
                    </asp:Label>

                 </div>
             </div>
      </div>
    </div>
      

      <div class="panel panel-default">
      <div class="panel-heading">Dados do Comprador</div>
      <div class="panel-body">
          
              <div class="row">

                <div class="col-sm-2">
                     <asp:Label ID="lblCpfCliente"  runat="server" Text="CPF: ">
                         <asp:TextBox  class="form-control input-sm" ID="txtCadCpfCliente" text=""  runat="server"></asp:TextBox>
                    </asp:Label>
                </div>
                
                <div class="col-sm-1">
                    <asp:Label ID="Label14"  runat="server" Text="Cliente: ">
                        <asp:TextBox ReadOnly="true" ID="txtCadCodigoInternoCliente"  class="form-control input-sm" runat="server"></asp:TextBox>
                        </asp:Label>
                </div>



               <div class="col-sm-6">
                     <asp:Label ID="lblClienteInput"  runat="server" Text="Cliente: ">
                         
                         <asp:TextBox ID="txtCadNomeCliente" ReadOnly="true" TextMode="Search" class="form-control input-sm" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>
                <div class="col-sm-3">
                    <asp:Label ID="Label5"  runat="server" Text="Aões: "><br /></asp:Label>
                    <asp:Button ID="Button1" class="btn btn-default"  runat="server" Text="Pesquisar" OnClick="Button1_Click1"/>
                    <asp:Button ID="Button6" class="btn btn-default"  runat="server" Text="Limpar" OnClick="Button6_Click"  />
                    
                </div>
            </div>

      </div>
  </div>
            
      
      

    <div class="panel panel-default">
      <div class="panel-heading">Itens do Pedido</div>
      <div class="panel-body">
          <!-- Busca produtos/Add produtos -->
          <div class="row">
              <%--<div class="col-md-3">
                    <label for="txtCodigo">Código</label>
                    <asp:TextBox class="form-control" ID="txtCodigoProdutoBusca" runat="server" OnTextChanged="txtCodigoProdutoBusca_TextChanged"></asp:TextBox>
                    <asp:HiddenField ID="idCodigoProdutoBusca" runat="server" />
               </div>
                <div class="col-md-3" >
                    <label for="txtDE_descricao" >Nome/Descrição</label>
                    <asp:TextBox class="form-control" Text="T.ARTE" ReadOnly="true" ID="txtDescricaoProdutoBusca" runat="server" ></asp:TextBox>
                   
                </div>--%>
                <div class="col-md-3">
                    <asp:Button ID="ButtonAdd" class="btn btn-primary btn-xs"  runat="server" Text="Adicionar Linha" OnClick="ButtonAdd_Click1" />
                    
					<asp:Button ID="BtnListarProdutos" class="btn btn-primary btn-xs"  runat="server" Text="Listar Produtos" OnClick="BtnListarProdutos_Click" AutoPostBack="false"/>
                </div>
           </div>

          <!--Fim Busca produtos/Add produtos -->

          <hr />

           <!-- Grid Itens de Pedido -->
              <div class="row">
                   <div class="col-sm-12">
                      <asp:gridview ID="Gridview1"
                           CssClass="table table-hover table-striped" BackColor="White"
                           runat="server" ShowFooter="True" AutoGenerateColumns="False" BorderStyle="None">
        <Columns>
        <asp:BoundField DataField="RowNumber" HeaderText="Item" />
        
        <asp:TemplateField HeaderText="ID">
            <ItemTemplate>
                <asp:TextBox class="form-control input-sm"   ID="txtIdProduto" runat="server" OnTextChanged="TextBox1_TextChanged"  Columns="10" AutoPostBack="true"></asp:TextBox>
            </ItemTemplate>
        	<ControlStyle  />
        </asp:TemplateField>
            <asp:TemplateField HeaderText="Produto">
            <ItemTemplate>
                <asp:TextBox class="form-control input-sm" ReadOnly="true"  ID="txtNomeProduto"  Columns="60" runat="server" ></asp:TextBox>
            </ItemTemplate>
        		<ControlStyle  />
        </asp:TemplateField>
            <asp:TemplateField HeaderText="Valor Unitário">
            <ItemTemplate>
                <asp:TextBox class="form-control input-sm" ReadOnly="true"  ID="txtValorUnitarioProduto" Columns="10"  runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

			    <asp:TemplateField HeaderText="% Desc">
            <ItemTemplate>
                <asp:TextBox class="form-control input-sm" TextMode="Number"  ID="txtPorcentagemDescontoProduto" OnTextChanged="TextBox1_TextChanged"  AutoPostBack="true" Columns="10"  runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

			<asp:TemplateField HeaderText="Vlr. Desc">
            <ItemTemplate>
                <asp:TextBox class="form-control input-sm" ReadOnly="false"  ID="txtValorDescontoProduto"  runat="server"  Columns="10"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

                <asp:TemplateField HeaderText="Quantidade">
            <ItemTemplate>
                <asp:TextBox TextMode="Number" class="form-control input-sm" Text="1"  ID="txtQuantidadeProduto"  Columns="5" OnTextChanged="TextBox1_TextChanged" AutoPostBack="true" runat="server" ></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

			<asp:TemplateField HeaderText="Vlr.Des.Tot.">
            <ItemTemplate>
                <asp:TextBox class="form-control input-sm" ReadOnly="true"  ID="txtValorDescontoTotalProduto"  runat="server"  Columns="10" OnTextChanged="txtValorDescontoTotalProduto_TextChanged"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Valor Total">
            <ItemTemplate>
                 <asp:TextBox class="form-control input-sm" ReadOnly="true "  ID="txtValorTotalProduto" Columns="10"  runat="server"></asp:TextBox>
            </ItemTemplate>
            <FooterStyle HorizontalAlign="Right" />
            <FooterTemplate>
             
                <asp:Button ID="Button2" class="btn btn-primary btn-xs"  runat="server" Text="Adicionar Linha" OnClick="ButtonAdd_Click1" />
            </FooterTemplate>
             </asp:TemplateField>
         
            <asp:TemplateField HeaderText="Ação">
            <ItemTemplate>
            <asp:Button ID="btnExcluirItem" runat="server" Text="Excluir" Columns="10"  CssClass="btn btn-danger btn-xs" />
            </ItemTemplate>

        </asp:TemplateField>
            
            
        </Columns>
</asp:gridview>
                    </div>
                  </div>
            <!-- Fim Grid Itens de Pedido -->
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">Resumo do Pedido</div>
      <div class="panel-body">
             <div class="row">
                 <div class="col-sm-1">
                     <asp:Label ID="lblValorPedidoInput" runat="server" Text="Vlr.Pedido: "></asp:Label>
                        <asp:TextBox  class="form-control input-sm" ID="txtValorPedidoInput"  ReadOnly="true" runat="server"></asp:TextBox>
                    
                 </div>

                  <div class="col-sm-1">
                      <asp:Label ID="lblValorDescontoInput" runat="server" Text="Vlr.Desconto: "></asp:Label>
                        <asp:TextBox  class="form-control input-sm" ID="txtValorDescontoInput" ReadOnly="true" runat="server" OnTextChanged="txtValorDescontoInput_TextChanged"></asp:TextBox>
                    
                 </div>
                 <div class="col-sm-2">
                     <asp:Label ID="Label10" runat="server" Text=": "> </asp:Label>
                     <asp:Button class="form-control input-sm" ID="Button5" runat="server" OnClick="Button5_Click" Text="Calcular Valores" />
                 </div>

				 <div class="col-sm-1">
                      <asp:Label ID="lblValorAcrescimo" runat="server" Text="Acréscimo: "> </asp:Label>
                        <asp:TextBox  class="form-control input-sm" ID="txtValorAcrescimoInput" runat="server" ReadOnly="false" OnTextChanged="txtValorAcrescimo_TextChanged"></asp:TextBox>
                 </div>
              
                  <div class="col-sm-2">
                      <asp:Label ID="lblValorTotalInput" runat="server" Text="Vlr.Total: "> </asp:Label>
                        <asp:TextBox  class="form-control input-sm" ID="txtValorTotalInput" runat="server" ReadOnly="true" OnTextChanged="txtValorTotalInput_TextChanged"></asp:TextBox>
                 </div>

                 <div class="col-sm-1">
                      <asp:Label ID="lblQuantidadeItensInput" runat="server" Text="Quantidade: ">
                        <asp:TextBox class="form-control input-sm" ID="txtQuantidadeItensInput"  ReadOnly="true" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>

                  <div class="col-sm-1">
                      <asp:Label ID="Label6" runat="server" Text=".">
                          <asp:CheckBoxList ID="chkGerarCR" runat="server">
                        <asp:ListItem  Value="1" Selected="True">Lançar CR</asp:ListItem>
                    </asp:CheckBoxList>
                      </asp:Label>
                  </div>

                 <div class="col-sm-1">
                      <asp:Label ID="Label20" runat="server" Text=".">
                          <asp:CheckBoxList ID="chkGerarCarne" runat="server">
                        <asp:ListItem  Value="1" Selected="True">Gerar Carnê</asp:ListItem>
                    </asp:CheckBoxList>
                      </asp:Label>
                  </div>

				  <div class="col-sm-2">
                                          <asp:Label ID="Label21" runat="server" Text="Nº Autorização / Contrato(s): "></asp:Label>
                                            <asp:TextBox  class="form-control" ID="txtNumeroContratoAutorizacao" text="0" TextMode="Number"  runat="server"></asp:TextBox>
                                           
                                      </div>

              </div>
          <hr />


          <!-- ---------------------------------------------------------------------------->
                                             <!-- Dados do Pagamento -->
                                  <div class="row">

                                      <div class="col-sm-1">
                                          <asp:Label ID="Label11" runat="server" Text="Entrada: ">
                                            <asp:TextBox class="form-control input-sm" Text="0" ID="txtValorEntradaCadastro" runat="server"></asp:TextBox>

                                        </asp:Label>
                                     </div>

                                         <div class="col-sm-1">
                                              <asp:Label ID="Label4" runat="server" Text="Saldo: "></asp:Label>
                                                <asp:TextBox class="form-control input-sm" ID="txtValorSaldoPagar" ReadOnly="false" runat="server"></asp:TextBox>
                    
                                         </div>

                                      <div class="col-sm-2">
                                         <asp:Label ID="Label13" runat="server" Text="Forma de Pagamento: ">
                                            <asp:DropDownList class="form-control input-sm" ID="ddlTipoPagamento" runat="server"></asp:DropDownList>
                                         </asp:Label>
                                     </div>

                                      <div class="col-sm-1">
                                          <asp:Label ID="Label16" runat="server" Text="Nº Parc(s): "></asp:Label>
                                            <asp:TextBox  class="form-control input-sm" ID="txtQuantidadeParcelaCadastro" text="1" TextMode="Number"  runat="server"></asp:TextBox>
                                           
                                      </div>

                                      <div class="col-sm-1">
                                          <asp:Label ID="Label17" runat="server" Text="Vlr.Parc.: ">
                                            <asp:TextBox  Text="0"  class="form-control input-sm" ReadOnly="true" ID="txtValorParcelaCadastro"  runat="server"></asp:TextBox>
                                           </asp:Label>
                                      </div>

                                       <div class="col-sm-2">
                                          <asp:Label ID="Label18" runat="server" Text="Data 1ª Parcela:">
                                            <asp:TextBox  class="form-control input-sm" ID="txtData1Parcela"  TextMode="Date"  runat="server"></asp:TextBox>
                                           </asp:Label>
                                      </div>

									  <div class="col-sm-2">
										<label for="txtDataCompra">Máquina Cartão</label>
										<asp:DropDownList class="form-control input-sm" ID="ddlMaquinaCartaoCadastro" runat="server"   ></asp:DropDownList>
									</div>
									<div class="col-sm-2">
										<label for="txtDataCompra">Bandeira Cartão</label>
										<asp:DropDownList class="form-control input-sm" ID="ddlBandeiraCartaoCadastro" runat="server"   ></asp:DropDownList>
									</div>

                                  </div>
                                  <!-- -->

                                  <div class="row">
                                        

                                         
                                      <div class="col-sm-1">
                                            <asp:Label ID="Label2" runat="server" Text=".">
                                            <asp:Button Visible="false" class="form-control input-sm" ID="btnGerarParcelas" runat="server" Text="Gerar Parcelas" OnClick="btnGerarParcelas_Click" />
                                                </asp:Label>
                                        </div>
                                  </div>

          
          
          
          <div class="row">
             <div class="col-sm-12">
               <asp:Label ID="Label3" runat="server" Text="."></asp:Label>
              <asp:GridView id="GridViewParcelas" Visible="false"
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="16" AutoGenerateColumns="False" >
          
          

        <Columns>
            <asp:BoundField DataField="RowNumber" HeaderText="Item" />
            <asp:TemplateField HeaderText="N.Parcela">
            <ItemTemplate>
                
                <asp:TextBox class="form-control"   ID="txtNumeroParcelaN" runat="server" OnTextChanged="TextBox1_TextChanged" AutoPostBack="true"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
            <asp:TemplateField HeaderText="Valor">
            <ItemTemplate>
                
                <asp:TextBox class="form-control"   ID="txtValorParcelaN" runat="server" OnTextChanged="TextBox1_TextChanged" AutoPostBack="true"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
            <asp:TemplateField HeaderText="Vencimento">
            <ItemTemplate>
                
                <asp:TextBox class="form-control"   ID="txtVencimentoParcelaN" runat="server" OnTextChanged="TextBox1_TextChanged" AutoPostBack="true"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView>

               </div>
           </div>

      </div>
    </div>
       <div class="panel panel-default">
      <div class="panel-heading">Observações</div>
      <div class="panel-body">

           <div class="row">
              <div class="col-sm-3">
                          <asp:Label ID="Label7" runat="server" Text="Data da Entrega: ">
                            <asp:TextBox  class="form-control input-sm" ID="txtDataEntregaInput" text="%# DateTime.Now.ToShortDateString() %>" TextMode="Date"  runat="server"></asp:TextBox>
               </asp:Label>
              </div>

              <div class="col-sm-3">
                      <asp:Label ID="Label8" runat="server" Text="Situação Entrega: ">
                        <asp:DropDownList   class="form-control input-sm"  ID="ddlSituacaoEntregaInput" runat="server"></asp:DropDownList>
                    </asp:Label>
                 </div>

                <div class="col-sm-6">
                    <asp:Label ID="lblMensagemEntrega" runat="server" ForeColor="#CC0000"></asp:Label>
                 </div>
           </div>

          

          <div class="row">



                <div class="col-sm-12">
                     <asp:Label ID="lblObservacaoInput" runat="server" Text="Observação: ">
                        <asp:TextBox  class="form-control input-sm" TextMode="MultiLine" ID="txtObservacaoInput" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>
                  </div>
      </div>
  </div>

       <div class="panel panel-default">
      <div class="panel-heading">Ações</div>
      <div class="panel-body">
          <div class="row">
                   <div class="col-sm-3">
                 <asp:Button ID="Button3" class="btn btn-success" runat="server" Text="Salvar" OnClick="Button1_Click" />
                <asp:Button ID="Button4" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="Button2_Click"  />
                </div>
           </div>
      </div>
  </div>

</div>


        <!-- -->

    </asp:Panel>
                 
    <asp:Panel ID="panelFiltro" runat="server" Visible="true"  CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
                
            <div class="row">
                <div class="col-sm-2">
                    <label for="txtNumPedido">Núm.Pedido</label>
                    <asp:TextBox class="form-control" ID="txtNumPedido" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtNomeCliente">Nome Cliente</label>
                    <asp:TextBox class="form-control" ID="txtNomeCliente" runat="server" ></asp:TextBox>
                   
                </div>

                <div class="col-sm-2">
                <label for="ddlFormaPagamento">Forma de Pagamento</label>
                    <asp:DropDownList ID="ddlFormaPagamento" class="form-control" runat="server" />
                 </div>

                 <div class="col-sm-3">
                     <asp:Label ID="Label12" runat="server" Text="Local da Venda: ">
                         <asp:DropDownList   class="form-control"  ID="ddlLocalVenda" runat="server"></asp:DropDownList>
                    </asp:Label>

                 </div>

                <div class="col-sm-2">
                <asp:Button ID="btnPesquisar" class="btn btn-info" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click"  />
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info" runat="server" Text="Limpar Campos" OnClick="btnLimparFiltros_Click"  />
                    </div>

                 
            </div>
       <br />
           
           

            <div class="row">
                <div class="col-sm-2">
                    <label for="txtDataCompra">Data da Compra (de)</label>
                    De <asp:TextBox class="form-control" ID="txtDataInicioCompra" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-2">
                    <label for="txtDataCompra">Data da Compra (até)</label>
                    <asp:TextBox class="form-control" ID="txtDataFimCompra" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>
				<div class="col-sm-2">
                    <label for="txtDataCompra">Máquina Cartão</label>
                    <asp:DropDownList class="form-control" ID="ddlMaquinaCartao" runat="server"   ></asp:DropDownList>
                </div>
				<div class="col-sm-2">
                    <label for="txtDataCompra">Bandeira Cartão</label>
                    <asp:DropDownList class="form-control" ID="ddlBandeiraCartao" runat="server"   ></asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label for="ddlStatusPedido">Status Pedido</label>
                    <asp:DropDownList ID="ddlStatusPedido" class="form-control" runat="server" />
                </div>
                <div class="col-sm-2">
                    <label for="ddlStatusEntrega">Status Entrega</label>
                    <asp:DropDownList ID="ddlStatusEntrega" class="form-control" runat="server" />
                </div>
            </div>
        
        
        </div>
    </div>
    

    </asp:Panel>

<!-------------------------------------------------------->
                <!-- Trigger the modal with a button -->

              <script type="text/javascript">
                  function openModalImagem() {
                      $('#myModalImagem').modal({
                          backdrop: 'static',
                          keyboard: false
                      })
				  }

				  function showListaProdutos() {

					  //$('.modal').modal();


					  $.ajax({
						  type: "GET",
						  url: "Pedido.aspx/showListaProdutosModal",
						  data: "{}",
						  contentType: "application/json; charset=utf-8",
						  datatype: "jsondata",
						  async: "true",
						  success: function (t) {
							  //set value to gridview or use normal table
							  openModalListaProdutos();
						  },
						  error: function (t) { }
					  })

				  }

                  function openModalMensagem() {
                      $('#myModalMensagem').modal('show');
                  }

                  function openModalConfirmacao() {
                      $('#myModalConfirmacao').modal('show');
                  }

                function openModal() {
                    $('#myModal').modal('show');
                }
                function openModalListaProdutos() {
                    //$('#myModalListaProdutos').modal('show');
                    $('#myModalListaProdutos').modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                }
                function closeModalListaProdutos() {
                    $('#myModalListaProdutos').modal('close');

                }
            </script>
                

            <asp:UpdatePanel ID="UpdatePanel3" runat="server" autopostback="false">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label9" runat="server" Text=""></asp:Label><br />
                    <div  class="modal fade" id="myModalListaProdutos" tabindex="-1" 
                        role="dialog" aria-labelledby="myModalLabelListaProdutos"
                        aria-hidden="true">
                        <div class="modal-dialog-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelListaProdutos">
                                        Listagem de Produtos </h4>
                                </div>
                                <div class="modal-body">

<!-- -------------------------------------------------------------------->
                                      <asp:GridView id="ProdutosGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" AutoGenerateColumns="False" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="Codigo" HeaderText="Código" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Departamento" HeaderText="Departamento" />
            <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
            <asp:BoundField DataField="Produto" HeaderText="Produto" />
            <asp:BoundField DataField="Venda" HeaderText="Venda" />
            <asp:BoundField DataField="Compra" HeaderText="Compra" />
            <asp:BoundField DataField="Minimo" HeaderText="Mínimo" />
            <asp:BoundField DataField="Quantidade" HeaderText="Quantidade" />
            <asp:BoundField DataField="Deposito" HeaderText="Depósito" />

             <asp:TemplateField>
                 <ItemTemplate>
                       <asp:Button AutoPostBack="false" data-dismiss="modal"  class="btn btn-default btn-xs" ID="btnAdicionar" runat="server"   Text="Incluir "  OnClick="btnAdicionar_Click" Visible="true" /> 
					 <asp:LinkButton ID="lnk_showInvoice" runat="server" OnClientClick="return false;">Add</asp:LinkButton>
                      </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
                   </asp:GridView>
<!-- -------------------------------------------------------------------->
                              

                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button ID="btnObtemIdProduto" Text="Salvar" class="btn btn-default"   runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
               </ContentTemplate>
</asp:UpdatePanel>


<!--INICIO MODAL - CONFIRMAÇÃO -->
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label15" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalConfirmacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabelConfirmacao"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelConfirmacao">Confirmação</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="txtModalIDConfirmacao">ID</label>
                                            <asp:TextBox ReadOnly="true" ID="txtModalIDConfirmacao" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                  <div class="row">
                                    <div class="col-sm-12">
                                        <label for="txtModalJustificatiaConfirmacao">Justificativa</label>
                                        <asp:TextBox ID="txtJustificativaCancelamento" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                      
                                  </div>

                                 <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagemModalConfirmacao" runat="server" Text=""></asp:Label>
                                    </div>
                                 </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button ID="btnConfirmarCancelamento" OnClick="btnConfirmarCancelamento_Click"  Text="Confirmar" class="btn btn-danger"   runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - CONFIRMAÇÃO -->
                
			<div class="container-full">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!-- Modal Itens Pedido - Detalhes-->
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Detalhes do Pedido  [ <asp:Label ID="lblModo" runat="server" Text=""></asp:Label> ] </h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-3">
                                        <label for="txtModalId">Id</label>
                                        <asp:TextBox ID="txtModalId" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                    </div>
                                      <div class="col-sm-3">
                                        <label for="txtModalCodigo">Código</label>
                                        <asp:TextBox ID="txtModalCodigo" runat="server" placeholder="Código" class="form-control"></asp:TextBox><br />
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="txtModalCodigo">Departamento</label>
                                        <asp:TextBox ID="txtModalDepartamento" runat="server" placeholder="Departamento" class="form-control"></asp:TextBox><br />
                                    </div>
                                  </div>

                                    <div class="row">
                                       
									
										 <asp:GridView id="ItensPedidoGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="1000" AutoGenerateColumns="False" >
											 
<Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="Produto" HeaderText="Produto" />
			<asp:BoundField DataField="ValorUnitario" HeaderText="R$.Unit." />
			<asp:BoundField DataField="ValorDescontoUnitario" HeaderText="R$.Desc.Unit." />
			<asp:BoundField DataField="Quantidade" HeaderText="Quantidade" />
			<asp:BoundField DataField="ValorDescontoTotal" HeaderText="R$.Desc.Total." />
			<asp:BoundField DataField="ValorTotal" HeaderText="R$ Total" />
            <asp:BoundField DataField="DataEntrega" HeaderText="DT.Entrega" />
 </Columns>
                   </asp:GridView>
									

                                    </div>
                                    
                                    
									 <div class="row">
                                    <div class="col-sm-3">
                                        <label for="txtObservacoesPedido">Observações</label>
                                        <asp:TextBox ID="txtObservacoesPedido" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                    </div>
                                  </div>

                                    <div class="row">
                                    <div class="col-sm-3">
                                        <label for="txtDetalhesPedido">Detalhes</label>
                                        <asp:TextBox ID="txtDetalhesPedido" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                    </div>
                                  </div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button ID="btnSalvarModal" Text="Salvar" class="btn btn-default"   runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
				</div>
<!--------------------------------------------------------->
       
               <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

                

               <asp:Panel ID="panelGrid" runat="server" Visible="true">Detalhe do Produto

                   <div class="well well-sm"><b>Total de Pedidos ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>
     
                 <h3>Lista de Pedidos</h3>
    <asp:GridView id="PedidosGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="1000" AutoGenerateColumns="False" OnSelectedIndexChanged="PedidosGridView_SelectedIndexChanged" OnDataBound="PedidosGridView_DataBound" OnRowDataBound="PedidosGridView_RowDataBound" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="Numero" HeaderText="Núm.Pedido" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="DataPedido" HeaderText="Data Pedido" />
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" />
            <asp:BoundField DataField="QtdItens" HeaderText="Qtd.Itens" />
            <%--<asp:BoundField DataField="ValorPedido" HeaderText="Valor Pedido" />--%>
            <%--<asp:BoundField DataField="ValorDesconto" HeaderText="Valor Desconto" />--%>
            <asp:BoundField DataField="ValorTotal" HeaderText="Valor Total" />
            <%--<asp:BoundField DataField="Observacao" HeaderText="Observação" />--%>
            <asp:BoundField DataField="TB_tipoPagamento" HeaderText="Forma Pag." />
            
            <asp:BoundField DataField="Vendedor" HeaderText="Vendedor" />
            <asp:BoundField DataField="StatusPedido" HeaderText="StatusPedido" />

              <asp:CommandField ShowSelectButton="false" />
            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" OnClick="ImageButton1_Click" Width="20px" />
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                 <ItemTemplate>
                       <asp:Button  class="btn btn-primary btn-xs" ID="btnDetalhar" runat="server"  Text="Detalhes" OnClick="btnDetalhar_Click" />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                
                  <ItemTemplate>
                    <asp:Button  class="btn btn-warning btn-xs" ID="btnEditar" runat="server" Text="Editar" />
                </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField>
                
                  <ItemTemplate>
                    <asp:Button  class="btn btn-danger btn-xs" ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
                   </asp:GridView>
          
          

   </asp:Panel>
          </ContentTemplate>
        </asp:UpdatePanel>
     
    

    
</asp:Content>


