﻿using AcessoBancoDados;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class MovimentoEstoque : System.Web.UI.Page
    {

        private DALConexao conexao;
        Util util = new Util();

        string connectionString = "";

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        //string sqlFornecedor = "select ID, NM_NOME from tb_fornecedor where st_situacaoregistro='A' order by nm_nome asc";

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            testCheckUserSoberana();

            btnCancelar.Attributes.Add("onClick", "javascript:history.back(); return false;");
            btnVoltar.Attributes.Add("onClick", "javascript:history.back(); return false;");

            panelBotoes.Visible = true;
            panelFiltro.Visible = true;

            connectionString = util.conexao();

            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                carregaGrid("");
            }

        }

        public void carregaGrid(String filtro)
        {

            String queryString = "select * from vw_movimentoEstoque ";



            if (!filtro.Equals(""))
            {
                queryString += filtro.ToString();
            }

            queryString += " order by id desc ";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);

            if (ds.Tables.Count > 0)
            {

                MyGridView.DataSource = ds;

                MyGridView.DataBind();

                lblTotal.Text = MyGridView.Rows.Count.ToString();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        DataSet GetData(String queryString)
        {


            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {
                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                //incluir textbox multline com o toString da ex
                //openModalMensagem();
            }

            return ds;


        }

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        protected void MyGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void MyGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the currently selected row using the SelectedRow property.
            GridViewRow row = MyGridView.SelectedRow;

            // Display the first name from the selected row.
            // In this example, the third column (index 2) contains
            // the first name.
            // lblMensagem.Text = "You selected " + row.Cells[2].Text + ".";

            //openModalFillData(Convert.ToInt32(row.Cells[0].Text));
        }

        protected void MyGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            // Get the currently selected row. Because the SelectedIndexChanging event
            // occurs before the select operation in the GridView control, the
            // SelectedRow property cannot be used. Instead, use the Rows collection
            // and the NewSelectedIndex property of the e argument passed to this 
            // event handler.
            GridViewRow row = MyGridView.Rows[e.NewSelectedIndex];

            // You can cancel the select operation by using the Cancel
            // property. For this example, if the user selects a customer with 
            // the ID "ANATR", the select operation is canceled and an error message
            // is displayed.
            if (row.Cells[1].Text == "ANATR")
            {
                e.Cancel = true;
                lblMensagem.Text = "You cannot select " + row.Cells[2].Text + ".";
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Server.Transfer("MovimentoEstoque.aspx", true);
        }
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            //exibeMVC("", "I", "1");
        }

        /*public string recuperaProximoIdNotaFiscalCompra()
        {

            string sql = "select (max(id) + 1) as ID from TB_notaFiscalEntrada";
            string proximoId = "";


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "TB_notaFiscalEntrada");

            foreach (DataRow pRow in customerOrders.Tables["TB_notaFiscalEntrada"].Rows)
            {

                proximoId = pRow["ID"].ToString();


            }


            return proximoId;

        }*/

        public void exibeMVC(string id, string modoMVC, string usuario)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;

            if (modoMVC.Equals("I"))
            {
                lblMVC.Text = "Modo de inclusão";
                btnSalvar.Visible = true;
                btnAtualizar.Visible = false;
                btnLimparCampos.Visible = true;
                btnCancelar.Visible = true;

                string proximoIdNotaFiscalCompra = "";
                //proximoIdNotaFiscalCompra = recuperaProximoIdNotaFiscalCompra();

            }
            else if (modoMVC.Equals("A"))
            {
                lblMVC.Text = "Modo de edição";
                btnSalvar.Visible = false;
                btnAtualizar.Visible = true;
                btnLimparCampos.Visible = false;
                btnCancelar.Visible = true;
            }

        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {

        }

        protected void btnAtualizar_Click(object sender, EventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btnDetalharGrid_Click(object sender, EventArgs e)
        {

        }

        protected void btnEditaGridr_Click(object sender, EventArgs e)
        {

        }
    }
}