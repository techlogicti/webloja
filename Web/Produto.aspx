﻿<%@ Page Title="Produto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Produto.aspx.cs" Inherits="Web.Produto" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

            <ContentTemplate>
      <hr />
        
    <asp:Panel ID="panelBotoes" runat="server" Visible="false">
        <div class="container-full">
            <asp:Button ID="btnListarProdutos" class="btn btn-primary" runat="server" Text="Listar Produtos" OnClick="btnListarProdutos_Click"/>
            <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Novo Produto" OnClick="btnNovo_Click"  />
         </div>
    </asp:Panel>

<!-- -------------------------------------------------------------------------------->
    <asp:Panel ID="panelCadastro" runat="server" Visible="false">

        <div class="container-full">
            <asp:Button ID="btnVoltar" class="btn btn-warning" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
  <h2>Cadastro de Produto</h2>
  <p>Identificação do Produto -   <asp:Label ID="lblModo" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label></p>

      <div class="panel-group">
        <div class="panel panel-default">
          <div class="panel-heading">Dados Básicos
              <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            </div>
              <div class="panel-body">
                   <div class="row">
                     <div class="col-md-1">
                    <asp:Label ID="lblId" runat="server" Text="ID: ">
                        <asp:TextBox class="form-control" ID="txtId" ReadOnly="true" runat="server"></asp:TextBox>
                    </asp:Label>
                    </div>
                   <div class="col-md-2">
                    <asp:Label ID="lblCodigoEAN" runat="server" Text="Código EAN: ">
                        <asp:TextBox class="form-control" ID="txtCodigoEANCadastro" runat="server"></asp:TextBox>
                    </asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblNomeProduto" runat="server" Text="Referência: ">
                            <asp:TextBox class="form-control" ID="txtNomeProdutoCadastro" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                    <div class="col-md-6">
                    <asp:Label ID="lblDescricaoProduto" runat="server" Text="Descriçao: ">
                        <asp:TextBox class="form-control" TextMode="Search" ID="txtDescricaoProdutoCadastro" runat="server"></asp:TextBox>
                    </asp:Label>
                     </div>
                    </div>
                  <!-- -->
                   <div class="row">
                       <div class="col-md-3">
                           <asp:Label ID="lblMarca" runat="server" Text="Marca: ">
                               <asp:DropDownList class="form-control" ID="ddlMarcaCadastro" runat="server"  />
                           </asp:Label>
                       </div> 
                       <div class="col-md-3">
                           <asp:Label ID="lblDepartamento" runat="server" Text="Departamento: ">
                               <asp:DropDownList class="form-control" ID="ddlDepartamentoCadastro" runat="server" />
                           </asp:Label>
                       </div> 
                       <div class="col-md-3">
                            <asp:Label ID="lblCategoria" runat="server" Text="Categoria: ">
                                <asp:DropDownList class="form-control" ID="ddlCategoriaCadastro" runat="server"  AutoPostBack="True"  OnSelectedIndexChanged="ddlCategoriaCadastro_SelectedIndexChanged" OnTextChanged="ddlCategoriaCadastro_TextChanged" />
                           </asp:Label>
                       </div> 
                       <div class="col-md-3">
                           <asp:Label ID="lblSubCategoria" runat="server" Text="SubCategoria: ">
                                <asp:DropDownList class="form-control" ID="ddlSubCategoriaCadastro" runat="server" />
                               </asp:Label>
                       </div> 
                   </div>
                   <!-- -->
                  <div class="row">
                    <div class="col-md-3">
                        <asp:Label ID="lblCodigoNCM" runat="server" Text="Código NCM: ">
                            <asp:TextBox class="form-control" ID="txtCodigoNCMCadastro" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblGrupoProduto" runat="server" Text="Grupo: ">
                            <asp:DropDownList class="form-control" ID="ddlGrupoProduto" runat="server" />
                        </asp:Label>
                    </div>
                    <div class="col-md-3">
                            <asp:Label ID="bllUnidadeVendade" runat="server" Text="Un.Venda: ">
                            <asp:DropDownList class="form-control" ID="ddlUnidadeVendaCadastro" runat="server" />
                            </asp:Label>
                    </div>
                    <div class="col-md-3">
                            <asp:Label ID="lblTipoEstoque" runat="server" Text="Tipo de Estoque">
                            <asp:DropDownList class="form-control" ID="ddlTipoEstoqueCadastro" runat="server" />
                             </asp:Label>
                    </div>
                </div>
                  <!-- -->
                <div class="row">
                   
                <div class="col-md-2">
                        <asp:Label ID="lblPrecoCompra" runat="server" Text="Vlr.Compra: ">
                            <asp:TextBox  class="form-control" ID="txtPrecoCompraCadastro" runat="server"></asp:TextBox>
                        </asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblPrecoCif" runat="server" Text="Vlr.Cif: ">
                        <asp:TextBox  class="form-control" ID="txtPrecoCifCadastro" runat="server"></asp:TextBox>
                    </asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblPrecoMinimo" runat="server" Text="Vlr.Mínimo: ">
                        <asp:TextBox  class="form-control" ID="txtPrecoMinimoCadastro" runat="server"></asp:TextBox>
                    </asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblPrecoAtacado" runat="server" Text="Vlr.Atacado: ">
                        <asp:TextBox  class="form-control" ID="txtPrecoAtacadoCadastro" runat="server"></asp:TextBox>
                    </asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblPrecoEtiqueta" runat="server" Text="Vlr.Etiqueta: ">
                        <asp:TextBox  class="form-control" ID="txtPrecoEtiquetaCadastro" runat="server"></asp:TextBox>
                    </asp:Label>
                </div>
                <div class="col-md-2">
                       <asp:Label ID="lblCEST" runat="server" Text="CEST: ">
                        <asp:TextBox  class="form-control" ID="txtCESTCadastro" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>

                    </div>
                      <!-- -->
                      <div class="row">
                          <div class="col-md-2">
                              <asp:Label ID="lblEstoqueMinimo" runat="server" Text="Estoque Mínimo: ">
                                  <asp:TextBox  class="form-control" ID="txtEstoqueMinimoCadastro" runat="server"></asp:TextBox>
                              </asp:Label>
                          </div>
                          <div class="col-md-2">
                              <asp:Label ID="lblEstoqueMaximo" runat="server" Text="Estoque Máximo: ">
                                  <asp:TextBox  class="form-control" ID="txtEstoqueMaximoCadastro" runat="server"></asp:TextBox>
                              </asp:Label>
                          </div>
                          <div class="col-md-2">
                              <asp:Label ID="lblEstoqueAtual" runat="server" Text="Estoque Atual: ">
                                  <asp:TextBox  class="form-control" ID="txtEstoqueAtualCadastro" runat="server"></asp:TextBox>
                              </asp:Label>
                          </div>
                          <div class="col-md-2">
                              <asp:Label ID="lblPis" runat="server" Text="PIS %: ">
                                  <asp:TextBox  class="form-control" ID="txtPISCadastro" runat="server"></asp:TextBox>
                              </asp:Label>
                          </div>
                          <div class="col-md-2">
                              <asp:Label ID="lblCOFINS" runat="server" Text="COFINS %: ">
                                  <asp:TextBox  class="form-control" ID="txtCOFINSCadastro" runat="server"></asp:TextBox>
                              </asp:Label>
                          </div>
                          <div class="col-md-2">
                              <asp:Label ID="lblICMS" runat="server" Text="ICMS %: ">
                                  <asp:TextBox  class="form-control" ID="txtICMSCadastro" runat="server"></asp:TextBox>
                              </asp:Label>
                          </div>

                    </div>
					
				  <div class="row">
                   
						<div class="col-md-12">
								<asp:Label ID="lblObservacoes" runat="server" Text="Observacoes:">
									<asp:TextBox  class="form-control" TextMode="MultiLine" ID="txtObservacoes" runat="server"></asp:TextBox>
								</asp:Label>
						</div>
					  </div>
                      <!-- -->
                      <div class="row">
                    </div>
                      <!-- -->
                  </div>
            <!-- -->      </div>
          <br />
              <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Ações</div>
          <div class="panel-body">
              <div class="row">
               <div class="col-sm-12">
                <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />
                <asp:Button ID="btnAtualizar" class="btn btn-primary" runat="server" Text="Atualizar" OnClick="btnAtualizar_Click" />
                <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
                <asp:Button ID="btnLimparCampos" class="btn btn-default" runat="server" Text="Limpar Campos" OnClick="btnLimparCampos_Click"/>
                <asp:Label ID="lblMensagem" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
            </div>  
            </div>
          </div>
      </div>
    </div>
            <!-- -->
          
      </div>

     </div>


        

        
    </asp:Panel>
<!-- -------------------------------------------------------------------------------->
                 <br />
    <asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-full" DefaultButton="btnPesquisar">
    <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
                

         
         

          

            <div class="row">
                <div class="col-sm-3">
                    <label for="txtCodigo">Código</label>
                    <asp:TextBox class="form-control" ID="txtCodigo" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-5">
                    <label for="txtDE_descricao">Nome/Descrição</label>
                    <asp:TextBox class="form-control" TextMode="Search" ID="txtDE_descricao" runat="server" OnTextChanged="txtDE_descricao_TextChanged"></asp:TextBox>   
                </div>

                <div class="col-sm-2">
                    <label for="chkbSaldo">Somente em estoque</label>
                    <asp:CheckBox 
            ID="chkbSaldo" 
            runat="server" 
            Text="" 
            
            AutoPostBack="true"
            Font-Names="Serif"
            Font-Size="X-Large" OnCheckedChanged="chkbSaldo_CheckedChanged"
            />
                </div>
                

                <div class="col-sm-2">
                <asp:Button ID="btnPesquisar" class="btn btn-info" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click" />
                    </div>

                 
            </div>
       <br />
           
           

            <div class="row">
                <div class="col-sm-3">
                    <label for="ddlDepartamento">Departamento</label>
                    <asp:DropDownList ID="ddlDepartamento" class="form-control" runat="server" />
                </div>
                <div class="col-sm-3">
                    <label for="ddlMarca">Marca</label>
                    <asp:DropDownList ID="ddlMarca" class="form-control" runat="server" />
                </div>
                <div class="col-sm-3">
                    <label for="ddlCategoria">Categoria</label>
                    <asp:DropDownList ID="ddlCategoria" class="form-control" runat="server" />
                </div>
                <div class="col-sm-3">
                    <label for="ddlSubCategoria">SubCategoria</label>
                    <asp:DropDownList ID="ddlSubCategoria" class="form-control" runat="server" />
                </div>
            </div>
        
        
        </div>
    </div>
    
 </asp:Panel>

<!-------------------------------------------------------->
                <!-- Trigger the modal with a button -->

              <script type="text/javascript">
    function openModal() {
        $('#myModal').modal('show');
				  }

				  function openModalImagensProduto() {
					  $('#myModalImagensProduto').modal('show');
				  }

</script>
                

                
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog-large">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Detalhes do Produto</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-3">
                                        <label for="txtModalId">Id</label>
                                        <asp:TextBox ID="txtModalId" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                    </div>
                                      <div class="col-sm-3">
                                        <label for="txtModalCodigo">Código</label>
                                        <asp:TextBox ID="txtModalCodigo" runat="server" placeholder="Código" class="form-control"></asp:TextBox><br />
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="txtModalCodigo">Departamento</label>
                                        <asp:TextBox ID="txtModalDepartamento" runat="server" placeholder="Departamento" class="form-control"></asp:TextBox><br />
                                    </div>
                                  </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalCategoria" runat="server" placeholder="Categoria" class="form-control"></asp:TextBox><br />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalSubcategoria" runat="server" placeholder="SubCategoria" class="form-control"></asp:TextBox><br />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalMarca" runat="server" placeholder="Marca" class="form-control"></asp:TextBox><br />
                                        </div>
                                     </div>
                                    
                                    <div class="row">
                                        <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalProduto" runat="server" placeholder="Produto" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoVenda" runat="server" placeholder="Preço Etiqueta" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoCompra" runat="server" placeholder="Preço Compra" class="form-control"></asp:TextBox><br />
                                        </div>
                                    </div>

                                     <div class="row">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalCif" runat="server" placeholder="Preço CIF" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoMinimo" runat="server" placeholder="Preço Mínimo" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalPrecoAtacado" runat="server" placeholder="Preço Atacado" class="form-control"></asp:TextBox><br />
                                        </div>
                                    </div>
                                    
                                     <div class="row">
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtModalQuantidade" runat="server" placeholder="Quantidade" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                             <asp:TextBox ID="txtModalAtacado" runat="server" placeholder="Atacado" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-sm-3">
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtModalObservacoes" runat="server" placeholder="Observações" class="form-control"></asp:TextBox><br />
                                        </div>
									</div>
                                    
                                    
                                   
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button Text="Salvar" class="btn btn-default" OnClick="Submit" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--------------------------------------------------------->

<!----- MODAL IMAGENS PRODUTO -->
<asp:UpdatePanel ID="UpdatePanel9" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalImagensProduto" tabindex="-1" role="dialog" aria-labelledby="myModalImagensProdutoLabel"
                        aria-hidden="true">
                        <div class="modal-dialog-large">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalImagensProdutoLabel">
                                        Imagens do Produto</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
										<div class="col-sm-3">
											<label for="txtModalIdProdutoImagens">Id</label>
												 <asp:TextBox ID="txtModalIdProdutoImagens" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
										  </div>
											<div class="col-sm-9">
										  </div>
									  </div>
									<div class="row">
                                    <div class="col-sm-12">
                                       <asp:GridView ID="GridViewImagensProduto" runat="server" AutoGenerateColumns="false" ShowHeader="false">
											<Columns>
												<asp:BoundField DataField="Text" />
												<asp:ImageField DataImageUrlField="Value" ControlStyle-Height="100" ControlStyle-Width="100" />
												<asp:TemplateField>
													<ItemTemplate>
														<asp:ImageButton ID="btnVerImagem" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" Width="20px" OnClick="btnVerImagem_Click" />
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>

										</asp:GridView>
                                    </div>
                                  </div>

									 <div class="row">
                                    <div class="col-sm-12">
                                       
                                    </div>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!----- FIM MODAL IMAGENS PRODUTO -->


               <asp:Panel ID="panelGrid" runat="server" Visible="true">
     <div class="well well-sm"><b>Total de Produtos ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

                 <h3>Lista de Produtos</h3>
    <asp:GridView id="MyGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" OnSelectedIndexChanged="MyGridView_SelectedIndexChanged"
         OnSelectedIndexChanging="MyGridView_SelectedIndexChanging" 
        AutoGenerateColumns="False" OnRowDataBound="MyGridView_RowDataBound" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="Codigo" HeaderText="Código" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Departamento" HeaderText="Departamento" Visible="false" />
            <asp:BoundField DataField="Categoria" HeaderText="Categoria" />
            <asp:BoundField DataField="SubCategoria" HeaderText="SubCategoria" />
            <asp:BoundField DataField="Produto" HeaderText="Produto" />
            <asp:BoundField DataField="Venda" HeaderText="Venda" />
            <asp:BoundField DataField="Compra" HeaderText="Compra" Visible="true" />
            <asp:BoundField DataField="Minimo" HeaderText="Mínimo"  />
            <asp:BoundField DataField="Atacado" HeaderText="Atacado"  />
            <asp:BoundField DataField="Saldo" HeaderText="Quantidade" />
            
             


            <asp:CommandField ShowSelectButton="true"  Visible="false"  />




            <asp:TemplateField>
           
                 <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnDetalhar" runat="server"  Text="Detalhes" OnClick="btnDetalhar_Click"  Visible="true"  />
                  </ItemTemplate>
           
        </asp:TemplateField>

        <asp:TemplateField>
            <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnExcluir" runat="server"  Text="Excluir" OnClick="btnExcluir_Click"  Visible="false"  />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                
                  <ItemTemplate>
                    
                      <asp:Button  class="btn btn-default btn-xs" ID="btnEditar" runat="server"  Text="Editar" OnClick="btnEditar_Click"  Visible="true"  />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField></asp:TemplateField>


			

        </Columns>
                   </asp:GridView>
          
          

   </asp:Panel>
          </ContentTemplate>
        </asp:UpdatePanel>
     
    

    
</asp:Content>

