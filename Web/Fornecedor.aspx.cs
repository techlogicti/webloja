﻿using ObjetoTransferencia;
using Negocios;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AcessoBancoDados;

namespace Web
{
    public partial class Fornecedor : System.Web.UI.Page
    {

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        private DALConexao conexao;
        Util util = new Util();
        BLLOperador bllOperador = null;

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            testCheckUserSoberana();
            connectionString = util.conexao();
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {


                panelBotoes.Visible = true;


                // Declare the query string.
                //String queryString = "select [IDProduto] as 'Id', [CODIGO] AS 'Código', [NM_nome] AS 'Produto', [DE_descricao] as 'Descrição', [precoVenda] as 'Etiqueta' , [precoCompra] as 'PC', [precoCompraImposto] as 'PCIF', [precoMinimo] as 'P.Mín', [precoAtacado] as 'P.Atacado', [estoqueFinal] as 'Qtde'  from [SoberanaLocal].[dbo].[TB_produto]";

                String queryString = " select [IDFornecedor] as ID, [NU_cpfCnpj] as CpfCnpj, [DE_razaoSocial] as RazaoSocial, [DE_nomeFantasia] as NomeFantasia ";
                queryString = queryString + "  from [TB_fornecedor] where 1=1  order by DE_nomeFantasia asc ";

                // Run the query and bind the resulting DataSet
                // to the GridView control.
                DataSet ds = GetData(queryString);
                if (ds.Tables.Count > 0)
                {
                    FornecedorGridView.DataSource = ds;

                    FornecedorGridView.DataBind();

                    lblTotal.Text = FornecedorGridView.Rows.Count.ToString();
                }
                else
                {
                    lblMensagem.Text = "Não foi possível conectar a base de dados.";
                    openModalMensagem();
                }
            }

        }


        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        public void atualizaGrid(string filtro)
        {

            // Declare the query string.

            String queryString = " select [IDFornecedor] as ID, [NU_cpfCnpj] as CpfCnpj, [DE_razaoSocial] as RazaoSocial, [DE_nomeFantasia] as NomeFantasia "; 
            queryString = queryString + "  from [dbo].[TB_fornecedor] where 1=1  ";



            if (!filtro.Equals(""))
            {
                queryString = queryString + " and NU_cpfCnpj like '%" + filtro.ToString() + "%'";
                queryString = queryString + " or DE_razaoSocial like '%" + filtro.ToString() + "%'";
                queryString = queryString + " or DE_nomeFantasia like '%" + filtro.ToString() + "%'";
                queryString = queryString + " or IDFornecedor like '%" + filtro.ToString() + "%'";
            }

            queryString = queryString + " order by [DE_nomeFantasia] asc";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                FornecedorGridView.DataSource = ds;
                FornecedorGridView.DataBind();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

            return ds;


        }

        protected void btnLimparFiltros_Click(object sender, EventArgs e)
        {
            txtDE_descricao.Text = "";
        }

        protected void btnListarFornecedores_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
        }

        protected void pesquisar()
        {
            string strDE_descricao = "";

            if (!txtDE_descricao.Text.Trim().Equals(""))
            {
                strDE_descricao = txtDE_descricao.Text.Trim().ToString();
                atualizaGrid(strDE_descricao);
            }
        }

        protected void txtDE_descricao_TextChanged(object sender, EventArgs e)
        {
            pesquisar();
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisar();


        }

        protected void btnNovoFornecedor_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;

            limpaCamposTela();
        }

        public void limpaCamposTela()
        {

            txtCpfCnpjInput.Text = "";
            txtNomeFantasiaInput.Text = "";
            txtRazaoSocialInput.Text = "";
           

        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
            panelBotoes.Visible = true;
        }

        public void gravarLog(String texto)
        {
            // Compose a string that consists of three lines.
            string lines = texto.ToString().Trim();

            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\temp\\Soberana.txt");
            file.WriteLine(lines);

            file.Close();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {

            //
            int idOperador = 0;
           


            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            bllOperador = new BLLOperador(conexao);
            ModeloOperador modeloOperador = new ModeloOperador();
            if (!string.IsNullOrEmpty(Session["user"] as string))
            {
                modeloOperador = bllOperador.recuperaIdLogin(Session["user"].ToString());
                idOperador = Convert.ToInt32(modeloOperador.Id);
            }


            //

            //recupera valores tela

            string iCpfCnpj, iRazaoSocial, iNomeFantasia, iTipoPessoa;

            iCpfCnpj = txtCpfCnpjInput.Text.ToString();
            iTipoPessoa = radioButtonListTipoPessoa.SelectedValue.ToString();
            iRazaoSocial = txtRazaoSocialInput.Text.ToString();
            iNomeFantasia = txtNomeFantasiaInput.Text.Replace("-", "");
            //valida dados
            string sqlInsertPedido = "";
            bool formOk = false;
            int count = 0;

            if (!iCpfCnpj.Equals("")) count++;
            if (!iRazaoSocial.Equals("")) count++;
            if (!iNomeFantasia.Equals("")) count++;
           

            if (count == 3) { formOk = true; } else { formOk = false; }


            if (formOk)
            {

                ModeloFornecedor modelo = new ModeloFornecedor();
                modelo.RazaoSocial = iRazaoSocial.ToString();
                modelo.NomeFantasia = iNomeFantasia.ToString();
                modelo.TipoPessoa = iTipoPessoa.ToString();
                modelo.CpfCnpj = iCpfCnpj.ToString();
                modelo.CD_codOperador = Convert.ToInt32(idOperador.ToString());

                try
                {

                    int id = 0;
                    conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                    BLLFornecedor bll = new BLLFornecedor(conexao);
                    id = bll.Incluir(modelo);
                    this.gravarLog("Execução do sql: " + sqlInsertPedido.ToString());

                    if(id > 0)
                    {
                        lblMensagem.Text = "Fornecedor Inserido com sucesso ! [ ID: " + id.ToString() + " ] ";
                        openModalMensagem();
                        limpaCamposTela();
                    }
                    else
                    {
                        lblMensagem.Text = "Verificar log de inclusão !";
                        openModalMensagem();
                    }

                    
                    
                }
                catch (Exception ex)
                {
                    //ex.ToString();
                    lblMensagem.Text = "Erro na gravação";
                    openModalMensagem();
                    this.gravarLog(ex.ToString());
                }
                

                

            }
            else
            {
                lblMensagem.Text = "Preencha todos os campos antes de salvar os dados ";
                openModalMensagem();
            }









        }
    }
}