﻿<%@ Page Title="Cliente" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cliente.aspx.cs" Inherits="Web.Cliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<hr />
    
     <script type="text/javascript">
                  function openModalMensagem() {
                      $('#myModalMensagem').modal('show');
		 }
		 function openModalDocumentosDigitalizados() {
					  $('#myModalDocumentosDigitalizados').modal('show');
				  }
      </script>

      <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

    <asp:Panel ID="panelBotoes" runat="server" Visible="false">
        <div class="container-full">
            <asp:Button ID="btnListarClientes" class="btn btn-primary" runat="server" Text="Listar Clientes" OnClick="btnListarClientes_Click"/>
            <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Novo Cliente" OnClick="btnNovo_Click"/>
         </div>
    </asp:Panel>

    <hr />

    <asp:Panel ID="panelAlertaCliente" runat="server" Visible="false">
        <div class="alert alert-danger" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span class="sr-only">Atenção:</span>
                 <asp:Label ID="lblMensagemAlertaCliente" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
            </div>
   </asp:Panel>
    
    
    <asp:Panel ID="panelCadastro" runat="server" Visible="false">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
        <div class="container-full">
            <asp:Button ID="btnVoltar" class="btn btn-warning" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
  <h2>Cliente</h2>
  <p>Identificação do Cliente -   <asp:Label ID="lblModo" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label></p>



  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Dados Básicos
          <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" Visible="false" />
        </div>
          <div class="panel-body">



              <!-- Linha 1-->
               <div class="row">
                   <div class="col-md-1">
                         <asp:Label ID="lblId" runat="server" Text="ID: ">
                            <asp:TextBox  class="form-control" ID="txtId" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                    <div class="col-md-2">
                         <asp:Label ID="lblCodFinancial" runat="server" Text="Cod.Finan: ">
                            <asp:TextBox  class="form-control" ID="txtCodFinancial" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-3">
                         <asp:Label ID="lblNomeCliente" runat="server" Text="Nome: ">
                            <asp:TextBox  class="form-control" ID="txtNomeCliente" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-3">
                         <asp:Label ID="lblApelido" runat="server" Text="Apelido: ">
                            <asp:TextBox  class="form-control" ID="txtApelido" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   <div class="col-md-3">
                         <asp:Label ID="lblDataNascimento" runat="server" Text="Data de Nascimento: ">
                             </asp:Label>
                            <asp:TextBox TextMode="Date" class="form-control" ID="txtDataNascimento" runat="server"></asp:TextBox>
                        
                    </div>
                </div>
             <!-- -->

             <!-- Linha 2-->
               <div class="row">
                    
                   <div class="col-md-3">
                         <asp:Label ID="lblNacionalidade" runat="server" Text="Nacionalidade: ">
                            <asp:TextBox class="form-control" ID="txtNacionalidade" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   <div class="col-md-3">
                         <asp:Label ID="lblEstadoCivil" runat="server" Text="Estado Civil: ">
                            <asp:DropDownList   class="form-control"  ID="ddlEstadoCivil" runat="server"></asp:DropDownList>
                             
                        </asp:Label>
                    </div>
                   <div class="col-md-3">
                         <asp:Label ID="lblNomeConjuge" runat="server" Text="Nome do Cônjuge: ">
                            <asp:TextBox  class="form-control" ID="txtNomeConjuge" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-3">
                         <asp:Label ID="lblProfissaoConjuge" runat="server" Text="Profissão do Cônjuge: ">
                            <asp:DropDownList   class="form-control"  ID="ddlProfissaoConjuge" runat="server" Visible="false"></asp:DropDownList>
                             <asp:TextBox  class="form-control" ID="txtDe_profissaoConjuge" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                </div>
             <!-- -->
             <!-- Linha 3-->
                <div class="row">

                    <div class="col-md-3">
                         <asp:Label ID="lblNaturalidade" runat="server" Text="Naturalidade: ">
                            <asp:TextBox  class="form-control" ID="txtNaturalidade" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                    <div class="col-md-3">
                         <asp:Label ID="lblNomeMae" runat="server" Text="Nome da Mãe: ">
                            <asp:TextBox  class="form-control" ID="txtNomeMae" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                    <div class="col-md-3">
                         <asp:Label ID="lblNomePai" runat="server" Text="Nome do Pai: ">
                            <asp:TextBox  class="form-control" ID="txtNomePai" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                </div>
             <!-- -->

               <!-- Linha da Mensagem de validação-->
                <div class="row">
                    <asp:Label ID="lblMensagemDadosBasicos" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                </div>
              <!-- -->

          </div>
      </div>
    </div>
            <hr />
<!---ENDEREÇO INICIO -------------------------------------------------->
<div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Endereço</div>
          <div class="panel-body">
            <div class="row">
                    <div class="col-md-3">
                         <asp:Label ID="lblPaisInput" runat="server" Text="Pais: ">
                              <asp:DropDownList   class="form-control"  ID="ddlPaisInput" runat="server"></asp:DropDownList>
                            
                        </asp:Label>
                    </div>
                    <div class="col-md-3">
                         <asp:Label ID="lblUfInput" runat="server" Text="Estado: ">
                             </asp:Label>
                             <asp:DropDownList   class="form-control"  ID="ddlUfClienteInput" runat="server" 
                                 AutoPostBack="True" 
                                 OnSelectedIndexChanged="txtDE_descricao_TextChanged" 
                                 OnTextChanged="ddlUfClienteInput_TextChanged"></asp:DropDownList>
                        
                    </div>
                <div class="col-md-6">
                         <asp:Label ID="lblCidadeInput" runat="server" Text="Cidade: ">
                            <asp:DropDownList   class="form-control"  ID="ddlCidadeInput" runat="server"></asp:DropDownList>
                        </asp:Label>
                </div>
          </div>

           <div class="row">
                    <div class="col-md-3">
                         <asp:Label ID="lblBairroInput" runat="server" Text="Bairro: ">
                            <asp:TextBox  class="form-control" ID="txtBairroInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                    <div class="col-md-3">
                         <asp:Label ID="lblEnderecoInput" runat="server" Text="Rua/Avenida: ">
                            <asp:TextBox  class="form-control" ID="txtEnderecoInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                <div class="col-md-3">
                         <asp:Label ID="lblNumeroInput" runat="server" Text="Nº: ">
                            <asp:TextBox  class="form-control" ID="txtNumeroInput" runat="server"></asp:TextBox>
                        </asp:Label>
                </div>
          </div>

        <div class="row">
                    <div class="col-md-3">
                         <asp:Label ID="lblCepInput" runat="server" Text="CEP: "></asp:Label>
                            <asp:TextBox  class="form-control" ID="txtCepInput" runat="server"></asp:TextBox>
                             <asp:Button ID="btnBuscarCep" class="btn btn-success" runat="server" Text="Buscar endereço pelo CEP" OnClick="btnBuscarCep_Click" />
                        
                    </div>
                    <div class="col-md-3">
                         <asp:Label ID="lblComplementoInput" runat="server" Text="Complemento: ">
                            <asp:TextBox  class="form-control" ID="txtComplementoInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                <div class="col-md-6">
                         <asp:Label ID="lblReferenciaInput" runat="server" Text="Ponto de referência: ">
                            <asp:TextBox  class="form-control" ID="txtReferenciaInput" runat="server"></asp:TextBox>
                        </asp:Label>
                </div>
          </div>

              <!-- Linha da Mensagem de validação-->
                <div class="row">
                    <asp:Label ID="lblMensagemEndereco" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                </div>
              <!-- -->

    </div>
</div>
<!---ENDEREÇO FIM ----------------------------------------------------->

            <hr />
            <!-- DOCUMENTOS -->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Documentos</div>
          <div class="panel-body">

              <!--Documentos Pessoa Física -->
 <div class="row">
                    <div class="col-md-2">
                         <asp:Label ID="lblCpf" runat="server" Text="CPF: ">
                            <asp:TextBox  class="form-control" ID="txtCpfInput" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-2">
                         <asp:Label ID="lblNumeroIdentidade" runat="server" Text="Nº Identidade: ">
                            <asp:TextBox  class="form-control" ID="txtNumeroIdentidade" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                    <div class="col-md-2">
                         <asp:Label ID="lblDataEmissaoIdentidade" runat="server" Text="Data Emissão RG: ">
                            <asp:TextBox  TextMode="Date" class="form-control" ID="txtDataEmissaoIdentidade" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   <div class="col-md-2">
                         <asp:Label ID="lblOrgaoEmissorIdentidade" runat="server" Text="Orgão Emissor RG: ">
                            <asp:TextBox  class="form-control"   ID="txtOrgaoEmissorIdentidade" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                    
                    <div class="col-md-2">
                         <asp:Label ID="lblUfEmissorIdentidade" runat="server" Text="UF Emissor RG: ">
                             <asp:DropDownList   class="form-control"  ID="ddlUfEmissorIdentidade" runat="server"></asp:DropDownList>
                              </asp:Label>
                    </div>
              

</div>

 <div class="row">
                    <div class="col-md-2">
                         <asp:Label ID="lblNumeroCNH" runat="server" Text="Nº CNH: ">
                            <asp:TextBox  class="form-control" ID="txtNumeroCNH" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-2">
                         <asp:Label ID="lblCategoriaCNH" runat="server" Text="Categoria CNH">
                             <asp:DropDownList   class="form-control"  ID="ddlCategoriaCNH" runat="server"></asp:DropDownList>
                        </asp:Label>
                    </div>
                    <div class="col-md-2">
                         <asp:Label ID="lblDataEmissaoCNH" runat="server" Text="Data Emissão CNH: ">
                            <asp:TextBox  TextMode="Date" class="form-control" ID="txtDataEmissaoCNH" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   <div class="col-md-2">
                         <asp:Label ID="lblUFEmissorCNH" runat="server" Text="UF Emissor CNH: "> 
                             <asp:DropDownList   class="form-control"  ID="ddlUfEmissorCNH" runat="server"></asp:DropDownList>
                              </asp:Label>
                    </div>

                  
</div>
<!-- -->
              <!-- Linha da Mensagem de validação-->
                <div class="row">
                    <asp:Label ID="lblMensagemDocumentos" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                </div>
              <!-- -->

          </div>
      </div>
    </div>
            <hr />
<!-- CONTATOS-->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Contatos</div>
          <div class="panel-body">

               <div class="row">
                    <div class="col-md-2">
                         <asp:Label ID="lblCelular1" runat="server" Text="Celular 1: ">
                            <asp:TextBox  class="form-control" ID="txtCelular1" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-2">
                         <asp:Label ID="lblCelular2" runat="server" Text="Celular 2: ">
                            <asp:TextBox  class="form-control" ID="txtCelular2" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-2">
                         <asp:Label ID="lblCelular3" runat="server" Text="Celular 3: ">
                            <asp:TextBox  class="form-control" ID="txtCelular3" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   <div class="col-md-2">
                         <asp:Label ID="lblCelular4" runat="server" Text="Celular 4: ">
                            <asp:TextBox  class="form-control"   ID="txtCelular4" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   <div class="col-md-4">
                         <asp:Label ID="lblEmail" runat="server" Text="Email: ">
                            <asp:TextBox  class="form-control"  ID="txtEmail" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   </div>

              <div class="row">
                    <div class="col-md-2">
                         <asp:Label ID="lblTelefoneFixo" runat="server" Text="Telefone Fixo: ">
                            <asp:TextBox  class="form-control" ID="txtTelefoneFixo" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                   <div class="col-md-2">
                         <asp:Label ID="lblTelRecado" runat="server" Text="Tel/Cel. Recado: ">
                            <asp:TextBox  class="form-control" ID="txtTelCelRecado" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
                  

                   <div class="col-md-2">
                         <asp:Label ID="lblParentescoContato" runat="server" Text="Parantesco Contato: ">
                            <asp:TextBox  class="form-control"  ID="txtParentescoContato" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   <div class="col-md-4">
                         <asp:Label ID="lblNomeContatoRecado" runat="server" Text="Nome Contato Recado: ">
                            <asp:TextBox  class="form-control" ID="txtNomeContatoRecado" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>

                   </div>

                
               <!-- Linha da Mensagem de validação-->
                <div class="row">
                    <asp:Label ID="lblMensagemContatos" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                </div>
              <!-- -->

              </div>
          </div>
      </div>
            <hr />
<!-- TRABALHO -->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Dados Trabalhistas</div>
          <div class="panel-body">



              <div class="row">

               <div class="col-md-3">
                <asp:Label ID="lblProfissao" runat="server" Text="Profissão: ">
                    <asp:DropDownList Visible="false"  class="form-control"  ID="ddlProfissaoCliente" runat="server"></asp:DropDownList>
                    <asp:TextBox  class="form-control" ID="txtDE_profissaoCliente" runat="server"></asp:TextBox>
                </asp:Label>
              </div>

                  <div class="col-md-2">
                <asp:Label ID="lblTipoAtividade" runat="server" Text="Tipo de Atividade: ">
                    <asp:DropDownList   class="form-control"  ID="ddlTipoAtividade" runat="server"></asp:DropDownList>
                </asp:Label>
              </div>

                  <div class="col-md-2">
                <asp:Label ID="lblDataAdmissao" runat="server" Text="Data de Admissão: ">
                    <asp:TextBox    class="form-control" ID="txtDataAdmissao" runat="server" TextMode="Date"></asp:TextBox>
                </asp:Label>
              </div>

               <div class="col-md-2">
                <asp:Label ID="lblSalario" runat="server" Text="Salário: ">
                    <asp:TextBox  class="form-control" ID="txtSalario" runat="server"></asp:TextBox>
                </asp:Label>
              </div>

                  
               <div class="col-md-3">
                <asp:Label ID="lblTelefoneComercial" runat="server" Text="Telefone Comercial: ">
                    <asp:TextBox  class="form-control" ID="txtTelefoneComercial" runat="server" TextMode="Phone"></asp:TextBox>
                </asp:Label>
              </div>

              </div>

                   <div class="row">

                       <div class="col-md-8">
                        <asp:Label ID="lblEmpresaTrabalha" runat="server" Text="Empresa: ">
                            <asp:TextBox  class="form-control" ID="txtEmpresaTrabalha" runat="server"></asp:TextBox>
                        </asp:Label>
                      </div>

                       <div class="col-md-8">
                        <asp:Label ID="lblNomeResponsavel" runat="server" Text="Responsável: ">
                            <asp:TextBox  class="form-control" ID="txtNomeResponsavel" runat="server"></asp:TextBox>
                        </asp:Label>
                      </div>

                   </div>

              <!-- Linha da Mensagem de validação-->
                <div class="row">
                    <asp:Label ID="lblMensagemDadosTrabahistas" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
                </div>
              <!-- -->

          </div>
      </div>
    </div>
<hr />
<!-- OBSERVAÇÕES -->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Observações</div>
          <div class="panel-body">
              <asp:TextBox  class="form-control" ID="txtOutros" runat="server" Height="113px" TextMode="MultiLine" Width="624px"></asp:TextBox>
          </div>
      </div>
    </div>
<hr />
    <!-- AUTORIZAÇÕES -->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Autorizações</div>
          <div class="panel-body">
               <div class="row">
               <div class="col-md-8">
                        <asp:Label ID="Label1" runat="server" Text="Formas de Pagamento Autorizadas: "></asp:Label>
                            <asp:CheckBoxList ID="chkBoxListFormasPagamento" runat="server">
<asp:ListItem Value="9">Aguardando Pagamento</asp:ListItem>                               
<asp:ListItem Value="11">Boleto</asp:ListItem>                               
<asp:ListItem Value="6">Carnê Soberana</asp:ListItem>
<asp:ListItem Value="3">Cartao de Crédito</asp:ListItem>
<asp:ListItem  Value="2">Cartão de Débito</asp:ListItem>
<asp:ListItem  Value="4">Cartão CDC</asp:ListItem>
<asp:ListItem Value="10">Cheque</asp:ListItem>
<asp:ListItem Value="1">Dinheiro</asp:ListItem>
<asp:ListItem  Value="5">Financeiras</asp:ListItem>
<asp:ListItem Value="7">Receber na entrega</asp:ListItem>
<asp:ListItem Value="8">Outra</asp:ListItem>
                        </asp:CheckBoxList>
                        
               </div>
               </div>
          </div>
      </div>
    </div>
<hr />
<!-- HISTÓRICO DE COMPRAS-->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Histórico de Compras</div>
          <div class="panel-body">

              
                  <asp:GridView id="gridViewHistoricoCompras" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="50" AutoGenerateColumns="False">

                   <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="NumeroPedido" HeaderText="Pedido" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" />
            <asp:BoundField DataField="dtcompra" HeaderText="DataCompra" />
            <asp:BoundField DataField="formapagamento" HeaderText="F.Pagamento" />
            <asp:BoundField DataField="ValorCompra" HeaderText="Valor Compra" />
            <asp:BoundField DataField="Situacao" HeaderText="Situação" />
                       <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" OnClick="ImageButton1_Click" Width="20px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowSelectButton="false" />
        </Columns>
              </asp:GridView>


          </div>
      </div>
    </div>
<hr />
    <!-- HISTÓRICO FINANCEIRO-->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Histórico Financeiro</div>
          <div class="panel-body">

              
                  <asp:GridView id="gridViewHistoricoFinanceiro" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="50" AutoGenerateColumns="False">

                   <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="NumeroPedido" HeaderText="Pedido" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" />
            <asp:BoundField DataField="DataVencimento" HeaderText="DT.Vencimento" />
            <asp:BoundField DataField="ValorParcela" HeaderText="Vlr.Parcela" />
            <asp:BoundField DataField="NumeroParcela" HeaderText="Nº" />
            <asp:BoundField DataField="QtdParcelas" HeaderText="Qtd" />
            <asp:BoundField DataField="DescricaoFormaPagamento" HeaderText="F.Pagamento" />
            <asp:BoundField DataField="StatusPagamento" HeaderText="Situação" />
                       <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" OnClick="ImageButton1_Click" Width="20px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowSelectButton="false" />
        </Columns>
              </asp:GridView>


          </div>
      </div>
    </div>
<hr />

<!-- DOCUMENTOS DIGITALIZADOS -->
<div class="panel-group">
  <div class="panel panel-default">
   <div class="panel-heading">Documentos Digitalizados</div>
      <div class="panel-body">
		    <div class="modal fade" id="myModalImagensProduto" tabindex="-1" role="dialog" aria-labelledby="myModalImagensProdutoLabel"
                        aria-hidden="true">
                        <div class="modal-dialog-large">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalImagensProdutoLabel">
                                        Imagens do Produto</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
										<div class="col-sm-3">
											<label for="txtModalIdProdutoImagens">Id</label>
												 <asp:TextBox ID="txtModalIdProdutoImagens" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
										  </div>
											<div class="col-sm-9">
										  </div>
									  </div>
									<div class="row">
                                    <div class="col-sm-12">
                                       <asp:GridView ID="GridViewDocumentosDigitalizados"  AllowSorting="True" AllowPaging="True" GridLines="None" CssClass="table table-hover table-striped" BackColor="White" Runat="Server" PageSize="50" AutoGenerateColumns="False">
											<Columns>
												<asp:BoundField DataField="Text" />
												<asp:ImageField DataImageUrlField="Value" ControlStyle-Height="100" ControlStyle-Width="100" />
												<asp:TemplateField>
													<ItemTemplate>
														<asp:ImageButton ID="btnVerImagem" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" Width="20px"  />
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>

										</asp:GridView>
                                    </div>
                                  </div>

									 <div class="row">
                                    <div class="col-sm-12">
                                       
                                    </div>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
      </div>
   </div>
</div>
<hr />

<!-- REISTRO SPC-->
 <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Registro no SPC</div>
          <div class="panel-body">
			  <div class="row">
                    <div class="col-md-3">
                         <asp:Label ID="lblDataRegistroSPC" runat="server" Text="Data do Registro: ">
                            <asp:TextBox TextMode="DateTime"  class="form-control" ID="txtDataRegistroSPC" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
					 <div class="col-md-9">
                         <asp:Label ID="lblMotivoRegistroSPC" runat="server" Text="Motivo do Registro: ">
                            <asp:TextBox  class="form-control" ID="txtMotivoRegistroSPC" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
				</div>

			  <div class="row">
				   <div class="col-md-9">
                         <asp:Label ID="lblObservacaoRegistroSPC" runat="server" Text="Observações: ">
                            <asp:TextBox  TextMode="MultiLine" class="form-control" ID="txtObservacaoRegistroSPC" runat="server"></asp:TextBox>
                        </asp:Label>
                    </div>
			  </div>

		  </div>
	  </div>
</div>
<!-- -->


   <!-- AÇÔES-->
            <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading">Ações</div>
          <div class="panel-body">
              <div class="row">
               <div class="col-sm-12">
                <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click"/>
                <asp:Button ID="btnAtualizar" class="btn btn-primary" runat="server" Text="Atualizar" OnClick="btnAtualizar_Click" />
                <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"/>
                <asp:Button ID="btnLimparCampos" class="btn btn-default" runat="server" Text="Limpar Campos" OnClick="btnLimparCampos_Click"/>
                
            </div>  
            </div>
          </div>
      </div>
    </div>


            <!-- -->

  </div>
                </ContentTemplate>
    </asp:UpdatePanel> 
    </asp:Panel>
    
   
    <asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">

        <div class="row">
             <div class="col-md-10">
                 <label for="txtDE_descricao">Código/Nome/Cpf/Celular:
                      <div class="input-group">
                 <asp:TextBox ID="txtDE_descricao" TextMode="Search" class="form-control" runat="server" OnTextChanged="txtDE_descricao_TextChanged" Wrap="True"></asp:TextBox>
                     <span class="input-group-btn">
                         <asp:Button ID="btnPesquisar" class="btn btn-default" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click" />
                      </span>
                          </div>
                 </label>
             </div>
            <div class="col-md-2">
                 <label for="txtDE_descricao">Ordenar por:
                <asp:DropDownList class="form-control" ID="ddlOrdemPesquisaCliente" runat="server"></asp:DropDownList>
                     </label>
            </div>
        </div>

        </div>
        </div>
    </asp:Panel>

     
   

    

               <asp:Panel ID="panelGrid" runat="server" Visible="false">
    <div class="well well-sm"><b>Total de Clientes ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div> 

           <h3>Lista de Clientes</h3>

          <asp:GridView id="GridViewClientes" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" 
          AutoGenerateColumns="False" OnRowDataBound="GridViewClientes_RowDataBound" >
         

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="Codigo" HeaderText="Código" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Nome" HeaderText="Nome" />
            <asp:BoundField DataField="Apelido" HeaderText="Apelido" />
            <asp:BoundField DataField="Documento" HeaderText="Cpf/Cnpj" />
            <asp:BoundField DataField="Fixo" HeaderText="Fixo" />
            <asp:BoundField DataField="Celular" HeaderText="Celular" />
            <asp:BoundField DataField="Situacao" HeaderText="Situação" />
            <asp:BoundField DataField="contemParcelamentoCarne" HeaderText="Carnê" />
			<asp:BoundField DataField="listaSpc" HeaderText="SPC" />
            

                        <asp:CommandField ShowSelectButton="false" />
            
            <asp:TemplateField>
                 <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnDetalhar" runat="server" Text="Detalhes" OnClick="btnDetalhar_Click" />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                
                  <ItemTemplate>
                    
                    <asp:Button  class="btn btn-default btn-xs" ID="btnEditar" runat="server"  Text="Editar" OnClick="btnEditar_Click" />
                </ItemTemplate>
            </asp:TemplateField> 
            
        </Columns>
                   </asp:GridView>

   </asp:Panel>

</asp:Content>
