﻿using AcessoBancoDados;
using Negocios;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class ContasAPagar : System.Web.UI.Page
    {

        private DALConexao conexao;
        BLLOperador bllOperador = null;
        Util util = new Util();

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        string sqlUsuarios = "select IDFuncionario as ID, NM_nomeCompleto as NM_nome from TB_funcionario where st_situacaoregistro='A' order by NM_nome asc";
        string sqlTipoPagamento = "select ID as ID, NM_nome as NM_nome from tb_tipoPagamento where st_situacaoregistro='A' order by NM_nome asc";
        string sqlCliente = "select ID as ID, NM_nomeCompleto as NM_nome from tb_cliente where st_situacaoregistro='A' order by NM_nomeCompleto asc";
        string sqlHistoricoFinanceiro = "select ID as ID, NM_nome as NM_nome from tb_historicoFinanceiro where st_situacaoregistro='A' order by NM_nome asc"; 

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            testCheckUserSoberana();
            util = new Util();
            connectionString = util.conexao();
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                panelBotoes.Visible = true;

                carregaGrid("");
                
                util.preencheListBox(listFormaPagamento, sqlTipoPagamento, "S");

                //foreach (ListItem li in listFormaPagamento.Items)
                    //li.Selected = true;

                //util.preencheComboStatusPagamento(ddlStatusPagamento);

                //util.preencheListStatusPagamento(listStatusPagamento);




            }
        }
        protected void btnNovo_Click(object sender, EventArgs e)
        {
            
        }

        public string recuperaFiltro()
        {
            string filtro = "";

            if (!txtNumeroNotaFiscal.Text.Trim().ToString().Equals("")) filtro += " and NumeroNotaFiscalEntrada like '%" + txtNumeroNotaFiscal.Text.Trim().ToString() + "%' ";

            if (!txtNomeFornecedor.Text.Trim().ToString().Equals("")) filtro += " and Fornecedor like '%" + txtNomeFornecedor.Text.Trim().ToString() + "%' ";

            //DATA DE EMISSAO
            if (!txtDataInicioEmissao.Text.Trim().ToString().Equals("")) { 
                int dataInicioEmissao = 0;
                int dataFimEmissao = 0;
                dataInicioEmissao = Convert.ToInt32(txtDataInicioEmissao.Text.Trim().ToString().Replace("-", ""));
                dataFimEmissao = Convert.ToInt32(txtDataFimEmissao.Text.Trim().ToString().Replace("-", ""));
                filtro += "and DataEmissaoUSA between " + dataInicioEmissao + " and " + dataFimEmissao;
            }

            //DATA DE VENCIMENTO
            if (!txtDataInicioVencimento.Text.Trim().ToString().Equals(""))
            {
                int dataInicioVencimento = 0;
                int dataFimVencimento = 0;
                dataInicioVencimento = Convert.ToInt32(txtDataInicioVencimento.Text.Trim().ToString().Replace("-", ""));
                dataFimVencimento = Convert.ToInt32(txtDataFimVencimento.Text.Trim().ToString().Replace("-", ""));
                filtro += "and DataVencimentoUSA between " + dataInicioVencimento + " and " + dataFimVencimento;
            }
            //DATA DE PAGAMENTO
            if (!txtDataInicioPagamento.Text.Trim().ToString().Equals(""))
            {
                int dataInicioPagamento = 0;
                int dataFimPagamento = 0;
                dataInicioPagamento = Convert.ToInt32(txtDataInicioPagamento.Text.Trim().ToString().Replace("-", ""));
                dataFimPagamento = Convert.ToInt32(txtDataFimPagamento.Text.Trim().ToString().Replace("-", ""));
                filtro += "and DataVencimentoUSA between " + dataInicioPagamento + " and " + dataFimPagamento;
            }
            //

            return filtro;
        }

        public void carregaGrid(String filtro)
        {
            String sql = "select * from VW_contasAPagar";

            sql += " where 1=1 ";

            if (!filtro.Equals(""))
            {
                sql += filtro.ToString();
            }

            string strcon = DadosDaConexao.StringDeConexao(util.obtemAmbiente());
            using (SqlConnection con = new SqlConnection(strcon.ToString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            ContasGridView.DataSource = dt;
                            ContasGridView.DataBind();

                            lblTotal.Text = ContasGridView.Rows.Count.ToString();
                        }
                    }
                }
            }
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            carregaGrid(recuperaFiltro());
        }

        protected void btnListar_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
        }

        protected void ContasGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string situacaoParcela = "";


                string string1 = e.Row.Cells[1].Text.ToString().Trim();
               /* string string2 = e.Row.Cells[2].Text.ToString().Trim();
                string string3 = e.Row.Cells[3].Text.ToString().Trim();
                string string4 = e.Row.Cells[4].Text.ToString().Trim();
                string string5 = e.Row.Cells[5].Text.ToString().Trim();
                string string6 = e.Row.Cells[6].Text.ToString().Trim();
                string string7 = e.Row.Cells[7].Text.ToString().Trim();
                string string8 = e.Row.Cells[8].Text.ToString().Trim();
                string string9 = e.Row.Cells[9].Text.ToString().Trim();
                string string10 = e.Row.Cells[10].Text.ToString().Trim();
                string string11 = e.Row.Cells[11].Text.ToString().Trim();
                string string12 = e.Row.Cells[12].Text.ToString().Trim();
                string string13 = e.Row.Cells[13].Text.ToString().Trim();*/


               /* if (!e.Row.Cells[13].Text.ToString().Trim().Equals(""))
                {
                    situacaoParcela = e.Row.Cells[13].Text.ToString();
                }*/

               

               /* foreach (TableCell cell in e.Row.Cells)
                {
                    if (situacaoParcela.Equals("Pendente"))
                    {
                        cell.BackColor = Color.Yellow;
                        cell.ForeColor = Color.Black;

                        e.Row.Cells[17].Enabled = false;
                        e.Row.Cells[18].Enabled = false;
                    }
                    else if (situacaoParcela.Equals("Quitado"))
                    {
                        cell.BackColor = Color.Green;
                        cell.ForeColor = Color.White;

                        e.Row.Cells[16].Enabled = false;
                        e.Row.Cells[18].Enabled = false;
                        //e.Row.Cells[14].Enabled = false;
                        //e.Row.Cells[14].Text = "";
                    }
                    else if (situacaoParcela.Equals("Cancelado"))
                    {
                        cell.BackColor = Color.Black;
                        cell.ForeColor = Color.Red;

                        e.Row.Cells[16].Enabled = false;
                        e.Row.Cells[18].Enabled = false;
                        //e.Row.Cells[14].Enabled = false;
                        //e.Row.Cells[14].Text = "";
                    }
                    else if (situacaoParcela.Equals("Atrasada"))
                    {
                        cell.BackColor = Color.DarkOrange;
                        cell.ForeColor = Color.White;

                        e.Row.Cells[18].Enabled = false;
                    }
                    else
                    {
                        cell.BackColor = Color.DarkRed;
                        cell.ForeColor = Color.White;
                        e.Row.Cells[18].Enabled = false;
                    }
                }*/
            }
        }
    }
}