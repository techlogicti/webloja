﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DevolucaoProduto.aspx.cs" Inherits="Web.DevolucaoProduto" %>
<asp:Content ID="Body" ContentPlaceHolderID="MainContent" runat="server">

	<script type="text/javascript">

    function closeModalMensagem() {
        $('#myModalMensagem').modal('hide');
    }

    function openModalMensagem() {
        $('#myModalMensagem').modal('show');
    }
</script>

	<asp:UpdatePanel runat="server">
	<ContentTemplate>


		  <hr />

       <asp:Panel ID="panelBotoes" runat="server" Visible="true">
          <div class="container-full">
            <asp:Button ID="btnListarDevolucoes" class="btn btn-primary" runat="server" Text="Listar" />
            <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Nova Devolução"  />
         </div>
        </asp:Panel>




<!--FILTRO -->
<hr />
<asp:Panel ID="panelFiltro" runat="server" Visible="true" BackColor="#C8C8C8" CssClass="container-full">
    <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-2">
                    <label for="txtCodigo">Nº Pedido</label>
                    <asp:TextBox class="form-control  input-sm" ID="txtNumeroPedido" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtDE_descricao">CPF Cliente</label>
                    <asp:TextBox class="form-control  input-sm" TextMode="number" ID="txtCpfCliente" runat="server"></asp:TextBox>   
                </div>
               
                <div class="col-sm-2">
                    <asp:Button ID="btnPesquisar" class="btn btn-info" runat="server" Text="Pesquisar"/>
                </div>
            </div>
        </div>
    </div>
    
 </asp:Panel>

		<hr />
	<asp:Panel ID="panelCadastro" runat="server" Visible="true">
		<div class="panel panel-default">
        <div class="panel-heading">Devolução de Mercadoria</div>
        <div class="panel-body">
		<div class="row">
			<div class="col-sm-1">
				<label for="txtCadIdPedido">ID Pedido</label>
				<asp:TextBox CssClass="form-control input-sm" TextMode="Number" ID="txtCadIdPedido" runat="server"></asp:TextBox>
			</div>
			<div class="col-sm-1">
				<label for="txtCadNumeroPedido">Nº Pedido</label>
				<asp:TextBox CssClass="form-control input-sm" TextMode="Number" ReadOnly="true" ID="txtCadNumeroPedido" runat="server"></asp:TextBox>
			</div>
			<div class="col-sm-2">
				<label for="txtCadCpfCliente">CPF Cliente</label>
				<asp:TextBox CssClass="form-control input-sm" TextMode="Number"  ReadOnly="true" ID="txtCadCpfCliente" runat="server"></asp:TextBox>
			</div>
			<div class="col-sm-1">
				<asp:Button ID="btnCadLocalizar" class="btn btn-info" runat="server" Text="Pesquisar" OnClick="btnCadLocalizar_Click"/>
			</div>
			<div class="col-sm-3">
				<label for="txtCadNomeCliente">Cliente</label>
				<asp:TextBox CssClass="form-control input-sm" TextMode="Search" ReadOnly="true" ID="txtCadNomeCliente" runat="server"></asp:TextBox>
			</div>
			<div class="col-sm-2">
				<label for="txtCadDataCompra">Data da Compra</label>
				<asp:TextBox CssClass="form-control input-sm" ReadOnly="true" ID="txtCadDataCompra" runat="server"></asp:TextBox>
			</div>
			<div class="col-sm-1">
				<label for="txtCadValorTotal">Valor Total</label>
				<asp:TextBox CssClass="form-control input-sm" ReadOnly="true" ID="txtCadValorTotal" runat="server"></asp:TextBox>
			</div>
		</div>
		</div>
		</div>
	</asp:Panel>

	<hr/> 

	<asp:Panel ID="panelGrid" runat="server" Visible="true">Devoluções

	</asp:Panel>



		
	</ContentTemplate>
	</asp:UpdatePanel>
		
</asp:Content>
