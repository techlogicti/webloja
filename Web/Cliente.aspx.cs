﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObjetoTransferencia;
using System.Drawing;
using AcessoBancoDados;
using Negocios;
using System.IO;

namespace Web
{
    public partial class Cliente : System.Web.UI.Page
    {

        

        string connectionString = "";
        int idOperador = 0;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);


        string sqlUf = "select ID, NM_nome from TB_uf order by NM_nome asc";
        string sqlCidade = "select ID, NM_nome from TB_municipio order by NM_nome asc";
        string sqlCategoriaCNH = "select ID, NM_nome from tb_categoriaCNH order by NM_nome asc";
        string sqlPais = "select ID, NM_nome from tb_pais order by NM_nome asc";
        string sqlEstadoCivil = "select ID, NM_nome from TB_estadoCivil order by NM_nome asc";
        string sqlTipoAtividade = "select ID, NM_nome from tb_tipoAtividade order by NM_nome asc";
        string sqlProfissao = "select ID, NM_nome from tb_profissao order by NM_nome asc";

        private DALConexao conexao;
        BLLOperador bllOperador = null;
        Util u = new Util();

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            testCheckUserSoberana();

            btnCancelar.Attributes.Add("onClick", "javascript:history.back(); return false;");
            btnVoltar.Attributes.Add("onClick", "javascript:history.back(); return false;");

            panelBotoes.Visible = true;
            panelFiltro.Visible = true;

            
            connectionString = u.conexao();

            u.preencheComboOrdemPesquisaCliente(ddlOrdemPesquisaCliente);

            panelGrid.Visible = true;

            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                // Declare the query string.
                String queryString = "select [ID] as 'Id', [CD_codigo] AS 'Codigo', [NM_nomeCompleto]  AS 'Nome', ";
                queryString = queryString + " [NM_apelido] AS 'Apelido', [NU_cpfCnpj] AS 'Documento', ";
                queryString = queryString + " [NU_telefoneFixo] as 'Fixo', [NU_celular1] as 'Celular', ";
                queryString = queryString + " [ST_situacaoRegistro] as 'Situacao' , ";

                queryString = queryString + " (select distinct 1 from  tb_cliente d, vw_contasareceber r  ";
                queryString = queryString + " where r.idcliente = d.ID  and r.DataRecebimentoUSA = 0 AND R.DataVencimentoUSA < CONVERT(VARCHAR(8),GETDATE(),112)  AND r.IDCliente = c.ID and r.FormaPagamento = 6) as contemParcelamentoCarne, ";

                //
                queryString = queryString + "  (select distinct 1 from tb_spc s where s.id_cliente = c.id) as listaSpc ";
                //

                queryString = queryString + " from [TB_cliente] c ";
                queryString = queryString + " order by [NM_nomeCompleto] asc ";

               
            }

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

            //Get the button that raised the event
            ImageButton btn = (ImageButton)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            //openModalVisualizaImagem(Convert.ToInt32(gvr.Cells[1].Text), "D", "N");

            string url = "http://joao-pc/Img/Scan/pedidos/";
            url = url + gvr.Cells[1].Text + ".jpeg";

            /*gravarAcesso("----------------------------------------------------------");
            gravarAcesso("Inicio - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("Clique - Abrir Imagem do Pedido " + gvr.Cells[1].Text);
            gravarAcesso(url.ToString());
            gravarAcesso("Fim - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("----------------------------------------------------------");*/

            string _cId = "";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newWindow", "window.open('../../DataControlManager/Online complain/frmComplaintRevision.aspx?ID=" + url + "','_blank','status=1,toolbar=0,menubar=0,location=1,scrollbars=1,resizable=1,width=30,height=30');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open('" + url + "', '_blank', 'height=600px,width=600px,scrollbars=1'); ", true);

            //http://servidor:8090/Img/Scan/pedidos/18766.jpeg
        }

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        public void recuperaHistoricoCompras(String idCliente)
        {
            // Declare the query string.

            String queryString = "select * from [VW_comprasCliente]  where 1=1 ";


            if (!idCliente.Equals(""))
            {
                queryString = queryString + " and  idCliente = '" + idCliente.ToString() + "'";
            }

            queryString = queryString + " order by NumeroPedido asc ";

            // Run the query and bind the resulting DataSet
            // to the GridView control.



            DataSet ds = null;
            //ds = GetData(queryString);
            ds = u.ObtemDados(queryString);

            if (ds.Tables.Count > 0)
            {
                gridViewHistoricoCompras.DataSource = ds;
                gridViewHistoricoCompras.DataBind();
            }
            else
            {
                //lblMensagemHistoricoCompras.Text = "Não foi possível conectar a base de dados.";
            }
        }

        public void recuperaHistoricoFinanceiro(String idCliente)
        {
            // Declare the query string.

            String queryString = "select * from VW_contasAReceber  where 1=1 ";


            if (!idCliente.Equals(""))
            {
                queryString = queryString + " and  idCliente = '" + idCliente.ToString() + "'";
            }

            queryString = queryString + " order by numeroPedido asc, NumeroParcela asc";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = null;
            //ds = GetData(queryString);
            ds = u.ObtemDados(queryString);
            if (ds.Tables.Count > 0)
            {
                gridViewHistoricoFinanceiro.DataSource = ds;
                gridViewHistoricoFinanceiro.DataBind();
            }
            else
            {
                //lblMensagemHistoricoFinanceiro.Text = "Não foi possível conectar a base de dados.";
            }
        }

        public void atualizaGrid(string filtro)
        {

            // Declare the query string.

            String queryString = "select [ID] as 'Id', [CD_codigo] AS 'Codigo', [NM_nomeCompleto]  AS 'Nome', ";
            queryString = queryString + " [NM_apelido] AS 'Apelido', [NU_cpfCnpj] AS 'Documento', ";
            queryString = queryString + " [NU_telefoneFixo] as 'Fixo', [NU_celular1] as 'Celular', ";
            queryString = queryString + " [ST_situacaoRegistro] as 'Situacao', ";

            queryString = queryString + " (select distinct 1 from  tb_cliente d, vw_contasareceber r ";
            queryString = queryString + " where r.idcliente = d.ID  and r.DataRecebimentoUSA = 0 AND R.DataVencimentoUSA < CONVERT(VARCHAR(8),GETDATE(),112)  AND r.IDCliente = c.ID and r.FormaPagamento = 6) as contemParcelamentoCarne, ";

            //
            queryString = queryString + "  (select distinct 1 from tb_spc s where s.id_cliente = c.id) as listaSpc ";
            //


            queryString = queryString + " from TB_cliente c where 1=1 ";
            
            if (!filtro.Equals(""))
            {
                //    queryString = queryString + " and [NM_nomeCompleto] like '%" + filtro.ToString() + "%'";

                filtro = filtro.Replace(" ", "%");

                filtro = filtro.Replace(".", "");
                filtro = filtro.Replace(",", "");
                filtro = filtro.Replace("-", "");
                filtro = filtro.Replace("/", "");

                //queryString = queryString + " and  NM_nomeCompleto like '%" + filtro.ToString() + "%'";
                queryString = queryString + " and  NM_nomeCompleto like '%" + filtro.ToString() + "%' ";

                queryString = queryString + " or NU_cpfCnpj like '%000" + filtro.ToString() + "%' ";

                //queryString = queryString + " or NU_cpfCnpj like '%" + filtro.ToString() + "%' ";
                queryString = queryString + " or ID like '%" + filtro.ToString() + "%'";
                queryString = queryString + " or nu_celular1 like '%" + filtro.ToString() + "%'";
                queryString = queryString + " or nu_celular2 like '%" + filtro.ToString() + "%'";
                
            }

            string order = "";
            order = ddlOrdemPesquisaCliente.SelectedValue.ToString();
            queryString = queryString + " order by "+order.ToString()+" asc";
            //queryString = queryString + " order by [NM_nomeCompleto] asc";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            //DataSet ds = GetData(queryString);
            DataSet ds = null;
            ds = u.ObtemDados(queryString);
            if (ds.Tables.Count > 0)
            {
                GridViewClientes.DataSource = ds;
                GridViewClientes.DataBind();

                lblTotal.Text = GridViewClientes.Rows.Count.ToString();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

      /*  DataSet GetData(String queryString)
        {

            

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.

                SqlConnection connection = null;

                connection = new SqlConnection(connectionString);



                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";

            }

            return ds;

        }*/

        protected void pesquisar()
        {
            string strDE_descricao = "";

            if (!txtDE_descricao.Text.Trim().Equals(""))
            {
                strDE_descricao = txtDE_descricao.Text.Trim().ToString();
                atualizaGrid(strDE_descricao);
            }
        }

        protected void txtDE_descricao_TextChanged(object sender, EventArgs e)
        {
            pesquisar();
        }

        protected void btnListarClientes_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
            panelBotoes.Visible = true;
            
            
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {

            exibeMVC("", "I", "1");

        }

        public void preencheCombo(DropDownList comboBox, string queryString, string todos)
        {

            DataSet ds = null;
            ds = u.ObtemDados(queryString);
            if (ds.Tables.Count > 0)
            {
                comboBox.Items.Clear();
                comboBox.DataSource = ds;
                comboBox.DataTextField = "NM_nome";
                comboBox.DataValueField = "ID";
                comboBox.DataBind();
                if (todos.Equals("S"))
                {
                    ListItem l = new ListItem("-", "0", true);
                    l.Selected = true;
                    comboBox.Items.Add(l);
                }
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        public void limpaCampos() {

            txtCodFinancial.Text = "";
            txtNomeCliente.Text = "";
            txtApelido.Text = "";
            txtDataNascimento.Text = "";

            txtNacionalidade.Text = "";
            ddlEstadoCivil.SelectedValue = "0";
            txtNomeConjuge.Text = "";

            //ddlProfissaoConjuge.SelectedValue = "-";
            txtDe_profissaoConjuge.Text = "";



            txtNaturalidade.Text = "";
            txtNomeMae.Text = "";
            txtNomePai.Text = "";


            /*ddlPaisInput.SelectedValue = "-";
            ddlUfClienteInput.SelectedValue = "-";
            ddlCidadeInput.SelectedValue = "-";*/

            ddlPaisInput.SelectedValue = "0";
            ddlUfClienteInput.SelectedValue = "0";
            ddlCidadeInput.SelectedValue = "0";


            txtBairroInput.Text = "";
            txtEnderecoInput.Text = "";
            txtNumeroInput.Text = "";

            txtCepInput.Text = "";
            txtComplementoInput.Text = "";
            txtReferenciaInput.Text = "";

            txtCpfInput.Text = "";
            txtNumeroIdentidade.Text = "";
            txtDataEmissaoIdentidade.Text = "";
            txtOrgaoEmissorIdentidade.Text = "";
            ddlUfEmissorIdentidade.SelectedValue = "18";

            txtNumeroCNH.Text = "";
            //ddlCategoriaCNH.SelectedValue = "-";
            ddlCategoriaCNH.SelectedValue = "0";
            txtDataEmissaoCNH.Text = "";
            ddlUfEmissorCNH.SelectedValue = "18";

            txtCelular1.Text = "";
            txtCelular2.Text = "";
            txtCelular3.Text = "";
            txtCelular4.Text = "";
            txtEmail.Text = "";
            txtTelefoneFixo.Text = "";
            txtTelCelRecado.Text = "";
            txtParentescoContato.Text = "";
            txtNomeContatoRecado.Text = "";


            //ddlProfissaoCliente.SelectedValue = "-";
            txtDE_profissaoCliente.Text = "";

            //ddlTipoAtividade.SelectedValue = "-";
            ddlTipoAtividade.SelectedValue = "0";

            txtDataAdmissao.Text = "";
            txtSalario.Text = "";
            txtTelefoneComercial.Text = "";
            txtEmpresaTrabalha.Text = "";
            txtNomeResponsavel.Text = "";

            txtOutros.Text = "";

        }


        public void camposClienteSomenteLeitura(String s) {

            if (s.Equals("S"))
            {
                txtCodFinancial.ReadOnly = true;
                txtNomeCliente.ReadOnly = true;
                txtApelido.ReadOnly = true;
                txtDataNascimento.ReadOnly = true;

                txtNacionalidade.ReadOnly = true;
                ddlEstadoCivil.Enabled = false;
                ddlEstadoCivil.CssClass = "form-control";
                txtNomeConjuge.ReadOnly = true;

                //ddlProfissaoConjuge.Enabled = false;
                //ddlProfissaoConjuge.CssClass = "form-control";

                txtDe_profissaoConjuge.ReadOnly = true;
                txtDE_profissaoCliente.ReadOnly = true;

                txtNaturalidade.ReadOnly = true;
                txtNomeMae.ReadOnly = true;
                txtNomePai.ReadOnly = true;


                ddlPaisInput.Enabled = false;
                ddlPaisInput.CssClass = "form-control";
                ddlUfClienteInput.Enabled = false;
                ddlUfClienteInput.CssClass = "form-control";
                ddlCidadeInput.Enabled = false;
                ddlCidadeInput.CssClass = "form-control";

                txtBairroInput.ReadOnly = true;
                txtEnderecoInput.ReadOnly = true;
                txtNumeroInput.ReadOnly = true;

                txtCepInput.ReadOnly = true;
                txtComplementoInput.ReadOnly = true;
                txtReferenciaInput.ReadOnly = true;

                txtCpfInput.ReadOnly = true;
                txtNumeroIdentidade.ReadOnly = true;
                txtDataEmissaoIdentidade.ReadOnly = true;
                txtOrgaoEmissorIdentidade.ReadOnly = true;

                ddlUfEmissorIdentidade.Enabled = false;
                ddlUfEmissorIdentidade.CssClass = "form-control";

                txtNumeroCNH.ReadOnly = true;

                ddlCategoriaCNH.Enabled = false;
                ddlCategoriaCNH.CssClass = "form-control";

                txtDataEmissaoCNH.ReadOnly = true;

                ddlUfEmissorCNH.Enabled = false;
                ddlUfEmissorCNH.CssClass = "form-control";

                txtCelular1.ReadOnly = true;
                txtCelular2.ReadOnly = true;
                txtCelular3.ReadOnly = true;
                txtCelular4.ReadOnly = true;
                txtEmail.ReadOnly = true;
                txtTelefoneFixo.ReadOnly = true;
                txtTelCelRecado.ReadOnly = true;
                txtParentescoContato.ReadOnly = true;
                txtNomeContatoRecado.ReadOnly = true;


                /*ddlProfissaoCliente.Enabled = false;
                ddlProfissaoCliente.CssClass = "form-control";*/

                ddlTipoAtividade.Enabled = false;
                ddlTipoAtividade.CssClass = "form-control";

                txtDataAdmissao.ReadOnly = true;
                txtSalario.ReadOnly = true;
                txtTelefoneComercial.ReadOnly = true;
                txtEmpresaTrabalha.ReadOnly = true;
                txtNomeResponsavel.ReadOnly = true;

                txtOutros.ReadOnly = true;
            }
            else
            {
                txtCodFinancial.ReadOnly = false;
                txtNomeCliente.ReadOnly = false;
                txtApelido.ReadOnly = false;
                txtDataNascimento.ReadOnly = false;

                txtNacionalidade.ReadOnly = false;
                ddlEstadoCivil.Enabled = true;
                ddlEstadoCivil.CssClass = "form-control";
                txtNomeConjuge.ReadOnly = false;

                /*ddlProfissaoConjuge.Enabled = true;
                ddlProfissaoConjuge.CssClass = "form-control";*/

                txtDe_profissaoConjuge.ReadOnly = false;
                txtDE_profissaoCliente.ReadOnly = false;

                txtNaturalidade.ReadOnly = false;
                txtNomeMae.ReadOnly = false;
                txtNomePai.ReadOnly = false;


                ddlPaisInput.Enabled = true;
                ddlPaisInput.CssClass = "form-control";
                ddlUfClienteInput.Enabled = true;
                ddlUfClienteInput.CssClass = "form-control";
                ddlCidadeInput.Enabled = true;
                ddlCidadeInput.CssClass = "form-control";

                txtBairroInput.ReadOnly = false;
                txtEnderecoInput.ReadOnly = false;
                txtNumeroInput.ReadOnly = false;

                txtCepInput.ReadOnly = false;
                txtComplementoInput.ReadOnly = false;
                txtReferenciaInput.ReadOnly = false;

                txtCpfInput.ReadOnly = false;
                txtNumeroIdentidade.ReadOnly = false;
                txtDataEmissaoIdentidade.ReadOnly = false;
                txtOrgaoEmissorIdentidade.ReadOnly = false;

                ddlUfEmissorIdentidade.Enabled = true;
                ddlUfEmissorIdentidade.CssClass = "form-control";

                txtNumeroCNH.ReadOnly = false;

                ddlCategoriaCNH.Enabled = true;
                ddlCategoriaCNH.CssClass = "form-control";

                txtDataEmissaoCNH.ReadOnly = false;

                ddlUfEmissorCNH.Enabled = true;
                ddlUfEmissorCNH.CssClass = "form-control";

                txtCelular1.ReadOnly = false;
                txtCelular2.ReadOnly = false;
                txtCelular3.ReadOnly = false;
                txtCelular4.ReadOnly = false;
                txtEmail.ReadOnly = false;
                txtTelefoneFixo.ReadOnly = false;
                txtTelCelRecado.ReadOnly = false;
                txtParentescoContato.ReadOnly = false;
                txtNomeContatoRecado.ReadOnly = false;


               /* ddlProfissaoCliente.Enabled = true;
                ddlProfissaoCliente.CssClass = "form-control";*/

                ddlTipoAtividade.Enabled = true;
                ddlTipoAtividade.CssClass = "form-control";

                txtDataAdmissao.ReadOnly = false;
                txtSalario.ReadOnly = false;
                txtTelefoneComercial.ReadOnly = false;
                txtEmpresaTrabalha.ReadOnly = false;
                txtNomeResponsavel.ReadOnly = false;

                txtOutros.ReadOnly = false;
            }

        }

        public void gravarLog(String texto)
        {
            // Compose a string that consists of three lines.
            string lines = texto.ToString().Trim();

            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\temp\\Soberana-Cliente-"+dataHoje+"-"+horaHoje+".txt");
            file.WriteLine(lines);

            file.Close();
        }

        public string montaSql(String queryString)
        {
            queryString += "SELECT [ID], [CD_codigo],[NM_nomeCompleto],[NM_apelido],[DT_dataNascimento],[CD_estadoCivil],[NM_nomeConjuge]";
            queryString += ",[CD_profissaoConjuge],[DE_nacionalidade],[DE_naturalidade],[NM_nomeMae],[NM_nomePai],[NU_cpfCnpj]";
            queryString += ",[NU_numeroIdentidade],[DT_emissaoIdentidade],[CD_ufEmissorIdentidade],[DE_orgaoEmissorIdentidade]";
            queryString += ",[NU_numeroCnh],[DT_emissaoCnh],[CD_ufEmissorCnh],[CD_categoriaCnh],[CD_pais],[CD_uf],[CD_cidade]";
            queryString += ",[DE_bairro],[DE_logradouro],[DE_numero],[DE_complemento],[DE_pontoDeReferencia],[DE_numeroCep]";
            queryString += ",[DE_empresa],[NU_telefoneComercial],[NM_responsavelComercial],[CD_tipoAtividade],[CD_profissao]";
            queryString += ",[DT_dataAdmissao],[VL_ordenado],[VL_ultimaCompra],[DT_ultimaCompra],[VL_creditoLoja],[NU_telefoneFixo]";
            queryString += ",[NU_celular1],[NU_celular2],[NU_celular3],[NU_celular4],[DE_email],[NU_telefoneRecado],[NM_nomeRecado]";
            queryString += ",[DE_parentescoRecado],[DE_observacoes],[CD_operador],[DT_dataCriacao],[HR_horaCriacao],[DT_dataAtualizacao]";
            queryString += ",[HR_horaAtualizacao],[ST_situacaoRegistro], [DE_profissaoCliente] , [DE_profissaoConjuge] ";
            queryString += "FROM [dbo].[TB_cliente] ";

            return queryString;
        }

        public bool verificaAutorizacaoItem(int idCliente, int idTipoPagamento)
        {
            bool itemAutorizado = false;

            string sqlAutorizacoesCliente = "select id, st_flagAutorizado from tb_autorizacaoCliente where id_tipoPagamento = "+ idTipoPagamento + " and id_cliente=" + idCliente.ToString();

            int idAutorizacao = 0;
            string strAutorizado = "";


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sqlAutorizacoesCliente, u.conexao());
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_autorizacaoCliente");

            foreach (DataRow pRow in customerOrders.Tables["tb_autorizacaoCliente"].Rows)
            {

                idAutorizacao = Convert.ToInt32(pRow["ID"].ToString());
                strAutorizado = pRow["st_flagAutorizado"].ToString();

                if (idAutorizacao > 0)
                {
                    if (strAutorizado.Equals("S"))
                    {
                        itemAutorizado = true;
                    }
                    else
                    {
                        itemAutorizado = false;
                    }
                }else
                {
                    itemAutorizado = false;
                }

            }


            return itemAutorizado;
        }

        public void recuperaInfoRegistroSPC(int idCliente)
        {

            ModeloSpc modeloSpc = new ModeloSpc();
            
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(u.obtemAmbiente()));
            BLLSpc bllSpc = new BLLSpc(conexao);

            modeloSpc.Id = 0;
            modeloSpc = bllSpc.CarregarModeloSpc(idCliente);


            if(modeloSpc.Id > 0) { 

            txtDataRegistroSPC.Text = Convert.ToInt32(modeloSpc.DT_dataRegistro).ToString();
            txtDataRegistroSPC.ReadOnly = true;
            txtMotivoRegistroSPC.Text = modeloSpc.DE_Motivo.ToString();
            txtMotivoRegistroSPC.ReadOnly = true;
            txtObservacaoRegistroSPC.Text = modeloSpc.DE_observacao.ToString();
            txtObservacaoRegistroSPC.ReadOnly = true;

            }
        }

        public void recuperaAutorizacoes(int idCliente)
        {

            

            foreach (ListItem item in chkBoxListFormasPagamento.Items)
            {
                string itemSelecionado = "";
                string itemAutorizacao = "";

                bool itemAutorizado = false;
                itemAutorizado = verificaAutorizacaoItem(idCliente, Convert.ToInt32(item.Value.ToString()));

                if (itemAutorizado)
                {
                    item.Selected = true;
                }
                else
                {
                    item.Selected = false;
                }
            }

        }

        public void recuperarDadosId(int id)
        {

            txtId.Text = id.ToString();

            string sqlUnico = montaSql("");
            sqlUnico = sqlUnico + " where id = " + id.ToString();

            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sqlUnico, u.conexao());
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_cliente");

            foreach (DataRow pRow in customerOrders.Tables["tb_cliente"].Rows)
            {

                //txtModalId.Text = pRow["ID"].ToString();
                //preencheCombo(ddlModalFornecedorInput, sqlFornecedor, "S");
                //ddlModalFornecedorInput.SelectedValue = pRow["IdFornecedor"].ToString();

                txtCodFinancial.Text = pRow["CD_codigo"].ToString();
                txtNomeCliente.Text = pRow["NM_nomeCompleto"].ToString();
                txtApelido.Text = pRow["NM_apelido"].ToString();
                string dataNascimento = "";
                dataNascimento = pRow["DT_dataNascimento"].ToString();

                if (dataNascimento.Equals("")) { dataNascimento = "0"; }
                if (dataNascimento != "0") dataNascimento = dataNascimento.Substring(0, 4) + "-" + dataNascimento.Substring(4, 2) + "-" + dataNascimento.Substring(6, 2);

                txtDataNascimento.Text = dataNascimento;
                txtNacionalidade.Text = pRow["DE_nacionalidade"].ToString();

                preencheCombo(ddlEstadoCivil, sqlEstadoCivil, "S");

                //se estado civil for vazio, usa o "Outro", 6
                if (pRow["CD_estadoCivil"].ToString().Equals(""))
                {
                    ddlEstadoCivil.SelectedValue = "6";
                }
                else
                {
                    ddlEstadoCivil.SelectedValue = pRow["CD_estadoCivil"].ToString();
                }

                txtNomeConjuge.Text = pRow["NM_nomeConjuge"].ToString();

                /*preencheCombo(ddlProfissaoConjuge, sqlProfissao, "S");
                ddlProfissaoConjuge.SelectedValue = pRow["CD_profissaoConjuge"].ToString();*/
                txtDe_profissaoConjuge.Text = pRow["DE_profissaoConjuge"].ToString();

                txtNaturalidade.Text = pRow["DE_naturalidade"].ToString();
                txtNomeMae.Text = pRow["NM_nomeMae"].ToString();
                txtNomePai.Text = pRow["NM_nomePai"].ToString();

                preencheCombo(ddlPaisInput, sqlPais, "S");

                //se pais for vazio, usa o Brasil
                if (pRow["CD_pais"].ToString().Equals(""))
                {
                    ddlPaisInput.SelectedValue = "1";
                }
                else
                {
                    ddlPaisInput.SelectedValue = pRow["CD_pais"].ToString();
                }
                preencheCombo(ddlUfClienteInput, sqlUf, "S");

                //se UF for vazio, usa o ES, 18
                if (pRow["CD_cidade"].ToString().Equals(""))
                {
                    ddlUfClienteInput.SelectedValue = "18";
                }
                else
                {
                    ddlUfClienteInput.SelectedValue = pRow["CD_uf"].ToString();
                }

                preencheCombo(ddlCidadeInput, sqlCidade, "S");
                //se cidade for vazio, usa vargem alta
                if (pRow["CD_cidade"].ToString().Equals(""))
                {
                    ddlCidadeInput.SelectedValue = "3170";
                }
                else
                {
                    ddlCidadeInput.SelectedValue = pRow["CD_cidade"].ToString();
                }


                txtBairroInput.Text = pRow["DE_bairro"].ToString();
                txtEnderecoInput.Text = pRow["DE_logradouro"].ToString();
                txtNumeroInput.Text = pRow["DE_numero"].ToString();
                txtCepInput.Text = pRow["DE_numeroCep"].ToString();
                txtComplementoInput.Text = pRow["DE_complemento"].ToString();
                txtReferenciaInput.Text = pRow["DE_pontoDeReferencia"].ToString();

                //If F, else if J, else - implementar
                string nDoc = pRow["NU_cpfCnpj"].ToString();

                if (nDoc.Trim().Equals("")){
                    nDoc = "0";
                }else
                {
                    nDoc = nDoc.Remove(0, 3);
                }
                
                txtCpfInput.Text = nDoc;
                //txtCpfInput.Text = pRow["NU_cpfCnpj"].ToString();

                txtNumeroIdentidade.Text = pRow["NU_numeroIdentidade"].ToString();

                string dataEmissaoIdentidade = "";
                dataEmissaoIdentidade = pRow["DT_emissaoIdentidade"].ToString();

                if (dataEmissaoIdentidade.Equals("")) dataEmissaoIdentidade = "0";
                if (dataEmissaoIdentidade != "0") dataEmissaoIdentidade = dataEmissaoIdentidade.Substring(0, 4) + "-" + dataEmissaoIdentidade.Substring(4, 2) + "-" + dataEmissaoIdentidade.Substring(6, 2);
                txtDataEmissaoIdentidade.Text = dataEmissaoIdentidade;

                txtOrgaoEmissorIdentidade.Text = pRow["DE_orgaoEmissorIdentidade"].ToString();

                preencheCombo(ddlUfEmissorIdentidade, sqlUf, "S");
                //se uf emissor for vazio, usa o ES, 18
                if (pRow["CD_ufEmissorIdentidade"].ToString().Equals(""))
                {
                    ddlUfEmissorIdentidade.SelectedValue = "18";
                }
                else
                {
                    ddlUfEmissorIdentidade.SelectedValue = pRow["CD_ufEmissorIdentidade"].ToString();
                }

                txtNumeroCNH.Text = pRow["NU_numeroCnh"].ToString();

                preencheCombo(ddlCategoriaCNH, sqlCategoriaCNH, "S");

                //se categoria cnh for vazia, usa outra, 8
                if (pRow["CD_categoriaCnh"].ToString().Equals("")) {
                    ddlCategoriaCNH.SelectedValue = "8";
                }else
                {
                    ddlCategoriaCNH.SelectedValue = pRow["CD_categoriaCnh"].ToString();
                }

                string dataEmissaoCNH = "";
                dataEmissaoCNH = pRow["DT_emissaoCnh"].ToString();
                if (dataEmissaoCNH.Equals("")) dataEmissaoCNH = "0";
                if (dataEmissaoCNH != "0")  dataEmissaoCNH = dataEmissaoCNH.Substring(0, 4) + "-" + dataEmissaoCNH.Substring(4, 2) + "-" + dataEmissaoCNH.Substring(6, 2);
                txtDataEmissaoCNH.Text = dataEmissaoCNH;


                preencheCombo(ddlUfEmissorCNH, sqlUf, "S");
                //se uf emissor for vazio, usa o ES, 18
                if (pRow["CD_ufEmissorCnh"].ToString().Equals(""))
                {
                    ddlUfEmissorCNH.SelectedValue = "18";
                }else
                {
                    ddlUfEmissorCNH.SelectedValue = pRow["CD_ufEmissorCnh"].ToString();
                }

                txtCelular1.Text = pRow["NU_celular1"].ToString();
                txtCelular2.Text = pRow["NU_celular2"].ToString();
                txtCelular3.Text = pRow["NU_celular3"].ToString();
                txtCelular4.Text = pRow["NU_celular4"].ToString();
                txtEmail.Text = pRow["DE_email"].ToString();
                txtTelefoneFixo.Text = pRow["NU_telefoneFixo"].ToString();
                txtTelCelRecado.Text = pRow["NU_telefoneRecado"].ToString();
                txtParentescoContato.Text = pRow["DE_parentescoRecado"].ToString();
                txtNomeContatoRecado.Text = pRow["NM_nomeRecado"].ToString();

                /*preencheCombo(ddlProfissaoCliente, sqlProfissao, "S");
                ddlProfissaoCliente.SelectedValue = pRow["CD_profissao"].ToString();*/

                txtDE_profissaoCliente.Text = pRow["DE_profissaoCliente"].ToString();

                preencheCombo(ddlTipoAtividade, sqlTipoAtividade, "S");
                //se tipo de atividade for vazia, usa NAO INFORMADA, 11
                if (pRow["CD_tipoAtividade"].ToString().Equals(""))
                {
                    ddlTipoAtividade.SelectedValue = "11";
                }
                else
                {
                    ddlTipoAtividade.SelectedValue = pRow["CD_tipoAtividade"].ToString();
                }
                

                string dataAdmissao = "";
                dataAdmissao = pRow["DT_dataAdmissao"].ToString();
                if (dataAdmissao.Equals("")) dataAdmissao = "0";
                if (dataAdmissao != "0") dataAdmissao = dataAdmissao.Substring(0, 4) + "-" + dataAdmissao.Substring(4, 2) + "-" + dataAdmissao.Substring(6, 2);
                txtDataAdmissao.Text = dataAdmissao;
                

                txtSalario.Text = pRow["VL_ordenado"].ToString();
                txtTelefoneComercial.Text = pRow["NU_telefoneComercial"].ToString();
                txtEmpresaTrabalha.Text = pRow["DE_empresa"].ToString();
                txtNomeResponsavel.Text = pRow["NM_responsavelComercial"].ToString();
                txtOutros.Text = pRow["DE_observacoes"].ToString();



            }


        }



        public void incializaCamposTeste() {


            lblMensagemDadosBasicos.Text = "11";
            lblMensagemEndereco.Text = "22";
            lblMensagemDocumentos.Text = "33";
            lblMensagemContatos.Text = "44";
            lblMensagemDadosTrabahistas.Text = "55";


            txtCodFinancial.Text = "123";
            txtNomeCliente.Text = "LUIS FELIPE FREITAS D ANDRADE";
            txtApelido.Text = "LUIS";
            txtDataNascimento.Text = "19871007";
            txtNacionalidade.Text = "BRASILEIRA";
            ddlEstadoCivil.SelectedValue = "1";
            txtNomeConjuge.Text = "CAROLINA MIRANDA PENNAFORTE";
            //ddlProfissaoConjuge.SelectedValue = "2614";
            txtDe_profissaoConjuge.Text = "Vendedor";
            txtNaturalidade.Text = "CACHOEIRO DE ITAPEMIRIM";
            txtNomeMae.Text = "RITA DE CASSIA FREITAS D ANDRADE";
            txtNomePai.Text = "JOAO BATISTA D ANDRADE FILHO";


            ddlPaisInput.SelectedValue = "1";
            ddlUfClienteInput.SelectedValue = "18";
            ddlCidadeInput.SelectedValue = "3164";
            txtBairroInput.Text = "PRACA DA BANDEIRA";
            txtEnderecoInput.Text = "RUA ORLEANS E BRAGANCA";
            txtNumeroInput.Text = "14";
            txtCepInput.Text = "29308475";
            txtComplementoInput.Text = "CASA";
            txtReferenciaInput.Text = "PROXIMO A PRACA ELISIO IMPERIAL";

            txtCpfInput.Text = "05815019771";
            txtNumeroIdentidade.Text = "2226960";
            txtDataEmissaoIdentidade.Text = "20000102";
            txtOrgaoEmissorIdentidade.Text = "SPTC";

            ddlUfEmissorIdentidade.SelectedValue = "18";
            txtNumeroCNH.Text = "123456";
            ddlCategoriaCNH.SelectedValue = "2";
            txtDataEmissaoCNH.Text = "20050304";
            ddlUfEmissorCNH.SelectedValue = "18";

            txtCelular1.Text = "28999990001";
            txtCelular2.Text = "28999990002";
            txtCelular3.Text = "28999990003";
            txtCelular4.Text = "28999990004";
            txtEmail.Text = "LUISFELIPEANDRADE@GMAIL.COM";
            txtTelefoneFixo.Text = "2835281711";
            txtTelCelRecado.Text = "2899999999";
            txtParentescoContato.Text = "ESPOSA";
            txtNomeContatoRecado.Text = "CAROL";

            //ddlProfissaoCliente.SelectedValue = "2614";
            txtDE_profissaoCliente.Text = "Vendedor";

            ddlTipoAtividade.SelectedValue = "11";
            txtDataAdmissao.Text = "20161201";
            txtSalario.Text = "1000.00";
            txtTelefoneComercial.Text = "2835282828";
            txtEmpresaTrabalha.Text = "SOBERANA MOVEIS E ELETRODOMESTICOS";
            txtNomeResponsavel.Text = "O PROPRIO";

            txtDataNascimento.Text = DateTime.Today.ToString("yyyy-MM-dd");
            txtDataAdmissao.Text = DateTime.Today.ToString("yyyy-MM-dd");
            txtDataEmissaoCNH.Text = DateTime.Today.ToString("yyyy-MM-dd");
            txtDataEmissaoIdentidade.Text = DateTime.Today.ToString("yyyy-MM-dd");


            //preencheCidade();

        }

        public string validaCamposInput(List<string> listaString)
        {
            string mensagemValidacao = "";
            int count = 0;

            foreach (string item in listaString)
            {
                if (!item.Equals(""))
                {
                    count++;
                }
            }

            return mensagemValidacao;
        }

        public string formataTelefone(string telefone)
        {
            string numeroTelefone = "0";

            if (!telefone.Equals("")) {

                telefone = telefone.Replace("(", "").Replace(")", "").Replace(".", "").Replace("-", "").Replace("+", "");

                numeroTelefone = telefone.Trim();
            }

            return numeroTelefone;
        }

        public void recuperaValoresTela(String acao)
        {


            
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(u.obtemAmbiente()));
            bllOperador = new BLLOperador(conexao);
            ModeloOperador modeloOperador = new ModeloOperador();
            if (!string.IsNullOrEmpty(Session["user"] as string))
            {
                modeloOperador = bllOperador.recuperaIdLogin(Session["user"].ToString());
                idOperador = Convert.ToInt32(modeloOperador.Id);
            }

            string v_codFinancial, v_nomeCliente, v_apelido, v_dataNascimento, v_nacionalidade, v_estadoCivil;
            string v_nomeConjuge, v_profissaoConjuge, v_naturalidade, v_nomeMae, v_nomePai;
            string v_pais, v_uf, v_cidade, v_bairro, v_endereco, v_numero, v_complemento, v_cep, v_referencia;
            string v_cpf, v_numeroIdentidade, v_dataEmissaoIdentidade, v_orgaoEmissorIdentidade, v_ufEmissorIdentidade;
            string v_numeroCNH, v_categoriaCNH, v_dataEmissaoCNH, v_ufEmissorCNH;
            string v_celular1, v_celular2, v_celular3, v_celular4, v_email, v_telefoneFixo, v_telRecado, v_nomeRecado, v_parentescoRecado;
            string v_profissao, v_tipoAtividade, v_dataAdmissao, v_salario, v_telefoneComercial, v_empresa, v_nomeResponsavel, v_outros;

            //set valor 0 para nao ficar vazio
            v_codFinancial = "0";
            v_nomeCliente = "0";
            v_apelido = "0";
            v_dataNascimento = "19000101";
            v_nacionalidade = "0";
            v_estadoCivil = "0";
            v_nomeConjuge = "0";
            v_profissaoConjuge = "0";
            v_naturalidade = "0";
            v_nomeMae = "0";
            v_nomePai = "0";
            v_pais = "0";
            v_uf = "0";
            v_cidade = "0";
            v_bairro = "0";
            v_endereco = "0";
            v_numero = "0";
            v_complemento = "0";
            v_cep = "0";
            v_referencia = "0";
            v_cpf = "0";
            v_numeroIdentidade = "0";
            v_dataEmissaoIdentidade = "0";
            v_orgaoEmissorIdentidade = "0";
            v_ufEmissorIdentidade = "0";
            v_numeroCNH = "0";
            v_categoriaCNH = "0";
            v_dataEmissaoCNH = "0";
            v_ufEmissorCNH = "0";
            v_celular1 = "0";
            v_celular2 = "0";
            v_celular3 = "0";
            v_celular4 = "0";
            v_email = "0";
            v_telefoneFixo = "0";
            v_telRecado = "0";
            v_nomeRecado = "0";
            v_parentescoRecado = "0";
            v_profissao = "0";
            v_tipoAtividade = "0";
            v_dataAdmissao = "0";
            v_salario = "0";
            v_telefoneComercial = "0";
            v_empresa = "0";
            v_nomeResponsavel = "0";
            v_outros = "0";
            //

            bool dadosValidos = false;

            int cDadosBasicos = 0;
            int cEndereco = 0;
            int cContatos = 0;
            int cDocumentos = 0;
            int cTrabalhistas = 0;

            bool bDadosBasicos = false;
            bool bEndereco = false;
            bool bContatos = false;
            bool bDocumentos = false;
            bool bTrabalhistas = false;

            // BLOCO - DADOS BASICOS (Total Obrigatorios=)
            v_codFinancial = txtCodFinancial.Text.ToString().Trim();
            v_nomeCliente = txtNomeCliente.Text.ToString().Trim();
            v_apelido = txtApelido.Text.ToString().Trim();
            v_dataNascimento = txtDataNascimento.Text.ToString().Trim();
            v_dataNascimento = v_dataNascimento.Replace("-", "");

            v_nacionalidade = txtNacionalidade.Text.ToString().Trim();
            v_estadoCivil = ddlEstadoCivil.SelectedValue.ToString();
            v_nomeConjuge = txtNomeConjuge.Text.ToString().Trim();
            //v_profissaoConjuge = ddlProfissaoConjuge.SelectedValue.ToString();
            v_profissaoConjuge = txtDe_profissaoConjuge.Text.ToString().Trim();
            v_naturalidade = txtNaturalidade.Text.ToString().Trim();
            v_nomeMae = txtNomeMae.Text.ToString().Trim();
            v_nomePai = txtNomePai.Text.ToString().Trim();

            lblMensagemDadosBasicos.Text = "";
            if (v_nomeCliente.Trim().Equals("")) { lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Nome Completo obrigatório | "; }else {
                cDadosBasicos ++;
            }
            if (v_dataNascimento.Trim().Equals("")) { lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Data de Nascimento obrigatória | ";}else { cDadosBasicos ++; }
            if (v_nacionalidade.Trim().Equals("")){ lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Nacionalidade obrigatória | "; }else { cDadosBasicos ++; }
            if (v_naturalidade.Trim().Equals("")){ lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Naturalidade obrigatória | "; }else { cDadosBasicos ++; }
            if (v_nomeMae.Trim().Equals("")){ lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Nome da Mãe obrigatório | "; }else { cDadosBasicos ++; }
            if (v_nomePai.Trim().Equals("")){ lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Nome do Pai obrigatório | "; }else { cDadosBasicos ++; }

            //Se for diferente de Solteiro e Viuvo (ou seja, casado, juntado) será validado o nome do Conjuge e profissao do mesmo

            if (v_estadoCivil.Trim().Equals("-")) {
                lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Estado civil obrigatório | ";

            } else {

                if (v_estadoCivil == "1" || v_estadoCivil == "4")
                {
                    if (v_nomeConjuge.Trim().Equals("")) { lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Nome do Conjuge obrigatório | "; } else { cDadosBasicos++; }
                    if (v_profissaoConjuge.Trim().Equals("")) { lblMensagemDadosBasicos.Text = lblMensagemDadosBasicos.Text + " | Profissão do Conjuge obrigatória | "; } else { cDadosBasicos++; }
                }

            }


            //Temporario
            //if (cDadosBasicos.Equals(6) || cDadosBasicos.Equals(8))
            ///if (cDadosBasicos.Equals(1))
            bDadosBasicos = true;
            if (cDadosBasicos > 1)
            {
                bDadosBasicos = true;
                lblMensagemDadosBasicos.Text = "Dados Básicos OK";
            }
            /* cDadosBasicos é valido quando for
             *  = 6, 
             *  = 8 para casado, juntado (conta nome e profissao do Conjuge)
             *  */






            //


            // BLOCO - ENDERECO (Total Obrigatorios=)
            v_pais = ddlPaisInput.SelectedValue.ToString();
            v_uf = ddlUfClienteInput.SelectedValue.ToString();
            v_cidade = ddlCidadeInput.SelectedValue.ToString();
            v_bairro = txtBairroInput.Text.ToString().Trim();
            v_endereco = txtEnderecoInput.Text.ToString().Trim();
            v_numero = txtNumeroInput.Text.ToString().Trim();
            v_cep = txtCepInput.Text.ToString().Trim();
            v_complemento = txtComplementoInput.Text.ToString().Trim();
            v_referencia = txtReferenciaInput.Text.ToString().Trim();

            lblMensagemEndereco.Text = "";

            if (v_pais.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Pais obrigatório | "; } else { cEndereco++; }
            if (v_uf.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | UF obrigatório | "; } else { cEndereco++; }
            if (v_cidade.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Cidade obrigatório | "; } else { cEndereco++; }
            if (v_bairro.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Bairro obrigatório | "; } else { cEndereco++; }
            if (v_endereco.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Endereço obrigatório | "; } else { cEndereco++; }
            if (v_numero.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Número obrigatório | "; } else { cEndereco++; }
            if (v_cep.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Cep obrigatório | "; } else { cEndereco++; }
            if (v_complemento.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Complemento obrigatório | "; } else { cEndereco++; }
            if (v_referencia.Trim().Equals("")) { lblMensagemEndereco.Text = lblMensagemEndereco.Text + " | Referência obrigatório | "; } else { cEndereco++; }

            if (cEndereco.Equals(9))
            {
                bEndereco = true;
                lblMensagemEndereco.Text = "Endereço OK";
            }


            // BLOCO - DOCUMENTOS (Total Obrigatorios=)
            v_cpf = txtCpfInput.Text.ToString().Trim();
            v_numeroIdentidade = txtNumeroIdentidade.Text.ToString().Trim();
            v_dataEmissaoIdentidade = txtDataEmissaoIdentidade.Text.ToString().Trim();
            v_dataEmissaoIdentidade = v_dataEmissaoIdentidade.Replace("-", "");

           



            v_orgaoEmissorIdentidade = txtOrgaoEmissorIdentidade.Text.ToString().Trim();
            v_ufEmissorIdentidade = ddlUfClienteInput.SelectedValue.ToString();
            v_numeroCNH = txtNumeroCNH.Text.ToString().Trim();
            v_categoriaCNH = ddlCategoriaCNH.SelectedValue.ToString();
            v_dataEmissaoCNH = txtDataEmissaoCNH.Text.ToString().Trim();
            v_dataEmissaoCNH = v_dataEmissaoCNH.Replace("-", "");
            v_ufEmissorCNH = ddlUfEmissorCNH.SelectedValue.ToString();

            lblMensagemDocumentos.Text = "";
            if (v_cpf.Trim().Equals("")) { lblMensagemDocumentos.Text = lblMensagemDocumentos.Text + " | CPF obrigatório | "; } else { cDocumentos++; }

            
            if (cDocumentos.Equals(1))
            {
                bDocumentos = true;
                lblMensagemDocumentos.Text = "Documentos OK";
            }else
            {
                lblMensagemDocumentos.Text = "CPF obrigatório";
            }



            ///////////
            // BLOCO - CONTATOS (Total Obrigatorios=)
            v_celular1 = txtCelular1.Text.ToString().Trim();
            v_celular2 = txtCelular2.Text.ToString().Trim();
            v_celular3 = txtCelular3.Text.ToString().Trim();
            v_celular4 = txtCelular4.Text.ToString().Trim();
            v_email = txtEmail.Text.ToString().Trim();
            v_telefoneFixo = txtTelefoneFixo.Text.ToString().Trim();
            v_telRecado = txtTelCelRecado.Text.ToString().Trim();
            v_parentescoRecado = txtParentescoContato.Text.ToString().Trim();
            v_nomeRecado = txtNomeContatoRecado.Text.ToString().Trim();


            if (v_celular1.Trim().Equals("")) { lblMensagemContatos.Text = lblMensagemContatos.Text + " | Celular 1 obrigatório | "; } else { cContatos++; }

            lblMensagemContatos.Text = "";
            if (cContatos.Equals(1))
            {
                bContatos = true;
                lblMensagemContatos.Text = "Contatos OK";
            }else
            {
                lblMensagemContatos.Text = "Telefone celular obrigatório";
            }


            // BLOCO - DADOS TRABALHISTAS (Total Obrigatorios=)
            //v_profissao = ddlProfissaoCliente.SelectedValue.ToString();
            v_profissao = txtDE_profissaoCliente.Text.ToString().Trim();
            v_tipoAtividade = ddlTipoAtividade.SelectedValue.ToString();
            v_dataAdmissao = txtDataAdmissao.Text.ToString().Trim();
            v_dataAdmissao = v_dataAdmissao.Replace("-", "");
            v_salario = txtSalario.Text.ToString().Trim();
            v_telefoneComercial = txtTelefoneComercial.Text.ToString().Trim();
            v_empresa = txtEmpresaTrabalha.Text.ToString().Trim();
            v_nomeResponsavel = txtNomeResponsavel.Text.ToString().Trim();




            lblMensagemDadosTrabahistas.Text = "";
            //if (v_profissao.Trim().Equals("-")) { lblMensagemDadosTrabahistas.Text = lblMensagemDadosTrabahistas.Text + " | Profissão obrigatória | "; } else { cTrabalhistas++; }
            if (v_profissao.Trim().Equals("")) {
                lblMensagemDadosTrabahistas.Text = lblMensagemDadosTrabahistas.Text + " | Profissão obrigatória | ";
            } else
            {
                cTrabalhistas++;
            }

            lblMensagemDadosTrabahistas.Visible = true;
            lblMensagemDadosTrabahistas.Text = "";

            if (cTrabalhistas.Equals(1))
            {
                bTrabalhistas = true;
                lblMensagemDadosTrabahistas.Text = "Dados Trabalhistas OK";
            }else
            {
                lblMensagemDadosTrabahistas.Text = "Profissão obrigatória";
            }

            v_outros = txtOutros.Text.ToString().Trim();


            //temp
            if (v_dataEmissaoIdentidade.Equals("")) v_dataEmissaoIdentidade = "0";
            if (v_dataEmissaoCNH.Equals("")) v_dataEmissaoCNH = "0";
            if (v_dataAdmissao.Equals("")) v_dataAdmissao = "0";

            v_telefoneComercial = formataTelefone(v_telefoneComercial);
            v_telefoneFixo = formataTelefone(v_telefoneFixo);
            v_celular1 = formataTelefone(v_celular1);
            v_celular2 = formataTelefone(v_celular2);
            v_celular3 = formataTelefone(v_celular3);
            v_celular4 = formataTelefone(v_celular4);

            /*
            if (v_telefoneComercial.Equals("")) v_telefoneComercial = "0";
            if (v_telefoneFixo.Equals("")) v_telefoneFixo = "0";
            if (v_celular1.Equals("")) v_celular1 = "0";
            if (v_celular2.Equals("")) v_celular2 = "0";
            if (v_celular3.Equals("")) v_celular3 = "0";
            if (v_celular4.Equals("")) v_celular4 = "0";*/

            if (v_codFinancial.Equals("")) v_codFinancial = "0";
            //if (v_profissao.Equals("-")) v_profissao = "0";
            if (v_profissao.Equals("")) v_profissao = "0";
            //if (v_profissaoConjuge.Equals("-")) v_profissaoConjuge = "0";
            if (v_profissaoConjuge.Equals("")) v_profissaoConjuge = "0";
            if (v_categoriaCNH.Equals("-")) v_categoriaCNH = "0";
            if (v_ufEmissorCNH.Equals("-")) v_ufEmissorCNH = "0";
            if (v_ufEmissorIdentidade.Equals("-")) v_ufEmissorIdentidade = "0";
            if (v_tipoAtividade.Equals("-")) v_tipoAtividade = "0";
            if (v_salario.Equals("")) v_salario = "0";
            if (v_telRecado.Equals("")) v_telRecado = "0";
            

            //Recupera Formas de Pagamento 


            //

            bool formOk = false;
            int count = 0;


            /*ClienteTO clienteTO = new ClienteTO();
            clienteTO.CD_codigo = v_codFinancial;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_apelido;
            clienteTO.CD_codigo = v_dataNascimento;
            clienteTO.CD_codigo = v_nacionalidade;
            clienteTO.CD_codigo = v_estadoCivil;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;
            clienteTO.CD_codigo = v_nomeCliente;*/


            /*  List<string> listaString = new List<string>();
              listaString.Add(v_codFinancial);
              listaString.Add(v_nomeCliente);
              listaString.Add(v_apelido);
              listaString.Add(v_dataNascimento);
              listaString.Add(v_nacionalidade);
              listaString.Add(v_estadoCivil);
              listaString.Add(v_nomeConjuge);
              listaString.Add(v_profissaoConjuge);
              listaString.Add(v_naturalidade);
              listaString.Add(v_nomeMae);
              listaString.Add(v_nomePai);

              listaString.Add(v_pais);
              listaString.Add(v_uf);
              listaString.Add(v_cidade);
              listaString.Add(v_bairro);
              listaString.Add(v_endereco);
              listaString.Add(v_numero);
              listaString.Add(v_cep);
              listaString.Add(v_complemento);
              listaString.Add(v_referencia);

              listaString.Add(v_cpf);
              listaString.Add(v_numeroIdentidade);
              listaString.Add(v_dataEmissaoIdentidade);
              listaString.Add(v_orgaoEmissorIdentidade);
              listaString.Add(v_ufEmissorIdentidade);
              listaString.Add(v_numeroCNH);
              listaString.Add(v_categoriaCNH);
              listaString.Add(v_dataEmissaoCNH);
              listaString.Add(v_ufEmissorCNH);

              listaString.Add(v_celular1);
              listaString.Add(v_celular2);
              listaString.Add(v_celular3);
              listaString.Add(v_celular4);
              listaString.Add(v_email);
              listaString.Add(v_telefoneFixo);
              listaString.Add(v_telRecado);
              listaString.Add(v_parentescoRecado);
              listaString.Add(v_nomeRecado);

              listaString.Add(v_profissao);
              listaString.Add(v_tipoAtividade);
              listaString.Add(v_dataAdmissao);
              listaString.Add(v_salario);
              listaString.Add(v_telefoneComercial);
              listaString.Add(v_empresa);
              listaString.Add(v_nomeResponsavel);
              listaString.Add(v_outros);



              foreach (string item in listaString)
              {
                  if (!item.Equals("")) count++;
              }*/

            //TEMPORARIO
            //FORÇA UM OK para cadastro sem validações obrigatórias
            //count = 5;
            //Remover
            bEndereco = true;
            bTrabalhistas = true;

            if (bDadosBasicos) count++;
            if (bDocumentos) count++;
            if (bEndereco) count++;
            if (bTrabalhistas) count++;
            if (bContatos) count++;


           


            if (count == 5) {

                formOk = true;

                lblMensagem.Text = "Dados OK";
                

            } else {

                formOk = false; }



            string sqlInsertCliente = "";

            if (formOk)
            {
                lblMensagem.Text = "Formulário OK";

                if (acao.Equals("INSERT"))
                {
                    sqlInsertCliente = "INSERT INTO TB_CLIENTE  ";
                    sqlInsertCliente += "([CD_codigo] ,[NM_nomeCompleto],[NM_apelido] ,[DT_dataNascimento] ,[CD_estadoCivil] ,[NM_nomeConjuge],[CD_profissaoConjuge] ,[DE_nacionalidade]";
                    sqlInsertCliente += ", [DE_naturalidade],[NM_nomeMae],[NM_nomePai],[NU_cpfCnpj],[NU_numeroIdentidade] ,[DT_emissaoIdentidade] ,[CD_ufEmissorIdentidade] ,[DE_orgaoEmissorIdentidade]";
                    sqlInsertCliente += ",[NU_numeroCnh],[DT_emissaoCnh] ,[CD_ufEmissorCnh],[CD_categoriaCnh],[CD_pais] ,[CD_uf] ,[CD_cidade],[DE_bairro] ,[DE_logradouro] ,[DE_numero]";
                    sqlInsertCliente += ",[DE_complemento],[DE_pontoDeReferencia],[DE_numeroCep] ,[DE_empresa] ,[NU_telefoneComercial] ,[NM_responsavelComercial] ,[CD_tipoAtividade]";
                    sqlInsertCliente += ",[CD_profissao] ,[DT_dataAdmissao] ,[VL_ordenado] ,[VL_ultimaCompra] ,[DT_ultimaCompra] ,[VL_creditoLoja]  ,[NU_telefoneFixo]  ,[NU_celular1] ,[NU_celular2]";
                    sqlInsertCliente += ",[NU_celular3] ,[NU_celular4] ,[DE_email] ,[NU_telefoneRecado]  ,[NM_nomeRecado] ,[DE_parentescoRecado]";
                    sqlInsertCliente += ",[DE_observacoes] ,[CD_operador] ,[DT_dataCriacao] ,[HR_horaCriacao] ,[DT_dataAtualizacao]  ,[HR_horaAtualizacao]  ,[ST_situacaoRegistro], [DE_profissaoCliente], [DE_profissaoConjuge])  VALUES (";
                    sqlInsertCliente += v_codFinancial.ToString() + " , ";
                    sqlInsertCliente += "'" + v_nomeCliente.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_apelido.ToString() + "'" + " , ";
                    if (v_dataNascimento.Equals("")){ v_dataNascimento = "0"; }
                    sqlInsertCliente += v_dataNascimento.ToString() + " , ";
                    sqlInsertCliente += v_estadoCivil.ToString() + " , ";
                    sqlInsertCliente += "'" + v_nomeConjuge.ToString() + "'" + " , ";
                    //sqlInsertCliente += v_profissaoConjuge.ToString() + " , ";
                    sqlInsertCliente += "0 , ";
                    sqlInsertCliente += "'" + v_nacionalidade.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_naturalidade.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_nomeMae.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_nomePai.ToString() + "'" + " , ";

                    v_cpf = v_cpf.Replace(".", "");
                    v_cpf = v_cpf.Replace("-", "");
                    v_cpf = v_cpf.Replace("/", "");

                    while (v_cpf.Length < 14)
                    {
                        v_cpf = "0" + v_cpf;
                    }

                    sqlInsertCliente += "'" + v_cpf.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_numeroIdentidade.ToString() + "'" + " , ";
                    sqlInsertCliente += v_dataEmissaoIdentidade.ToString() + " , ";
                    sqlInsertCliente += v_ufEmissorIdentidade.ToString() + " , ";
                    sqlInsertCliente += "'" + v_orgaoEmissorIdentidade.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_numeroCNH.ToString() + "'" + " , ";
                    sqlInsertCliente += v_dataEmissaoCNH.ToString() + " , ";
                    sqlInsertCliente += v_ufEmissorCNH.ToString() + " , ";
                    sqlInsertCliente += v_categoriaCNH.ToString() + " , ";
                    sqlInsertCliente += v_pais.ToString() + " , ";
                    sqlInsertCliente += v_uf.ToString() + " , ";
                    sqlInsertCliente += v_cidade.ToString() + " , ";
                    sqlInsertCliente += "'" + v_bairro.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_endereco.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_numero.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_complemento.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_referencia.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_cep.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_empresa.ToString() + "'" + " , ";
                    sqlInsertCliente += v_telefoneComercial.ToString() + " , ";
                    sqlInsertCliente += "'" + v_nomeResponsavel.ToString() + "'" + " , ";
                    sqlInsertCliente += v_tipoAtividade.ToString() + " , ";
                    //sqlInsertCliente += v_profissao.ToString() + " , ";
                    sqlInsertCliente += "0 , ";
                    sqlInsertCliente += v_dataAdmissao.ToString() + " , ";
                    sqlInsertCliente += v_salario.ToString() + " , ";
                    sqlInsertCliente += "0 , ";
                    sqlInsertCliente += "0 , ";
                    sqlInsertCliente += "0 , ";
                    sqlInsertCliente += v_telefoneFixo.ToString() + " , ";
                    sqlInsertCliente += v_celular1.ToString() + " , ";
                    sqlInsertCliente += v_celular2.ToString() + " , ";
                    sqlInsertCliente += v_celular3.ToString() + " , ";
                    sqlInsertCliente += v_celular4.ToString() + " , ";
                    sqlInsertCliente += "'" + v_email.ToString() + "'" + " , ";
                    sqlInsertCliente += v_telRecado.ToString() + " , ";
                    sqlInsertCliente += "'" + v_nomeRecado.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_parentescoRecado.ToString() + "'" + " , ";
                    sqlInsertCliente += "'" + v_outros.ToString() + "'" + " , ";
                    //sqlInsertCliente += "1,";
                    sqlInsertCliente += idOperador.ToString()+ ",";
                    
                    sqlInsertCliente += dataHoje + " , "; ;
                    sqlInsertCliente += horaHoje + " , "; ;
                    sqlInsertCliente += dataHoje + " , "; ;
                    sqlInsertCliente += horaHoje + " , "; ;
                    sqlInsertCliente += "'A' "+ ", ";
                    sqlInsertCliente += "'" + v_profissao + "' , "; ;
                    sqlInsertCliente += "'" + v_profissaoConjuge + "'  "; ;
                    
                    sqlInsertCliente += " );  select @@IDENTITY; ";

                    lblMensagem.Text = sqlInsertCliente.ToString();

                    SqlConnection conn = new SqlConnection(u.conexao());
                    SqlCommand cmd = new SqlCommand();
                    int codigoClienteInserido = 0;
                    try
                    {
                        cmd.CommandText = sqlInsertCliente;

                        cmd.Connection = conn;

                        conn.Open();

                        //cmd.ExecuteNonQuery();
                        codigoClienteInserido = Convert.ToInt32(cmd.ExecuteScalar());

                        //int id = (int)cmd.ExecuteScalar();
                        
                        conn.Close();
                        this.gravarLog("Execução do sql: " + sqlInsertCliente.ToString());

                        if(codigoClienteInserido > 0)
                        {
                            limpaCampos();
                            lblMensagem.Text = "Cliente Inserido com sucesso ! [ ID: " + codigoClienteInserido.ToString() + " ] ";
                            openModalMensagem(); 

                            //realiza inserção na tabela de autorizações do cliente
                            salvaAutorizacoes(codigoClienteInserido,"INSERT");
                                

                            //

                        }
                        else
                        {
                            lblMensagem.Text = "Cliente com ID 0. Verificar com o administrador.";
                            openModalMensagem();
                            this.gravarLog(Environment.NewLine + "Data/Hora:" + dataHoje.ToString() + "/" + horaHoje.ToString() + "Cliente com ID 0. Verificar com o administrador." + Environment.NewLine + sqlInsertCliente.ToString());
                        }

                        
                        

                    }
                    catch (Exception ex)
                    {
                        //ex.ToString();
                        lblMensagem.Text = "Erro na gravação";
                        openModalMensagem();
                        this.gravarLog(Environment.NewLine + "Data/Hora:" +dataHoje.ToString() + "/" +horaHoje.ToString()  + ex.ToString() + Environment.NewLine + sqlInsertCliente.ToString());
                    }
                }else if (acao.Equals("DELETE"))
                {
                    /*using (SqlConnection con = new SqlConnection(constring))
                    {
                        using (SqlCommand cmd = new SqlCommand("DELETE FROM Persons WHERE Name = @Name", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@Name", name);
                            con.Open();
                            int rowsAffected = cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }*/
                }
                else if (acao.Equals("UPDATE"))
                {
                    string sqlUpdateCliente = "";

                    sqlUpdateCliente += " UPDATE[dbo].[TB_cliente] ";

                    sqlUpdateCliente += " SET  CD_codigo = '" + v_codFinancial.ToString() + "', ";
                    sqlUpdateCliente += "NM_nomeCompleto = '" + v_nomeCliente.ToString() + "'" + " , ";
                    sqlUpdateCliente += "NM_apelido      = '" + v_apelido.ToString() + "'" + " , ";
                    if (v_dataNascimento.Equals("")){ v_dataNascimento = "0"; }
                    sqlUpdateCliente += "DT_dataNascimento =" + v_dataNascimento.ToString() + " , ";
                    sqlUpdateCliente += "CD_estadoCivil    =" + v_estadoCivil.ToString() + " , ";
                    sqlUpdateCliente += "NM_nomeConjuge    ='" + v_nomeConjuge.ToString() + "'" + " , ";
                    //sqlUpdateCliente += "NM_nomeConjuge    ='" + v_nomeConjuge.ToString() + "'" + "  ";

                    //sqlUpdateCliente += "CD_profissaoConjuge = "+ v_profissaoConjuge.ToString() + " , ";
                    sqlUpdateCliente += "DE_profissaoConjuge ='" + v_profissaoConjuge.ToString() + "' , ";
                    sqlUpdateCliente += "DE_nacionalidade    ='"+ v_nacionalidade.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_naturalidade     ='"+ v_naturalidade.ToString() + "'" + " , ";
                    sqlUpdateCliente += "NM_nomeMae          ='"+ v_nomeMae.ToString() + "'" + " , ";
                    sqlUpdateCliente += "NM_nomePai          ='"+ v_nomePai.ToString() + "'" + " , ";

                    v_cpf = v_cpf.Replace(".", "");
                    v_cpf = v_cpf.Replace("-", "");
                    v_cpf = v_cpf.Replace("/", "");

                    while (v_cpf.Length < 14)
                    {
                        v_cpf = "0" + v_cpf;
                    }

                    sqlUpdateCliente += "NU_cpfCnpj          ='"+ v_cpf.ToString() + "'" + " , ";
                    sqlUpdateCliente += "NU_numeroIdentidade ='"+ v_numeroIdentidade.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DT_emissaoIdentidade= "+ v_dataEmissaoIdentidade.ToString() + " , ";
                    sqlUpdateCliente += "CD_ufEmissorIdentidade="+ v_ufEmissorIdentidade.ToString() + " , ";
                    sqlUpdateCliente += "DE_orgaoEmissorIdentidade='" + v_orgaoEmissorIdentidade.ToString() + "'" + " , ";
                    sqlUpdateCliente += "NU_numeroCnh          ='"+ v_numeroCNH.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DT_emissaoCnh         =" + v_dataEmissaoCNH.ToString() + " , ";
                    sqlUpdateCliente += "CD_ufEmissorCnh       =" + v_ufEmissorCNH.ToString() + " , ";
                    sqlUpdateCliente += "CD_categoriaCnh       =" + v_categoriaCNH.ToString() + " , ";
                    sqlUpdateCliente += "CD_pais               =" + v_pais.ToString() + " , ";
                    sqlUpdateCliente += "CD_uf                 =" + v_uf.ToString() + " , ";
                    sqlUpdateCliente += "CD_cidade             =" + v_cidade.ToString() + " , ";
                    sqlUpdateCliente += "DE_bairro             ='"+ v_bairro.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_logradouro         ='"+ v_endereco.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_numero             ='"+ v_numero.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_complemento        ='"+ v_complemento.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_pontoDeReferencia  ='"+ v_referencia.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_numeroCep          ='"+ v_cep.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_empresa            ='"+ v_empresa.ToString() + "'" + " , ";
                    sqlUpdateCliente += "NU_telefoneComercial  =" + v_telefoneComercial.ToString() + " , ";
                    sqlUpdateCliente += "NM_responsavelComercial='"+v_nomeResponsavel.ToString() + "'" + " , ";
                    sqlUpdateCliente += "CD_tipoAtividade        ="+v_tipoAtividade.ToString() + " , ";
                    //sqlUpdateCliente += "CD_profissao            ="+v_profissao.ToString() + " , ";
                    sqlUpdateCliente += "DE_profissaoCliente     ='" + v_profissao.ToString() + "' , ";
                    sqlUpdateCliente += "DT_dataAdmissao         ="+v_dataAdmissao.ToString() + " , ";
                    sqlUpdateCliente += "VL_ordenado             ='"+v_salario.ToString() + "', ";
                    /*sqlUpdateCliente += "VL_ultimaCompra=0 , ";
                    sqlUpdateCliente += "DT_ultimaCompra=0 , ";
                    sqlUpdateCliente += "VL_creditoLoja=0 , ";*/
                    sqlUpdateCliente += "NU_telefoneFixo        =" + v_telefoneFixo.ToString() + " , ";
                    sqlUpdateCliente += "NU_celular1            =" + v_celular1.ToString() + " , ";
                    sqlUpdateCliente += "NU_celular2            =" + v_celular2.ToString() + " , ";
                    sqlUpdateCliente += "NU_celular3            =" + v_celular3.ToString() + " , ";
                    sqlUpdateCliente += "NU_celular4            =" + v_celular4.ToString() + " , ";
                    sqlUpdateCliente += "DE_email               ='" + v_email.ToString() + "'" + " , ";
                    sqlUpdateCliente += "NU_telefoneRecado      =" + v_telRecado.ToString() + " , ";
                    sqlUpdateCliente += "NM_nomeRecado          ='" + v_nomeRecado.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_parentescoRecado    ='" + v_parentescoRecado.ToString() + "'" + " , ";
                    sqlUpdateCliente += "DE_observacoes         ='" + v_outros.ToString() + "'" + " , ";

                    //sqlUpdateCliente += "CD_operador=1,";
                    sqlUpdateCliente += "CD_operador="+ idOperador.ToString() + ",";

                    /*sqlUpdateCliente += "DT_dataCriacao="+dataHoje +" , ";;
                    sqlUpdateCliente += "HR_horaCriacao="+horaHoje;+ " , ";*/
                    sqlUpdateCliente += "DT_dataAtualizacao = " + dataHoje + ",";
                    sqlUpdateCliente += "HR_horaAtualizacao = " + horaHoje +",";
                    sqlUpdateCliente += "ST_situacaoRegistro = 'A'";

                    sqlUpdateCliente += " where id = " + txtId.Text;

                    lblMensagem.Text = sqlUpdateCliente.ToString();

                    salvaAutorizacoes(Convert.ToInt32(txtId.Text.Trim()), "UPDATE");

                    SqlConnection conn = new SqlConnection(u.conexao());
                    SqlCommand cmd = new SqlCommand();

                    try
                    {
                        cmd.CommandText = sqlUpdateCliente;

                        cmd.Connection = conn;

                        conn.Open();

                        cmd.ExecuteNonQuery();

                        //int id = (int)cmd.ExecuteScalar();
                        int id = 0;
//                        id = (int)cmd.ExecuteNonQuery(); //numero de linhas afetadas
                        conn.Close();
                        this.gravarLog("Execução do sql: " + sqlUpdateCliente.ToString());
                        
                        //limpaCampos();
                        lblMensagem.Text = "Cliente Atualizado com sucesso ! [ ID: " + id.ToString() + " ] ";
                        openModalMensagem();


                    }
                    catch (Exception ex)
                    {
                        //ex.ToString();
                        lblMensagem.Text = "Erro na gravação";
                        //this.gravarLog(ex.ToString()+" ##### " + sqlUpdateCliente.ToString());
                        this.gravarLog(Environment.NewLine + "Data/Hora:" + dataHoje.ToString() + "/" + horaHoje.ToString() + ex.ToString() + Environment.NewLine + sqlUpdateCliente.ToString());
                    }
                }
                else
                {

                }

                



            }
            else
            {

                lblMensagem.Text = "Preencha todos os campos";
                openModalMensagem();
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {

            recuperaValoresTela("INSERT");

        }

        public void salvaAutorizacoes(int idCliente, string acao)
        {
            foreach (ListItem item in chkBoxListFormasPagamento.Items)
            {
                //if (item.Selected) {  //comentado para inserir o item mesmo que nao esteja seleciado, porém, com o status S e N
                    //lblStates.Text += item.Text + "<br>";

                    string itemSelecionado = "";
                    string itemAutorizacao = "";
                    
                //itemSelecionado = item.Text.ToString();
                    if (item.Selected){
                        itemSelecionado = "S";
                    } else {
                        itemSelecionado = "N";
                    }

                    itemAutorizacao = item.Value.ToString();

                string sqlAutorizacao = "";

                if (acao.Equals("INSERT"))
                {
                    sqlAutorizacao = "insert into tb_autorizacaoCliente values ";
                    //sqlAutorizacao += "(" + idCliente + "," + itemAutorizacao + ",'" + itemSelecionado.ToString() + "',1," + dataHoje + "," + horaHoje + "," + dataHoje + "," + horaHoje + ",'A');  select @@IDENTITY; ";
                    sqlAutorizacao += "(" + idCliente + "," + itemAutorizacao + ",'" + itemSelecionado.ToString() + "',"+ idOperador.ToString()+ "," + dataHoje + "," + horaHoje + "," + dataHoje + "," + horaHoje + ",'A');  select @@IDENTITY; ";
                }
                else if (acao.Equals("UPDATE"))
                {
                    //sqlAutorizacao = "update tb_autorizacaoCliente set st_flagAutorizado='"+ itemSelecionado.ToString()+ "', dt_dataatualizacao=" + dataHoje + ", hr_horaatualizacao=" + horaHoje +  " where id_cliente ="+ idCliente.ToString()+ " and id_tipoPagamento= " + itemAutorizacao + "; select @@IDENTITY; ";
                    sqlAutorizacao = "update tb_autorizacaoCliente set st_flagAutorizado='" + itemSelecionado.ToString() + "', dt_dataatualizacao=" + dataHoje + ", hr_horaatualizacao=" + horaHoje +" , cd_operador="+idOperador.ToString()+ " where id_cliente =" + idCliente.ToString() + " and id_tipoPagamento= " + itemAutorizacao + "; select @@IDENTITY; ";
                }
                else
                {

                }
                   

                    SqlConnection conn = new SqlConnection(u.conexao());
                    SqlCommand cmd = new SqlCommand();
                    int codigoLinhaAutorizacao = 0;
                    try
                    {
                        cmd.CommandText = sqlAutorizacao;

                        cmd.Connection = conn;

                        conn.Open();

                        //cmd.ExecuteNonQuery();
                        codigoLinhaAutorizacao = Convert.ToInt32(cmd.ExecuteScalar());

                        conn.Close();
                        this.gravarLog("Execução do sql: " + sqlAutorizacao.ToString());
                    }
                    catch (Exception ex)
                    {
                        //ex.ToString();
                        lblMensagem.Text = "Erro na gravação";
                    openModalMensagem();
                    this.gravarLog(Environment.NewLine + "Data/Hora:" + dataHoje.ToString() + "/" + horaHoje.ToString() + ex.ToString() + Environment.NewLine + sqlAutorizacao.ToString());
                    }

                //}

            }
        }

        public void escreveValores()
        {
            //lblMensagem.Text = txtDataAdmissao.Text;
            lblMensagem.Text = "teste";


            txtOutros.Text = "Dados:" + Environment.NewLine +
            txtCodFinancial.Text + Environment.NewLine +
            txtNomeCliente.Text + Environment.NewLine +
            txtApelido.Text + Environment.NewLine +
            txtDataNascimento.Text + Environment.NewLine +
            txtNacionalidade.Text + Environment.NewLine +
            ddlEstadoCivil.SelectedValue.ToString() + Environment.NewLine +
            txtNomeConjuge.Text + Environment.NewLine +
            //ddlProfissaoConjuge.SelectedValue.ToString() + Environment.NewLine +
            txtDe_profissaoConjuge.Text + Environment.NewLine +
            txtNaturalidade.Text + Environment.NewLine +
            txtNomeMae.Text + Environment.NewLine +
            txtNomePai.Text + Environment.NewLine +

            ddlPaisInput.SelectedValue.ToString() + Environment.NewLine +
            ddlUfClienteInput.SelectedValue.ToString() + Environment.NewLine +
            ddlCidadeInput.SelectedValue.ToString() + Environment.NewLine +
            txtBairroInput.Text + Environment.NewLine +
            txtEnderecoInput.Text + Environment.NewLine +
            txtNumeroInput.Text + Environment.NewLine +
            txtCepInput.Text + Environment.NewLine +
            txtComplementoInput.Text + Environment.NewLine +
            txtReferenciaInput.Text + Environment.NewLine +

            txtCpfInput.Text + Environment.NewLine +
            txtNumeroIdentidade.Text + Environment.NewLine +
            txtDataEmissaoIdentidade.Text + Environment.NewLine +
            txtOrgaoEmissorIdentidade.Text + Environment.NewLine +
            ddlUfEmissorIdentidade.SelectedValue.ToString() + Environment.NewLine +
            txtNumeroCNH.Text + Environment.NewLine +
            ddlCategoriaCNH.SelectedValue.ToString() + Environment.NewLine +
            txtDataEmissaoCNH.Text + Environment.NewLine +
            ddlUfEmissorCNH.SelectedValue.ToString() + Environment.NewLine +
            txtCelular1.Text + Environment.NewLine +
            txtCelular2.Text + Environment.NewLine +
            txtCelular3.Text + Environment.NewLine +
            txtCelular4.Text + Environment.NewLine +
            txtEmail.Text + Environment.NewLine +
            txtTelefoneFixo.Text + Environment.NewLine +
            txtTelCelRecado.Text + Environment.NewLine +
            txtParentescoContato.Text + Environment.NewLine +
            txtNomeContatoRecado.Text + Environment.NewLine +

            //ddlProfissaoCliente.SelectedValue.ToString() + Environment.NewLine +
            txtDE_profissaoCliente.Text + Environment.NewLine +
            ddlTipoAtividade.SelectedValue.ToString() + Environment.NewLine +
            txtDataAdmissao.Text + Environment.NewLine +
            txtSalario.Text + Environment.NewLine +
            txtTelefoneComercial.Text + Environment.NewLine +
            txtEmpresaTrabalha.Text + Environment.NewLine +
            txtNomeResponsavel.Text + Environment.NewLine;
        }

        protected void btnBuscarCep_Click(object sender, EventArgs e)
        {
            if (txtCepInput.Text.Equals(""))
            {
                ddlPaisInput.SelectedValue = "-";
                ddlUfClienteInput.SelectedValue = "-";
                ddlCidadeInput.SelectedValue = "-";
                txtBairroInput.Text = "";
                txtEnderecoInput.Text = "";

            }
            else
            {
                ddlPaisInput.SelectedValue = "1";
                ddlUfClienteInput.SelectedValue = "18";
                ddlCidadeInput.SelectedValue = "3170";
                txtBairroInput.Text = "Centro";
                txtEnderecoInput.Text = "Rua Elizeu Gasparini";

            }



        }

        protected void ddlUfClienteInput_SelectedIndexChanged(object sender, EventArgs e)
        {

            preencheCidade("");
        }

        protected void ddlUfClienteInput_TextChanged(object sender, EventArgs e)
        {
            preencheCidade("");
        }

        public void preencheCidade(string idCidade)
        {
            string sqlCidade = "SELECT ID, NM_nome FROM TB_municipio WHERE 1 = 1";
            string v_ufInput = "";

            if (idCidade.Equals(""))
            {
                v_ufInput = ddlUfClienteInput.SelectedValue.ToString();
                sqlCidade += " and CD_uf=" + v_ufInput + " ORDER BY NM_nome";
                ddlCidadeInput.Items.Clear();
                preencheCombo(ddlCidadeInput, sqlCidade, "S");
            }
            else
            {
                sqlCidade += " and CD_uf=18 ORDER BY NM_nome";
                ddlCidadeInput.Items.Clear();
                preencheCombo(ddlCidadeInput, sqlCidade, "S");
                ddlCidadeInput.SelectedValue = "3170";
            }

            
           
        }

        protected void btnLimparCampos_Click(object sender, EventArgs e)
        {
            limpaCampos();
        }

        protected void btnDetalhar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;


            exibeMVC(gvr.Cells[0].Text.ToString(), "V", "1");
        }

       

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            exibeMVC(gvr.Cells[0].Text.ToString(), "A", "1");
        }

        public string recuperaProximoIdCliente()
        {

            string sql = "select (max(id) + 1) as ID from tb_cliente";
            string proximoId = "";


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, u.conexao());
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_cliente");

            foreach (DataRow pRow in customerOrders.Tables["tb_cliente"].Rows)
            {

                proximoId = pRow["ID"].ToString();


            }


            return proximoId;

        }

        public void exibeMVC(string id, string modoMVC, string usuario)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;


            if (modoMVC.Equals("I"))
            {
                lblModo.Text = "Modo de inclusão";
                btnSalvar.Visible = true;
                btnAtualizar.Visible = false;
                btnLimparCampos.Visible = true;
                btnCancelar.Visible = true;

                string proximoIdCliente = "";

                proximoIdCliente = recuperaProximoIdCliente();

                txtId.Text = proximoIdCliente.ToString();



                preencheCombo(ddlUfClienteInput, sqlUf, "S");
                preencheCombo(ddlUfEmissorCNH, sqlUf, "S");
                preencheCombo(ddlUfEmissorIdentidade, sqlUf, "S");
                preencheCombo(ddlCategoriaCNH, sqlCategoriaCNH, "S");
                preencheCombo(ddlPaisInput, sqlPais, "S");
                preencheCombo(ddlEstadoCivil, sqlEstadoCivil, "S");
                //preencheCombo(ddlProfissaoCliente, sqlProfissao, "S");
                //preencheCombo(ddlProfissaoConjuge, sqlProfissao, "S");
                preencheCombo(ddlTipoAtividade, sqlTipoAtividade, "S");

                //Campos do SPC não precisam aparecer
                txtDataRegistroSPC.Visible = false;
                txtMotivoRegistroSPC.Visible = false;
                txtObservacaoRegistroSPC.Visible = false;
                //

                //Temporario
                ddlPaisInput.SelectedValue = "1";
                ddlUfClienteInput.SelectedValue = "18";
                ddlCidadeInput.SelectedValue = "3170";
                txtBairroInput.Text = "Centro";
                txtEnderecoInput.Text = "Rua Elizeu Gasparini";

                preencheCidade("18");

                //Fim-Temporario

                //incializaCamposTeste();
            }
            else if (modoMVC.Equals("A"))
            {
                lblModo.Text = "Modo de edição";
                btnSalvar.Visible = false;
                btnAtualizar.Visible = true;
                btnLimparCampos.Visible = false;
                btnCancelar.Visible = true;

                camposClienteSomenteLeitura("N");

                if (!id.Equals(""))
                {
                    recuperarDadosId(Convert.ToInt32(id));
                    recuperaAutorizacoes(Convert.ToInt32(id));
                    recuperaInfoRegistroSPC(Convert.ToInt32(id));
                    
                }

                
            }
            else if (modoMVC.Equals("V"))
            {
                lblModo.Text = "Modo de visualização";
                btnSalvar.Visible = false;
                btnAtualizar.Visible = false;
                btnLimparCampos.Visible = false;
                btnCancelar.Visible = true;

                camposClienteSomenteLeitura("S");

                if (!id.Equals(""))
                {
                    recuperarDadosId(Convert.ToInt32(id));
                    recuperaAutorizacoes(Convert.ToInt32(id));
                    recuperaHistoricoCompras(id);
                    recuperaHistoricoFinanceiro(id);
                    recuperaInfoRegistroSPC(Convert.ToInt32(id));
                    listarDocumentosDigitalizados(Convert.ToInt32(id)); //20180712
                }

                //recuperarDadosId(Convert.ToInt32(id));
                //recuperaAutorizacoes(Convert.ToInt32(id));

                //recuperaHistoricoCompras(id);
                //recuperaHistoricoFinanceiro(id);

            }
            else
            {
                lblModo.Text = "Modo ?";
            }

            



        
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            incializaCamposTeste();
        }

        protected void btnAtualizar_Click(object sender, EventArgs e)
        {
            recuperaValoresTela("UPDATE");
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Server.Transfer("Cliente.aspx", true);
            
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            pesquisar();
        }

        //20180712
        public void listarDocumentosDigitalizados(int idCliente)
        {
            string caminho = "";
            caminho = "~/Img/Scan/Documentos/" + idCliente + "/";
            string[] filePaths = Directory.GetFiles(Server.MapPath(caminho));
            List<ListItem> files = new List<ListItem>();
            foreach (string filePath in filePaths)
            {
                string fileName = Path.GetFileName(filePath);
                files.Add(new ListItem(fileName, caminho + fileName));
            }
            GridViewDocumentosDigitalizados.DataSource = files;
            GridViewDocumentosDigitalizados.DataBind();
        }
        //

        protected void GridViewClientes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int contemParcelamentoCarne = 0;
                int listaSpc = 0;

                if (!e.Row.Cells[8].Text.ToString().Trim().Equals("&nbsp;"))
                {
                    contemParcelamentoCarne = Convert.ToInt32(e.Row.Cells[8].Text.ToString());
                }

                if (!e.Row.Cells[9].Text.ToString().Trim().Equals("&nbsp;"))
                {
                    listaSpc = Convert.ToInt32(e.Row.Cells[9].Text.ToString());
                }

                

                /*int dataPagamento = 0;
                dataPagamento = Convert.ToInt32(e.Row.Cells[9].Text.ToString());*/

                foreach (TableCell cell in e.Row.Cells)
                {



                    if (contemParcelamentoCarne > 0)
                    {
                        cell.BackColor = Color.Yellow;
                    }


                    if (listaSpc > 0)
                    {
                        cell.BackColor = Color.Black;
                        cell.ForeColor = Color.Red;
                    }


                    /*if (StatusEntrega == "Entregue")
                    {
                        cell.BackColor = Color.Green;
                        cell.ForeColor = Color.White;
                    }*/
                    else
                    {
                        //cell.BackColor = Color.Blue;
                    }

                }
            }
        }
    }
}
