﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NotaFiscalCompra.aspx.cs" Inherits="Web.NotaFiscalCompra" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	
<script type="text/javascript">

    function closeModalMensagem() {
        $('#myModalMensagem').modal('hide');
    }

    function openModalMensagem() {
        $('#myModalMensagem').modal('show');
    }
</script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
      <hr />

       <asp:Panel ID="panelBotoes" runat="server" Visible="false">
          <div class="container-full">
            <asp:Button ID="btnListarNotas" class="btn btn-primary" runat="server" Text="Listar" />
            <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Incluir Nota Fiscal de Entrada" OnClick="btnNovo_Click"  />
         </div>
        </asp:Panel>




<!--FILTRO -->
<hr />
<asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-full">
    <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-2">
                    <label for="txtCodigo">Código</label>
                    <asp:TextBox class="form-control  input-sm" ID="txtCodigo" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtDE_descricao">Número da NF</label>
                    <asp:TextBox class="form-control  input-sm" TextMode="Search" ID="txt_numeroNotaFiscalEntrada" runat="server"></asp:TextBox>   
                </div>
                <div class="col-sm-5">
                    <label for="txtDE_descricao">Fornecedor</label>
                    <asp:TextBox class="form-control  input-sm" TextMode="Search" ID="txt_nomeFornecedor" runat="server"></asp:TextBox>   
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="btnPesquisar" class="btn btn-info" runat="server" Text="Pesquisar"/>
                </div>
            </div>
        </div>
    </div>
    
 </asp:Panel>
<!-------------------------------------------------------->
<!-- INICIO CADASATRO ------------------------------------->
<asp:Panel ID="panelCadastro" runat="server" Visible="false">
        <div class="container-full">
            <asp:Button ID="btnVoltar" class="btn btn-warning" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
            <h2>Nota de Entrada</h2>
                    <p>Identificação da Nota -   <asp:Label ID="lblMVC" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label></p>
            

        <!-- -->
         <!-- DADOS BASICOS-->
                <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">Dados Básicos</div>
                      <div class="panel-body">
                          <!-- Início da Linha -->
                          <div class="row"> 
                               <div class="col-md-1">
                                    <asp:Label ID="lblId" runat="server" Text="ID: ">
                                       <asp:TextBox class="form-control  input-sm" ID="txtIdCadastro" ReadOnly="true" runat="server"></asp:TextBox>
                                    </asp:Label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lblChaveNfe" runat="server" Text="Chave: ">
                                      <asp:TextBox class="form-control  input-sm" ID="txtChaveNfeCadastro" TextMode="Search"  runat="server"></asp:TextBox>
                                    </asp:Label>
                                </div>
							  <div class="col-md-1">
                                          <asp:Label ID="Label9" runat="server" Text="Better: ">
                                            <asp:Button ID="btnBuscarDadosBetter" class="btn btn-default btn-sm" runat="server" Text="BUSCAR" OnClick="btnBuscarDadosBetter_Click" />
											  </asp:Label>
                                </div>
                               <div class="col-md-2">
                                    <asp:Label ID="lblNumeroNotaCadastro" runat="server" Text="Número: ">
                                      <asp:TextBox class="form-control  input-sm" ID="txtNumeroNotaCadastro"   runat="server"></asp:TextBox>
                                    </asp:Label>
                                </div>
                               <div class="col-md-2">
                                          <asp:Label ID="lblDataEmissaoCadastro" runat="server" Text="Data Emissão: ">
                                            <asp:TextBox  class="form-control  input-sm" ID="txtDataEmissaoCadastro"  TextMode="Date"  runat="server"></asp:TextBox>
                                           </asp:Label>
                                </div>
                              <div class="col-md-2">
                                          <asp:Label ID="lblDataEntradaCadastro" runat="server" Text="Data Entrada: ">
                                            <asp:TextBox  class="form-control  input-sm" ID="txtDataEntradaCadastro"  TextMode="Date"  runat="server"></asp:TextBox>
                                           </asp:Label>
                                </div>
                              
                           </div> <!-- Fim da Linha -->
                          <!-- Início da Linha -->
                           <div class="row"> 
                                <div class="col-md-3">
                                   <asp:Label ID="lblCfopEntradaCadastro" runat="server" Text="CFOP Entrada: ">
                                       <asp:DropDownList class="form-control  input-sm" ID="ddlCfopEntradaCadastro" runat="server" />
                                   </asp:Label>
                                </div>
                               <div class="col-md-1">
                                    <asp:Label ID="lblSerieCadastro" runat="server" Text="Série: ">
                                      <asp:TextBox class="form-control  input-sm" ID="txtSerieCadastro"   runat="server"></asp:TextBox>
                                    </asp:Label>
                                </div>
                               <div class="col-md-2">
                                   <asp:Label ID="lblModeloCadastro" runat="server" Text="Modelo: ">
                                       <asp:DropDownList class="form-control  input-sm" ID="ddlModeloCadastro" runat="server"  />
                                   </asp:Label>
                               </div> 
                               <div class="col-md-2">
                                   <asp:Label ID="lblFormaPagamentoCadastro" runat="server" Text="Forma de Pagamento: ">
                                       <asp:DropDownList class="form-control  input-sm" ID="ddlFormaPagamentoCadastro" runat="server"  />
                                   </asp:Label>
                               </div> 
                               <div class="col-md-4">
                                   <asp:Label ID="lblFornecedorCadastro" runat="server" Text="Fornecedor: ">
                                       <asp:DropDownList class="form-control  input-sm" ID="ddlFornecedorCadastro" runat="server"  />
                                   </asp:Label>
                               </div> 
                               

                           </div><!-- Fim da Linha -->
                           <!-- Início da Linha -->
                           <div class="row"> 
                    
                            <div class="col-md-2">
                                    <asp:Label ID="lblBaseCalcICMSCadastro" runat="server" Text="Base Cálculo ICMS: ">
                                        <asp:TextBox  class="form-control  input-sm" ID="txtBaseCalcICMSCadastro" runat="server"></asp:TextBox>
                                    </asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblValorICMSCadastro" runat="server" Text="Valor do ICMS: ">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtValorICMSCadastro" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblBaseCalcICMSSubCadastro" runat="server" Text="Base Calc. ICMS. Sub: ">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtBaseCalcICMSSubCadastro" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblValorICMSSubCadastro" runat="server" Text="Valor ICMS Sub.: ">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtValorICMSSubCadastro" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-md-4">
                                   <asp:Label ID="lblTransportadoraCadastro" runat="server" Text="Transportadora: ">
                                       <asp:DropDownList class="form-control  input-sm" ID="ddlTransportadoraCadastro" runat="server" />
                                   </asp:Label>
                               </div> 
                           </div><!-- Fim da Linha -->
                           <!-- Início da Linha -->
                           <div class="row"> 
                    
                            <div class="col-md-2">
                                    <asp:Label ID="lblValorFreteCadastro" runat="server" Text="Valor do Frete: ">
                                        <asp:TextBox  class="form-control  input-sm" ID="txtValorFreteCadastro" runat="server"></asp:TextBox>
                                    </asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblValorSeguroCadastro" runat="server" Text="Valor do Seguro: ">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtValorSeguroCadastro" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblValorOutrasDespesas" runat="server" Text="Outras Despesas">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtValorOutrasDespesas" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblValorIPICadastro" runat="server" Text="Valor IPI: ">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtValorIPICadastro" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                               <div class="col-md-2">
                                <asp:Label ID="lblValorTotalProdutosCadastro" runat="server" Text="Valor Total dos Produtos: ">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtValorTotalProdutosCadastro" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lblValorTotalNotaCadastro" runat="server" Text="Valor Total da Nota: ">
                                    <asp:TextBox  class="form-control  input-sm" ID="txtValorTotalNotaCadastro" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                           </div><!-- Fim da Linha -->
                          <!------------------ -->

                        </div>
                  </div>
                 </div>
            </div>
        <!-- -->
	 <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">Itens da Nota Fiscal</div>
                      <div class="panel-body">
						  <div class="row">
							<div class="col-md-1">
                                    <asp:Label ID="Label1" runat="server" Text="ID Produto: ">
                                       <asp:TextBox class="form-control  input-sm" ID="txtIdProdutoNota" ReadOnly="true" runat="server"></asp:TextBox>
                                    </asp:Label>
                                </div>
                                <div class="col-md-5">
                                    <asp:Label ID="Label2" runat="server" Text="Produto: ">
                                      <asp:TextBox class="form-control  input-sm" ID="txtProdutoNota" TextMode="Search"  runat="server"></asp:TextBox>
                                    </asp:Label>
                                </div>
							  
                               <div class="col-md-1">
                                    <asp:Label ID="Label3" runat="server" Text="Qtd: ">
                                      <asp:TextBox class="form-control  input-sm" ID="txtQuantidadeProdutoNota"   runat="server"></asp:TextBox>
                                    </asp:Label>
                                </div>
							  <div class="col-md-1">
                                          <asp:Label ID="Label8" runat="server" Text="Estoque: ">
                                            
											  <asp:DropDownList  class="form-control  input-sm" ID="ddlLocalEstoque" runat="server"></asp:DropDownList>
                                           </asp:Label>
                                </div>
                               <div class="col-md-1">
                                          <asp:Label ID="Label4" runat="server" Text="R$ Unitário: ">
                                            <asp:TextBox  class="form-control  input-sm" ID="txtPrecoUnitario"    runat="server"></asp:TextBox>
                                           </asp:Label>
                                </div>
                              <div class="col-md-1">
                                          <asp:Label ID="Label5" runat="server" Text="R$ SubTotal: ">
                                            <asp:TextBox  class="form-control  input-sm" ID="TextBox1"    runat="server"></asp:TextBox>
                                           </asp:Label>
                                </div>
							  <div class="col-md-1">
                                          <asp:Label ID="Label6" runat="server" Text="R$ Desc.: ">
                                            <asp:TextBox  class="form-control  input-sm" ID="TextBox2"    runat="server"></asp:TextBox>
                                           </asp:Label>
                                </div>
							  <div class="col-md-1">
                                          <asp:Label ID="Label7" runat="server" Text="Ações: ">
                                            <asp:Button ID="btnIncluirProdutoNota" class="btn btn-default btn-sm" runat="server" Text="INCLUIR" />
                                           </asp:Label>
                                </div>
                              
						  </div>
					   </div>
				</div>
		 </div>
        <!-- -->
             <!-- AÇÔES-->
                <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">Ações</div>
                      <div class="panel-body">
                          <div class="row">
                           <div class="col-sm-12">
                            <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click" />
                            <asp:Button ID="btnAtualizar" class="btn btn-primary" runat="server" Text="Atualizar" OnClick="btnAtualizar_Click" />
                            <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
                            <asp:Button ID="btnLimparCampos" class="btn btn-default" runat="server" Text="Limpar Campos" />
                            <asp:Label ID="lblMensagemAcoes" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
                        </div>  
                        </div>
                      </div>
                  </div>
                </div>
        <!-- -->

         </div>
</asp:Panel>

<!-- FIM CADASTRO ----------------------------------------->
 <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

<!--INICIO GRIDVIEW ----------------------------------------------------->
 <asp:Panel ID="panelGrid" runat="server" Visible="true">
     <div class="well well-sm"><b>Total de Notas de Entrada ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

    <h3>Lista de Notas de Entrada</h3>
    <asp:GridView id="MyGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" OnSelectedIndexChanged="MyGridView_SelectedIndexChanged"
         OnSelectedIndexChanging="MyGridView_SelectedIndexChanging" 
        AutoGenerateColumns="False" OnRowDataBound="MyGridView_RowDataBound" >
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="NumeroNotaFiscalEntrada" HeaderText="NF." >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Fornecedor" HeaderText="Fornecedor" />
            <asp:BoundField DataField="Transportadora" HeaderText="Transportadora" />
            <asp:BoundField DataField="dtEmissao" HeaderText="Dt.Emissão" />
            <asp:BoundField DataField="dtEntrada" HeaderText="Dt.Entrada" />
            <asp:BoundField DataField="ValorTotalProdutos" HeaderText="Vlr.Tot.Prod." />
            <asp:BoundField DataField="ValorTotalNota" HeaderText="Vlr.Total.NF" />
              
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" OnClick="ImageButton1_Click" Width="20px" />
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                 <ItemTemplate>
                       <asp:Button  class="btn btn-primary btn-xs" ID="btnDetalharGrid" runat="server"  Text="Detalhes" OnClick="btnDetalharGrid_Click" />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                
                  <ItemTemplate>
                    <asp:Button  class="btn btn-warning btn-xs" ID="btnEditaGridr" runat="server" Text="Editar" OnClick="btnEditaGridr_Click" />
                </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField>
                
                  <ItemTemplate>
                    <asp:Button  class="btn btn-danger btn-xs" ID="btnCancelarGrid" runat="server" Text="Cancelar" OnClick="btnCancelar_Click" />
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
   </asp:Panel>
<!--FIM    GRIDVIEW ----------------------------------------------------->

      </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>