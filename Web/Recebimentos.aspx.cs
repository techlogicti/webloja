﻿using AcessoBancoDados;
using Negocios;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Recebimentos : System.Web.UI.Page
    {

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        private DALConexao conexao;
        Util util = new Util();
        BLLOperador bllOperador = null;

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }


        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
               // lblMensagem.Text = "Não foi possível conectar a base de dados.";
                //openModalMensagem();
            }

            return ds;


        }

        protected void pesquisar()
        {
            string strDE_descricao = "";

                atualizaGrid(strDE_descricao);
            
        }

        public void atualizaGrid(string filtro)
        {

            // Declare the query string.
            String queryString = " select [ID] as ID, NumeroPedido as Pedido, VL_valorConta as ValorConta, VL_ValorRecebido as ValorRecebido, DataRecebimento as DataRecebimento, FPag as FormaPagamento, DataVencimento, NumeroParcela as NumeroParcela, QuantidadeParcela as QuantidadeParcela";
            queryString = queryString + " from vw_recebimentos where FPag in('Dinheiro','Carnê Loja','Cheque') order by DataAtualizacaoUSA desc";


            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                RecebimentoGridView.DataSource = ds;
                RecebimentoGridView.DataBind();
            }
            else
            {
                //lblMensagem.Text = "Não foi possível conectar a base de dados.";
                //openModalMensagem();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            testCheckUserSoberana();
            connectionString = util.conexao();

            pesquisar();

            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {


                panelGrid.Visible = true;

                
                String queryString = " select [ID] as ID, NumeroPedido as Pedido, VL_valorConta as ValorConta, VL_ValorRecebido as ValorRecebido, DataRecebimento as DataRecebimento, FPag as FormaPagamento, DataVencimento, NumeroParcela as NumeroParcela, QuantidadeParcela as QuantidadeParcela";
                queryString = queryString + " from vw_recebimentos where FPag in('Dinheiro','Carnê Loja','Cheque') order by DataAtualizacaoUSA desc";

                // Run the query and bind the resulting DataSet
                // to the GridView control.
                DataSet ds = GetData(queryString);
                if (ds.Tables.Count > 0)
                {
                    RecebimentoGridView.DataSource = ds;

                    RecebimentoGridView.DataBind();

                    //lblTotal.Text = RecebimentoGridView.Rows.Count.ToString();
                }
                else
                {
                    //lblMensagem.Text = "Não foi possível conectar a base de dados.";
                    //openModalMensagem();
                }
            }
        }
    }



        
    }
