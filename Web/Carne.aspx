﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Carne.aspx.cs" Inherits="Web.Carne" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">

    <ContentTemplate>
    <hr />
     <hr />
    <asp:Panel ID="panelBotoes" runat="server" Visible="false">
     <div class="container-fluid">
         <asp:Button ID="btnListarCarne" class="btn btn-primary" runat="server" Text="Listar Carnês" OnClick="btnListarCarne_Click" />
         <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Novo Carnê" OnClick="btnNovo_Click"  />
     </div>
    </asp:Panel>

     <hr />

    <asp:Panel ID="panelCadastro" runat="server" Visible="false">
     
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                 <div class="container-full">
                    <asp:Button ID="btnVoltar" class="btn btn-warning" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
                    
                     <h2>Cadastro de Carnê</h2>
                      <p>Preencha o formulário abaixo para registrar o Pedido de Venda no Sistema. Somente após todo o preenchimento a venda será realizada.</p>
                      <div class="panel-group">
                        <div class="panel panel-default">
                          <div class="panel-heading">Identificação do Carnê</div>
                          <div class="panel-body">

                               <div class="row">
                                    <div class="col-sm-3">
                                        aa
                                    </div>
                               </div>
                           </div>
                           </div>
                          </div>
                 </div>
            </ContentTemplate>
        </asp:UpdatePanel>


    </asp:Panel>

    <asp:Panel ID="panelDetalhe" runat="server" Visible="false">
        Detalhe do Carnê
    </asp:Panel>

    <asp:Panel ID="panelFiltro" runat="server" Visible="false" BackColor="#C8C8C8" CssClass="container-fluid">
    </asp:Panel>

      <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>

     <div class="well well-sm"><b>Total de Carnês ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

      <asp:Panel ID="panelGrid" runat="server" Visible="false">Detalhe do Pedido de Compra
     
                 <h3>Lista de Carnês</h3>

            <asp:GridView id="CarnesGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" AutoGenerateColumns="False" >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="NumeroPedido" HeaderText="Núm.Pedido" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" />
            <asp:BoundField DataField="DataCompra" HeaderText="Data Pedido" />
            <asp:BoundField DataField="NumeroParcela" HeaderText="Nº Parcela" />
            <asp:BoundField DataField="ValorParcela" HeaderText="Valor Parcela" />
            <asp:BoundField DataField="DataVencimento" HeaderText="Data Vencimento" />
            <asp:BoundField DataField="ValorCompra" HeaderText="Valor Compra" />
            <asp:BoundField DataField="QtdParcelas" HeaderText="Qtde.Parcelas" />
            
            <asp:BoundField DataField="StatusCarne" HeaderText="StatusCarne" />

            <asp:CommandField ShowSelectButton="false" />

            <asp:TemplateField>
                 <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnDetalhar" runat="server"  OnClick="BtnDetalhar_Click" Text="Detalhes" />
                      </ItemTemplate>
            </asp:TemplateField>

        </Columns>
                   </asp:GridView>

      </asp:Panel>

     </ContentTemplate>
        </asp:UpdatePanel>

</asp:Content>