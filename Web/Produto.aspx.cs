﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Produto : System.Web.UI.Page
    {

        private DALConexao conexao;

        string connectionString = "";

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        string sqlDepartamento = "select ID, NM_NOME from tb_departamento where st_situacaoregistro='A' order by nm_nome asc";
        string sqlMarca = "select ID, NM_NOME from tb_marca where st_situacaoregistro='A' order by nm_nome asc";
        string sqlCategoria = "select ID, NM_NOME from tb_categoria where st_situacaoregistro='A' and idpai=0 order by nm_nome asc";
        string sqlSubCategoria = "";

        string sqlGrupoProduto = "select ID, NM_NOME from tb_grupoProduto where st_situacaoregistro='A' order by nm_nome asc";
        string sqlUnidadeVenda = "select ID, NM_NOME from tb_tipoUnidade where st_situacaoregistro='A' order by nm_nome asc";
        string sqlTipoEstoque = "select ID, NM_NOME from tb_tipoEstoque where st_situacaoregistro='A' order by nm_nome asc";

        int filtroSaldo = 0;

        Util util = new Util();
        BLLOperador bllOperador = null;


        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            testCheckUserSoberana();
            btnCancelar.Attributes.Add("onClick", "javascript:history.back(); return false;");
            btnVoltar.Attributes.Add("onClick", "javascript:history.back(); return false;");

           

            
            connectionString = util.conexao();

            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                panelBotoes.Visible = true;
                panelFiltro.Visible = true;

                preencheCombo(ddlDepartamento,sqlDepartamento, "S");
                preencheCombo(ddlMarca,sqlMarca, "S");
                preencheCombo(ddlCategoria,sqlCategoria, "S");


                if (chkbSaldo.Checked)
                {
                    filtroSaldo = 1;
                }else
                {
                    filtroSaldo = 0;
                }

                // Declare the query string.
                //String queryString = "select [IDProduto] as 'Id', [CODIGO] AS 'Código', [NM_nome] AS 'Produto', [DE_descricao] as 'Descrição', [precoVenda] as 'Etiqueta' , [precoCompra] as 'PC', [precoCompraImposto] as 'PCIF', [precoMinimo] as 'P.Mín', [precoAtacado] as 'P.Atacado', [estoqueFinal] as 'Qtde'  from [SoberanaLocal].[dbo].[TB_produto]";

                String queryString = "select * from vw_produtosEstoque ";

                if (filtroSaldo > 0)
                {
                    queryString = queryString + " and Saldo > 0 ";
                }

                queryString += " order by Produto asc ";

                
            }
        }

        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "3 - Não foi possível conectar a base de dados. [ " + dataHoje + " - " + horaHoje + " ]";

            }

            return ds;

        }

        //public void preencheCombo(DropDownList comboBox, string queryString) {
        public void preencheCombo(DropDownList comboBox, string queryString, string todos)

        { 

            DataSet ds = GetData(queryString);

            if (ds.Tables.Count > 0)
            {
                comboBox.Items.Clear();
                comboBox.DataSource = ds;
                comboBox.DataTextField = "NM_nome";
                comboBox.DataValueField = "ID";
                comboBox.DataBind();
                  if (todos.Equals("S"))
                {
                    ListItem l = new ListItem("-", "-", true);
                    l.Selected = true;
                    comboBox.Items.Add(l);
                }   
}
            else
            {
                
                lblMensagem.Text = "4 - Não foi possível conectar a base de dados. [ " + dataHoje + " - " + horaHoje + " ] "+ comboBox.ID + " - "+ queryString.ToString();
            }

        }
    

      

        public void atualizaGrid(
            string descricaoProduto,
            string codigoProduto, 
            int idCategoria, 
            int idSubCategoria,
            int idDepartamento,
            int idMarca,
            int saldoEstoque) {

            // Declare the query string.
            /*String queryString =
              " SELECT p.ID, P.[codigo] as Codigo ,D.NM_nome as Departamento ,C.NM_nome as Categoria " +
                              " ,C2.NM_nome as SubCategoria ,P.[NM_nome] as Produto " +
                              " ,P.[precoVenda] as Venda ,P.[precoMinimo] as Minimo, P.[precoAtacado] as Atacado, P.[precoCompra] Compra, P.[precoAtacado] as Atacado" +
                              ", P.[estoqueAtual] as Quantidade, " +
                                "P.[estoqueAtacado] as Deposito, 0 as Reservado FROM [TB_produto] P, [TB_departamento] " +
                                " D, TB_categoria c, TB_categoria c2 " +
                                " where d.ID = p.id_departamento " +
                                " and p.[id_categoria] = c.ID" +
                                " and p.id_subcategoria = c2.ID and 1=1 ";*/

            String queryString = "select * from vw_produtosEstoque where 1 = 1";  

            if (!descricaoProduto.Equals(""))
            {
                //queryString = queryString + " and P.[DE_descricao] like '%" + descricaoProduto.ToString() + "%'";
                queryString = queryString + " and Produto like '%" + descricaoProduto.ToString() + "%'";
            }

            if (!codigoProduto.Equals(""))
            {
                queryString = queryString + " and codigo like '%" + codigoProduto.ToString() + "%'";
            }

            if (idDepartamento > 0)
            {
                queryString = queryString + " and id_departamento = '" + idDepartamento.ToString() + "'";
            }

            if (idMarca > 0)
            {
                queryString = queryString + " and id_marca = '" + idMarca.ToString() + "'";
            }

            if (idCategoria != 65)
            {
                //queryString = queryString + " and P.[id_categoria] = '" + idCategoria.ToString() + "'";
                queryString = queryString + " and id_categoria = '" + idCategoria.ToString() + "'";

                sqlSubCategoria = "select ID, NM_NOME from tb_categoria where st_situacaoregistro='A' and 1=1 ";

                sqlSubCategoria += " and IDPai =  "+ idCategoria + "";

                sqlSubCategoria += " order by  NM_nome asc";

                preencheCombo(ddlSubCategoria, sqlSubCategoria,"S");

                if (idSubCategoria > 0)
                {
                    //queryString = queryString + " and P.[id_subcategoria] = '" + idSubCategoria.ToString() + "'";
                    queryString = queryString + " and id_subcategoria = '" + idSubCategoria.ToString() + "'";
                }

            }
            

            if (saldoEstoque > 0)
            {
                queryString = queryString + " and Saldo > 0 ";
            }

            //queryString += " order by  P.[NM_nome] asc";
            queryString += " order by Produto asc";

            

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                MyGridView.DataSource = ds;
                MyGridView.DataBind();

                lblTotal.Text = MyGridView.Rows.Count.ToString();
            }
            else
            {
                string msg = "5 - Não foi possível conectar a base de dados. [ " + dataHoje + " - " + horaHoje + " ] = " + queryString;
                lblMensagem.Text = msg;
                this.gravarLog("Execução do sql: " + msg.ToString());
            }

        }
        public void gravarLog(String texto)
        {
            // Compose a string that consists of three lines.
            string lines = texto.ToString().Trim();

            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\temp\\Soberana-Produto-" + dataHoje + "-" + horaHoje + ".txt");
            file.WriteLine(lines);

            file.Close();
        }


        protected void txtDE_descricao_TextChanged(object sender, EventArgs e)
        {
            executaBusca();
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Server.Transfer("Produto.aspx", true);

        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            exibeMVC("", "I", "1");
        }

        public string recuperaProximoIdProduto()
        {

            string sql = "select (max(id) + 1) as ID from tb_produto";
            string proximoId = "";


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_produto");

            foreach (DataRow pRow in customerOrders.Tables["tb_produto"].Rows)
            {

                proximoId = pRow["ID"].ToString();


            }

            return proximoId;

        }

        public void exibeMVC(string id, string modoMVC, string usuario)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;

            if (modoMVC.Equals("I"))
            {
                lblModo.Text = "Modo de inclusão";

                btnSalvar.Visible = true;
                btnAtualizar.Visible = false;
                btnLimparCampos.Visible = true;
                btnCancelar.Visible = true;

                string proximoIdProduto = "";

                proximoIdProduto = recuperaProximoIdProduto();

                txtId.Text = proximoIdProduto.ToString();


                txtEstoqueAtualCadastro.Text = "0";

                preencheCombo(ddlDepartamentoCadastro, sqlDepartamento, "S");
                preencheCombo(ddlMarcaCadastro, sqlMarca, "S");
                preencheCombo(ddlGrupoProduto, sqlGrupoProduto, "S");
                preencheCombo(ddlCategoriaCadastro, sqlCategoria, "S");
                //SUbCategoria Carrega no momento da Escolha da Categoria
                preencheCombo(ddlTipoEstoqueCadastro, sqlTipoEstoque, "S");
                preencheCombo(ddlUnidadeVendaCadastro, sqlUnidadeVenda, "S");

                //Fim-Temporario

            }
            else if (modoMVC.Equals("A"))
            {
                lblModo.Text = "Modo de edição";
                btnSalvar.Visible = false;
                btnAtualizar.Visible = true;
                btnLimparCampos.Visible = false;
                btnCancelar.Visible = true;

                //camposProdutosSomenteLeitura("N");

                if (!id.Equals(""))
                {
                    recuperarDadosProdutoId(Convert.ToInt32(id));
                    //recuperaAutorizacoes(Convert.ToInt32(id));
                }
                txtEstoqueAtualCadastro.Text = "0";
            }
            else if (modoMVC.Equals("V"))
            {
                lblModo.Text = "Modo de visualização";
                btnSalvar.Visible = false;
                btnAtualizar.Visible = false;
                btnLimparCampos.Visible = false;
                btnCancelar.Visible = true;
            }
            else
            {
                lblModo.Text = "Modo ?";
            }
        }

        public void recuperarDadosProdutoId(int id)
        {


            //Preenche combos
            preencheCombo(ddlMarcaCadastro, sqlMarca, "S");
            preencheCombo(ddlDepartamentoCadastro, sqlDepartamento, "S");
            preencheCombo(ddlCategoriaCadastro, sqlCategoria, "S");
            

            
            preencheCombo(ddlGrupoProduto, sqlGrupoProduto, "S");
            preencheCombo(ddlTipoEstoqueCadastro, sqlTipoEstoque, "S");
            preencheCombo(ddlUnidadeVendaCadastro, sqlUnidadeVenda, "S");

            //Depois com os Ids, seleciona o desejado

            ModeloProduto modeloProduto = new ModeloProduto();
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLProduto bLLProduto = new BLLProduto(conexao);

            modeloProduto = bLLProduto.CarregarModeloProduto(id);

            //Linha 1 
            txtId.Text = modeloProduto.id.ToString();
            txtCodigoEANCadastro.Text = modeloProduto.codigoEAN.ToString();
            txtNomeProdutoCadastro.Text = modeloProduto.nmNome.ToString();
            txtDescricaoProdutoCadastro.Text = modeloProduto.deDescricao.ToString();

            //Linha 2 - 
            ddlMarcaCadastro.SelectedValue = modeloProduto.idMarca.ToString();
            ddlDepartamentoCadastro.SelectedValue = modeloProduto.idDepartamento.ToString();
            ddlCategoriaCadastro.SelectedValue = modeloProduto.idCategoria.ToString();
            //sub categoria
            preencheComboSubCategoriaCadastro(modeloProduto.idCategoria,modeloProduto.idSubCategoria);
            ddlSubCategoriaCadastro.SelectedValue = modeloProduto.idSubCategoria.ToString();
            //ddlSubCategoriaCadastro.SelectedValue = modeloProduto.idSubCategoria.ToString();
            //

            //Linha 3 - 
            txtCodigoNCMCadastro.Text = modeloProduto.codigoNCM.ToString();
            ddlGrupoProduto.SelectedValue = modeloProduto.idGrupo.ToString();
            ddlUnidadeVendaCadastro.SelectedValue = modeloProduto.idUnidadeMedida.ToString();
            ddlTipoEstoqueCadastro.SelectedValue = modeloProduto.idTipoEstoque.ToString();

            //Linha 4 - Valores //incluir formatação do preço para apresentação
            txtPrecoCompraCadastro.Text = modeloProduto.precoCompra.ToString();
            txtPrecoCifCadastro.Text = modeloProduto.precoCompraImposto.ToString();
            txtPrecoMinimoCadastro.Text = modeloProduto.precoMinimo.ToString();
            txtPrecoAtacadoCadastro.Text = modeloProduto.precoAtacado.ToString();
            txtPrecoEtiquetaCadastro.Text = modeloProduto.precoVenda.ToString();
            txtCESTCadastro.Text = modeloProduto.cest.ToString();

            //Linha 5 - Valores
            txtEstoqueMinimoCadastro.Text = modeloProduto.estoqueMinimo.ToString();
            txtEstoqueMaximoCadastro.Text = modeloProduto.estoqueMaximo.ToString();
            //txtEstoqueAtualCadastro.Text = bLLProduto.ObtemSaldoAtualProdutoPorId(id).ToString();
            txtPISCadastro.Text = modeloProduto.pis.ToString();
            txtCOFINSCadastro.Text = modeloProduto.cofins.ToString();
            txtICMSCadastro.Text = modeloProduto.icms.ToString();

            txtObservacoes.Text = modeloProduto.DE_observacoes.ToString();
        }

        protected void btnListarProdutos_Click(object sender, EventArgs e)
        {
            
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
            panelBotoes.Visible = true;

        }

        protected void Submit(object sender, EventArgs e)
        {
            //Salvar
            //Label1.Text = TextBox1.Text + " " + TextBox2.Text + " " + TextBox3.Text;
        }

        protected void MyGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the currently selected row using the SelectedRow property.
            GridViewRow row = MyGridView.SelectedRow;

            // Display the first name from the selected row.
            // In this example, the third column (index 2) contains
            // the first name.
           // lblMensagem.Text = "You selected " + row.Cells[2].Text + ".";

            openModalFillData(Convert.ToInt32(row.Cells[0].Text));
        }

        protected void MyGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            // Get the currently selected row. Because the SelectedIndexChanging event
            // occurs before the select operation in the GridView control, the
            // SelectedRow property cannot be used. Instead, use the Rows collection
            // and the NewSelectedIndex property of the e argument passed to this 
            // event handler.
            GridViewRow row = MyGridView.Rows[e.NewSelectedIndex];

            // You can cancel the select operation by using the Cancel
            // property. For this example, if the user selects a customer with 
            // the ID "ANATR", the select operation is canceled and an error message
            // is displayed.
            if (row.Cells[1].Text == "ANATR")
            {
                e.Cancel = true;
                lblMensagem.Text = "You cannot select " + row.Cells[2].Text + ".";
            }
        }

        

        public void openModalFillData(int idSelecionado) {
            this.recuperaDadosModal(idSelecionado);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }

        public void openModalImagensProduto(int idSelecionado)
        {
            this.listarImagensProdutos(idSelecionado);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalImagensProduto();", true);
        }

        

        public void recuperaDadosModal(int id) {

            txtModalId.Text = id.ToString();
            txtModalCodigo.Text = "11";
            txtModalDepartamento.Text = "111";
            txtModalCategoria.Text = "1111";
            txtModalSubcategoria.Text = "11111";
            txtModalMarca.Text = "2";
            txtModalProduto.Text = "22";
            txtModalPrecoVenda.Text = "222";
            txtModalPrecoCompra.Text = "2222";
            txtModalCif.Text = "3";
            txtModalPrecoMinimo.Text = "33";
            txtModalPrecoAtacado.Text = "333";
            txtModalQuantidade.Text = "3333";
            txtModalAtacado.Text = "33333";
            txtModalObservacoes.Text = "-";
        }


        public void atualizaGridComFiltro()
        {
            //recupera filtros

            string descricaoProduto = "";
            string codigoProduto = "";
            int idCategoria = 0;
            int idSubCategoria = 0;
            int idDepartamento = 0;
            int idMarca = 0;
            int saldoEstoque = 0;

            if (chkbSaldo.Checked)
            {
                saldoEstoque = 1;
            }
            else
            {
                saldoEstoque = 0;
            }

            if (!txtDE_descricao.Text.Trim().Equals(""))
            {
                descricaoProduto = txtDE_descricao.Text.Trim();
            }else
            {
                descricaoProduto = "";
            }

            if (!txtCodigo.Text.Trim().Equals(""))
            {
                codigoProduto = txtCodigo.Text.Trim();
            }
            else
            {
                codigoProduto = "";
            }


            
            


            if (ddlCategoria.SelectedValue != "-")
            {
                idCategoria = Convert.ToInt32(ddlCategoria.SelectedValue);
            }
            else
            {
                idCategoria = 65; // -
            }

            /*if (ddlSubCategoria.SelectedValue != "-")
            {
                idSubCategoria = Convert.ToInt32(ddlSubCategoria.SelectedValue);
            }
            else
            {
                idSubCategoria = 0;
            }*/
            idSubCategoria = 0;


            if (ddlDepartamento.SelectedValue != "-")
            {
                idDepartamento = Convert.ToInt32(ddlDepartamento.SelectedValue);
            }
            else
            {
                idDepartamento = 0;
            }

            if (ddlMarca.SelectedValue != "-")
            {
                idMarca = Convert.ToInt32(ddlMarca.SelectedValue);
            }
            else
            {
                idMarca = 0;
            }


            
            
            atualizaGrid(descricaoProduto, codigoProduto, idCategoria, idSubCategoria, idDepartamento, idMarca, saldoEstoque);

        }


        protected void btnPesquisar_Click(object sender, EventArgs e)
        {

            atualizaGridComFiltro();
        }

        public void recuperaFiltros() {
            //recupera filtros

            string descricaoProduto = txtDE_descricao.Text.Trim();
            string codigoProduto = txtCodigo.Text.Trim();
            int idCategoria = Convert.ToInt32(ddlCategoria.SelectedValue);
            int idSubCategoria = Convert.ToInt32(ddlSubCategoria.SelectedValue);
            int idDepartamento = Convert.ToInt32(ddlDepartamento.SelectedValue);
            int idMarca = Convert.ToInt32(ddlMarca.SelectedValue);
        }

        public void executaBusca()
        {
           
        }

      
        protected void btnDetalhar_Click(object sender, EventArgs e)
        {

            //Get the button that raised the event
            Button btn = (Button)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            int idProduto = 0;
            idProduto = Convert.ToInt32(gvr.Cells[0].Text);

            txtModalIdProdutoImagens.Text = idProduto.ToString();

            openModalImagensProduto(idProduto);
            
        }

        public string Get8CharacterRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path.Substring(0, 8);  // Return 8 character string
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            txtCodigoEANCadastro.Text = Get8CharacterRandomString().ToString();
            txtNomeProdutoCadastro.Text = Get8CharacterRandomString().ToString();
            txtDescricaoProdutoCadastro.Text = Get8CharacterRandomString().ToString();
            txtCodigoNCMCadastro.Text = Get8CharacterRandomString().ToString();


            preencheCombo(ddlDepartamentoCadastro, sqlDepartamento, "S");
            preencheCombo(ddlMarcaCadastro, sqlMarca, "S");
            preencheCombo(ddlCategoriaCadastro, sqlCategoria, "S");
            //preencheCombo(ddlSubCategoriaCadastro, sqlSubCategoria, "S");
            preencheCombo(ddlGrupoProduto, sqlGrupoProduto, "S");
            preencheCombo(ddlUnidadeVendaCadastro, sqlUnidadeVenda, "S");
            preencheCombo(ddlTipoEstoqueCadastro, sqlTipoEstoque, "S");


            txtPrecoCompraCadastro.Text = "10.50";
            txtPrecoCifCadastro.Text = "12.75";
            txtPrecoMinimoCadastro.Text = "24.90";
            txtPrecoAtacadoCadastro.Text = "19.90";
            txtPrecoEtiquetaCadastro.Text = "29.90";

            txtCESTCadastro.Text = "7";
            txtPISCadastro.Text = "5";
            txtCOFINSCadastro.Text = "2";
            txtICMSCadastro.Text = "8";

            txtEstoqueMinimoCadastro.Text = "5";
            txtEstoqueMaximoCadastro.Text = "18";
            txtEstoqueAtualCadastro.Text = "7";

        }

        public void preencheDDL(Object o, DropDownList ddl, string status) {

            
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            recuperaValoresTela("INSERT");
        }


        public void recuperaValoresTela(string acao)
        {


            //

            //

            ModeloProduto produto = new ModeloProduto();
            produto.id = Convert.ToInt32(txtId.Text.ToString());
            produto.codigoEAN = txtCodigoEANCadastro.Text.Trim();
            produto.nmNome = txtNomeProdutoCadastro.Text.Trim();
            produto.deDescricao = txtDescricaoProdutoCadastro.Text.Trim();
            produto.idMarca = Convert.ToInt32(ddlMarcaCadastro.SelectedValue);
            produto.idDepartamento = Convert.ToInt32(ddlDepartamentoCadastro.SelectedValue);
            produto.idCategoria = Convert.ToInt32(ddlCategoriaCadastro.SelectedValue);
            produto.idSubCategoria = Convert.ToInt32(ddlSubCategoriaCadastro.SelectedValue);
            produto.idGrupo = Convert.ToInt32(ddlGrupoProduto.SelectedValue);
            produto.idTipoEstoque = Convert.ToInt32(ddlTipoEstoqueCadastro.SelectedValue);
            produto.idUnidadeMedida = Convert.ToInt32(ddlUnidadeVendaCadastro.SelectedValue);
            produto.codigoNCM = txtCodigoNCMCadastro.Text.Trim();

            //primeira troca o . por "" e depois a "," por "."
            produto.precoCompra = txtPrecoCompraCadastro.Text.Trim().Replace(",", ".");
            produto.precoCompraImposto = txtPrecoCifCadastro.Text.Trim().Replace(",", ".");
            produto.precoMinimo = txtPrecoMinimoCadastro.Text.Trim().Replace(",",".");
            produto.precoAtacado = txtPrecoAtacadoCadastro.Text.Trim().Replace(",", ".");
            produto.precoVenda = txtPrecoEtiquetaCadastro.Text.Trim().Replace(",", ".");
            produto.estoqueMinimo = Convert.ToInt32(txtEstoqueMinimoCadastro.Text.Trim());
            produto.estoqueMaximo = Convert.ToInt32(txtEstoqueMaximoCadastro.Text.Trim());
            txtEstoqueAtualCadastro.Text = "0";
            produto.estoqueAtual = Convert.ToInt32(txtEstoqueAtualCadastro.Text.Trim());
            
            produto.cest = txtCESTCadastro.Text.Trim();
            produto.pis = Convert.ToInt32(txtPISCadastro.Text.Trim());
            produto.cofins = Convert.ToInt32(txtCOFINSCadastro.Text.Trim());
            produto.icms = Convert.ToInt32(txtICMSCadastro.Text.Trim());
            produto.status = "A";

            

            int idOperador = 0;
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            bllOperador = new BLLOperador(conexao);
            ModeloOperador modeloOperador = new ModeloOperador();
            if (!string.IsNullOrEmpty(Session["user"] as string))
            {
                modeloOperador = bllOperador.recuperaIdLogin(Session["user"].ToString());
                idOperador = Convert.ToInt32(modeloOperador.Id);
            }
            

            produto.CD_operador = idOperador;
            produto.DT_dataCriacao = 0; //não vai ser considerada na classe DAL
            produto.HR_horaCriacao = 0; //não vai ser considerada na classe DAL
            produto.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
            produto.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
            produto.ST_situacaoRegistro = "A";

            produto.DE_observacoes = txtObservacoes.Text.Trim();



            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            int idProdutoNovo = 0;

            BLLProduto bllProduto = new BLLProduto(conexao);

            if (acao.Equals("INSERT"))
            {
                
                idProdutoNovo = bllProduto.Incluir(produto);

                if (idProdutoNovo > 0)
                {
                    lblMensagem.Text = "Produto cadastrado com sucesso! ID = " + idProdutoNovo.ToString();

                    limpaCamposCadastro();
                }
                else
                {
                    lblMensagem.Text = "Produto não cadastrado. Favor olhar o log do sistema";
                }
            }
            else if (acao.Equals("UPDATE"))
            {
                int t = 0;
                t = bllProduto.Alterar(produto);

                if (t > 0)
                {
                    //lblMensagem.Text = "Produto alterado com sucesso!";
                    lblMensagem.Text = "Produto não alterado. Favor olhar o log do sistema";
                    //limpaCamposCadastro();
                }
                else
                {
                    lblMensagem.Text = "Produto alterado com sucesso!";
                    //lblMensagem.Text = "Produto não alterado. Favor olhar o log do sistema";
                }
            }
            else
            {

            }

            
        }

        public void limpaCamposCadastro()
        {
            txtCodigoEANCadastro.Text = "";
            txtNomeProdutoCadastro.Text = "";
            txtDescricaoProdutoCadastro.Text = "";
            preencheCombo(ddlMarca, sqlMarca, "S");
            preencheCombo(ddlDepartamentoCadastro, sqlDepartamento, "S");
            preencheCombo(ddlCategoriaCadastro, sqlCategoria, "S");
            preencheCombo(ddlGrupoProduto, sqlGrupoProduto, "S");
            preencheCombo(ddlTipoEstoqueCadastro, sqlTipoEstoque, "S");
            preencheCombo(ddlUnidadeVendaCadastro, sqlUnidadeVenda, "S");
            txtCodigoNCMCadastro.Text = "";
            txtPrecoCompraCadastro.Text = "";
            txtPrecoCifCadastro.Text = "";
            txtPrecoMinimoCadastro.Text = "";
            txtPrecoAtacadoCadastro.Text = "";
            txtPrecoEtiquetaCadastro.Text = "";
            txtCESTCadastro.Text = "";
            txtPISCadastro.Text = "";
            txtCOFINSCadastro.Text = "";
            txtICMSCadastro.Text = "";
            txtEstoqueMinimoCadastro.Text = "";
            txtEstoqueMaximoCadastro.Text = "";
            txtEstoqueAtualCadastro.Text = "0";
            txtObservacoes.Text = "";
        }


        protected void ddlCategoriaCadastro_SelectedIndexChanged(object sender, EventArgs e)
        {

            preencheComboSubCategoriaCadastro();

        }

        public void preencheComboSubCategoriaCadastro(int idCategoriaSelecionada, int idSubCategoriaSelecionada )
        {
            
            if (idCategoriaSelecionada != 65)
            {


                sqlSubCategoria = "select ID, NM_NOME from tb_categoria where st_situacaoregistro='A' and 1=1 ";

                sqlSubCategoria += " and IDPai =  " + idCategoriaSelecionada + "";

                sqlSubCategoria += " order by  NM_nome asc";

                preencheCombo(ddlSubCategoriaCadastro, sqlSubCategoria, "S");
                
            }
        }

        public void preencheComboSubCategoriaCadastro()
        {
            int idCategoriaSelecionada = 0;

            idCategoriaSelecionada = Convert.ToInt32(ddlCategoriaCadastro.SelectedValue);

            if (idCategoriaSelecionada != 65)
            {


                sqlSubCategoria = "select ID, NM_NOME from tb_categoria where st_situacaoregistro='A' and 1=1 ";

                sqlSubCategoria += " and IDPai =  " + idCategoriaSelecionada + "";

                sqlSubCategoria += " order by  NM_nome asc";

                preencheCombo(ddlSubCategoriaCadastro, sqlSubCategoria, "S");

            }
        }

        protected void ddlCategoriaCadastro_TextChanged(object sender, EventArgs e)
        {
            preencheComboSubCategoriaCadastro();
        }

        protected void btnLimparCampos_Click(object sender, EventArgs e)
        {
            limpaCamposCadastro();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Server.Transfer("Produto.aspx", true);
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            int idProduto = 0;
            idProduto = Convert.ToInt32(gvr.Cells[0].Text);


            //Abre página para edição do produto utilizando o ID do produto acionado
            exibeMVC(idProduto.ToString(), "A", "1");

            //

        }
        

        protected void btnExcluir_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            int idProduto = 0;
            idProduto = Convert.ToInt32(gvr.Cells[0].Text);


            // Verifica antes de excluir

            // Exclui produto
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLProduto bllProduto = new BLLProduto(conexao);

            bllProduto.Excluir(idProduto);

            atualizaGridComFiltro();

        }

        protected void MyGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //int saldoProduto = Convert.ToInt32(e.Row.Cells[8].Text.ToString());
                string c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10 = "";

                c0 = e.Row.Cells[0].Text.ToString();
                c1 = e.Row.Cells[1].Text.ToString();
                c2 = e.Row.Cells[2].Text.ToString();
                c3 = e.Row.Cells[3].Text.ToString();
                c4 = e.Row.Cells[4].Text.ToString();
                c5 = e.Row.Cells[5].Text.ToString();
                c6 = e.Row.Cells[6].Text.ToString();
                c7 = e.Row.Cells[7].Text.ToString();
                c8 = e.Row.Cells[8].Text.ToString();
                c9 = e.Row.Cells[9].Text.ToString();
                c10 = e.Row.Cells[10].Text.ToString();

                
                int saldoProduto = 0;

                saldoProduto = Convert.ToInt32(c10);

                foreach (TableCell cell in e.Row.Cells)
                {
                    if (saldoProduto < 0)
                    {
                        cell.BackColor = Color.DarkRed;
                        cell.ForeColor = Color.White;
                    }
                    else if (saldoProduto == 0)
                    {
                        cell.BackColor = Color.Yellow;
                        cell.ForeColor = Color.Black;
                    }
                    else
                    {
                        cell.BackColor = Color.White;
                        cell.ForeColor = Color.Black;
                    }

                    }
                }
            }

        protected void chkbSaldo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbSaldo.Checked)
            {
                filtroSaldo = 1;
            }
            else
            {
                filtroSaldo = 0;
            }
        }

        protected void btnAtualizar_Click(object sender, EventArgs e)
        {
            recuperaValoresTela("UPDATE");
        }

        protected void btnVerImagem_Click(object sender, ImageClickEventArgs e)
        {
            //Get the button that raised the event
            ImageButton btn = (ImageButton)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            //openModalVisualizaImagem(Convert.ToInt32(gvr.Cells[1].Text), "D", "N");

            string idProduto = "0";
            idProduto = txtModalIdProdutoImagens.Text.ToString();

            string url = "http://joao-pc/Img/produtos/";
            url = url + idProduto + "/" + gvr.Cells[0].Text;

            /*gravarAcesso("----------------------------------------------------------");
            gravarAcesso("Inicio - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("Clique - Abrir Imagem do Pedido " + gvr.Cells[1].Text);
            gravarAcesso(url.ToString());
            gravarAcesso("Fim - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("----------------------------------------------------------");*/

            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open('" + url + "', '_blank', 'height=600px,width=600px,scrollbars=1'); ", true);

            //
            openModalImagensProduto(Convert.ToInt32(idProduto));
        }


        public void listarImagensProdutos(int idProduto)
        {
            string caminho = "";
            caminho = "~/Img/Produtos/" + idProduto + "//";
            string[] filePaths = Directory.GetFiles(Server.MapPath(caminho));
            List<ListItem> files = new List<ListItem>();
            foreach (string filePath in filePaths)
            {
                string fileName = Path.GetFileName(filePath);
                files.Add(new ListItem(fileName, caminho + fileName));
            }
            GridViewImagensProduto.DataSource = files;
            GridViewImagensProduto.DataBind();
        }
    }
    }

