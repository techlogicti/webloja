﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PedidoCompra.aspx.cs" Inherits="Web.PedidoCompra" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">

    <ContentTemplate>
    <hr />
                 <asp:Panel ID="panelBotoes" runat="server" Visible="false">
    <div class="container-fluid">
        <asp:Button ID="btnListarPedidosCompra" class="btn btn-primary" runat="server" Text="Listar Pedidos de Compra" OnClick="btnListarPedidosCompra_Click" />
         <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Novo Pedido de Compra" OnClick="btnNovo_Click" />
     </div>
    </asp:Panel>

    <hr />

    <asp:Panel ID="panelCadastro" runat="server" Visible="false">
        
          <div class="container-fluid">
            <h2>Cadastro de Pedido de Compra</h2>
            <p>Preencha o formulário abaixo para registrar o Pedido de Venda no Sistema. Somente após todo o preenchimento a venda será realizada.</p>
              <div class="panel-group">
                <div class="panel panel-default">
                  <div class="panel-heading">Identificação do Pedido</div>
                  <div class="panel-body">


                      <!-- -->
                       <div class="row">
                <div class="col-md-3">
                     <asp:Label ID="lblNumeroPedidoInput" runat="server" Text="Núm.Pedido: ">
                    <asp:TextBox  class="form-control" ID="txtNumeroPedidoInput" runat="server"></asp:TextBox>
                    </asp:Label>
                </div>

                 <div class="col-md-3">
                      <asp:Label ID="lblDataPedidoInput" runat="server" Text="Data Pedido: ">
                         <asp:TextBox  class="form-control" ID="txtDataPedidoInput" TextMode="Date"  runat="server"></asp:TextBox>
                        
                          
                    </asp:Label>
                 </div>

                  <div class="col-md-6">
                     <asp:Label ID="lblFuncionarioInput"  runat="server" Text="Comprador: ">
                         <asp:DropDownList   class="form-control"  ID="ddlFuncionarioInput" runat="server"></asp:DropDownList>
                    </asp:Label>
                 </div>

             </div>

            <div class="row">

                 <div class="col-md-6">
                     <asp:Label ID="lblFornecedorInput" runat="server" Text="Fornecedor: ">
                         <asp:DropDownList   class="form-control"  ID="ddlFornecedorInput" runat="server"></asp:DropDownList>
                    </asp:Label>
                 </div>
               

            </div>

         <div class="row">
                 <div class="col-md-3">
                     <asp:Label ID="lblValorPedidoInput" runat="server" Text="Vlr.Pedido: ">
                        <asp:TextBox  class="form-control" ID="txtValorPedidoInput" runat="server" ClientIDMode="Static"></asp:TextBox>
                    </asp:Label>
                 </div>

                  <div class="col-md-3">
                      <asp:Label ID="lblValorDescontoInput" runat="server" Text="Vlr.Desconto: ">
                        <asp:TextBox  class="form-control" ID="txtValorDescontoInput" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>

              <div class="col-md-3">
                      <asp:Label ID="lblValorImpostoInput" runat="server" Text="Vlr.Imposto: ">
                        <asp:TextBox  class="form-control" ID="txtValoImpostoInput" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>

              <div class="col-md-3">
                      <asp:Label ID="lblValorFreteInput" runat="server" Text="Vlr.Frete: ">
                        <asp:TextBox  class="form-control" ID="txtValoFreteInput" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>

              </div>

                      <div class="row">
                            <div class="col-md-3">
                          <asp:Label ID="lblValorTotalInput" runat="server" Text="Vlr.Total: "> </asp:Label>
                            <asp:TextBox  class="form-control" ID="txtValorTotalInput" runat="server"></asp:TextBox>
                    
                          
                         </div>

                         <div class="col-md-3">
                              <asp:Label ID="lblQuantidadeItensInput" runat="server" Text="Quantidade: ">
                                <asp:TextBox class="form-control" ID="txtQuantidadeItensInput" runat="server"></asp:TextBox>
                            </asp:Label>
                         </div>

                          <div class="col-md-3">
                              <asp:Label ID="lblFormaPagamento" runat="server" Text="Forma de Pagamento: ">
                                <asp:TextBox class="form-control" ID="txtFormaPagamentoInput" runat="server"></asp:TextBox>
                            </asp:Label>
                         </div>
                      
                      </div>

                      <!-- -->
                  </div>
                </div>
              </div>
          

         <div class="panel panel-default">
      <div class="panel-heading">Observações</div>
      <div class="panel-body">
          <div class="row">

                <div class="col-md-12">
                     <asp:Label ID="lblObservacaoInput" runat="server" Text="Observação: ">
                        <asp:TextBox  class="form-control" TextMode="MultiLine" ID="txtObservacaoInput" runat="server"></asp:TextBox>
                    </asp:Label>
                 </div>
                  </div>
      </div>
  </div>


         <div class="panel panel-default">
      <div class="panel-heading">Ações</div>
      <div class="panel-body">
          <div class="row">
               <div class="col-md-3">
             <asp:Button ID="btnSalvar" class="btn btn-success" runat="server" Text="Salvar" OnClick="btnSalvar_Click"  />
            <asp:Button ID="btnCancelar" class="btn btn-warning" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"  />
            </div>
                
                  </div>
      </div>
  </div>
</div>
</div>
        
    </asp:Panel>
                 
    <asp:Panel ID="panelFiltro" runat="server" Visible="false" CssClass="container-full">
         <div class="container-full">
        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
               
            <div class="row">
                <div class="col-md-3">
                    <label for="txtNumPedidoFornecedor">Núm.Pedido Fornecedor</label>
                    <asp:TextBox class="form-control" ID="txtNumPedidoFornecedor" runat="server"></asp:TextBox>
               </div>
                <div class="col-md-3">
                    <label for="ddlFornecedor">Fornecedor</label>
                    <asp:DropDownList ID="ddlFornecedor" class="form-control" runat="server" />
                </div>
                <div class="col-md-2">
                    <label for="txtDataCompra">Data da Compra</label>
                    <asp:TextBox class="form-control" TextMode="Date" ID="txtDataInicioCompra" runat="server"  ></asp:TextBox>
                 </div>
                 <div class="col-md-2">
                     <label for="txtDataCompra">(de - até)</label>
                     <asp:TextBox class="form-control" TextMode="Date" ID="txtDataFimCompra" runat="server"  ></asp:TextBox>
                 </div> 
            </div>
       <br />
           
           

            <div class="row">

                <div class="col-md-3">
                    <label for="ddlFormaPagamento">Forma de Pagamento</label>
                    <asp:DropDownList ID="ddlFormaPagamento" class="form-control" runat="server" />
                </div>
                <div class="col-md-3">
                    <label for="ddlStatusPedido">Status Pedido</label>
                    <asp:DropDownList ID="ddlStatusPedido" class="form-control" runat="server" />
                </div>
                <div class="col-md-3">
                    <label for="ddlStatusEntrega">Status Entrega</label>
                    <asp:DropDownList ID="ddlStatusEntrega" class="form-control" runat="server" />
                </div>

                <div class="col-md-1">
                    <label for="ddlFornecedor"></label>
                    <asp:Button ID="btnPesquisar" class="btn btn-info" runat="server" Text="Pesquisar" OnClick="btnPesquisar_Click"  />
                </div>
                <div class="col-md-1">
                     <label for="ddlFornecedor"></label>
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info" runat="server" Text="Limpar Campos" OnClick="btnLimparFiltros_Click"  />

                </div>

            </div>
        
        
        </div>
    </div>
    
</div>
    </asp:Panel>

<!-------------------------------------------------------->
                <!-- Trigger the modal with a button -->

              <script type="text/javascript">


                  function openModalMensagem() {
                      $('#myModalMensagem').modal('show');
				  }

				  function openModalConfirmacao() {
					  $('#myModalConfirmacao').modal('show');
				  }

                  function openModal() {

        $('#myModal').modal('show');
    }
</script>

		<!--INICIO MODAL - CONFIRMAÇÃO -->
    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label15" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalConfirmacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabelConfirmacao"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelConfirmacao">Confirmação</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label for="txtModalIDConfirmacao">ID</label>
                                            <asp:TextBox ReadOnly="true" ID="txtModalIDConfirmacao" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                  <div class="row">
                                    <div class="col-sm-12">
                                        <label for="txtModalJustificatiaConfirmacao">Justificativa</label>
                                        <asp:TextBox ID="txtJustificativaCancelamento" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                      
                                  </div>

                                 <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagemModalConfirmacao" runat="server" Text=""></asp:Label>
                                    </div>
                                 </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>
                                    <asp:Button ID="btnConfirmarCancelamento" OnClick="btnConfirmarCancelamento_Click"  Text="Confirmar" class="btn btn-danger"   runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - CONFIRMAÇÃO -->
                
        <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text=""></asp:Label>
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

                
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog-large">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Detalhes do Pedido de Compra [ <asp:Label ID="lblModo" runat="server" Text=""></asp:Label> ] </h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-md-2">
                                        <label for="txtModalId">Id</label>
                                        <asp:TextBox ID="txtModalId" runat="server" placeholder="Id" class="form-control"></asp:TextBox>
                                    </div>
                                      <div class="col-md-2">
                                        <label for="txtModalNumPedidoFornecedor">Núm.Pedido</label>
                                        <asp:TextBox ID="txtModalNumPedidoFornecedor" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="txtModalDataPedido">Data do Pedido</label>
                                        <asp:TextBox ID="txtModalDataPedido" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                    </div>
                                     <div class="col-md-6">
                                            <label for="ddlModalFornecedorInput">Fornecedor</label>
                                            <asp:DropDownList  class="form-control"  ID="ddlModalFornecedorInput" runat="server"></asp:DropDownList>
                                        </div>
                                  </div>
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <label for="txtModalComprador">Comprador</label>
                                            <asp:TextBox ID="txtModalComprador" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                        </div>

                                        <div class="col-md-3">
                                            <label for="txtModalDataPrevisaoEntrega">Dt.Prev.Entrega</label>
                                            <asp:TextBox ID="txtModalDataPrevisaoEntrega" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                        </div>
                                        
                                        <div class="col-md-3">
                                             <label for="txtModalStatusPedido">Status Pedido</label>
                                             <asp:TextBox ID="txtModalStatusPedido" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                        </div>

                                        

                                     </div>

                                    <div class="row">

                                        <div class="col-md-3">
                                             <label for="txtModalQuantidade">Quantidade</label>
                                             <asp:TextBox ID="txtModalQuantidade" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                        </div>

                                        <div class="col-md-3">
                                            <label for="txtModalFormaPagamento">Forma de Pagamento</label>
                                            <asp:TextBox ID="txtModalFormaPagamento" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                        </div>

                                        <div class="col-md-3">
                                             <label for="txtModalDataEntrega">Data da Entrega</label>
                                             <asp:TextBox ID="txtModalDataEntrega" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-md-3">
                                             <label for="txtModalStatusEntrega">Status Entrega</label>
                                             <asp:TextBox ID="txtModalStatusEntrega" runat="server" placeholder="" class="form-control"></asp:TextBox><br />
                                        </div>

                                        
                                     </div>
                                    
                                    <div class="row">

                                          <div class="col-md-3">
                                            <label for="txtModalValorPedido">Valor Pedido</label>
                                             <asp:TextBox ID="txtModalValorPedido" runat="server" placeholder="Produto" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-md-3">
                                             <label for="txtModalValorDesconto">Valor Desconto</label>
                                             <asp:TextBox ID="txtModalValorDesconto" runat="server" placeholder="Preço Etiqueta" class="form-control"></asp:TextBox><br />
                                        </div>
                                      
                                         <div class="col-md-3">
                                             <label for="txtModalValorImposto">Valor Imposto</label>
                                             <asp:TextBox ID="txtModalValorImposto" runat="server" placeholder="Preço Compra" class="form-control"></asp:TextBox><br />
                                        </div>
                                  

                                     
                                        <div class="col-md-3">
                                            <label for="txtModalValorFrete">Valor Frete</label>
                                            <asp:TextBox ID="txtModalValorFrete" runat="server" placeholder="Preço CIF" class="form-control"></asp:TextBox><br />
                                        </div>
                                         <div class="col-md-3">
                                             <label for="txtModalValorTotal">Valor Total</label>
                                             <asp:TextBox ID="txtModalValorTotal" runat="server" placeholder="Preço Mínimo" class="form-control"></asp:TextBox><br />
                                        </div>
                                         
                                    </div>
                                    
                                    


                                     <div class="row">
                                        <div class="col-md-3">
                                            <label for="txtModalObservacoes">Observações</label>
                                            <asp:TextBox ID="txtModalObservacoes" TextMode="MultiLine" runat="server" placeholder="Quantidade" class="form-control"></asp:TextBox><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        Fechar</button>
                                    <%--<button type="button"  class="btn btn-primary">
                                        Save changes</button>--%>

                                    <asp:Button ID="btnSalvarModal" Text="Salvar" class="btn btn-default"   runat="server" OnClick="btnSalvarModal_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--------------------------------------------------------->
       
               

                <div class="well well-sm"><b>Total de Pedidos de Compra ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

               <asp:Panel ID="panelGrid" runat="server" Visible="false">
     
                 <h3>Lista de Pedidos de Compra</h3>


    <asp:GridView id="PedidosDeCompraGridView" 
    AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" AutoGenerateColumns="False" OnSelectedIndexChanged="PedidosDeCompraGridView_SelectedIndexChanged" OnSelectedIndexChanging="PedidosDeCompraGridView_SelectedIndexChanging" OnRowDataBound="PedidosDeCompraGridView_RowDataBound"  >
          
          

        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="NumeroPedido" HeaderText="Núm.Pedido" >
            <ItemStyle ForeColor="Red" />
            </asp:BoundField>
            <asp:BoundField DataField="DataPedido" HeaderText="Data Pedido" />
            <asp:BoundField DataField="Funcionario" HeaderText="Comprador" />
            <asp:BoundField DataField="NomeFantasia" HeaderText="Fornecedor" />
            <asp:BoundField DataField="Quantidade" HeaderText="Qtd.Itens" />
            <asp:BoundField DataField="ValorPedido" HeaderText="Valor Pedido" Visible="false" />
            <asp:BoundField DataField="ValorDesconto" HeaderText="Valor Desconto" Visible="false"  />
            <asp:BoundField DataField="ValorImposto" HeaderText="Valor Imposto" Visible="false"  />
            <asp:BoundField DataField="ValorFrete" HeaderText="Valor Desconto" Visible="false" />
            <asp:BoundField DataField="ValorTotal" HeaderText="Valor Total" />
            <asp:BoundField DataField="StatusPedido" HeaderText="StatusPedido" Visible="false" />
            <asp:BoundField DataField="StatusEntrega" HeaderText="StatusEntrega" />

            <asp:CommandField ShowSelectButton="false" />
            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/Img/ver-detalhe.jpg" OnClick="ImageButton1_Click" Width="20px" />
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                 <ItemTemplate>
                       <asp:Button  class="btn btn-default btn-xs" ID="btnDetalhar" runat="server"  OnClick="BtnDetalhar_Click" Text="Detalhes" />
                      </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                
                  <ItemTemplate>
                    
                    <asp:Button  class="btn btn-default btn-xs" ID="btnEditar" runat="server" OnClick="BtnEditar_Click" Text="Editar" />
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button  class="btn btn-default btn-xs" ID="btnChegou" runat="server" Text="Chegou" OnClick="btnChegou_Click" />
                </ItemTemplate>
            </asp:TemplateField>

			
            
        	<asp:TemplateField>
				<ItemTemplate>
					<asp:Button class="btn btn-default btn-xs" ID="btnCancelarPedidoCompra" runat="server" Text="Cancelar" OnClick="btnCancelarPedidoCompra_Click" />
				</ItemTemplate>
			</asp:TemplateField>

			
            
        </Columns>
                   </asp:GridView>
          
          

   </asp:Panel>
          </ContentTemplate>
        </asp:UpdatePanel>
     
    

    
</asp:Content>