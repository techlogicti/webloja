﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NotaFiscalCompraDetalhe.aspx.cs" Inherits="Web.NotaFiscalCompraDetalhe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<hr />
    
<script type="text/javascript">
function openModalMensagem() {
   $('#myModalMensagem').modal('show');
}
</script>

 <!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">

                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

<!-- PAINEL ALERTA -->
<asp:Panel ID="panelAlerta" runat="server" Visible="false">
        <div class="alert alert-danger" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span class="sr-only">Atenção:</span>
                 <asp:Label ID="lblMensagemAlerta" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
            </div>
</asp:Panel>

<!-- FIM PAINEL ALERTA -->

<!-- PAINEL PRINCIPAL -->
 <asp:Panel ID="panelCadastro" runat="server" Visible="false">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
        <div class="container-full">
            <asp:Button ID="btnVoltar" class="btn btn-warning" runat="server" Text="Voltar" OnClick="btnVoltar_Click" />
            <h2>Nota Fiscal ID <asp:Label ID="lblIdNotaFiscal" runat="server" Text="Label"></asp:Label></h2>
            <p>Identificação da Nota - <asp:Label ID="lblModo" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label></p>

             <div class="panel-group">
                 <!-- Painel = Identificação da Nota -->
                <div class="panel panel-default">
                <div class="panel-heading">Identificação da Nota Fiscal</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-2">
                                <asp:Label ID="lblNumeroNotaFiscalInput" runat="server" Text="Núm.NF: ">
                                <asp:TextBox  ReadOnly="false" class="form-control" ID="txtNumeroNotaFiscalInput" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-sm-4">
                                <asp:Label ID="lblFornecedorNotaFiscalInput" runat="server" Text="Fornecedor: ">
                                <asp:TextBox  ReadOnly="false" class="form-control" ID="txtFornecedorNotaFiscalInput" TextMode="Search" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-sm-2">
                                <asp:Label ID="lblDataEmissaoNotaFicalInput" runat="server" Text="DT.Emissão: ">
                                <asp:TextBox  ReadOnly="false" class="form-control" TextMode="Date" ID="txtDataEmissaoNotaFicalInput" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-sm-2">
                                <asp:Label ID="lblDataEntregaNotaFiscal" runat="server" Text="DT.Entrega: ">
                                <asp:TextBox  ReadOnly="false" class="form-control" TextMode="Date" ID="txtDataEntregaNotaFiscalInput" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                            <div class="col-sm-2">
                                <asp:Label ID="lblValorTotalNotaFiscalInput" runat="server" Text="Valor.NF: ">
                                <asp:TextBox  ReadOnly="false" class="form-control" ID="txtValorTotalNotaFiscalInput" runat="server"></asp:TextBox>
                                </asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                 <!-- FIM = Painel = Identificação da Nota -->
                 <!-- INICIO = Painel = Itens da Nota -->
                <div class="panel panel-default">
                    <div class="panel-heading">Itens da Nota Fiscal</div>
                        <div class="panel-body">

                            <hr />
                            <div class="row">
                                <div class="col-sm-"
                            </div>
                            <hr />

                        </div>
                    </div>
                </div>
                 <!-- FIM = Painel = Itens da Nota -->


                </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIM PAINEL PRINCIPAL -->


<hr />
</asp:Content>