﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class DevolucaoProduto : System.Web.UI.Page
    {
        private DALConexao conexao;
        Util util = new Util();
        //.
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCadLocalizar_Click(object sender, EventArgs e)
        {
            string idPedido = "0";


            /*O campo usado para localizar o PEDIDO deve ser o ID, uma vez que ele é único
             *O número do pedido não poder ser usado, visto que ele pode ter sido cancelado e
             * o número reutilizado*/

            /*Se o ID for diferente de vazio, realiza busca dos dados do Pedido para prosseguir
             * com o cadastro da devolução da mercadoria*/
            if (!txtCadIdPedido.Text.ToString().Trim().Equals(""))
            {
                idPedido = txtCadIdPedido.Text.ToString();

                //Recupera Dados do Pedido
                conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                ModeloPedidoVenda pedido = new ModeloPedidoVenda();
                BLLPedidoVenda bllPedidoVenda = new BLLPedidoVenda(conexao);

                pedido = bllPedidoVenda.CarregarModeloPedidoVenda(Convert.ToInt32(idPedido));
                txtCadNumeroPedido.Text = pedido.NumeroPedido.ToString();
                txtCadDataCompra.Text = pedido.DataPedido.ToString();
                txtCadValorTotal.Text = pedido.ValorTotal.ToString();

                ModeloCliente cliente = new ModeloCliente();
                BLLCliente bllCliente = new BLLCliente(conexao);
                cliente = bllCliente.CarregarModeloCliente(pedido.IdCliente);
                txtCadCpfCliente.Text = cliente.NU_cpfCnpj.ToString();
                txtCadNomeCliente.Text = cliente.NM_nomeCompleto.ToString();

                //Recupera Itens do Pedido
            }
            else
            {
                limpaCamposCadastro();
            }

            
        }
        public void limpaCamposCadastro()
        {
            
            txtCadNumeroPedido.Text = "";
            txtCadCpfCliente.Text = "";
            txtCadDataCompra.Text = "";
            txtCadNomeCliente.Text = "";
            txtCadValorTotal.Text = "";
        }
    }
}