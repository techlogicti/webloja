﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class ContasAReceber : System.Web.UI.Page
    {

        //variaveis dinamicas de impressao de comprovante de recebimento
        string impressaoNumeroPedido = "";
        string impressaoDataVencimentoPrestacao = "";
        string impressaoValorPrestacao = "";
        string impressaoValorPago = "";
        string impressaoValorTroco = "";
        string impressaoValorSaldo = "";
        string impressaoSituacaoPrestacao = "Quitada";
        string impressaoParcelaPrestacao = "";
        string impressaoQuantidadePrestacao = "";
        string funcionarioRecebimento = "";
        //
        private PrintDocument document = new PrintDocument();
        private DALConexao conexao;

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        string sqlUsuarios = "select IDFuncionario as ID, NM_nomeCompleto as NM_nome from TB_funcionario where st_situacaoregistro='A' order by NM_nome asc";
        string sqlTipoPagamento = "select ID as ID, NM_nome as NM_nome from tb_tipoPagamento where st_situacaoregistro='A' order by NM_nome asc";
        string sqlCliente = "select ID as ID, NM_nomeCompleto as NM_nome from tb_cliente where st_situacaoregistro='A' order by NM_nomeCompleto asc";
        //string sqlHistoricoFinanceiro = "select ID as ID, NM_nome as NM_nome from tb_historicoFinanceiro where st_situacaoregistro='A' order by NM_nome asc";
        string sqlHistoricoFinanceiro = "select ID as ID, Convert(varchar, id) + ' - '+ Convert(varchar,nm_nome) as NM_nome from tb_historicoFinanceiro where st_situacaoregistro='A' and id_tipoHistorico in('1','6') order by NM_nome asc";



        Util util = new Util();
        
        BLLOperador bllOperador = null;
      
        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }

            


        }

        public void gravarAcesso(String texto)
        {
            string path = @"c:\temp\logSoberana\" + util.obtemAmbiente().ToString() + "\\Acesso-" + dataHoje + "-" + horaHoje + ".txt";

            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("#############################");
                    sw.WriteLine("Arquivo criado em " + dataHoje + " as " + horaHoje);
                    sw.WriteLine("#############################");
                }
            }

            // Compose a string that consists of three lines.
            string lines = texto.ToString().Trim();

            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(lines);
            }

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

            //Get the button that raised the event
            ImageButton btn = (ImageButton)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            //openModalVisualizaImagem(Convert.ToInt32(gvr.Cells[1].Text), "D", "N");

            string url = "http://joao-pc/Img/Scan/pedidos/";
            url = url + gvr.Cells[1].Text + ".jpeg";

            gravarAcesso("----------------------------------------------------------");
            gravarAcesso("Inicio - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("Clique - Abrir Imagem do Pedido " + gvr.Cells[1].Text);
            gravarAcesso(url.ToString());
            gravarAcesso("Fim - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("----------------------------------------------------------");

            string _cId = "";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newWindow", "window.open('../../DataControlManager/Online complain/frmComplaintRevision.aspx?ID=" + url + "','_blank','status=1,toolbar=0,menubar=0,location=1,scrollbars=1,resizable=1,width=30,height=30');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open('" + url + "', '_blank', 'height=600px,width=600px,scrollbars=1'); ", true);

            //http://servidor:8090/Img/Scan/pedidos/18766.jpeg
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            testCheckUserSoberana();
            util = new Util();
            connectionString = util.conexao();
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                panelBotoes.Visible = true;

                //carregaGrid("");
                //util.preencheCombo(ddlFormaPagamento, sqlTipoPagamento, "S");
                util.preencheListBox(listFormaPagamento, sqlTipoPagamento, "S");

                foreach (ListItem li in listFormaPagamento.Items)
                    li.Selected = true;

                //util.preencheComboStatusPagamento(ddlStatusPagamento);
                util.preencheListStatusPagamento(listStatusPagamento);


               

            }

            

                

               

            


        }

        public void carregaHistoricoFinanceiroConta(string idConta)
        {
            String sql = "";
            sql = sql + " ";

            sql = sql + " select id, ";
            sql = sql + " id_tabela, ";
            sql = sql + " id_conta, ";
            sql = sql + " id_tipoconta, ";
            sql = sql + " id_tipopagamento, ";
            sql = sql + " id_historicofinanceiro, ";
            sql = sql + " vl_valorevento, ";
            sql = sql + " Convert(varchar(10),CONVERT(date,convert(varchar,dt_dataevento),106),103) as dt_dataevento, ";
            sql = sql + " de_observacao, ";
            sql = sql + " cd_operador, ";
            sql = sql + " dt_datacriacao, ";
            sql = sql + " hr_horacriacao, ";
            sql = sql + " dt_dataatualizacao, ";
            sql = sql + " hr_horaatualizacao, ";
            sql = sql + " st_situacaopagamento, ";
            sql = sql + " st_situacaoregistro ";
            sql = sql + " from tb_eventoFinanceiro ";

            if (!idConta.Equals(""))
            {
                sql += "where id_conta = " + idConta.ToString();
            }

            sql += " order by dt_dataEvento desc, id asc";

            string strcon = DadosDaConexao.StringDeConexao(util.obtemAmbiente());

            using (SqlConnection con = new SqlConnection(strcon.ToString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);

                            GridViewHistoricoFinanceiro.DataSource = dt;
                            GridViewHistoricoFinanceiro.DataBind();
                        }
                    }
                }
            }
                        }

        public void carregaGrid(String filtro)
        {
            String sql = "";
            sql = sql + " select ID as ID, ";
            sql = sql + " NumeroPedido as Pedido, ";
            sql = sql + " NumeroPedido as NumeroPedido, ";
            sql = sql + " IDCliente as IDCliente, ";
            sql = sql + " Cliente as Cliente, ";
            sql = sql + " DataVencimentoUSA, ";
            sql = sql + " Convert(varchar(10),CONVERT(date,convert(varchar,DataEmissaoUSA),106),103) as DataEmissao, ";
            sql = sql + " NumeroParcela as NumeroParcela, ";
            sql = sql + " QtdParcelas as QtdParcelas, ";
            sql = sql + " QtdParcelas as QuantidadeParcela, ";
            sql = sql + " ValorParcela as ValorConta, ";
            sql = sql + " ValorParcela as ValorParcela, ";
            sql = sql + " DataVencimento, ";
            sql = sql + " Multa as ValorMulta, ";
            sql = sql + " Multa , ";
            sql = sql + " Juros as ValorJuros, ";
            sql = sql + " Juros , ";
            sql = sql + " ValorTotal as ValorTotal, ";
            sql = sql + " ValorRecebido as ValorRecebido, ";
            sql = sql + " StatusPagamento as SituacaoConta, ";
            sql = sql + " StatusPagamento, ";
            sql = sql + " FormaPagamento as FormaPagamento, ";
            sql = sql + "DescricaoFormaPagamento, ";
            sql = sql + " case DataRecebimentoUSA when '0' then '0'";
            sql = sql + " else Convert(varchar(10), CONVERT(date, convert(varchar, DataRecebimentoUSA), 106), 103)";
            sql = sql + " end as DataRecebimento,";
            sql = sql + " Observacoes as Observacoes ";
            sql = sql + " from VW_contasAReceber  ";

            if (!filtro.Equals(""))
            {
                sql += filtro.ToString();
            }

            sql += " order by NumeroPedido desc, NumeroParcela asc, DataVencimentoUSA asc ";


            
            string strcon = DadosDaConexao.StringDeConexao(util.obtemAmbiente());
            using (SqlConnection con = new SqlConnection(strcon.ToString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {

                            try
                            {
                                sda.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                    ContasGridView.DataSource = dt;
                                    ContasGridView.DataBind();
                                }
                                //Calculate Sum and display in Footer Row


                                if (ContasGridView.Rows.Count > 0)
                                {
                                    ContasGridView.FooterRow.Cells[5].Text = "Total";
                                    ContasGridView.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;

                                    decimal parcela = dt.AsEnumerable().Sum(row => row.Field<decimal>("ValorParcela"));
                                    ContasGridView.FooterRow.Cells[6].Text = parcela.ToString("N2");

                                    decimal multa = dt.AsEnumerable().Sum(row => row.Field<decimal>("Multa"));
                                    ContasGridView.FooterRow.Cells[8].Text = multa.ToString("N2");

                                    decimal juros = dt.AsEnumerable().Sum(row => row.Field<decimal>("Juros"));
                                    ContasGridView.FooterRow.Cells[9].Text = juros.ToString("N2");

                                    decimal total = dt.AsEnumerable().Sum(row => row.Field<decimal>("ValorTotal"));
                                    ContasGridView.FooterRow.Cells[10].Text = total.ToString("N2");
                                }
                            }
                            catch(Exception ex)
                            {

                            }
                           

                          

                            

                           
                        }
                    }
                }
            }
        }
        
        public void carregaGrid2(String filtro)
        {

            //String queryString = " select * from VW_contasAReceber    ";
            String sql = "";
            sql = sql + " select ID as ID, ";
            sql = sql + " NumeroPedido as Pedido, ";
            sql = sql + " NumeroPedido as NumeroPedido, ";
            sql = sql + " IDCliente as IDCliente, ";
            sql = sql + " Cliente as Cliente, ";
            sql = sql + " DataVencimentoUSA, ";
            sql = sql + " Convert(varchar(10),CONVERT(date,convert(varchar,DataEmissaoUSA),106),103) as DataEmissao, ";
            sql = sql + " NumeroParcela as NumeroParcela, ";
            sql = sql + " QtdParcelas as QtdParcelas, ";
            sql = sql + " QtdParcelas as QuantidadeParcela, ";
            sql = sql + " ValorParcela as ValorConta, ";
            sql = sql + " ValorParcela as ValorParcela, ";
            sql = sql + " ValorRecebido as ValorRecebido, ";
            sql = sql + " DataVencimento, "; 
            sql = sql + " Multa as ValorMulta, ";
            sql = sql + " Multa , ";
            sql = sql + " Juros as ValorJuros, ";
            sql = sql + " Juros , ";
            sql = sql + " ValorTotal as ValorTotal, ";
            sql = sql + " StatusPagamento as SituacaoConta, ";
            sql = sql + " StatusPagamento, ";
            sql = sql + " FormaPagamento as FormaPagamento, ";
            sql = sql + "DescricaoFormaPagamento, ";
            sql = sql + " case DataRecebimentoUSA when '0' then '0'";
            sql = sql + " else Convert(varchar(10), CONVERT(date, convert(varchar, DataRecebimentoUSA), 106), 103)";
            sql = sql + " end as DataRecebimento,";
            sql = sql + " Observacoes as Observacoes ";
            sql = sql + " from VW_contasAReceber  ";

            if (!filtro.Equals(""))
            {
                sql += filtro.ToString();
            }

            sql += " order by NumeroPedido desc, NumeroParcela asc, DataVencimentoUSA asc ";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(sql);
            
            if (ds.Tables.Count > 0)
            {
                ContasGridView.DataSource = ds;

                ContasGridView.DataBind();

                ContasGridView.FooterRow.Cells[1].Text = "Total";
                ContasGridView.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                //Total de Registros
                lblTotal.Text = ContasGridView.Rows.Count.ToString();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

            return ds;


        }

        protected void btnListar_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
        }

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
            
            
        }

        public string recuperaFiltro()
        {
            //recupera valor do Filtro
            string v_numPedido, v_nomeCliente, v_dataInicioEmissao, v_dataFimEmissao, v_dataInicioVencimento, v_dataFimVencimento, v_dataInicioRecebimento, v_dataFimRecebimento, v_FormaPagamento, v_StatusPagamento, v_statusRegistro;

            v_numPedido = txtNumPedido.Text.ToString().Trim();
            v_nomeCliente = txtNomeCliente.Text;

            v_dataInicioEmissao = txtDataInicioEmissao.Text;
            v_dataFimEmissao = txtDataFimEmissao.Text;
            v_dataInicioEmissao = v_dataInicioEmissao.Replace("-", "");
            v_dataFimEmissao = v_dataFimEmissao.Replace("-", "");

            v_dataInicioVencimento = txtDataInicioVencimento.Text;
            v_dataFimVencimento = txtDataFimEmissao.Text;
            v_dataInicioVencimento = v_dataFimVencimento.Replace("-", "");
            v_dataFimVencimento = v_dataFimVencimento.Replace("-", "");

            v_dataInicioRecebimento = txtDataInicioRecebimento.Text;
            v_dataFimRecebimento = txtDataFimRecebimento.Text;
            v_dataInicioRecebimento = v_dataInicioRecebimento.Replace("-", "");
            v_dataFimRecebimento = v_dataFimRecebimento.Replace("-", "");


            //v_FormaPagamento = ddlFormaPagamento.SelectedValue;
            v_FormaPagamento = "";
            foreach (ListItem item in listFormaPagamento.Items)
            {
                if (item.Selected)
                {
                    v_FormaPagamento += item.Value.ToString() + ",";
                }
            }

            v_FormaPagamento += "99999"; //so para nao terminar com a , sozinha


            //v_StatusPagamento = ddlStatusPagamento.SelectedValue;
            v_StatusPagamento = "";
            
            foreach (ListItem item in listStatusPagamento.Items)
            {
                if (item.Selected)
                {
                    v_StatusPagamento += "'"+item.Value.ToString() + "',";
                    
                }
            }
            v_StatusPagamento += "'99999'"; //so para nao terminar com a , sozinha

            v_statusRegistro = ddlStatusRegistro.SelectedValue;

            string filtro = "";

            filtro += " where 1=1 ";

            if (v_numPedido.Trim() != "") filtro = filtro + " and NumeroPedido like '%" + v_numPedido + "%'";


            if (v_nomeCliente.Trim() != "")
            {
                filtro = filtro + " and ( Cliente like '%" + v_nomeCliente + "%' or CpfCnpj like '%" + v_nomeCliente + "%' )";
            }

            //if (v_FormaPagamento.Trim() != "0") filtro = filtro + " and FormaPagamento in( " + v_FormaPagamento + ")";
            if (v_FormaPagamento.Trim() != "99999") filtro = filtro + " and FormaPagamento in( " + v_FormaPagamento + ")";
            if (v_StatusPagamento.Trim() != "99999") filtro = filtro + " and StatusPag in (" + v_StatusPagamento + ")";
            
            //if (v_StatusPagamento.Trim() != "-") filtro = filtro + " and StatusPagamento like '%" + v_StatusPagamento + "%' ";
            //if (v_statusRegistro.Trim() != "-") filtro = filtro + " and st_situacaoRegistro = '" + v_statusRegistro + "' ";


            //---------- DATA EMISSAO
            if (v_dataFimEmissao.Equals(""))
            {

                if (v_dataInicioEmissao.Trim() != "") filtro = filtro + " and DataEmissaoUSA = '" + v_dataInicioEmissao + "' ";
            }
            else
            {

                if (v_dataInicioEmissao.Trim() != "")
                {

                    //filtro = filtro + " and DT_dataEmissao between '" + v_dataInicioEmissao + "' and  '" + v_dataFimEmissao + "'" + " and st_situacaoEntrega='" + v_statusEntrega + "' ";
                    filtro = filtro + " and DataEmissaoUSA between '" + v_dataInicioEmissao + "' and  '" + v_dataFimEmissao + "'";
                }
            }

            //---------- DATA VENCIMENTO
            if (v_dataFimVencimento.Equals(""))
            {

                if (v_dataInicioVencimento.Trim() != "") filtro = filtro + " and DataVencimentoUSA = '" + v_dataInicioVencimento + "' ";
            }
            else
            {

                if (v_dataInicioVencimento.Trim() != "")
                {


                    filtro = filtro + " and DataVencimentoUSA between '" + v_dataInicioVencimento + "' and  '" + v_dataFimVencimento + "'";
                }
            }

            //---------- DATA RECEBIMENTO
            if (v_dataFimRecebimento.Equals(""))
            {

                if (v_dataInicioRecebimento.Trim() != "") filtro = filtro + " and DataRecebimentoUSA = '" + v_dataInicioRecebimento + "' ";
            }
            else
            {

                if (v_dataInicioRecebimento.Trim() != "")
                {


                    filtro = filtro + " and DataRecebimentoUSA between '" + v_dataInicioRecebimento + "' and  '" + v_dataFimRecebimento + "'";
                }
            }

            return filtro;
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
           carregaGrid(recuperaFiltro());
        }

        protected void btnReceber_Click(object sender, EventArgs e)
        {

            GetAllPrinterList();

            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;


            /* ALterado em 27/12/2017 - 
             * Devido ao problema de "refresh" o qual a tela fica inacessivel com um modal vazio por cima
             * foi realizada a alteração para exebição do painel na tela, não como modal */
            //openModalFillData(Convert.ToInt32(gvr.Cells[0].Text), "D", "N");
            openModalFillData(Convert.ToInt32(gvr.Cells[0].Text), "D", "S");



        }

        public void openModalFillData(int idSelecionado, string modo, string salvar)
        {

            if (modo.Equals("E"))
            {
                lblModo.Text = "Modo de edição";
            }
            else
            {
                lblModo.Text = "Modo de visualização";
            }

            if (salvar.Equals("S"))
            {
                btnSalvarModal.Visible = true;
                //camposModalSomenteLeitura("N");
            }
            else
            {
                btnSalvarModal.Visible = false;
                //camposModalSomenteLeitura("S");
            }

            this.recuperaDadosModal(idSelecionado);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }

        public string montaSql(String sql)
        {
            sql = sql + " select ID as ID, ";
            sql = sql + " NumeroPedido as Pedido, ";
            sql = sql + " IDCliente as IDCliente, ";
            sql = sql + " Convert(varchar(10),CONVERT(date,convert(varchar,DataEmissaoUSA),106),103) as DataEmissao, ";
            sql = sql + " NumeroParcela as NumeroParcela, ";
            sql = sql + " QtdParcelas as QuantidadeParcela, ";
            sql = sql + " ValorParcela as ValorConta, ";
            sql = sql + " Multa as ValorMulta, ";
            sql = sql + " Juros as ValorJuros, ";
            sql = sql + " ValorTotal as ValorTotal, ";
            sql = sql + " ValorRecebido as ValorRecebido, ";      
            sql = sql + " StatusPagamento as SituacaoConta, ";
            sql = sql + " FormaPagamento as FormaPagamento, ";
            sql = sql + " case DataRecebimentoUSA when '0' then '0'";
            sql = sql + " else Convert(varchar(10), CONVERT(date, convert(varchar, DataRecebimentoUSA), 106), 103)";
            sql = sql + " end as DataRecebimento,";
            //sql = sql + " (select NM_nomeCompleto from TB_Funcionario v where v.IDVendedor = ID_funcionario ) as Funcionario, ";
            //sql = sql + " (select DE_nomeFantasia from TB_Fornecedor f where f.IDfornecedor = ID_fornecedor ) as NomeFantasia, ";
            /*sql = sql + " DE_pedidoFornecedor as NumeroPedido, ";

            sql = sql + " Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataPrevisaoEntrega),106),103) as DataPrevisaoEntrega, ";
            sql = sql + " ST_situacaoEntrega as StatusEntrega, ";
            sql = sql + " Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataEmissao),106),103) as DataEmissao, ";*/
            //sqlPedidoCompra = sqlPedidoCompra + " DT_dataEntrega as DataEntrega, ";

            /*sql = sql + " Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataPedido),106),103) as DataPedido, ";
            sql = sql + " ST_situacaoPedido as StatusPedido, ";
            sql = sql + " case ST_situacaoEntrega ";
            sql = sql + " when 'A' then 'Aguardando' ";
            sql = sql + " when 'E' then 'Entregue' ";
            sql = sql + " else 'Verificar' ";
            sql = sql + " end as StatusEntrega, ";*/
            sql = sql + " Observacoes as Observacoes, ";
            sql = sql + " ValorPendente as ValorPendente";
            sql = sql + " from VW_contasAReceber  ";

            return sql;
        }
        public void preencheCombo(DropDownList comboBox, string queryString, string todos)
        {

            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                comboBox.Items.Clear();



                comboBox.DataSource = ds;
                comboBox.DataTextField = "nm_nome";
                comboBox.DataValueField = "id";
                comboBox.DataBind();

                if (todos.Equals("S"))
                {
                    ListItem l = new ListItem("Todos", "Todos", true);
                    l.Selected = true;
                    comboBox.Items.Add(l);
                }
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        public void recuperaDadosModal(int id)
        {

            preencheCombo(ddlUsuarioAutorizacao, sqlUsuarios, "S");

            string sqlUnico = montaSql("");
            sqlUnico = sqlUnico + " where id = " + id.ToString();

            sqlUnico += "  order by NumeroPedido desc, DataVencimentoUSA asc ";

            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sqlUnico, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "VW_contasAReceber");

            foreach (DataRow pRow in customerOrders.Tables["VW_contasAReceber"].Rows)
            {
                //                Console.WriteLine(pRow["ID"]);
                txtModalId.Text = pRow["ID"].ToString();
                txtModalPedido.Text = pRow["Pedido"].ToString();
                //ddlModalCliente.Text = pRow["Cliente"].ToString();
                txtModalDataEmissao.Text = pRow["DataEmissao"].ToString();
                preencheCombo(ddlModalCliente, sqlCliente, "S");
                ddlModalCliente.SelectedValue = pRow["IDCliente"].ToString();


                util.preencheCombo(ddlModalHistoricoFinanceiro, sqlHistoricoFinanceiro, "S");
                util.preencheCombo(ddlModalTipoPagamento, sqlTipoPagamento, "S");
                ddlModalTipoPagamento.SelectedValue = pRow["FormaPagamento"].ToString();

                txtModalNumeroParcela.Text = pRow["NumeroParcela"].ToString();
                txtModalQuantidadeParcela.Text = pRow["QuantidadeParcela"].ToString();

                txtModalValorConta.Text = pRow["ValorConta"].ToString();
                txtModalValorMulta.Text = pRow["ValorMulta"].ToString();
                txtModalValorJuros.Text = pRow["ValorJuros"].ToString();
                txtModalValorTotal.Text = pRow["ValorTotal"].ToString(); //campo dinamico, calculo na view

                txtModalValorPago.Text = pRow["ValorRecebido"].ToString();
                txtModalValorRecebimento.Text = pRow["ValorPendente"].ToString();

                carregaHistoricoFinanceiroConta(pRow["ID"].ToString());

                string dataRecebimento = pRow["DataRecebimento"].ToString();


                txtModalDataRecebimento.Text = DateTime.Today.ToString("yyyy-MM-dd");
                txtModalDataRecebimento.ReadOnly = false;

                /*
                if (dataRecebimento== "0")
                {
                    txtModalDataRecebimento.Text = DateTime.Today.ToString("yyyy-MM-dd");
                    txtModalDataRecebimento.ReadOnly = false;
                }
                else
                {
                    txtModalDataRecebimento.Text = pRow["DataRecebimento"].ToString();
                    txtModalDataRecebimento.ReadOnly = true;
                }
                */
                txtModalStatusConta.Text = pRow["SituacaoConta"].ToString();



                txtModalObservacoes.Text = pRow["Observacoes"].ToString();

                //                preencheCombo(ddlModalFornecedorInput, sqlFornecedor, "S");
                //ddlModalFornecedorInput.SelectedValue = pRow["IdFornecedor"].ToString();
                //txtModalFornecedor.Text = pRow["NomeFantasia"].ToString();
                /*txtModalComprador.Text = pRow["Funcionario"].ToString();
                txtModalDataPrevisaoEntrega.Text = pRow["DataPrevisaoEntrega"].ToString();
                txtModalStatusPedido.Text = pRow["StatusPedido"].ToString();
                txtModalQuantidade.Text = pRow["Quantidade"].ToString();
                txtModalFormaPagamento.Text = pRow["FormaPagamento"].ToString();
                txtModalDataEntrega.Text = pRow["DataEntrega"].ToString();
                txtModalStatusEntrega.Text = pRow["StatusEntrega"].ToString();
                txtModalValorPedido.Text = pRow["ValorPedido"].ToString();
                txtModalValorDesconto.Text = pRow["ValorDesconto"].ToString();
                txtModalValorImposto.Text = pRow["ValorImposto"].ToString();
                txtModalValorFrete.Text = pRow["ValorFrete"].ToString();
                txtModalValorTotal.Text = pRow["ValorTotal"].ToString();
                txtModalObservacoes.Text = pRow["Funcionario"].ToString();
                */


            
            }


        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ContasGridView.PageIndex = e.NewPageIndex;
            this.carregaGrid(recuperaFiltro());
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                ContasGridView.AllowPaging = false;
                
                this.carregaGrid(recuperaFiltro());
                

                ContasGridView.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in ContasGridView.HeaderRow.Cells)
                {
                    cell.BackColor = ContasGridView.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in ContasGridView.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = ContasGridView.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = ContasGridView.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                ContasGridView.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void ContasGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string situacaoParcela = "";


                string string1 = e.Row.Cells[1].Text.ToString().Trim();
                string string2 = e.Row.Cells[2].Text.ToString().Trim();
                string string3 = e.Row.Cells[3].Text.ToString().Trim();
                string string4 = e.Row.Cells[4].Text.ToString().Trim();
                string string5 = e.Row.Cells[5].Text.ToString().Trim();
                string string6 = e.Row.Cells[6].Text.ToString().Trim();
                string string7 = e.Row.Cells[7].Text.ToString().Trim();
                string string8 = e.Row.Cells[8].Text.ToString().Trim();
                string string9 = e.Row.Cells[9].Text.ToString().Trim();
                string string10 = e.Row.Cells[10].Text.ToString().Trim();
                string string11 = e.Row.Cells[11].Text.ToString().Trim();
                string string12 = e.Row.Cells[12].Text.ToString().Trim();
                string string13 = e.Row.Cells[13].Text.ToString().Trim();


                if (!e.Row.Cells[13].Text.ToString().Trim().Equals(""))
                {
                    situacaoParcela = e.Row.Cells[13].Text.ToString();
                }

                /*if (Session["user"].ToString().Equals("cleiton"))
                {
                    e.Row.Cells[18].Visible = true;
                }
                else
                {
                    e.Row.Cells[18].Visible = false;
                }*/

                foreach (TableCell cell in e.Row.Cells)
                {
                    if (situacaoParcela.Equals("Pendente"))
                    {
                        cell.BackColor = Color.Yellow;
                        cell.ForeColor = Color.Black;

                        e.Row.Cells[17].Enabled = false;
                        
                        
                    }
                    else if (situacaoParcela.Equals("Quitado"))
                    {
                        cell.BackColor = Color.Green;
                        cell.ForeColor = Color.White;

                        e.Row.Cells[16].Enabled = false;
                        //e.Row.Cells[18].Enabled = false;
                        //e.Row.Cells[14].Enabled = false;
                        //e.Row.Cells[14].Text = "";
                    }
                    else if (situacaoParcela.Equals("Conferir"))
                    {
                        cell.BackColor = Color.LightGreen;
                        cell.ForeColor = Color.Black;

                        e.Row.Cells[16].Enabled = false;
                        //e.Row.Cells[18].Enabled = false;
                        //e.Row.Cells[14].Enabled = false;
                        //e.Row.Cells[14].Text = "";
                    }
                    else if (situacaoParcela.Equals("Cancelado"))
                    {
                        cell.BackColor = Color.Black;
                        cell.ForeColor = Color.Red;

                        e.Row.Cells[16].Enabled = false;
                        e.Row.Cells[17].Enabled = false;
                        e.Row.Cells[18].Enabled = false;
                        //e.Row.Cells[14].Enabled = false;
                        //e.Row.Cells[14].Text = "";
                    }
                    else if (situacaoParcela.Equals("Atrasada"))
                    {
                        cell.BackColor = Color.DarkOrange;
                        cell.ForeColor = Color.White;

                        //e.Row.Cells[18].Enabled = false;
                    }
                    else if (situacaoParcela.Equals("Parcial"))
                    {
                        cell.BackColor = Color.DarkBlue;
                        cell.ForeColor = Color.White;

                        //e.Row.Cells[18].Enabled = false;
                    }
                    else
                    {
                        cell.BackColor = Color.DarkRed;
                        cell.ForeColor = Color.White;
                        //e.Row.Cells[18].Enabled = false;
                    }

                    string user = Session["user"] as string;
                    if (user.Equals("luis"))
                    {
                        e.Row.Cells[18].Enabled = true;
                    }
                    else
                    {
                        e.Row.Cells[18].Enabled = false;
                    }

                }
            }
        }

        public void limpaCamposTela()
        {


        }

        protected void btnLimparCampos_Click(object sender, EventArgs e)
        {
            limpaCamposTela();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        public int recuperaIdInternoNumeroPedido(string numeroPedido)
        {
            int id = 0;
            
            string sql = "select idPedido from tb_pedido where nu_numeroPedido='"+numeroPedido+"'";
            


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_pedido");

            foreach (DataRow pRow in customerOrders.Tables["tb_pedido"].Rows)
            {

                id = Convert.ToInt32(pRow["idPedido"].ToString());


            }

            return id;
        }


        public void salvarContasAPagar(int pedido, int idCliente, int idTipoPagamento,  int quantidadeParcela, decimal valorParcelaCadastro, int dataEmissao, int data1Parcela )
        {
            //recuperaValoresTela("INSERT");

            //
            int idOperador = 0;
            bllOperador = new BLLOperador(conexao);
            ModeloOperador modeloOperador = new ModeloOperador();
            if (!string.IsNullOrEmpty(Session["user"] as string))
            {
                //modeloOperador = bllOperador.recuperaIdLogin(Session["user"].ToString());
                //idOperador = Convert.ToInt32(modeloOperador.Id);
                idOperador = 1;
            }
            //



            DateTime dataAux = DateTime.Today;
            int count = 0;
            int parcela = 1;

            string numeroContratoAutorizacaoCartao = "";
            numeroContratoAutorizacaoCartao = txtNumeroContratoAutorizacao.Text.ToString();

            for (int i = 0; i < quantidadeParcela; i++)
            {
               
             
                //realiza inserção das parcelas no Contas a Receber
                ModeloContaReceber conta = new ModeloContaReceber();
                conta.IdPedido = recuperaIdInternoNumeroPedido(pedido.ToString());
                conta.IdCliente = Convert.ToInt32(idCliente.ToString());
                conta.IdTipoPagamento = Convert.ToInt32(idTipoPagamento);
                conta.NumeroParcela = parcela;
                conta.QuantidadeParcela = quantidadeParcela;
                conta.CD_codOperador = idOperador;
                conta.NumeroContratoAutorizacao = numeroContratoAutorizacaoCartao;
                conta.DataEmissao = Convert.ToInt32(dataEmissao);



                /**/
                //conta.Observacoes = "Inserido via Contas a Receber";
                conta.Observacoes = txtObservacaoConta.Text.ToString().Trim();
                /**/
                conta.ValorConta = Convert.ToDecimal(valorParcelaCadastro);
                conta.ValorJuros = 0;
                conta.ValorMulta = 0;


                //07032018 - Campos Novos - Maquina e Bandeira Cartão (ou financeira)
                conta.IdMaquinaCartao = 0;
                conta.IdBandeiraCartao = 0;
                //

                conta.ValorTotal = Convert.ToDecimal(valorParcelaCadastro);



                /*string dataPedido = iDataPedido.ToString();
                DateTime dtAtual = DateTime.ParseExact(dataPedido,
                                                    "yyyyMMdd",
                                                    CultureInfo.InvariantCulture,
                                                    DateTimeStyles.None);*/

                string sugestao1Vencimento = data1Parcela.ToString();
                DateTime dt1Vencimento = DateTime.ParseExact(sugestao1Vencimento,
                                                    "yyyyMMdd",
                                                    CultureInfo.InvariantCulture,
                                                    DateTimeStyles.None);


                /*na primeira ITERAÇÃO us a data 1venc sugerida
                 * na segunda em dia fica sendo a data +30 */
                if (quantidadeParcela == 1)
                {
                    dataAux = dt1Vencimento;

                }
                else
                {
                    dataAux = dt1Vencimento.AddDays(30 * i);


                    //atualiza data Auxiliar em +30 dias
                    //dataAux = dataAux.AddDays(30);
                }

                txtData1Parcela.Text = dataAux.ToString("yyyy-MM-dd");
                conta.DataVencimento = Convert.ToInt32(txtData1Parcela.Text.ToString().Replace("-", ""));

                //conta.DataRecebimento = 0;

                conta.ValorRecebido = 0;
                conta.SituacaoPagamento = "Pendente";
                conta.DataRecebimento = 0;

                if (conta.IdTipoPagamento == 1)
                {
                    conta.SituacaoPagamento = "Quitado";
                    conta.ValorRecebido = conta.ValorConta;
                    conta.DataRecebimento = conta.DataEmissao;
                }
                if (conta.IdTipoPagamento == 2)
                {
                    conta.SituacaoPagamento = "Conferir";
                    conta.ValorRecebido = conta.ValorConta;
                    conta.DataRecebimento = conta.DataVencimento;
                }
                if (conta.IdTipoPagamento == 3)
                {
                    conta.SituacaoPagamento = "Conferir";
                    conta.ValorRecebido = conta.ValorConta;
                    conta.DataRecebimento = conta.DataVencimento;
                }
                if (conta.IdTipoPagamento == 4)
                {
                    conta.SituacaoPagamento = "Conferir";
                    conta.ValorRecebido = conta.ValorConta;
                    conta.DataRecebimento = conta.DataVencimento;
                }
                if (conta.IdTipoPagamento == 5)
                {
                    conta.SituacaoPagamento = "Conferir";
                    conta.ValorRecebido = conta.ValorConta;
                    conta.DataRecebimento = conta.DataVencimento;
                }
                
                    
                



                conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                BLLContaReceber contasBLL = new BLLContaReceber(conexao);

                int idConta = 0;
                idConta = Convert.ToInt32(contasBLL.Incluir(conta));

                /*Se incluiu a conta, também inclui um evento para esta*/
                if(idConta > 0)
                {
                    ModeloLogEvento modeloLogEvento = new ModeloLogEvento();
                    modeloLogEvento.IdTabela = 4; //Tabela de Contas a Receber
                    modeloLogEvento.IdCampo = idConta;
                    modeloLogEvento.Descricao = "Inclusão de Conta a Receber";
                    modeloLogEvento.Valor = "Motivo= Inclusão via Tela de Contas a Receber";
                    modeloLogEvento.Valor2 = "Manual";

                    BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);
                    bllLogEvento.Incluir(modeloLogEvento);


                    // Gera tb o Evento Financeiro
                    ModeloEventoFinanceiro modeloEventoFinanceiro = new ModeloEventoFinanceiro();
                    modeloEventoFinanceiro.IdTabela = 6; //Tabela de Eventos Financeiros ( TB_eventoFinanceiro )
                    modeloEventoFinanceiro.IdConta = idConta;
                    modeloEventoFinanceiro.IdTipoConta = 1; //1-A Receber / 2-A Pagar
                    modeloEventoFinanceiro.IdTipoPagamento = Convert.ToInt32(conta.IdTipoPagamento);
                    modeloEventoFinanceiro.IdHistoricoFinanceiro = 0;
                    modeloEventoFinanceiro.ValorEvento = Convert.ToDecimal(conta.ValorRecebido);
                    modeloEventoFinanceiro.DataEvento = Convert.ToInt32(conta.DataRecebimento);
                    modeloEventoFinanceiro.DE_Observacoes = "";
                    modeloEventoFinanceiro.CD_operador = 0;
                    modeloEventoFinanceiro.DT_dataCriacao = Convert.ToInt32(dataHoje);
                    modeloEventoFinanceiro.HR_horaCriacao = Convert.ToInt32(horaHoje);
                    modeloEventoFinanceiro.SituacaoPagamento = "Recebido";
                    modeloEventoFinanceiro.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
                    modeloEventoFinanceiro.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
                    modeloEventoFinanceiro.ST_situacaoRegistro = "A";

                    BLLEventoFinanceiro bllEventoFinanceiro = new BLLEventoFinanceiro(conexao);
                    bllEventoFinanceiro.Incluir(modeloEventoFinanceiro);

                }
                /*  */

                //contasBLL.Incluir(conta);
                parcela++;
                count++;
            }

            
            if(count > 0) { 
                lblMensagem.Text = "Conta(s) a Receber Inserida(s) com sucesso !";
                openModalMensagem();
                limpaCamposTela();
            }
            else
            {
                lblMensagem.Text = "Conta não inserida.";
                openModalMensagem();
            }
            
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            int pedido = 0;
            pedido = Convert.ToInt32(txtIdPedido.Text);
            int idCliente = 0;
            idCliente = Convert.ToInt32(txtIdClienteCadastro.Text);
            int idTipoPagamento = 0;
            idTipoPagamento = Convert.ToInt32(ddlTipoPagamento.SelectedValue); 
            int quantidadeParcelas = 0;
            quantidadeParcelas = Convert.ToInt32(txtQuantidadeParcelaCadastro.Text);
            decimal valorParcela = 0;
            valorParcela = Convert.ToDecimal(txtValorParcelaCadastro.Text.ToString().Replace(".", ","));
            int dataEmissao = 0;
            dataEmissao = Convert.ToInt32(txtDataEmissaoCadastro.Text.ToString().Replace("-", ""));
            int data1Parcela = 0;
            data1Parcela = Convert.ToInt32(txtData1Parcela.Text.ToString().Replace("-", ""));

            salvarContasAPagar(pedido, idCliente, idTipoPagamento, quantidadeParcelas, valorParcela, dataEmissao, data1Parcela);
        }

        protected void btnAtualizar_Click(object sender, EventArgs e)
        {
         //   recuperaValoresTela("UPDATE");
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Server.Transfer("ContasAReceber.aspx", true);

        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            exibeMVC("", "I", "1");
        }

        public string recuperaProximoIdContaAReceber()
        {

            string sql = "select (max(id) + 1) as ID from tb_contasReceber";
            string proximoId = "";


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_contasReceber");

            foreach (DataRow pRow in customerOrders.Tables["tb_contasReceber"].Rows)
            {

                proximoId = pRow["ID"].ToString();


            }

            if (proximoId.Equals("")) proximoId = "1";

            return proximoId;

        }

        public void exibeMVC(string id, string modoMVC, string usuario)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
           
            panelBotoes.Visible = false;

            if (modoMVC.Equals("I"))
            {
                panelCadastro.Visible = true;

                lblMVC.Text = "Modo de inclusão";
                btnSalvar.Visible = true;
                btnAtualizar.Visible = false;
                btnLimparCampos.Visible = true;
                btnCancelar.Visible = true;

                string proximoIdConta = "";

                proximoIdConta = recuperaProximoIdContaAReceber();

                txtIdCadastro.Text = proximoIdConta.ToString();
                txtDataEmissaoCadastro.Text = DateTime.Today.ToString("yyyy-MM-dd");
                DateTime dt1Parcela = DateTime.Today;
                txtData1Parcela.Text = dt1Parcela.AddDays(30).ToString("yyyy-MM-dd");

                //Temporario




                //Fim-Temporario

                //incializaCamposTeste();
            }
            else if (modoMVC.Equals("A"))
            {
                lblMVC.Text = "Modo de edição";
                btnSalvar.Visible = false;
                btnAtualizar.Visible = true;
                btnLimparCampos.Visible = false;
                btnCancelar.Visible = true;

                //camposClienteSomenteLeitura("N");

                if (!id.Equals(""))
                {
                    //recuperarDadosId(Convert.ToInt32(id));
                }
            }
        }

        protected void btnBuscarClienteCadastro_Click(object sender, EventArgs e)
        {
            string cpfCliente = "";

            cpfCliente = txtCpfClienteCadastro.Text.ToString();

            if (cpfCliente != "")
            {
                //busca o cpf na base de dados e recupear o Codigo Interno + Nome do Cliente

                cpfCliente = cpfCliente.Replace(".", "").Replace("-", "").Replace("-", "").Replace("/", "");
                while (cpfCliente.Length < 14)
                {
                    cpfCliente = "0" + cpfCliente;
                }

                // procura dados do produto

                ModeloCliente modelo = new ModeloCliente();

                DALConexao conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                //SqlDataAdapter da = new SqlDataAdapter("select id from tb_cliente where nu_cpfcnpj= @cpjcnpj", conexao.StringConexao);

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conexao.ObjetoConexao;
                cmd.CommandText = "select id, cd_codigo, nm_nomecompleto from tb_cliente where nu_cpfcnpj = @cpjcnpj";
                cmd.Parameters.AddWithValue("@cpjcnpj", cpfCliente.ToString());
                conexao.Conectar();

                SqlDataReader registro = cmd.ExecuteReader();
                if (registro.HasRows)
                {
                    registro.Read();
                    modelo.Id = Convert.ToInt32(registro["id"].ToString());
                    modelo.CD_codigo = registro["cd_codigo"].ToString();
                    modelo.NM_nomeCompleto = registro["nm_nomecompleto"].ToString();

                    string sqlTipoPagamentoCliente = "select ID as ID, NM_nome as NM_nome from tb_tipoPagamento where 1 = 1 ";
                    sqlTipoPagamentoCliente += " and id in( select ID_tipoPagamento from TB_autorizacaoCliente where id_cliente="+ modelo.Id + " and ST_flagAutorizado='S' )";
                    sqlTipoPagamentoCliente += " and st_situacaoregistro='A' order by NM_nome asc";

                    preencheCombo(ddlTipoPagamento, sqlTipoPagamentoCliente, "N");
                }

                conexao.Desconectar();

                //txtIdCadastro.Text = modelo.Id.ToString();
                txtNomeClienteCadastro.Text = modelo.CD_codigo + " - "+ modelo.NM_nomeCompleto.ToString();
                txtIdClienteCadastro.Text = modelo.Id.ToString();


            }
        }



        public void atualizaValorParcela()
        {

            double valorContaCadastro = 0;
            double valorParcelaCadastro = 0;
            int quantiadeParcelaCadastro = 0;

            quantiadeParcelaCadastro = Convert.ToInt32(txtQuantidadeParcelaCadastro.Text.Trim().Replace(".",","));

            valorContaCadastro = Convert.ToDouble(txtValorContaCadastro.Text.Trim().Replace(".", ","));

            if(quantiadeParcelaCadastro > 0)
            {
                valorParcelaCadastro = valorContaCadastro / quantiadeParcelaCadastro;
                txtValorParcelaCadastro.Text = valorParcelaCadastro.ToString();
            }else
            {
                txtValorParcelaCadastro.Text = txtValorContaCadastro.Text;
            }
            

        }

        protected void txtValorContaCadastro_TextChanged(object sender, EventArgs e)
        {
            atualizaValorParcela();
        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            atualizaValorParcela();
        }

        protected void btnSalvarModal_Click(object sender, EventArgs e)
        {
            //Abre o Modal
            
        }

        [WebMethod]
        public static void cripMD5()
        {
            string senhaDigitada = "";

            //senhaDigitada = txtSenhaAutorizacao.Text.ToString().Trim();

            if (!senhaDigitada.ToString().Trim().Equals(""))
            {

                

                // Calcula a senha com base na string senha e armazena na string senhaCriptografada 

                //string senhaCriptografada = util.CalculaHash(senhaDigitada);

                //txtSenhaAutorizacao.Text = senhaCriptografada;
            }
        }


        protected void btnConfirmarRecebiment_Click(object sender, EventArgs e)
        {
            //cripMD5();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalMensagem();", true);

            //realiza inserção das parcelas no Contas a Receber
            ModeloContaReceber conta = new ModeloContaReceber();
            conta.Id = Convert.ToInt32(txtModalId.Text);
            conta.DataRecebimento = Convert.ToInt32(txtModalDataRecebimento.Text.ToString().Replace("-", ""));
            conta.ValorConta = Convert.ToDecimal(txtModalValorConta.Text.ToString().Replace(".", ","));
            conta.ValorJuros = Convert.ToDecimal(txtModalValorJuros.Text.ToString().Replace(".", ","));
            conta.ValorMulta = Convert.ToDecimal(txtModalValorMulta.Text.ToString().Replace(".", ","));
            //conta.ValorTotal = Convert.ToDecimal(txtModalValorTotal.Text.ToString().Replace(".", ","));
            //campo valor Total é dinâmico

            //conta.ValorRecebido = Convert.ToDecimal(txtModalValorTotal.Text.ToString().Replace(".", ","));
            conta.ValorRecebido
                = Convert.ToDecimal(txtModalValorRecebimento.Text.ToString().Replace(".", ","));

            string tipoRecebimento = "";

           

            //incluir ddl historico financeiro

            //conta.SituacaoPagamento = ;

            string motivoBaixa = "";
            motivoBaixa = ddlModalHistoricoFinanceiro.SelectedItem.ToString();
            motivoBaixa = "0";

            string tipoPagamento = "";
            tipoPagamento = ddlModalTipoPagamento.SelectedItem.ToString();
            tipoPagamento = "0";
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLContaReceber contasBLL = new BLLContaReceber(conexao);

            int idConta = 0;
            idConta = conta.Id;

            int linhasAfetadas = 0;
            linhasAfetadas = Convert.ToInt32(contasBLL.BaixarConta(conta,tipoRecebimento));



            /* Até a data de 23/04/2018
             * Ao realizar a baixa da parcela,seja total ou parcial, o sistema atualiza os campos;
             * Data Recebimento e Valor Recebimento, sendo este ultimo, menor que a conta, considerado parcial,
             * no entanto, fica inviavel fazer mais de um recebimento parcial.
             * 
             * Para resolver o problema foi criada a tabela TB_eventoFinanceiro onde é lançados os recebimentos da conta, 
             * e assim, o valor pago da conta é o total de recebimentos da mesmo presentes na tabela TB_eventoFinanceiro (representado
             * na View VW_contasAReceber) e não mais o campo físico da tabela TB_contasReceber 
             *
             * Um registro na tabela TB_logEvento era feito e agora serão dois, um para a tabela tb_contasReceber (continua) e um 
             * para a tabela TB_eventoFinanceiro
             */

            ModeloEventoFinanceiro modeloEventoFinanceiro = new ModeloEventoFinanceiro();
            modeloEventoFinanceiro.IdTabela = 6; //Tabela de Eventos Financeiros ( TB_eventoFinanceiro )
            modeloEventoFinanceiro.IdConta = idConta;
            modeloEventoFinanceiro.IdTipoConta = 1; //1-A Receber / 2-A Pagar
            modeloEventoFinanceiro.IdTipoPagamento = Convert.ToInt32(tipoPagamento);
            modeloEventoFinanceiro.IdHistoricoFinanceiro = Convert.ToInt32(motivoBaixa);
            modeloEventoFinanceiro.ValorEvento = Convert.ToDecimal(txtModalValorRecebimento.Text.ToString().Replace(".", ","));
            modeloEventoFinanceiro.DataEvento = Convert.ToInt32(txtModalDataRecebimento.Text.ToString().Replace("-", ""));
            modeloEventoFinanceiro.DE_Observacoes = txtModalObservacoes.Text.Trim().ToString();
            modeloEventoFinanceiro.CD_operador = 0;
            modeloEventoFinanceiro.DT_dataCriacao = Convert.ToInt32(dataHoje);
            modeloEventoFinanceiro.HR_horaCriacao = Convert.ToInt32(horaHoje);
            modeloEventoFinanceiro.SituacaoPagamento = "Recebido";
            modeloEventoFinanceiro.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
            modeloEventoFinanceiro.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
            modeloEventoFinanceiro.ST_situacaoRegistro = "A";

            BLLEventoFinanceiro bllEventoFinanceiro = new BLLEventoFinanceiro(conexao);
            bllEventoFinanceiro.Incluir(modeloEventoFinanceiro);


            /*teste impressão comprovante de baixa
             * Tem que ter opção de imprimir ou não*/

            impressaoParcelaPrestacao = txtModalNumeroParcela.Text.ToString();
            impressaoQuantidadePrestacao = txtModalQuantidadeParcela.Text.ToString();




            //impressaoValorTroco = "";



            /* Para obtenção do valor do Saldo da Parcela é necessário checar se já existe
             * pagamento para a prestação em questão, afim de evitar subtração errada*/
            //obtemSaldoAPagarDaPrestacao(conta.Id, conta.NumeroParcela);
            //impressaoValorSaldo = (conta.ValorConta - conta.ValorRecebido).ToString();



            string imprimirComprovanteRec = "";
            imprimirComprovanteRec =  ConfigurationManager.AppSettings["imprimirComprovanteRec"];
            if (imprimirComprovanteRec.Equals("SIM")) imprimirComprovanteRecebimento();
            

            //

            /**/

            //registra evento ao concluir baixa da parcela
            ModeloLogEvento modeloLogEvento = new ModeloLogEvento();
            modeloLogEvento.IdTabela = 4; //Tabela de Contas a Receber
            modeloLogEvento.IdCampo = idConta;

            if (linhasAfetadas > 0)
            {
                

                modeloLogEvento.Descricao = "Baixa de Conta a Receber";
                modeloLogEvento.Valor = "Motivo="+ motivoBaixa;
                

                //verificar se o valor do recebimento é Parcial
                if (conta.ValorRecebido.Equals(conta.ValorConta))
                {
                    tipoRecebimento = "T"; //pagamento TOTAL
                    modeloLogEvento.Valor2 = "Baixada Total";
                }
                else if (conta.ValorRecebido < conta.ValorConta)
                {
                    tipoRecebimento = "P"; //pagamento PARCIAL, gerar pendencia
                    modeloLogEvento.Valor2 = "Baixada Parcial";
                }
                else if (conta.ValorRecebido > conta.ValorConta)
                {
                    tipoRecebimento = "M"; //pagamento MAIOR, credito cliente
                    modeloLogEvento.Valor2 = "Baixada Maior";
                }
                else
                {
                    tipoRecebimento = "?";
                    modeloLogEvento.Valor2 = "Baixada ?";
                }
                //


            }
            else { 
                modeloLogEvento.Descricao = "Baixa de Conta a Receber";
                modeloLogEvento.Valor = "Nenhuma linha afetada";
                modeloLogEvento.Valor2 = "Falha";
                lblMensagem.Text = "Erro ao baixar conta ID: " + conta.Id;
            }

            //carregaGrid(" and id_pedido=" + conta.IdPedido);

            BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);
            bllLogEvento.Incluir(modeloLogEvento);

        }

        public string obtemSaldoAPagarDaPrestacao(string idConta, string parcela)
        {
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLContaReceber bLLContaReceber = new BLLContaReceber(conexao);
            bLLContaReceber.obtemSaldoAPagarDaPrestacao(idConta, parcela);
            return "";
        }

        private void CarregarListaDeImpressoras()
        {
            ddlModalImpressora.Items.Clear();

            foreach (var printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                ddlModalImpressora.Items.Add(printer.ToString());
            }
        }

        private void GetAllPrinterList()
        {
            ManagementScope objScope = new ManagementScope(@"\root\cimv2");
            //ManagementScope objScope = new ManagementScope(ManagementPath.DefaultPath); //For the local Access
            objScope.Connect();

            SelectQuery selectQuery = new SelectQuery();
            selectQuery.QueryString = "Select * from win32_Printer";
            ManagementObjectSearcher MOS = new ManagementObjectSearcher(objScope, selectQuery);
            ManagementObjectCollection MOC = MOS.Get();
            ddlModalImpressora.Items.Clear();
            foreach (ManagementObject mo in MOC)
            {
                ddlModalImpressora.Items.Add(mo["Name"].ToString());
            }
        }


        private void imprimirComprovanteRecebimento()
        {
            using (var printDocument = new System.Drawing.Printing.PrintDocument())
            {
                printDocument.PrintPage += printDocument_PrintPage;
                printDocument.PrinterSettings.PrinterName = ddlModalImpressora.SelectedItem.ToString();
                printDocument.Print();
            }
        }

        void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            // for(int i = 0; i < 2; i++)
            //{

            string dataImpressao = String.Format("{0:dd/MM/yyyy}", DateTime.Now.Date);
            string horaImpressao = String.Format("{0:HH:mm:ss}", DateTime.Now);

            DateTime dtVen ;
            dtVen = Convert.ToDateTime(txtModalDataRecebimento.Text);
            impressaoDataVencimentoPrestacao = dtVen.ToString("dd/MM/yyyy");
            impressaoNumeroPedido = txtModalPedido.Text.ToString();

            string specifier = "C";

            impressaoValorPago = String.Format("{0:C}", txtModalValorRecebimento.Text.ToString());
            impressaoValorPrestacao = String.Format("{0:C}", txtModalValorConta.Text.ToString());
            //impressaoValorSaldo = obtemSaldoAPagarDaPrestacao(txtModalId.Text.ToString(), txtModalNumeroParcela.Text.ToString());

            

            var printDocument = sender as System.Drawing.Printing.PrintDocument;

            string textoTemp = "";
            textoTemp = textoTemp + Environment.NewLine + "";
            textoTemp = textoTemp + Environment.NewLine + "Soberana Móveis e Eletrodomésticos ";
            textoTemp = textoTemp + Environment.NewLine + "";
            textoTemp = textoTemp + Environment.NewLine + "Data: " + dataImpressao + " - " + horaImpressao;
            textoTemp = textoTemp + Environment.NewLine + "Recebimento de Prestação";
            textoTemp = textoTemp + Environment.NewLine + "Pedido: "+ txtModalPedido.Text.ToString();
            textoTemp = textoTemp + Environment.NewLine + "Recebido por: " +  ddlUsuarioAutorizacao.SelectedItem.ToString();
            textoTemp = textoTemp + Environment.NewLine + "Vencimento: "+impressaoDataVencimentoPrestacao+" | Parcela: "+ impressaoParcelaPrestacao + " / "+ impressaoQuantidadePrestacao;
            //textoTemp = textoTemp + Environment.NewLine + "Parce: R$ " + impressaoValorPrestacao + " | Pagam: R$ " + impressaoValorPago;

            if (impressaoValorSaldo == "0" && txtModalValorPago.Text.ToString().Trim().Equals(""))
            {
                textoTemp = textoTemp + Environment.NewLine + "Tipo: Integral" + " | Parcela: R$ " + impressaoValorPrestacao;
            }
            else
            {
                textoTemp = textoTemp + Environment.NewLine + "Tipo: Parcial" + " | Parcela: R$ " + impressaoValorPrestacao;
            }

            textoTemp = textoTemp + Environment.NewLine + "Pagamento: R$ " + impressaoValorPago;
            //textoTemp = textoTemp + Environment.NewLine + "Saldo: R$ " + impressaoValorSaldo;
            //textoTemp = textoTemp + Environment.NewLine + "Troco: R$ " + impressaoValorTroco + " | Saldo: R$ " + impressaoValorSaldo;
            textoTemp = textoTemp + Environment.NewLine + "";
            textoTemp = textoTemp + Environment.NewLine + "Mantenha suas prestações em dia.";
            textoTemp = textoTemp + Environment.NewLine + "Obrigado pela preferência.";
            textoTemp = textoTemp + Environment.NewLine + "";
            //textoTemp = textoTemp + Environment.NewLine + "Pedido Nº " + nPedido + Environment.NewLine + "Item: " + itemPedido + Environment.NewLine + "Quantidade: " + quantidade;

            if (printDocument != null)
            {
                //using (var font = new Font("Times New Roman", 10))
                using (var font = new Font("Lucida Console", 8))
                using (var brush = new SolidBrush(Color.Black))
                {



                    e.Graphics.DrawString(
                        textoTemp.ToString(),
                        font,
                        brush,
                        new RectangleF(0, 0, printDocument.DefaultPageSettings.PrintableArea.Width, printDocument.DefaultPageSettings.PrintableArea.Height));
                }
            }
            //}


        }


        protected void btnEstornar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            bool contaEstornavel = false;

            int idConta = 0;

            //Obtem o ID da Conta a Receber para realizar a verificação de eligibilidade de Estorno
            //Elaborar regras com a Camila
            idConta = Convert.ToInt32(gvr.Cells[0].Text);

            if (idConta > 0)
            {
                /*Verifica se a conta a receber pode ser cancelada
                 * 1.Checa se a mesma já está recebida, se sim, informa que o estorno deve ser realizado antes 
                 * (por padrão o botão de exclusao não deve aparecer, porém a checagem é por segurança)
                 * 2.Checa a tabela de carnes
                 * 3.Checa a tabela de cupom fiscal emitido
                 * 4.Checa a movimentação de produtos no estoque
                 * 
                 * ** Registra na tabela TB_evento
                 * */


                //recupera lista de impressoras
                //CarregarListaDeImpressoras();
                GetAllPrinterList();
                //contaCancelavel = verificaSeAContaPodeSerExcluida(idConta);



                contaEstornavel = true;

                if (contaEstornavel)
                {

                    /*preenche listbox Evento Financeiro, com os recebimentos
                     * que a conta em questão possui na tabela de eventoFinanceiro*/
                    string sqlEventosFinanceirosConta = "";
                    //sqlEventosFinanceirosConta = "select ID as ID, ('R$ ' +cast(vl_valorEvento as varchar) + ' em ' + cast(Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataEvento),106),103) as varchar) )as nm_nome  from tb_eventoFinanceiro where id_conta= "+ idConta.ToString() + " order by dt_dataEvento asc, id asc";
                    sqlEventosFinanceirosConta = "select ID as ID, ('R$ ' +cast(vl_valorEvento as varchar) + ' de ' + cast(Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataEvento),106),103) as varchar) +' | Recebido em ' +  cast(Convert(varchar(10),CONVERT(date,convert(varchar,dt_dataAtualizacao),106),103) as varchar) + ' as ' + cast(HR_horaAtualizacao as varchar)   )as nm_nome  from tb_eventoFinanceiro where id_conta= " + idConta.ToString() + " order by dt_dataEvento asc, id asc";
                    util.preencheCombo(ddlEventoFinanceiro, sqlEventosFinanceirosConta, "S");
                    ddlEventoFinanceiro.Visible = true;
                    //

                    txtJustificativaEstornoConta.Visible = true;
                    btnConfirmarEstornoConta.Visible = true;
                    btnConfirmarEstornoConta.Enabled = true;
                    txtModalIDConfirmacaoEstornoConta.Text = idConta.ToString();
                    //Abre o Modal
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalConfirmacaoEstornoConta();", true);
                }
                else
                {
                    txtJustificativaEstornoConta.Visible = false;
                    btnConfirmarEstornoConta.Visible = false;
                    btnConfirmarEstornoConta.Enabled = false;
                }



            }
            else
            {
                //id pedido não pode ser 0
            }

        }

        protected void btnExcluir_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;


            bool contaCancelavel = false;

            int idConta = 0;

            //Obtem o ID da Conta a Receber para realizar a verificação de eligibilidade de Exclusão
            idConta = Convert.ToInt32(gvr.Cells[0].Text);

            if (idConta > 0)
            {
                /*Verifica se a conta a receber pode ser cancelada
                 * 1.Checa se a mesma já está recebida, se sim, informa que o estorno deve ser realizado antes 
                 * (por padrão o botão de exclusao não deve aparecer, porém a checagem é por segurança)
                 * 2.Checa a tabela de carnes
                 * 3.Checa a tabela de cupom fiscal emitido
                 * 4.Checa a movimentação de produtos no estoque
                 * 
                 * ** Registra na tabela TB_evento
                 * */

                //contaCancelavel = verificaSeAContaPodeSerExcluida(idConta);

                contaCancelavel = true;

                if (contaCancelavel)
                {


                    txtJustificativaExclusaoConta.Visible = true;
                    btnConfirmarExclusaoConta.Visible = true;
                    btnConfirmarExclusaoConta.Enabled = true;
                    txtModalIDConfirmacaoExclusaoConta.Text = idConta.ToString();
                    //Abre o Modal
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalConfirmacaoExclusaoConta();", true);
                }
                else
                {
                    txtJustificativaExclusaoConta.Visible = false;
                    btnConfirmarExclusaoConta.Visible = false;
                    btnConfirmarExclusaoConta.Enabled = false;
                }



            }
            else
            {
                //id pedido não pode ser 0
            }



        }



        protected void btnConfirmarExclusaoConta_Click(object sender, EventArgs e)
        {
            int idConta = 0;
            idConta = Convert.ToInt32(txtModalIDConfirmacaoExclusaoConta.Text.ToString());

            

            string justificativa = "";
            justificativa = txtJustificativaExclusaoConta.Text.ToString().Trim();

            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLContaReceber bLLContaReceber = new BLLContaReceber(conexao);

            int qtdLinhasAfetadas = 0;

            ModeloLogEvento modeloLogEvento = new ModeloLogEvento();

            if (idConta > 0)
            {
                //se afetou a linha, ou seja, atualizou, registra o evento de cancelamento
                qtdLinhasAfetadas = bLLContaReceber.ExcluirContasAReceber(idConta);

                modeloLogEvento.IdTabela = 4; //Tabela de Contas a Receber
                modeloLogEvento.IdCampo = idConta;

                if (qtdLinhasAfetadas > 0)
                {
                    modeloLogEvento.Descricao = "Exclusão de Conta a Receber";
                    modeloLogEvento.Valor = "Motivo=" + justificativa;
                    modeloLogEvento.Valor2 = "Excluido";
                    BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);
                    bllLogEvento.Incluir(modeloLogEvento);
                }
                else //senao registra o nao cancelamento
                {
                    modeloLogEvento.Descricao = "Exclusão de Conta a Receber";
                    modeloLogEvento.Valor = "Nenhuma linha afetada";
                    modeloLogEvento.Valor2 = "Falha";
                }



            }
            else
            {
                lblMensagem.Text = "O ID da conta a receber não pode ser ZERO.";
                openModalMensagem();
            }

        }

        protected void btnConfirmarEstornoConta_Click(object sender, EventArgs e)
        {
            int idConta = 0;
            idConta = Convert.ToInt32(txtModalIDConfirmacaoEstornoConta.Text.ToString());

            int idEventoFinanceiro = 0;
            idEventoFinanceiro = Convert.ToInt32(ddlEventoFinanceiro.SelectedValue);

            string justificativa = "";
            justificativa = txtJustificativaEstornoConta.Text.ToString().Trim();

            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLContaReceber bLLContaReceber = new BLLContaReceber(conexao);

            BLLEventoFinanceiro bLLEventoFinanceiro = new BLLEventoFinanceiro(conexao);

            int qtdLinhasAfetadas = 0;

            ModeloLogEvento modeloLogEvento = new ModeloLogEvento();

            if (idConta > 0)
            {
                //se estornou, atualiza valore staea
                ModeloEventoFinanceiro modelo = new ModeloEventoFinanceiro();
                modelo.IdConta = idConta;
                conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                BLLEventoFinanceiro dEvFin = new BLLEventoFinanceiro(conexao);
                dEvFin.atualizaValorRecebidoTabelaContasReceberAposRecebimento(modelo);


                //se afetoua linha, ou seja, atualizou, registra o evento de cancelamento

                //qtdLinhasAfetadas = bLLContaReceber.EstornarContasAReceber(idConta);
                qtdLinhasAfetadas = bLLEventoFinanceiro.EstornarEventoFinanceiro(idConta,idEventoFinanceiro);

                modeloLogEvento.IdTabela = 4; //Tabela de Contas a Receber
                modeloLogEvento.IdCampo = idConta;

                if (qtdLinhasAfetadas > 0)
                {
                    modeloLogEvento.Descricao = "Estorno de Conta a Receber";
                    modeloLogEvento.Valor = "Motivo=" + justificativa;
                    modeloLogEvento.Valor2 = "Estornada";
                    BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);
                    bllLogEvento.Incluir(modeloLogEvento);
                }
                else //senao registra o nao cancelamento
                {
                    modeloLogEvento.Descricao = "Estorno de Conta a Receber";
                    modeloLogEvento.Valor = "Nenhuma linha afetada";
                    modeloLogEvento.Valor2 = "Falha";
                }



            }
            else
            {
                lblMensagem.Text = "O ID da conta a receber não pode ser ZERO.";
                openModalMensagem();
            }
        }
    }
    
}
