﻿using AcessoBancoDados;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class _Default : Page
    {

        private DALConexao conexao;

        string connectionString = "";
        Util util = new Util();

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
            else
            {
                lblUsuarioLogado.Text = "Olá, bem vindo(a) "+Session["user"].ToString();
            }
        }

        


        protected void Page_Load(object sender, EventArgs e)
        {
            testCheckUserSoberana();

            util = new Util();
            connectionString = DadosDaConexao.StringDeConexao(util.obtemAmbiente());
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                //aniversariantesDoMes();

            }
            

            

            aniversariantesDoMes();
            
        }

        

        


        public void aniversariantesDoMes()  {

            //string niverMes = "select NM_nomeCompleto, dt_datanascimento  from TB_cliente where left(right(dt_dataNascimento, 4), 2) = left(right(replace(CONVERT(date, GETDATE()), '-', ''), 4), 2)";
            string niverMes = "select NM_nomeCompleto as Cliente, ";
            niverMes += " Convert(varchar(10),CONVERT(date,convert(varchar,dt_datanascimento),106),103) as Data  ";
            niverMes += " from TB_cliente where left(right(dt_dataNascimento, 4), 2) = left(right(replace(CONVERT(date, GETDATE()), '-', ''), 4), 2)";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            //DataSet ds = GetData(niverMes);
            DataSet ds = util.ObtemDados(niverMes);
            if (ds.Tables.Count > 0)
            {
                GridViewAniverMes.DataSource = ds;
                GridViewAniverMes.DataBind();
                lblMensagemAniverMes.Text = "";
            }
            else
            {
                lblMensagemAniverMes.Text = "Não foi possível conectar a base de dados.";
            }

        }

       

        
    }
}