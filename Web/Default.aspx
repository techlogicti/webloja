﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Web._Default" %>

<script runat="server">
    void Page_Load(object sender, EventArgs e)
    {
        //Welcome.Text = "Hello, " + Context.User.Identity.Name;
        //aniversariantesDoMes();
    }

    void Signout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Redirect("Logon.aspx");
    }
</script>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


	<asp:Panel ID="panelCadastro" runat="server" Visible="true" >
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
        <div class="container-full">

			
			<!-- -->
			 <div class="panel-group">
				<div class="panel panel-default">
				  <div class="panel-heading">
						  <div class="alert alert-info">
						  <asp:Label ID="lblUsuarioLogado" runat="server" Text=""></asp:Label>
						</div>
					</div>
					  <div class="panel-body">

						  <!--- -------------------------->
						  <div class="row">
        <div class="col-md-4">
            <h2>Aniversariantes do Mês</h2>
            <p>
                 
                <asp:Label ID="lblMensagemAniverMes" runat="server" Text=""></asp:Label>
                <asp:GridView ID="GridViewAniverMes" 
                     AllowSorting="True" AllowPaging="True"
         GridLines="None"
         CssClass="table table-hover table-striped" BackColor="White"
Runat="Server" PageSize="10000" 
          AutoGenerateColumns="True"></asp:GridView>
            </p>
            <p>
                <a class="btn btn-default" href="#">ver mais &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Contas à Receber</h2>
            <p>
                Soberana Móveis e Eletrodomésticos. Soberana Móveis e Eletrodomésticos. Soberana Móveis e Eletrodomésticos. Soberana Móveis e Eletrodomésticos.
            </p>
            <p>
                <a class="btn btn-default" href="ContasAReceber.aspx">ver mais &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Contas à Pagar</h2>
            <p>
                Soberana Móveis e Eletrodomésticos. Soberana Móveis e Eletrodomésticos. Soberana Móveis e Eletrodomésticos. Soberana Móveis e Eletrodomésticos.
            </p>
            <p>
                <a class="btn btn-default" href="#">ver mais &raquo;</a>
            </p>
        </div>
    </div>

		</div>
						  <!-- --------------------------->

					  </div>
				</div>
				</div>
			  <!-- -->
			
			</ContentTemplate>
		 </asp:UpdatePanel>
	</asp:Panel>
	

    

    
</asp:Content>
