﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Sair : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["user"] = "";
            Response.Redirect("~/Logon.aspx");
            //Server.Transfer("Logon.aspx", true);
        }

        protected void btnSair_Click(object sender, EventArgs e)
        {
            Session["user"] = "";
            //Response.Redirect("~/Default.aspx");
            Server.Transfer("Logon.aspx", true);
        }
    }
}