﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EntradaMercadoria.aspx.cs" Inherits="Web.EntradaMercadoria" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
      <hr />
       <asp:Panel ID="panelBotoes" runat="server" Visible="false">
          <div class="container-full">
            <asp:Button ID="btnListarProdutos" class="btn btn-primary" runat="server" Text="Listar" />
            <asp:Button ID="btnNovo" class="btn btn-success" runat="server" Text="Nova Entrada"  />
         </div>
        </asp:Panel>
      </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>