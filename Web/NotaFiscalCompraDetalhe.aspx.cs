﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class NotaFiscalCompraDetalhe : System.Web.UI.Page
    {

        private DALConexao conexao;
        Util util = new Util();

        string connectionString = "";

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        int idNotaFiscal = 0;
        int idFornecedor = 0;

        ModeloNotaFiscalEntrada modeloNFEntrada;

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            testCheckUserSoberana();   

            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {
                idNotaFiscal = Convert.ToInt32(Page.Request.QueryString["idNotaFiscalEntrada"]);

                if (idNotaFiscal > 0)
                {
                    panelCadastro.Visible = true;

                    modeloNFEntrada = new ModeloNotaFiscalEntrada();

                    modeloNFEntrada = ObtemDadosDaNotaFiscalDeEntrada(idNotaFiscal);

                    txtNumeroNotaFiscalInput.Text = modeloNFEntrada.NumeroNotaFiscal.ToString();

                    idFornecedor = Convert.ToInt32(modeloNFEntrada.IdFornecedor.ToString());

                    ModeloFornecedor modeloFornecedor = new ModeloFornecedor();
                    conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                    BLLFornecedor fornecedorBLL = new BLLFornecedor(conexao);
                    modeloFornecedor = fornecedorBLL.CarregarModeloFornecedor(idFornecedor);

                    txtFornecedorNotaFiscalInput.Text = modeloFornecedor.IdFornecedor + " - " + modeloFornecedor.NomeFantasia;


                    txtDataEmissaoNotaFicalInput.Text = modeloNFEntrada.DataEmissao.ToString();
                    txtDataEntregaNotaFiscalInput.Text = modeloNFEntrada.DataEntrega.ToString();
                    txtValorTotalNotaFiscalInput.Text = modeloNFEntrada.ValorTotalNota.ToString();
                    

                }
                else
                {
                    lblMensagem.Text = "Nao foi possível localizar os dados da nota fiscal";
                    this.openModalMensagem();
                    Server.Transfer("NotaFiscalCompra.aspx", true);
                }

            }

            lblIdNotaFiscal.Text = idNotaFiscal.ToString();

        }

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        public ModeloNotaFiscalEntrada ObtemDadosDaNotaFiscalDeEntrada(int idNotaFiscalEntrada)
        {
            ModeloNotaFiscalEntrada modeloNota = new ModeloNotaFiscalEntrada();
            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLNotaFiscalEntrada bllNota = new BLLNotaFiscalEntrada(conexao);

            int idNota = 0;
            idNota = idNotaFiscalEntrada;

            if (idNota > 0) {

                modeloNota = bllNota.CarregarModeloNotaFiscalEntrada(idNota);

            }
            else
            {
                modeloNota.Id = 0;
            }

            return modeloNota;
        }

       
        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            Server.Transfer("NotaFiscalCompra.aspx", true);
        }

    }
}