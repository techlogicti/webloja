﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cobranca.aspx.cs" Inherits="Web.Cobranca" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

    <ContentTemplate>
     <hr />
    
     <script type="text/javascript">


                  function openModalMensagem() {
                      $('#myModalMensagem').modal('show');
                  }
      </script>
<!--INICIO MODAL - MENSAGEM-->
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <!-- Modal -->
                    <asp:Label ID="Label19" runat="server" Text=""></asp:Label><br />
                    <div class="modal fade" id="myModalMensagem" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMensagem"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabelMensagem">Mensagem</h4>
                                </div>
                                <div class="modal-body">


                                  <div class="row">
                                    <div class="col-sm-12">
                                       <asp:Label ID="lblMensagem" runat="server" Text="#" Font-Bold="True" ForeColor="#FF3300"></asp:Label>     
                                    </div>
                                      
                                  </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
<!--FIM MODAL - MENSAGEM -->

<!--INICIO FILTRO -->
<asp:Panel ID="panelFiltro" runat="server" Visible="true" CssClass="container-full">

        <div class="panel panel-default">
        <div class="panel-heading">Pesquisa</div>
        <div class="panel-body">
                
            <div class="row">
                <div class="col-sm-3">
                    <label for="txtNumPedido">Núm.Pedido</label>
                    <asp:TextBox class="form-control" ID="txtNumPedido" runat="server"></asp:TextBox>
               </div>
                <div class="col-sm-3">
                    <label for="txtNomeCliente">Cliente</label>
                    <asp:TextBox class="form-control" ID="txtNomeCliente" runat="server" ></asp:TextBox>
                   
                </div>

                <div class="col-sm-3">
                <label for="ddlFormaPagamento">Forma de Pagamento</label>
                    <asp:DropDownList ID="ddlFormaPagamento" class="form-control" runat="server" />
                 </div>

                <div class="col-sm-2">
                <asp:Button ID="btnPesquisar" class="btn btn-info" runat="server" Text="Pesquisar"/>
                    <asp:Button ID="btnLimparFiltros" class="btn btn-info" runat="server" Text="Limpar Campos"   />
                    </div>

                 
            </div>
       <br />
           
           

            <div class="row">
                <div class="col-sm-3">
                    <label for="txtDataInicioEmissao">Data de Emissão (de)</label>
                    <asp:TextBox class="form-control" ID="txtDataInicioEmissao" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimEmissao">Data de Emissão (até)</label>
                    <asp:TextBox class="form-control" ID="txtDataFimEmissao" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>
                <div class="col-sm-3">
                    <label for="ddlStatusPagamento">Status Pagamento</label>
                    <asp:DropDownList ID="ddlStatusPagamento" class="form-control" runat="server" />
                </div>
                <div class="col-sm-3">
                    <label for="ddlStatusRegistro">Status </label>
                    <asp:DropDownList ID="ddlStatusRegistro" class="form-control" runat="server" />
                </div>
            </div>
        
            <div class="row">
                <div class="col-sm-3">
                    <label for="txtDataInicioVencimento">Data de Vencimento (de)</label>
                    <asp:TextBox class="form-control" ID="txtDataInicioVencimento" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimVencimento">Data de Vencimento (até)</label>
                    <asp:TextBox class="form-control" ID="txtDataFimVencimento" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>

      <div class="col-sm-3">
                    <label for="txtDataInicioRecebimento">Data de Recebimento(de)</label>
                    <asp:TextBox class="form-control" ID="txtDataInicioRecebimento" runat="server"   TextMode="Date"></asp:TextBox>
                   
                </div>
                <div class="col-sm-3">
                    <label for="txtDataFimRecebimento">Data de Recebimento(até)</label>
                    <asp:TextBox class="form-control" ID="txtDataFimRecebimento" runat="server"   TextMode="Date"></asp:TextBox>
                    
                </div>                

            </div>
        
        </div>
    </div>
    

    </asp:Panel>
<!--FIM FILTRO -->

<!--INICIO LISTAGEM -->
     <asp:Panel ID="panelGrid" runat="server" Visible="true">Detalhe do Pedido de Compra

     <div class="well well-sm"><b>Total de Contas ( <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> )</b></div>

                 <h3>Cobrança</h3>

           

      </asp:Panel>
<!--FIM LISTAGEM -->
    </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
