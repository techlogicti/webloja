﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class PedidoCompra : System.Web.UI.Page
    {

        private DALConexao conexao;
        BLLOperador bllOperador = null;

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);
        string sqlFornecedor = "select idFornecedor as id, DE_nomeFantasia as nm_nome from tb_fornecedor order by de_nomefantasia asc";
        string sqlFuncionario = "select idFuncionario as id, NM_nomeCompleto as nm_nome from tb_funcionario order by NM_nomeCompleto asc";

        String sqlPedidoCompra = "";
       

        Util util = new Util();

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            testCheckUserSoberana();

            lblMensagem.Text = "";
            Util u = new Util();
            connectionString = u.conexao();
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {

                panelBotoes.Visible = true;

                preencheCombo(ddlFornecedorInput, sqlFornecedor,"S");
                preencheCombo(ddlFornecedor, sqlFornecedor, "S");
                util.preencheComboStatusEntrega(ddlStatusEntrega);

                //txtDataInicioCompra.Text = DateTime.Today.ToString("yyyy-MM-dd"); 

                //txtDataFimCompra.Text = DateTime.Today.ToString("yyyy-MM-dd");

                panelFiltro.Visible = true;
                panelGrid.Visible = true;
                panelCadastro.Visible = false;
                atualizaGrid("");


            }

          
        }

        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();

            }

            return ds;


        }

        public string montaSql(String sqlPedidoCompra)
        {
             sqlPedidoCompra = " select c.IDPedido as ID,  " +
                " (select NM_nomeCompleto from TB_Funcionario f where f.IDfuncionario = c.ID_funcionario ) as Funcionario, " +
             " (select DE_nomeFantasia from TB_Fornecedor f where f.IDfornecedor = c.ID_fornecedor ) as NomeFantasia, " +
             " c.ID_fornecedor as Id_fornecedor, " +
             " c.DE_pedidoFornecedor as NumeroPedido, " +
             " c.VL_valorPedido as ValorPedido, " +
             " c.VL_valorDesconto as ValorDesconto, " +
             " c.VL_valorImposto as ValorImposto, " +
             " c.VL_valorFrete as ValorFrete, " +
             " c.VL_valorTotal as ValorTotal, " +
             " c.QT_itens as Quantidade, " +
             " Convert(varchar(10), CONVERT(date, convert(varchar, c.DT_dataPedido), 106), 103) as DataPedido, " +
             " case DT_dataPrevisaoEntrega when 0 then '01/01/9999' " +
             " else Convert(varchar(10), CONVERT(date, convert(varchar, c.DT_dataPrevisaoEntrega), 106), 103) end as DataPrevisaoEntrega, " +
             " case DT_dataEntrega when 0 then '01/01/9999' "+
             " else Convert(varchar(10), CONVERT(date, convert(varchar, c.DT_dataEntrega), 106), 103) end as DataEntrega, " +
             " c.tp_formaPagamento as FormaPagamento, " +
             " c.ST_situacaoPedido as StatusPedido, " +
             " case c.ST_situacaoEntrega" +
             " when 'A' then 'Aguardando'" +
             " when 'E' then 'Entregue'" +
             " when 'C' then 'Cancelado'" +
             " else 'Verificar'" +
             " end as StatusEntrega, " +
             " c.DE_observacao as Observacoes " +
             " from TB_pedidoCompra c, tb_funcionario f where 1 = 1  AND C.ID_funcionario = F.IDFuncionario  ";

            return sqlPedidoCompra;
        }

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        public void openModalFillData(int idSelecionado,string modo, string salvar)
        {

            if(modo.Equals("E"))
            {
                lblModo.Text = "Modo de edição";
            }
            else
            {
                lblModo.Text = "Modo de visualização";
            }

            if (salvar.Equals("S"))
            {
                btnSalvarModal.Visible = true;
                camposModalSomenteLeitura("N");
            }
            else
            {
                btnSalvarModal.Visible = false;
                camposModalSomenteLeitura("S");
            }

            this.recuperaDadosModal(idSelecionado);
            //util.recuperaDadosModalPedidoCompra(idSelecionado);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }

        public void recuperaDadosModal(int id)
        {

            string sqlUnico = montaSql("");
            sqlUnico = sqlUnico + " AND idPedido = " + id.ToString();

            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sqlUnico, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_pedidoCompra");

            foreach (DataRow pRow in customerOrders.Tables["tb_pedidoCompra"].Rows)
            { 
//                Console.WriteLine(pRow["ID"]);
                txtModalId.Text = pRow["ID"].ToString();
                txtModalNumPedidoFornecedor.Text = pRow["NumeroPedido"].ToString();

                txtModalDataPedido.Text = pRow["DataPedido"].ToString();
                txtModalDataPrevisaoEntrega.Text = pRow["DataPrevisaoEntrega"].ToString();
                txtModalDataEntrega.Text = pRow["DataEntrega"].ToString();

                preencheCombo(ddlModalFornecedorInput, sqlFornecedor, "S");
                ddlModalFornecedorInput.SelectedValue = pRow["Id_Fornecedor"].ToString();
                //txtModalFornecedor.Text = pRow["NomeFantasia"].ToString();
                txtModalComprador.Text = pRow["Funcionario"].ToString();
                

                txtModalStatusPedido.Text = pRow["StatusPedido"].ToString();
                txtModalQuantidade.Text = pRow["Quantidade"].ToString();
                txtModalFormaPagamento.Text = pRow["FormaPagamento"].ToString();
                
                txtModalStatusEntrega.Text = pRow["StatusEntrega"].ToString();
                txtModalValorPedido.Text = pRow["ValorPedido"].ToString();
                txtModalValorDesconto.Text = pRow["ValorDesconto"].ToString();
                txtModalValorImposto.Text = pRow["ValorImposto"].ToString();
                txtModalValorFrete.Text = pRow["ValorFrete"].ToString();
                txtModalValorTotal.Text = pRow["ValorTotal"].ToString();
                txtModalObservacoes.Text = pRow["Funcionario"].ToString();

               

            }

            
        }

        public void camposModalSomenteLeitura(String sn)
        {
            if (sn.Equals("S"))
            {
                txtModalId.ReadOnly = true;
                txtModalNumPedidoFornecedor.ReadOnly = true;
                txtModalDataPedido.ReadOnly = true;
                //txtModalFornecedor.ReadOnly = true;
                ddlModalFornecedorInput.Enabled = false;
                txtModalComprador.ReadOnly = true;
                txtModalDataPrevisaoEntrega.ReadOnly = true;
                txtModalStatusPedido.ReadOnly = true;
                txtModalQuantidade.ReadOnly = true;
                txtModalFormaPagamento.ReadOnly = true;
                txtModalDataEntrega.ReadOnly = true;
                txtModalStatusEntrega.ReadOnly = true;
                txtModalValorPedido.ReadOnly = true;
                txtModalValorDesconto.ReadOnly = true;
                txtModalValorImposto.ReadOnly = true;
                txtModalValorFrete.ReadOnly = true;
                txtModalValorTotal.ReadOnly = true;
                txtModalObservacoes.ReadOnly = true;
            }
            else
            {
                txtModalId.ReadOnly = true;
                txtModalNumPedidoFornecedor.ReadOnly = false;
                txtModalDataPedido.ReadOnly = false;
                //txtModalFornecedor.ReadOnly = false;
                ddlModalFornecedorInput.Enabled = true;
                txtModalComprador.ReadOnly = false;
                txtModalDataPrevisaoEntrega.ReadOnly = false;
                txtModalStatusPedido.ReadOnly = false;
                txtModalQuantidade.ReadOnly = false;
                txtModalFormaPagamento.ReadOnly = false;
                txtModalDataEntrega.ReadOnly = false;
                txtModalStatusEntrega.ReadOnly = false;
                txtModalValorPedido.ReadOnly = false;
                txtModalValorDesconto.ReadOnly = false;
                txtModalValorImposto.ReadOnly = false;
                txtModalValorFrete.ReadOnly = false;
                txtModalValorTotal.ReadOnly = false;
                txtModalObservacoes.ReadOnly = false;
            }

        }



        public void atualizaGrid(string filtro)
        {

            // Declare the query string.

            String queryString = " select c.IDPedido as ID,  " + 
                " (select NM_nomeCompleto from TB_Funcionario f where f.IDfuncionario = c.ID_funcionario ) as Funcionario, " +
             "(select DE_nomeFantasia from TB_Fornecedor f where f.IDfornecedor = c.ID_fornecedor ) as NomeFantasia, " +
             "c.DE_pedidoFornecedor as NumeroPedido, "  +
             "c.VL_valorPedido as ValorPedido, " +
             "c.VL_valorDesconto as ValorDesconto, " +
             "c.VL_valorImposto as ValorImposto, " +
             "c.VL_valorFrete as ValorFrete, " +
             "c.VL_valorTotal as ValorTotal, " +
             "c.QT_itens as Quantidade, " +
             "Convert(varchar(10), CONVERT(date, convert(varchar, c.DT_dataPedido), 106), 103) as DataPedido, " +
             "c.ST_situacaoPedido as StatusPedido, " +
             "case c.ST_situacaoEntrega " +
             " when 'A' then 'Aguardando'" +
             " when 'E' then 'Entregue'" +
             " when 'C' then 'Cancelado'" +
             " else 'Verificar'" +
             " end as StatusEntrega, " +
             " c.DE_observacao as Observacoes " +
             "from TB_pedidoCompra c, tb_funcionario f where 1 = 1  AND C.ID_funcionario = F.IDFuncionario ";



            if (!filtro.Equals(""))
            {
                queryString = queryString + filtro.ToString();
            }

            queryString = queryString + " order by C.DT_dataPedido desc";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                PedidosDeCompraGridView.DataSource = ds;
                PedidosDeCompraGridView.DataBind();
                lblTotal.Text = PedidosDeCompraGridView.Rows.Count.ToString();
            }
            else
            {
                lblTotal.Text = "0";
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }


        public string recuperaDadosFiltro()
        {
            //recupera valor do Filtro
            string v_numPedido, v_nomeFornecedor, v_dataInicioCompra, v_dataFimCompra, v_FormaPagamento, v_StatusPedido, v_statusEntrega;

            v_numPedido = txtNumPedidoFornecedor.Text;
            v_nomeFornecedor = ddlFornecedor.SelectedValue.ToString();
            v_dataInicioCompra = txtDataInicioCompra.Text;

            v_dataFimCompra = txtDataFimCompra.Text;


            v_FormaPagamento = ddlFormaPagamento.SelectedValue;
            v_StatusPedido = ddlStatusPedido.SelectedValue;
            v_statusEntrega = ddlStatusEntrega.SelectedValue;

            string filtro = "";

            if (v_numPedido.Trim() != "") filtro = filtro + " and C.DE_pedidoFornecedor = " + v_numPedido + " ";
            if (v_nomeFornecedor.Trim() != "Todos") filtro = filtro + " and C.ID_fornecedor = " + v_nomeFornecedor + " ";

            if (v_statusEntrega.Trim() != "-") filtro = filtro + " and C.st_situacaoEntrega = '" + v_statusEntrega + "' ";

            if (v_dataFimCompra.Equals(""))
            {
                //v_dataInicioCompra = util.transformaDataAAAAMMDD(v_dataInicioCompra);
                v_dataInicioCompra = v_dataInicioCompra.Replace("-", "");
                //v_dataFimCompra = util.transformaDataAAAAMMDD(v_dataFimCompra);
                if (v_dataInicioCompra.Trim() != "") filtro = filtro + " and C.DT_dataPedido = '" + v_dataInicioCompra + "' ";
            }
            else
            {
                //v_dataInicioCompra = util.transformaDataAAAAMMDD(v_dataInicioCompra);
                v_dataInicioCompra = v_dataInicioCompra.Replace("-", "");
                //v_dataFimCompra = util.transformaDataAAAAMMDD(v_dataFimCompra);
                v_dataFimCompra = v_dataFimCompra.Replace("-", "");
                if (v_dataInicioCompra.Trim() != "")
                {
                    filtro = filtro + " and C.DT_dataPedido between '" + v_dataInicioCompra + "' and  '" + v_dataFimCompra + "'";
                }
            }

            return filtro;

        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {

            string filtro = "";
            filtro = recuperaDadosFiltro();

            atualizaGrid(filtro);
        }

        public void preencheCombo(DropDownList comboBox, string queryString,string todos)
        {

            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                comboBox.Items.Clear();

               

                comboBox.DataSource = ds;
                comboBox.DataTextField = "nm_nome";
                comboBox.DataValueField = "id";
                comboBox.DataBind();

                if (todos.Equals("S")) { 
                    ListItem l = new ListItem("Todos", "Todos", true);
                    l.Selected = true;
                    comboBox.Items.Add(l);
                }
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        protected void btnLimparFiltros_Click(object sender, EventArgs e)
        {
            txtNumPedidoFornecedor.Text = "";
            ddlFornecedor.SelectedValue = "Todos";
            txtDataInicioCompra.Text = "";
            txtDataFimCompra.Text = "";
            ddlFormaPagamento.SelectedIndex = 0;
            ddlStatusPedido.SelectedIndex = 0;
            ddlStatusEntrega.SelectedIndex = 0;
        }

        protected void btnListarPedidosCompra_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
        }

        protected void btnNovo_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;

            preencheCombo(ddlFornecedorInput, sqlFornecedor,"N");
            preencheCombo(ddlFuncionarioInput, sqlFuncionario,"N");

            limpaCamposTela();
        }

        public void limpaCamposTela()
        {

            txtNumeroPedidoInput.Text = "";
            txtDataPedidoInput.Text = "";
            txtValorPedidoInput.Text = "";
            txtValoImpostoInput.Text = "";
            txtValoFreteInput.Text = "";
            txtValorDescontoInput.Text = "";
            txtValorTotalInput.Text = "";
            txtQuantidadeItensInput.Text = "";
            txtObservacaoInput.Text = "";
            txtFormaPagamentoInput.Text = "";


        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
            panelBotoes.Visible = true;
        }

        public void gravarLog(String texto)
        {
            // Compose a string that consists of three lines.
            string lines = texto.ToString().Trim();

            // Write the string to a file.
            //System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\temp\\Soberana-PedidoCompra.txt");
            //file.WriteLine(lines);

            //file.Close();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            //recupera valores tela

            string iNumeroPedido, iDataPedido, iComprador, iFornecedor, iValorPedido, iValorDesconto, iValorImposto, iValorFrete, iValorTotal, iQuantidade, iFormaPagamento, iObservacao = "";

            iNumeroPedido = txtNumeroPedidoInput.Text.ToString();
            iDataPedido = txtDataPedidoInput.Text.ToString();
            iDataPedido = iDataPedido.Replace("-", "");


            iValorPedido = txtValorPedidoInput.Text.ToString().Replace(",", ".");
            iValorImposto = txtValoImpostoInput.Text.ToString().Replace(",", ".");
            iValorFrete = txtValoFreteInput.Text.ToString().Replace(",", ".");
            iValorDesconto = txtValorDescontoInput.Text.ToString().Replace(",", ".");
            iValorTotal = txtValorTotalInput.Text.ToString().Replace(",", ".");
            iQuantidade = txtQuantidadeItensInput.Text.ToString();
            iObservacao = txtObservacaoInput.Text.ToString();
            iComprador = ddlFuncionarioInput.SelectedValue;
            iFornecedor = ddlFornecedorInput.SelectedValue;
            iFormaPagamento = txtFormaPagamentoInput.Text.ToString();
            //valida dados
            string sqlInsertPedidoCompra = "";
            bool formOk = false;
            int count = 0;

            if (!iNumeroPedido.Equals("")) count++;
            if (!iDataPedido.Equals("")) count++;
            if (!iValorPedido.Equals("")) count++;
            if (!iValorImposto.Equals("")) count++;
            if (!iValorFrete.Equals("")) count++;
            if (!iValorDesconto.Equals("")) count++;
            if (!iValorTotal.Equals("")) count++;
            if (!iQuantidade.Equals("")) count++;
            if (!iFornecedor.Equals("")) count++;
            if (!iComprador.Equals("")) count++;
            if (!iFormaPagamento.Equals("")) count++;
            if (!iObservacao.Equals("")) count++;

            if (count == 12) { formOk = true; } else { formOk = false; }


            if (formOk)
            {
                sqlInsertPedidoCompra = "INSERT INTO TB_PEDIDOCOMPRA VALUES ( ";
                sqlInsertPedidoCompra += iComprador.ToString() + " , ";
                sqlInsertPedidoCompra += iFornecedor.ToString() + " , '";
                sqlInsertPedidoCompra += iNumeroPedido.ToString() + "' , ";
                sqlInsertPedidoCompra += "'" + iValorPedido.ToString() + " ' , ";
                sqlInsertPedidoCompra += "'" + iValorDesconto.ToString() + " ' , ";
                sqlInsertPedidoCompra += "'" + iValorImposto.ToString() + " ' , ";
                sqlInsertPedidoCompra += "'" + iValorFrete.ToString() + " ' , ";
                sqlInsertPedidoCompra += "'" + iValorTotal.ToString() + " ' ,";
                sqlInsertPedidoCompra += iQuantidade.ToString() + ", ";
                sqlInsertPedidoCompra += "'" + iFormaPagamento.ToString() + " ', ";
                sqlInsertPedidoCompra += iDataPedido.ToString() + " , ";

                string iDataBase, iData1Vencimento, iDataPrevisaoEntrega, iSituacaoPedido, iSituacaoEntrega, iDataEntrega;
                iDataBase = "0";
                iData1Vencimento = "0";
                iDataPrevisaoEntrega = "0";
                iSituacaoPedido = "P";
                iSituacaoEntrega = "A";
                iDataEntrega = "0";


                sqlInsertPedidoCompra += iDataBase.ToString() + " , ";
                sqlInsertPedidoCompra += iData1Vencimento.ToString() + " , ";
                sqlInsertPedidoCompra += iDataPrevisaoEntrega.ToString() + " ,'";
                sqlInsertPedidoCompra += iSituacaoPedido.ToString() + "','";
                sqlInsertPedidoCompra += iObservacao.ToString() + "' , ";

                sqlInsertPedidoCompra += " 1, "+dataHoje+", "+ horaHoje + ", "+dataHoje+", "+horaHoje+", 'A','" +iSituacaoEntrega +"',"+iDataEntrega+");";

                //lblMensagem.Text = sqlInsertPedidoCompra.ToString();

                //grava no banco


                //Inserts the FirstName variable into the db-table
                //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand();

                try
                {
                    cmd.CommandText = sqlInsertPedidoCompra;

                    cmd.Connection = conn;

                    conn.Open();

                    cmd.ExecuteNonQuery();

                    //int id = (int)cmd.ExecuteScalar();
                    int id = 0;
                    conn.Close();
                    this.gravarLog("Execução do sql: " + sqlInsertPedidoCompra.ToString());
                    limpaCamposTela();
                    lblMensagem.Text = "Pedido de Compra Inserido com sucesso ! [ ID: " + id.ToString() + " ] ";
                    openModalMensagem();
                }
                catch (Exception ex)
                {
                    //ex.ToString();
                    lblMensagem.Text = "Erro na gravação";
                    openModalMensagem();
                    this.gravarLog(ex.ToString());
                }
            }
            else
            {
                lblMensagem.Text = "Preencha todos os campos antes de salvar os dados ";
                openModalMensagem();
            }








        }

        protected void PedidosDeCompraGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblModo.Text = "Modo de visualização";
            // Get the currently selected row using the SelectedRow property.
            GridViewRow row = PedidosDeCompraGridView.SelectedRow;

            // Display the first name from the selected row.
            // In this example, the third column (index 2) contains
            // the first name.
            // lblMensagem.Text = "You selected " + row.Cells[2].Text + ".";

            openModalFillData(Convert.ToInt32(row.Cells[0].Text),"D","S");
        }

        protected void PedidosDeCompraGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            lblModo.Text = "Modo de visualização";
            // Get the currently selected row. Because the SelectedIndexChanging event
            // occurs before the select operation in the GridView control, the
            // SelectedRow property cannot be used. Instead, use the Rows collection
            // and the NewSelectedIndex property of the e argument passed to this 
            // event handler.
            GridViewRow row = PedidosDeCompraGridView.Rows[e.NewSelectedIndex];

            // You can cancel the select operation by using the Cancel
            // property. For this example, if the user selects a customer with 
            // the ID "ANATR", the select operation is canceled and an error message
            // is displayed.
            if (row.Cells[1].Text == "ANATR")
            {
                e.Cancel = true;
                lblMensagem.Text = "You cannot select " + row.Cells[2].Text + ".";
                openModalMensagem();
            }
        }

        

      
        protected void BtnDetalhar_Click(object sender, EventArgs e)
        {
            

            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            openModalFillData(Convert.ToInt32(gvr.Cells[0].Text),"D","N");
        }

        protected void BtnEditar_Click(object sender, EventArgs e)
        {
            

            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            openModalFillData(Convert.ToInt32(gvr.Cells[0].Text),"E","S");
        }

        protected void btnChegou_Click(object sender, EventArgs e)
        {

            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            string idRegistro = "";
            idRegistro = gvr.Cells[0].Text.ToString();


            string statusEntrega = "E";
            string sqlUpdatePedido = "update tb_pedidoCompra set ST_situacaoEntrega = 'E', dt_dataAtualizacao= "+dataHoje+", hr_horaAtualizacao="+horaHoje + " where IDPedido="+idRegistro;

            lblMensagem.Text = sqlUpdatePedido.ToString();
            //openModalMensagem();

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = sqlUpdatePedido;

                cmd.Connection = conn;

                conn.Open();

                cmd.ExecuteNonQuery();

                //int id = (int)cmd.ExecuteScalar();
                int id = 0;
                conn.Close();
                this.gravarLog("Execução do sql: " + sqlUpdatePedido.ToString());

                //limpaCampos();
                lblMensagem.Text = "Pedido de Compra Atualizado com sucesso ! [ ID: " + id.ToString() + " ] ";
                openModalMensagem();

                string filtro = "";
                filtro = recuperaDadosFiltro();

                atualizaGrid(filtro);

            }
            catch (Exception ex)
            {
                //ex.ToString();
                lblMensagem.Text = "Erro na gravação";
                openModalMensagem();
                //this.gravarLog(ex.ToString()+" ##### " + sqlUpdateCliente.ToString());
                this.gravarLog(Environment.NewLine + "Data/Hora:" + dataHoje.ToString() + "/" + horaHoje.ToString() + ex.ToString() + Environment.NewLine + sqlUpdatePedido.ToString());
            }


        }

        protected void PedidosDeCompraGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string StatusEntrega = e.Row.Cells[12].Text.ToString();

                foreach (TableCell cell in e.Row.Cells)
                {
                    if (StatusEntrega == "Aguardando")
                    {
                        cell.BackColor = Color.DarkBlue;
                        cell.ForeColor = Color.White;
                    }
                    if (StatusEntrega == "Verificar")
                    {
                        cell.BackColor = Color.Gray;
                        cell.ForeColor = Color.Black;
                    }
                    if (StatusEntrega == "Cancelado")
                    {
                        cell.BackColor = Color.Red;
                        cell.ForeColor = Color.White;
                    }
                    if (StatusEntrega == "Entregue")
                    {
                        cell.BackColor = Color.Green;
                        cell.ForeColor = Color.White;

                        e.Row.Cells[16].Text = "";
                        e.Row.Cells[16].Enabled = false;
                        

                        e.Row.Cells[17].Text = "";
                        e.Row.Cells[17].Enabled = false;
                        


                    }
                    if (StatusEntrega == "Cancelado")
                    {
                        cell.BackColor = Color.DarkRed;
                        cell.ForeColor = Color.White;

                        e.Row.Cells[16].Text = "";
                        e.Row.Cells[16].Enabled = false;


                        e.Row.Cells[17].Text = "";
                        e.Row.Cells[17].Enabled = false;



                    }
                    else
                    {
                        //cell.BackColor = Color.Yellow;
                    }

                }
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

            //Get the button that raised the event
            ImageButton btn = (ImageButton)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            //openModalVisualizaImagem(Convert.ToInt32(gvr.Cells[1].Text), "D", "N");

            string url = "http://joao-pc/Img/Scan/PedidosDeCompras/";
            url = url + gvr.Cells[0].Text + ".jpeg";

            /*gravarAcesso("----------------------------------------------------------");
            gravarAcesso("Inicio - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("Clique - Abrir Imagem do Pedido " + gvr.Cells[1].Text);
            gravarAcesso(url.ToString());
            gravarAcesso("Fim - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("----------------------------------------------------------");*/

            string _cId = "";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newWindow", "window.open('../../DataControlManager/Online complain/frmComplaintRevision.aspx?ID=" + url + "','_blank','status=1,toolbar=0,menubar=0,location=1,scrollbars=1,resizable=1,width=30,height=30');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open('" + url + "', '_blank', 'height=600px,width=600px,scrollbars=1'); ", true);

            
        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
           
        }

        protected void btnSalvarModal_Click(object sender, EventArgs e)
        {
            //ALTERAR PARA OBJETO aqui e no INSERT

            //recupera valores tela

            string iNumeroPedido, iDataPedido, iComprador, iFornecedor, iValorPedido, iValorDesconto, iValorImposto, iValorFrete, iValorTotal, iQuantidade, iFormaPagamento, iObservacao = "";

            iNumeroPedido = txtNumeroPedidoInput.Text.ToString();
            iDataPedido = txtDataPedidoInput.Text.ToString();
            iDataPedido = iDataPedido.Replace("-", "");


            iValorPedido = txtValorPedidoInput.Text.ToString().Replace(",", ".");
            iValorImposto = txtValoImpostoInput.Text.ToString().Replace(",", ".");
            iValorFrete = txtValoFreteInput.Text.ToString().Replace(",", ".");
            iValorDesconto = txtValorDescontoInput.Text.ToString().Replace(",", ".");
            iValorTotal = txtValorTotalInput.Text.ToString().Replace(",", ".");
            iQuantidade = txtQuantidadeItensInput.Text.ToString();
            iObservacao = txtObservacaoInput.Text.ToString();
            iComprador = ddlFuncionarioInput.SelectedValue;
            iFornecedor = ddlFornecedorInput.SelectedValue;
            iFormaPagamento = txtFormaPagamentoInput.Text.ToString();

            //valida dados
            string sqlUpdatePedidoCompra = "";
            bool formOk = false;
            int count = 0;

            if (!iNumeroPedido.Equals("")) count++;
            if (!iDataPedido.Equals("")) count++;
            if (!iValorPedido.Equals("")) count++;
            if (!iValorImposto.Equals("")) count++;
            if (!iValorFrete.Equals("")) count++;
            if (!iValorDesconto.Equals("")) count++;
            if (!iValorTotal.Equals("")) count++;
            if (!iQuantidade.Equals("")) count++;
            if (!iFornecedor.Equals("")) count++;
            if (!iComprador.Equals("")) count++;
            if (!iFormaPagamento.Equals("")) count++;
            if (!iObservacao.Equals("")) count++;

            if (count == 12) { formOk = true; } else { formOk = false; }


            if (formOk)
            {
                sqlUpdatePedidoCompra = "update TB_PEDIDOCOMPRA SET ";
                sqlUpdatePedidoCompra += "ID_FUNCIONARIO=" + iComprador.ToString() + " , ";
                sqlUpdatePedidoCompra += "ID_FORNECEDOR=" + iFornecedor.ToString() + " , '";
                sqlUpdatePedidoCompra += "DE_PEDIDOFORNECEDOR=" + "'" + iNumeroPedido.ToString() + "' , ";
                sqlUpdatePedidoCompra += "VL_VALORPedido=" + "'" + iValorPedido.ToString() + " ' , ";
                sqlUpdatePedidoCompra += "VL_VALORDesconto=" + "'" + iValorDesconto.ToString() + " ' , ";
                sqlUpdatePedidoCompra += "VL_VALORImposto=" + "'" + iValorImposto.ToString() + " ' , ";
                sqlUpdatePedidoCompra += "VL_ValorFrete=" + "'" + iValorFrete.ToString() + " ' , ";
                sqlUpdatePedidoCompra += "VL_ValorTotal=" + "'" + iValorTotal.ToString() + " ' ,";
                sqlUpdatePedidoCompra += "QT_Itens=" + iQuantidade.ToString() + ", ";
                sqlUpdatePedidoCompra += "TP_formaPagamento=" + "'" + iFormaPagamento.ToString() + " ', ";
                sqlUpdatePedidoCompra += "DT_dataPedido=" + iDataPedido.ToString() + " , ";

                string iDataBase, iData1Vencimento, iDataPrevisaoEntrega, iSituacaoPedido, iSituacaoEntrega, iDataEntrega;
                iDataBase = "0";
                iData1Vencimento = "0";
                iDataPrevisaoEntrega = "0";
                iSituacaoPedido = "P";
                iSituacaoEntrega = "A";
                iDataEntrega = "0";


                sqlUpdatePedidoCompra += "DT_dataBase=" + iDataBase.ToString() + " , ";
                sqlUpdatePedidoCompra += "DT_dataPrimeiroVencimento=" + iData1Vencimento.ToString() + " , ";
                sqlUpdatePedidoCompra += "DT_dataPrevisaoEntrega=" + iDataPrevisaoEntrega.ToString() + " ,'";
                sqlUpdatePedidoCompra += "ST_situacaoPedido=" + iSituacaoPedido.ToString() + "','";
                sqlUpdatePedidoCompra += "DE_observacao=" + iObservacao.ToString() + "' , ";

                sqlUpdatePedidoCompra += "CD_operador= 1, ";
                //sqlUpdatePedidoCompra += "dt_dataCriacao=" + dataHoje.ToString() + " , "; 
                //sqlUpdatePedidoCompra += "HR_horaCriacao=" + horaHoje.ToString() + " , ";
                sqlUpdatePedidoCompra += "DT_dataAtualizacao=" + dataHoje.ToString() + " , ";
                sqlUpdatePedidoCompra += "HR_horaAtualizacao=" + horaHoje.ToString() + " , ";
                sqlUpdatePedidoCompra += "ST_situacaoRegistro='A'";
                sqlUpdatePedidoCompra += "ST_situacaoEntrega=" + iSituacaoEntrega.ToString() + " , ";
                sqlUpdatePedidoCompra += "DT_dataEntrega=" + iDataEntrega.ToString() + " ; ";

                //lblMensagem.Text = sqlUpdatePedidoCompra.ToString();

                //grava no banco
                SqlConnection conn = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand();

                try
                {
                    cmd.CommandText = sqlUpdatePedidoCompra;

                    cmd.Connection = conn;

                    conn.Open();

                    //cmd.ExecuteNonQuery();

                    int id = 0;
                    id = (int)cmd.ExecuteNonQuery();
                    
                    conn.Close();
                    this.gravarLog("Execução do sql: " + sqlUpdatePedidoCompra.ToString());
                    limpaCamposTela();

                    if(id > 0)
                    {
                        lblMensagem.Text = "Pedido de Compra Atualizado com sucesso ! [ ID: " + id.ToString() + " ] ";
                    }
                    else
                    {
                        lblMensagem.Text = "Falha no alteração.";
                    }

                    
                    openModalMensagem();
                }
                catch (Exception ex)
                {
                    //ex.ToString();
                    lblMensagem.Text = "Erro na gravação";
                    openModalMensagem();
                    this.gravarLog(ex.ToString());
                }
            }
            else
            {
                lblMensagem.Text = "Preencha todos os campos antes de salvar os dados ";
                openModalMensagem();
            }










        }

        public bool verificaSeOPedidoChegou(int idPedido)
        {
            bool pedidoPodeSerCancelado = true;

           
            return pedidoPodeSerCancelado;
        }

        protected void btnCancelarPedidoCompra_Click(object sender, EventArgs e)
        {

            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            bool pedidoCancelavel = false;

            int idPedido = 0;

            //Obtem o ID do Pedido para realizar a verificação de eligibilidade para cancelamento
            idPedido = Convert.ToInt32(gvr.Cells[0].Text);

            if (idPedido > 0)
            {
                /*Verifica se o pedido pode ser cancelado
                 * 1.Checa a tabela de contas a receber
                 * 2.Checa a tabela de carnes
                 * 3.Checa a tabela de cupom fiscal emitido
                 * 4.Checa a movimentação de produtos no estoque
                 * 
                 * ** Registra na tabela TB_evento
                 * */

                //se chegou não pode excluir
                pedidoCancelavel = verificaSeOPedidoChegou(idPedido);


                if (pedidoCancelavel)
                {


                    txtJustificativaCancelamento.Visible = true;
                    btnConfirmarCancelamento.Visible = true;
                    btnConfirmarCancelamento.Enabled = true;
                    txtModalIDConfirmacao.Text = idPedido.ToString();
                    //Abre o Modal
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalConfirmacao();", true);
                }
                else
                {
                    txtJustificativaCancelamento.Visible = false;
                    btnConfirmarCancelamento.Visible = false;
                    btnConfirmarCancelamento.Enabled = false;
                }



            }
            else
            {
                //id pedido não pode ser 0
            }
        }

        protected void btnConfirmarCancelamento_Click(object sender, EventArgs e)
        {
            int idPedido = 0;
            idPedido = Convert.ToInt32(txtModalIDConfirmacao.Text.ToString());

            string justificativa = "";
            justificativa = txtJustificativaCancelamento.Text.ToString().Trim();

            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLPedidoCompra bllPedidoDeCompra = new BLLPedidoCompra(conexao);

            int qtdLinhasAfetadas = 0;

            ModeloLogEvento modeloLogEvento = new ModeloLogEvento();

            if (idPedido > 0)
            {
                //se afetoua linha, ou seja, atualizou, registra o evento de cancelamento
                qtdLinhasAfetadas = bllPedidoDeCompra.CancelarPedido(idPedido);

                modeloLogEvento.IdTabela = 1;
                modeloLogEvento.IdCampo = idPedido;

                if (qtdLinhasAfetadas > 0)
                {
                    modeloLogEvento.Descricao = "Cancelamento de Pedido de Pedido de Compra";
                    modeloLogEvento.Valor = "Motivo=" + justificativa;
                    modeloLogEvento.Valor2 = "Cancelado";
                    BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);
                    bllLogEvento.Incluir(modeloLogEvento);

                }
                else //senao registra o nao cancelamento
                {
                    modeloLogEvento.Descricao = "Cancelamento de Pedido de Compra";
                    modeloLogEvento.Valor = "Nenhuma linha afetada";
                    modeloLogEvento.Valor2 = "Falha";
                }



            }
            else
            {
                lblMensagem.Text = "O ID do pedido não pode ser ZERO.";
                openModalMensagem();
            }
        }
    }
}