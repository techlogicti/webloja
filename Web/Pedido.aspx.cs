﻿using AcessoBancoDados;
using Negocios;
using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Pedido : System.Web.UI.Page
    {

        private DALConexao conexao;
        BLLOperador bllOperador = null;

        string connectionString = "";
        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        string sqlVendedor = "select ID, NM_nomeCompleto as NM_nome from tb_vendedor where st_situacaoregistro='A' order by NM_nome asc";
        string sqlCliente = "select CD_codigo as ID, (NM_nomeCompleto + ' - ' + CD_codigo ) as NM_nome from tb_cliente where st_situacaoregistro='A' order by NM_nome asc";
        string sqlTipoPagamento = "select ID as ID, NM_nome as NM_nome from tb_tipoPagamento where st_situacaoregistro='A' order by NM_nome asc";
        string sqlLocalVenda = "select ID as ID, NM_nome as NM_nome from tb_localVenda where st_situacaoregistro='A' order by ID asc";

        string sqlMaquinaCartao = "select ID as ID, NM_nome as NM_nome from TB_maquinaCartao where st_situacaoregistro='A' order by ID asc";
        string sqlBandeiraCartao = "select ID as ID, NM_nome as NM_nome from TB_bandeiraCartao where st_situacaoregistro='A' order by ID asc";


        decimal valorPedido = 0;

        Util util = new Util();

        private void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
            dt.Columns.Add(new DataColumn("Column4", typeof(string)));
            dt.Columns.Add(new DataColumn("Column5", typeof(string)));
            dt.Columns.Add(new DataColumn("Column6", typeof(string)));
            dt.Columns.Add(new DataColumn("Column7", typeof(string)));
            dt.Columns.Add(new DataColumn("Column8", typeof(string)));
            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Column1"] = string.Empty;
            dr["Column2"] = string.Empty;
            dr["Column3"] = string.Empty;
            dr["Column4"] = string.Empty;
            dr["Column5"] = string.Empty;
            dr["Column6"] = string.Empty;
            dr["Column7"] = string.Empty;
            dr["Column8"] = string.Empty;
            dt.Rows.Add(dr);
            //dr = dt.NewRow();

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            Gridview1.DataSource = dt;
            Gridview1.DataBind();
        }

        private void SetValoresParcelas()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
            dt.Columns.Add(new DataColumn("Column4", typeof(string)));
            dt.Columns.Add(new DataColumn("Column5", typeof(string)));
            dt.Columns.Add(new DataColumn("Column6", typeof(string)));
            dt.Columns.Add(new DataColumn("Column7", typeof(string)));
            dt.Columns.Add(new DataColumn("Column8", typeof(string)));

            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Column1"] = string.Empty;
            dr["Column2"] = string.Empty;
            dr["Column3"] = string.Empty;
            dr["Column4"] = string.Empty;
            dr["Column5"] = string.Empty;
            dr["Column6"] = string.Empty;
            dr["Column7"] = string.Empty;
            dr["Column8"] = string.Empty;

            dt.Rows.Add(dr);
            //dr = dt.NewRow();

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            GridViewParcelas.DataSource = dt;
            GridViewParcelas.DataBind();
        }

        public void testCheckUserSoberana()
        {
            if (string.IsNullOrEmpty(Session["user"] as string))
            {
                //Server.Transfer("Logon.aspx", true);
                Response.Redirect("~/Logon.aspx");
            }
        }

        private void AddNewRow()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox TextBoxName =
                          (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtIdProduto");
                        TextBox TextBoxAge =
                          (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtNomeProduto");
                        TextBox TextBoxAddress =
                          (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtValorUnitarioProduto");
                        
                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["RowNumber"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["Column1"] = TextBoxName.Text;
                        dtCurrentTable.Rows[i - 1]["Column2"] = TextBoxAge.Text;
                        dtCurrentTable.Rows[i - 1]["Column3"] = TextBoxAddress.Text;
                        
                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }
            SetPreviousData();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            testCheckUserSoberana();

            panelBotoes.Visible = true;

            Util u = new Util();
            connectionString = u.conexao();
            //chama apenas na primeira carga da pagina
            if (!Page.IsPostBack)
            {

                SetInitialRow();
                //IniciaGridProdutosPedido();
                SetValoresParcelas();

                preencheCombo(ddlVendedorInput, sqlVendedor, "N");
                //preencheCombo(ddlClienteInput, sqlCliente, "S");
                preencheCombo(ddlTipoPagamento, sqlTipoPagamento,"S");
                preencheCombo(ddlFormaPagamento, sqlTipoPagamento, "S");

                /*
                 * Máquina Cartão
                 * Bandeira Cartão
                 * 28/02/2018
                 * */
                preencheCombo(ddlMaquinaCartao, sqlMaquinaCartao, "S");
                preencheCombo(ddlBandeiraCartao, sqlBandeiraCartao, "S");
                preencheCombo(ddlMaquinaCartaoCadastro, sqlMaquinaCartao, "S");
                preencheCombo(ddlBandeiraCartaoCadastro, sqlBandeiraCartao, "S");
                /*Fim*/

                preencheCombo(ddlLocalVenda, sqlLocalVenda, "S");
                preencheCombo(ddlLocalVendaInput, sqlLocalVenda, "N");
                preencheComboEntrega(ddlStatusEntrega);
                preencheComboEntrega(ddlSituacaoEntregaInput);
                
                

                //txtDataInicioCompra.Text = DateTime.Today.ToString("dd/MM/yyyy");
                //txtDataFimCompra.Text = DateTime.Today.ToString("dd/MM/yyyy");

                //txtDataInicioCompra.Text = DateTime.Today.ToString("yyyy-MM-dd");
                //txtDataFimCompra.Text = DateTime.Today.ToString("yyyy-MM-dd");


                // Declare the query string.
                //String queryString = "select [IDProduto] as 'Id', [CODIGO] AS 'Código', [NM_nome] AS 'Produto', [DE_descricao] as 'Descrição', [precoVenda] as 'Etiqueta' , [precoCompra] as 'PC', [precoCompraImposto] as 'PCIF', [precoMinimo] as 'P.Mín', [precoAtacado] as 'P.Atacado', [estoqueFinal] as 'Qtde'  from [SoberanaLocal].[dbo].[TB_produto]";

                String queryString = " select IDPedido as ID, NU_numeroPedido as Numero, Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataPedido),106),103) as DataPedido,";
                queryString = queryString + " VL_valorPedido as ValorPedido, VL_valorDesconto as ValorDesconto, ";
                queryString = queryString + " VL_valorTotal as ValorTotal, VL_valorTotal as VL_valorTotal, QT_itens as QtdItens,";
                queryString = queryString + " DE_observacao as Observacao, ";
                queryString = queryString + " (select t.nm_nome from tb_tipoPagamento t where t.id = id_tipopagamento) as TB_tipoPagamento, ";
                queryString = queryString + " (select NM_nomeCompleto from TB_cliente c where c.cd_codigo = ID_Cliente )as Cliente, ";                
                queryString = queryString + " (select NM_nomeCompleto from TB_vendedor v where v.ID = ID_vendedor ) as Vendedor, ";
                queryString = queryString + " ST_situacaoPedido as StatusPedido  from [dbo].[TB_pedido] order by NU_numeroPedido desc";

                // Run the query and bind the resulting DataSet
                // to the GridView control.

                /*DataSet ds = GetData(queryString);

                lblMensagem.Text = "";

                if (ds.Tables.Count > 0)
                {
                    PedidosGridView.DataSource = ds;

                    PedidosGridView.DataBind();

                    lblTotal.Text = PedidosGridView.Rows.Count.ToString();
                }
                else
                {
                    lblMensagem.Text = "Não foi possível conectar a base de dados.";
                }*/
            }
        }

        DataSet GetData(String queryString)
        {

            // Retrieve the connection string stored in the Web.config file.
            //String connectionString = ConfigurationManager.ConnectionStrings["NorthWindConnectionString"].ConnectionString;

            DataSet ds = new DataSet();

            try
            {
                // Connect to the database and run the query.
                SqlConnection connection = new SqlConnection(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                SqlDataAdapter adapter = new SqlDataAdapter(queryString, connection);

                // Fill the DataSet.
                adapter.Fill(ds);

            }
            catch (Exception ex)
            {

                // The connection failed. Display an error message.
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();

            }

            return ds;

        
    }

        public void atualizaGrid(string filtro)
        {
            // Declare the query string.

            String queryString = " select IDPedido as ID, NU_numeroPedido as Numero, Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataPedido),106),103) as DataPedido,";
            queryString = queryString + " VL_valorPedido as ValorPedido, VL_valorDesconto as ValorDesconto, ";
            queryString = queryString + " VL_valorTotal as ValorTotal, VL_valorTotal as VL_valorTotal, QT_itens as QtdItens, ";
            queryString = queryString + " DE_observacao as Observacao, ";
            queryString = queryString + " (select t.nm_nome from tb_tipoPagamento t where t.id = id_tipopagamento) as TB_tipoPagamento, ";
            queryString = queryString + " (select NM_nomeCompleto from TB_cliente c where c.id = ID_Cliente )as Cliente, ";
            queryString = queryString + " (select NM_nomeCompleto from TB_vendedor v where v.ID = ID_vendedor )as Vendedor, ";
            queryString = queryString + " ST_situacaoPedido as StatusPedido  from [dbo].[TB_pedido] where 1=1  ";



            if (!filtro.Equals(""))
            {
                queryString = queryString + filtro.ToString();
            }

            queryString = queryString + " order by [NU_numeroPedido] desc";



            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                PedidosGridView.DataSource = ds;
                PedidosGridView.DataBind();
                lblTotal.Text = PedidosGridView.Rows.Count.ToString();
            }
            else
            {
                lblTotal.Text = "0";
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }


           /* string strcon = DadosDaConexao.StringDeConexao(util.obtemAmbiente());
            using (SqlConnection con = new SqlConnection(strcon.ToString()))
            {
                using (SqlCommand cmd = new SqlCommand(queryString))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            PedidosGridView.DataSource = dt;
                            PedidosGridView.DataBind();

                            PedidosGridView.Visible = true;

                            //Calculate Sum and display in Footer Row

                           // PedidosGridView.FooterRow.Cells[4].Text = "Total";
                           // PedidosGridView.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;

                            
                           // decimal total = dt.AsEnumerable().Sum(row => row.Field<decimal>("VL_valorTotal"));
                            //PedidosGridView.FooterRow.Cells[5].Text = total.ToString("N2");
                        }
                    }
                }
            }*/
        }

    
        
        public void atualizaGrid2(string filtro)
        {

            // Declare the query string.

            String queryString = " select IDPedido as ID, NU_numeroPedido as Numero, Convert(varchar(10),CONVERT(date,convert(varchar,DT_dataPedido),106),103) as DataPedido,";
            queryString = queryString + " VL_valorPedido as ValorPedido, VL_valorDesconto as ValorDesconto, ";
            queryString = queryString + " VL_valorTotal as ValorTotal, QT_itens as QtdItens, ";
            queryString = queryString + " DE_observacao as Observacao, ";
            queryString = queryString + " (select t.nm_nome from tb_tipoPagamento t where t.id = id_tipopagamento) as TB_tipoPagamento, ";
            queryString = queryString + " (select NM_nomeCompleto from TB_cliente c where c.id = ID_Cliente )as Cliente, ";
            queryString = queryString + " (select NM_nomeCompleto from TB_vendedor v where v.ID = ID_vendedor )as Vendedor, ";
            queryString = queryString + " ST_situacaoPedido as StatusPedido  from [dbo].[TB_pedido] where 1=1  ";



            if (!filtro.Equals(""))
            {
                queryString = queryString + filtro.ToString();
            }

            queryString = queryString + " order by [NU_numeroPedido] desc";

            //this.gravarAcesso(queryString);
            
            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                PedidosGridView.DataSource = ds;
                PedidosGridView.DataBind();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        protected void btnListarPedidos_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
            panelBotoes.Visible = true;
        }

        protected void btnConfirmarCancelamento_Click(object sender, EventArgs e)
        {
            int idPedido = 0;
            idPedido = Convert.ToInt32(txtModalIDConfirmacao.Text.ToString());

            string justificativa = "";
            justificativa = txtJustificativaCancelamento.Text.ToString().Trim();

            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLPedidoVenda bllPedidoVenda = new BLLPedidoVenda(conexao);

            int qtdLinhasAfetadas = 0;

            ModeloLogEvento modeloLogEvento = new ModeloLogEvento();

            if(idPedido > 0)
            {
                //se afetoua linha, ou seja, atualizou, registra o evento de cancelamento
                qtdLinhasAfetadas = bllPedidoVenda.CancelarPedido(idPedido);

                modeloLogEvento.IdTabela = 1;
                modeloLogEvento.IdCampo = idPedido;

                if (qtdLinhasAfetadas > 0)
                {
                    modeloLogEvento.Descricao = "Cancelamento de Pedido";
                    modeloLogEvento.Valor = "Motivo="+ justificativa;
                    modeloLogEvento.Valor2 = "Cancelado";
                    BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);
                    bllLogEvento.Incluir(modeloLogEvento);

                    //Cancela Contas a Receber do Pedido em questão
                    BLLContaReceber bllContaReceber = new BLLContaReceber(conexao);
                    if(bllContaReceber.CancelarContasAReceber(idPedido) > 0)
                    {
                        ModeloLogEvento modeloLogEventoCR = new ModeloLogEvento();

                        modeloLogEvento.IdTabela = 4;
                        modeloLogEvento.IdCampo = idPedido;
                        modeloLogEventoCR.Descricao = "Cancelamento de Pedido";
                        modeloLogEventoCR.Valor = "Motivo=" + justificativa;
                        modeloLogEventoCR.Valor2 = "Cancelado";

                        BLLLogEvento bllLogEventoCR = new BLLLogEvento(conexao);
                        bllLogEventoCR.Incluir(modeloLogEvento);
                    }

                    //Cancela Movimentação Estoque
                    ModeloMovimentoEstoque modeloMovimentoEstoque = new ModeloMovimentoEstoque();
                    modeloMovimentoEstoque.IdDocumento = idPedido;
                    BLLMovimentoEstoque bllMovimentoEstoque = new BLLMovimentoEstoque(conexao);
                   /* if (bllMovimentoEstoque.RetornaItensPedidoCancelado(modeloMovimentoEstoque) > 0)
                    {
                        ModeloLogEvento modeloLogEventoCR = new ModeloLogEvento();

                        modeloLogEvento.IdTabela = 4;
                        modeloLogEvento.IdCampo = idPedido;
                        modeloLogEventoCR.Descricao = "Retorno de Produto Devido a Cancelamento de Pedido";
                        modeloLogEventoCR.Valor = "Motivo=" + justificativa;
                        modeloLogEventoCR.Valor2 = "Cancelado";

                        BLLLogEvento bllLogEventoCR = new BLLLogEvento(conexao);
                        bllLogEventoCR.Incluir(modeloLogEvento);
                    }*/


                }
                else //senao registra o nao cancelamento
                {
                    modeloLogEvento.Descricao = "Cancelamento de Pedido";
                    modeloLogEvento.Valor = "Nenhuma linha afetada";
                    modeloLogEvento.Valor2 = "Falha";
                }

                

            }
            else
            {
                lblMensagem.Text = "O ID do pedido não pode ser ZERO.";
                openModalMensagem();
            }
            
        }

        protected void btnLimparFiltros_Click(object sender, EventArgs e)
        {
            txtNumPedido.Text = "";
            txtNomeCliente.Text = "";
            txtDataInicioCompra.Text = "";
            txtDataFimCompra.Text = "";
            ddlFormaPagamento.SelectedIndex = 0;
            ddlStatusPedido.SelectedIndex = 0;
            ddlStatusEntrega.SelectedIndex = 0;

            string ultimoPedido = "";

            ultimoPedido = recuperaUltimoPedido();

            txtNumeroPedidoInput.Text = ultimoPedido.ToString();

        }

        public string recuperaFiltro()
        {
            string v_localVenda, v_numPedido, v_nomeCliente, v_dataInicioCompra, v_dataFimCompra, v_FormaPagamento, v_StatusPedido, v_statusEntrega, v_idMaquinaCartao, v_idBandeiraCartao;

            v_localVenda = ddlLocalVenda.SelectedValue;

            v_numPedido = txtNumPedido.Text;
            v_nomeCliente = txtNomeCliente.Text;
            v_dataInicioCompra = txtDataInicioCompra.Text;
            v_dataFimCompra = txtDataFimCompra.Text;

            v_dataInicioCompra = v_dataInicioCompra.Replace("-", "");
            v_dataFimCompra = v_dataFimCompra.Replace("-", "");

            v_FormaPagamento = ddlFormaPagamento.SelectedValue;
            v_StatusPedido = ddlStatusPedido.SelectedValue;
            v_statusEntrega = ddlStatusEntrega.SelectedValue;

            string filtro = "";

            //
            v_idMaquinaCartao = ddlMaquinaCartao.SelectedValue;
            v_idBandeiraCartao = ddlBandeiraCartao.SelectedValue;

            if (v_idMaquinaCartao.Trim() != "-") filtro = filtro + " and id_maquinaCartao = " + v_idMaquinaCartao + " ";
            if (v_idBandeiraCartao.Trim() != "-") filtro = filtro + " and id_bandeiraCartao = " + v_idBandeiraCartao + " ";

            //

            if (v_localVenda.Trim() != "-") filtro = filtro + " and id_LocalVenda = " + v_localVenda + " ";

            if (v_numPedido.Trim() != "") filtro = filtro + " and NU_numeroPedido = " + v_numPedido + " ";

            if (v_FormaPagamento.Trim() != "-") filtro = filtro + " and id_tipopagamento = " + v_FormaPagamento + " ";

            if (v_statusEntrega.Trim() != "-") filtro = filtro + " and st_situacaoEntrega = '" + v_statusEntrega + "' ";

            if (v_dataFimCompra.Equals(""))
            {

                if (v_dataInicioCompra.Trim() != "") filtro = filtro + " and DT_dataPedido = '" + v_dataInicioCompra + "' ";
            }
            else
            {

                if (v_dataInicioCompra.Trim() != "")
                {
                    //filtro = filtro + " and DT_dataPedido between '" + v_dataInicioCompra + "' and  '" + v_dataFimCompra + "'"+ " and st_situacaoEntrega='"+ v_statusEntrega+"' ";
                    filtro = filtro + " and DT_dataPedido between '" + v_dataInicioCompra + "' and  '" + v_dataFimCompra + "'";
                }
            }

            return filtro;
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            //recupera valor do Filtro
            atualizaGrid(recuperaFiltro());
        }

        public string recuperaUltimoPedido() {

            string sql = "select (max(NU_numeroPedido) + 1) as ID from tb_pedido";
            string ultimoPedido = "";
            

            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_pedido");

            foreach (DataRow pRow in customerOrders.Tables["tb_pedido"].Rows)
            {

                ultimoPedido = pRow["ID"].ToString();
             

            }


            return ultimoPedido;

        }


        public bool verificaSeOPedidoTemContasAReceber(int idPedido) {

            bool existeContaARecebe = false;

            string sql = "select 1 from tb_contasReceber where id_pedido="+ idPedido + " and st_situacaoPagamento='Quitado'";
            


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_contasReceber");

            int i = 0;

            foreach (DataRow pRow in customerOrders.Tables["tb_contasReceber"].Rows)
            {

                i++;
                //ultimoPedido = pRow["ID"].ToString();


            }

            if (i > 0) existeContaARecebe = true;

            
            return existeContaARecebe;
        }

        public bool verificaSeOPedidoPodeSerCancelado(int idPedido)
        {
            /*Verifica se o pedido pode ser cancelado
                * 1.Checa a tabela de contas a receber
                * 2.Checa a tabela de carnes
                * 3.Checa a tabela de cupom fiscal emitido
                * 4.Checa a movimentação de produtos no estoque
                * 
                * ** Registra na tabela TB_evento
                * */

            bool pedidoPodeSerCancelado = true;

            //ao implementar o Evento que vai gerar tb o CAIXA, lembrar de estornar  o valor recebido em caso de pedidos com dinheiro

            //1.Checa a tabela de contas a receber com 1 ou mais prestações QUITADAS
            if (verificaSeOPedidoTemContasAReceber(idPedido))
            {
                pedidoPodeSerCancelado = false;
                //lblMensagemModalConfirmacao.Text = "Atenção. Pedido 1 ou mais lançamento(s) quitados no contas a receber. Faça o estorno antes do cancelamento.";
                lblMensagem.Text = "Atenção. Pedido 1 ou mais lançamento(s) quitados no contas a receber. Faça o estorno antes do cancelamento.";
                openModalMensagem();




                //this.gravarLog(ex.ToString());
            }
            else
            {

            }
            //2.Checa a tabela de carnes
            //if (verificaSeOPedidoTemCarne(idPedido)) pedidoPodeSerCancelado = false;
            //3.Checa a tabela de cupom fiscal emitido
            //if (verificaSeOPedidoTemCarne(idPedido)) pedidoPodeSerCancelado = false;
            //*4.Checa a movimentação de produtos no estoque
            //if (verificaSeOPedidoTemCarne(idPedido)) pedidoPodeSerCancelado = false;



            return pedidoPodeSerCancelado;
        }

        

        public bool verificaSeOPedidoExiste(int numeroPedido)
        {
            /*13/04/2018*/
            //string sql = "select 1 from tb_pedido where nu_numeroPedido="+ numeroPedido;
            string sql = "select 1 from tb_pedido where 1=1 and ST_situacaoPedido!='Cancelado' and nu_numeroPedido=" + numeroPedido;
            /*13/04/2018*/
            bool pedidoExiste = false;


            //https://msdn.microsoft.com/pt-br/library/bh8kx08z(v=vs.110).aspx
            SqlDataAdapter custAdapter = new SqlDataAdapter(sql, connectionString);
            DataSet customerOrders = new DataSet();
            custAdapter.Fill(customerOrders, "tb_pedido");

            int i = 0;

            foreach (DataRow pRow in customerOrders.Tables["tb_pedido"].Rows)
            {

                i++;
                //ultimoPedido = pRow["ID"].ToString();


            }

            if (i > 0) pedidoExiste = true;

            return pedidoExiste;

        }


        protected void btnNovo_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = false;
            panelGrid.Visible = false;
            panelCadastro.Visible = true;
            panelBotoes.Visible = false;

            limpaCamposTela();

            txtQuantidadeItensInput.Text = "0";

            string ultimoPedido = "";

            ultimoPedido = recuperaUltimoPedido();

            txtNumeroPedidoInput.Text = ultimoPedido.ToString();
            

            ddlTipoPagamento.Items.Clear();


            txtDataPedidoInput.Text = dataHoje.ToString();

            txtDataPedidoInput.Text = DateTime.Today.ToString("yyyy-MM-dd");
            txtData1Parcela.Text = DateTime.Today.AddDays(30).ToString("yyyy-MM-dd");

            //define campos como somente leitura
            //txtNumeroPedidoInput.ReadOnly = true;
            //txtDataPedidoInput.ReadOnly = true;
            txtValorTotalInput.ReadOnly = true;
            txtValorSaldoPagar.ReadOnly = true;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            panelFiltro.Visible = true;
            panelGrid.Visible = true;
            panelCadastro.Visible = false;
            panelBotoes.Visible = true;
        }

        

        public void preencheComboEntrega(DropDownList comboBox)
        {
               ListItem l = new ListItem("-", "-", true);
               l.Selected = true;
            comboBox.Items.Add("-");
            comboBox.Items.Add("Produto Entregue");
            comboBox.Items.Add("Entrega Agendada");
        }


        public void preencheCombo(DropDownList comboBox, string queryString, string todos)
        {

            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                comboBox.Items.Clear();
                

                comboBox.DataSource = ds;
                comboBox.DataTextField = "NM_nome";
                comboBox.DataValueField = "ID";
                comboBox.DataBind();

                if (todos.Equals("S"))
                {
                    ListItem l = new ListItem("-", "-", true);
                    l.Selected = true;
                    comboBox.Items.Add(l);
                }

            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }

        }

        public void GerarCarnePedido(int idPedido)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {



            //recupera valores tela

            string iNumeroPedido, iDataPedido, iCliente, iVendedor, iValorPedido, iValorDesconto, iValorTotal, iValorEntrada, iValorSaldoPagar, iQuantidade, iObservacao, iTipoPagamento = "";
            string iDataEntrega, iSituacaoEntrega, idata1Vencimento, iLocalVenda, iSituacaoPedido = "";

          
            
            iNumeroPedido = txtNumeroPedidoInput.Text.ToString();


            /*Verifica se o pedido já existe na tb_pedido
             *Se o mesmo existir e estiver como CANCELADO,
             *deixa passar*/
            if (verificaSeOPedidoExiste(Convert.ToInt32(iNumeroPedido)))
            {
                lblMensagem.Text = "Pedido " + iNumeroPedido.ToString() + " já cadastrado";
                openModalMensagem();
            }
            else
            {

                //25-01-2018 - Gerar Carnê - Inicio
                int iFlagGerarCarne = 0;
                if (chkGerarCarne.SelectedValue == "1")
                {
                    iFlagGerarCarne = 1;
                }
                //25-01-2018 - Gerar Carnê - Fim

                int iFlagGerarCR = 0;
                if (chkGerarCR.SelectedValue == "1")
                {
                    iFlagGerarCR = 1;
                }

                iDataPedido = txtDataPedidoInput.Text.ToString();
                iDataPedido = iDataPedido.Replace("-", "");

                idata1Vencimento = txtData1Parcela.Text.ToString();
                idata1Vencimento = idata1Vencimento.Replace("-", "");

                iDataEntrega = txtDataEntregaInput.Text.ToString();
                iDataEntrega = iDataEntrega.Replace("-", "");


                /*iDataPedido = iDataPedido.Replace("/", "");
                string ano, mes, dia = "";
                ano = iDataPedido.Substring(4, 4);
                mes = iDataPedido.Substring(2, 2);
                dia = iDataPedido.Substring(0, 2);

                iDataPedido = ano + mes + dia;*/

                iValorPedido = txtValorPedidoInput.Text.ToString().Replace(",", ".");
                iValorDesconto = txtValorDescontoInput.Text.ToString().Replace(",", ".");
                iValorTotal = txtValorTotalInput.Text.ToString().Replace(",", ".");

                iValorEntrada = txtValorEntradaCadastro.Text.ToString().Replace(",", ".");
                iValorSaldoPagar = txtValorSaldoPagar.Text.ToString().Replace(",", ".");

                iQuantidade = txtQuantidadeItensInput.Text.ToString();
                iObservacao = txtObservacaoInput.Text.ToString();
                //iCliente = ddlClienteInput.SelectedValue;
                iCliente = txtCadCodigoInternoCliente.Text;
                iVendedor = ddlVendedorInput.SelectedValue;
                iTipoPagamento = ddlTipoPagamento.SelectedValue;

                if (iTipoPagamento.Equals("1"))
                {
                    iSituacaoPedido = "Quitado";
                }
                else
                {
                    iSituacaoPedido = "Pedido";
                }

                iSituacaoEntrega = ddlSituacaoEntregaInput.SelectedValue;

                iLocalVenda = ddlLocalVendaInput.SelectedValue;

                //valida dados
                string sqlInsertPedido = "";
                bool formOk = false;
                int count = 0;

                if (ddlVendedorInput.SelectedValue.ToString().Equals("1008"))
                {
                    lblMensagemEntrega.Text = "Selecione um vendedor";
                }
                else
                {
                    count++;
                }

                //Inicio-20180726
                string numeroContratoAutorizacaoCartao = "";
                numeroContratoAutorizacaoCartao = txtNumeroContratoAutorizacao.Text.ToString();
                if (!numeroContratoAutorizacaoCartao.Equals("0")) count++;
                //Fim-20180726

                if (!iNumeroPedido.Equals("")) count++;
                if (!iDataPedido.Equals("")) count++;
                if (!idata1Vencimento.Equals("")) count++;
                if (!iValorPedido.Equals("")) count++;
                if (!iValorDesconto.Equals("")) count++;
                if (!iValorTotal.Equals("")) count++;
                if (!iValorSaldoPagar.Equals("")) count++;
                if (!iValorEntrada.Equals("")) count++;
                if (!iQuantidade.Equals("")) count++;
                if (!iCliente.Equals("")) count++;
                if (!iVendedor.Equals("")) count++;
                if (!iObservacao.Equals("")) count++;

                if (!iTipoPagamento.Equals("") && !iTipoPagamento.Equals("-")) count++;
                if (!iDataEntrega.Equals("")) count++;

                if (!iSituacaoEntrega.Equals("") && !iSituacaoEntrega.Equals("-"))
                {
                    if (iSituacaoEntrega.Equals("Entrega Agendada") &&
                        (Convert.ToInt64(iDataEntrega) < Convert.ToInt64(iDataPedido)))
                    {
                        lblMensagemEntrega.Text = "A data da entrega deve ser maior ou igual a data de hoje";

                    }
                    else if (iSituacaoEntrega.Equals("Produto Entregue") &&
                      (Convert.ToInt64(iDataEntrega) < Convert.ToInt64(iDataPedido)))
                    {
                        lblMensagemEntrega.Text = "A data da entrega deve ser iguala  data de hoje";
                    }
                    else
                    {
                        count++;
                        lblMensagemEntrega.Text = "";
                    }


                }


                //
                int idOperador = 0;
                conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                bllOperador = new BLLOperador(conexao);
                ModeloOperador modeloOperador = new ModeloOperador();
                if (!string.IsNullOrEmpty(Session["user"] as string))
                {
                    modeloOperador = bllOperador.recuperaIdLogin(Session["user"].ToString());
                    idOperador = Convert.ToInt32(modeloOperador.Id);
                }
                //

                //20180726
                if (count == 17) { formOk = true; } else { formOk = false; }
                //if (count == 16) { formOk = true; } else { formOk = false; }


                if (formOk)
                {


                    ModeloPedidoVenda pedidoVenda = new ModeloPedidoVenda();
                    //pedidoVenda.IdPedido = ;  //Chave primária auto incremental, gerada automaticamente
                    pedidoVenda.IdCliente = Convert.ToInt32(iCliente);
                    pedidoVenda.IdVendedor = Convert.ToInt32(iVendedor);
                    pedidoVenda.NumeroPedido = iNumeroPedido.ToString();
                    pedidoVenda.DataEntrega = Convert.ToInt32(iDataEntrega);
                    pedidoVenda.DataPedido = Convert.ToInt32(iDataPedido);
                    pedidoVenda.Observacoes = iObservacao.ToString();
                    pedidoVenda.QuantidadeItens = Convert.ToInt32(iQuantidade);
                    pedidoVenda.SituacaoEntrega = iSituacaoEntrega.ToString().Trim();
                    pedidoVenda.SituacaoPedido = iSituacaoPedido.ToString().Trim();
                    pedidoVenda.IdTipoPagamento = Convert.ToInt32(iTipoPagamento.ToString().Trim());

                    pedidoVenda.ValorDesconto = Convert.ToDecimal(txtValorDescontoInput.Text.ToString());
                    //pedidoVenda.ValorDesconto = Convert.ToDecimal(iValorDesconto);
                    //pedidoVenda.ValorDesconto = pedidoVenda.ValorDesconto / 100;

                    pedidoVenda.ValorEntrada = Convert.ToDecimal(txtValorEntradaCadastro.Text.ToString());
                    //pedidoVenda.ValorEntrada = Convert.ToDecimal(iValorEntrada);
                    //pedidoVenda.ValorEntrada = pedidoVenda.ValorEntrada / 100;

                    pedidoVenda.ValorPedido = Convert.ToDecimal(txtValorPedidoInput.Text.ToString());
                    //pedidoVenda.ValorPedido = Convert.ToDecimal(iValorPedido);
                    //pedidoVenda.ValorPedido = pedidoVenda.ValorPedido / 100;

                    pedidoVenda.ValorSaldo = Convert.ToDecimal(txtValorSaldoPagar.Text.ToString());
                    //pedidoVenda.ValorSaldo = Convert.ToDecimal(iValorSaldoPagar);
                    //pedidoVenda.ValorSaldo = pedidoVenda.ValorSaldo / 100;

                    pedidoVenda.ValorTotal = Convert.ToDecimal(txtValorTotalInput.Text.ToString());
                    //pedidoVenda.ValorTotal = Convert.ToDecimal(iValorTotal);
                    //pedidoVenda.ValorTotal = pedidoVenda.ValorTotal / 100;

                    pedidoVenda.CD_operador = idOperador;
                    pedidoVenda.DT_dataCriacao = Convert.ToInt32(dataHoje);
                    pedidoVenda.HR_horaCriacao = Convert.ToInt32(horaHoje);

                    

                    pedidoVenda.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
                    pedidoVenda.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
                    pedidoVenda.ST_situacaoRegistro = "A";

                    pedidoVenda.flagGerarContaReceber = iFlagGerarCR;
                    pedidoVenda.flagGerarCarne = iFlagGerarCarne;
                    pedidoVenda.IdLocalVenda = Convert.ToInt32(iLocalVenda);

                    // 1 - Cadastro do Pedido de Venda
                    BLLPedidoVenda bllPedidoVenda = new BLLPedidoVenda(conexao);
                    int idPedidoVendaGerado = 0;
                    idPedidoVendaGerado = bllPedidoVenda.Incluir(pedidoVenda);

                    //Se incluiu o pedido, inclui os itens
                    if(idPedidoVendaGerado > 0)
                    {

                        /* Grava Evento Financeiro Relativo ao Valor da Entrada
                         * e também o valor e*/

                        //

                        // 2.1 - Cadastro do(s) Item(ns) do Pedido de Venda
                        List<ModeloItemPedidoVenda> listaItensPedidoVenda = new List<ModeloItemPedidoVenda>();
                        // 2.2 - Cadastro do(s) Item(ns) no Movimento Estoque
                        List<ModeloMovimentoEstoque> listaMovimentoEstoque = new List<ModeloMovimentoEstoque>();



                        // Ler Grid Itens do Pedido e Criar um Objeto para Cada


                        int rowscount = Gridview1.Rows.Count;
                        int columnscount = Gridview1.Columns.Count;
                        for (int i = 0; i < rowscount; i++)
                        {
                            for (int j = 1; j < columnscount; j++)
                            {
                                // I want to get data of each cell in a row
                                // I want to read the corresponding header and store
                            }
                        }

                        foreach (GridViewRow row in Gridview1.Rows)
                        {
                            ModeloItemPedidoVenda itemPedidoVenda = new ModeloItemPedidoVenda();

                            //itemPedidoVenda.IdItemPedido = 0; //gerado automaticamente via auto increment, chave primária
                            itemPedidoVenda.IdPedido = idPedidoVendaGerado; //idPedidoVendaGerado é o Número do pedido, preciso do ID da tabela

                            TextBox idProduto = row.FindControl("txtIdProduto") as TextBox;
                            itemPedidoVenda.IdProduto = Convert.ToInt32(idProduto.Text);

                            TextBox valorUnitario = row.FindControl("txtValorUnitarioProduto") as TextBox;
                            itemPedidoVenda.ValorUnitario = Convert.ToDecimal(valorUnitario.Text);

                            //row.Cells[3].Text - Porcentagem Desconto, apenas na tela
                            TextBox valorDescontoUnitario = row.FindControl("txtValorDescontoProduto") as TextBox;
                            itemPedidoVenda.ValorDescontoUnitario = Convert.ToDecimal(valorDescontoUnitario.Text);

                            TextBox quantidadeProduto = row.FindControl("txtQuantidadeProduto") as TextBox;
                            itemPedidoVenda.QuantidadeItens = Convert.ToInt32(quantidadeProduto.Text);

                            TextBox valorDescontoTotal = row.FindControl("txtValorDescontoTotalProduto") as TextBox;
                            itemPedidoVenda.ValorDescontoTotal = Convert.ToDecimal(valorDescontoTotal.Text);

                            TextBox valorTotal = row.FindControl("txtValorTotalProduto") as TextBox;
                            itemPedidoVenda.ValorTotal = Convert.ToDecimal(valorTotal.Text);
                            

                            itemPedidoVenda.DE_observacoes = "";
                            itemPedidoVenda.CD_operador = idOperador;
                            itemPedidoVenda.DT_dataCriacao = Convert.ToInt32(dataHoje);
                            itemPedidoVenda.HR_horaCriacao = Convert.ToInt32(horaHoje);

                            itemPedidoVenda.DT_dataEntrega = Convert.ToInt32(iDataEntrega);

                            itemPedidoVenda.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
                            itemPedidoVenda.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
                            itemPedidoVenda.ST_situacaoRegistro = "A";

                            listaItensPedidoVenda.Add(itemPedidoVenda);

                            /*cria e popula objeto movimento estoque condicionado ao itemPedido
                             * por isso no mesmo LOOP*/
                            ModeloMovimentoEstoque modeloMovimentoEstoque = new ModeloMovimentoEstoque();
                            modeloMovimentoEstoque.IdDocumento = itemPedidoVenda.IdPedido;
                            modeloMovimentoEstoque.IdProduto = itemPedidoVenda.IdProduto;
                            modeloMovimentoEstoque.IdTabela = 8; //tb_movimentoEstoque
                            modeloMovimentoEstoque.IdLocalOrigem = 0;
                            modeloMovimentoEstoque.IdLocalDestino = 0;
                            modeloMovimentoEstoque.Quantidade = itemPedidoVenda.QuantidadeItens;
                            modeloMovimentoEstoque.TipoMovimento = "S";

                            /*Data do Movimento será a Data de Saída do Produto (dt_entrega) ou a data da venda ja?
                             * Nesse momento vamos usar a data da venda como saida do estoque
                             * sendo assim, ao consultar o estoque e o produto estiver NEGATIVO, porém
                             * com unidades na loja, depósito.. é porque é o produto de alguém que ainda
                             * não foi entregue, poréme esta vendido */

                            //modeloMovimentoEstoque.DataMovimento = itemPedidoVenda.DT_dataEntrega;
                            /*força a data do movimento ser igual a do PEDIDO, para já descontar no estoque
                             * e evitar vender itens já vendidos. Se está 0 no sistema e tem ainda unidade
                             * no estoque físico, certamente é de pedido ainda não entregue*/
                            modeloMovimentoEstoque.DataMovimento = pedidoVenda.DataPedido;

                            modeloMovimentoEstoque.CD_operador = idOperador;
                            modeloMovimentoEstoque.DT_dataCriacao = Convert.ToInt32(dataHoje);
                            modeloMovimentoEstoque.HR_horaCriacao = Convert.ToInt32(horaHoje);
                            modeloMovimentoEstoque.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
                            modeloMovimentoEstoque.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
                            modeloMovimentoEstoque.ST_situacaoRegistro = "A";

                            listaMovimentoEstoque.Add(modeloMovimentoEstoque);

                        }

                        /*Ler Lista de Itens Objeto e realizar a inclusão na tabela 
                         * TB_ITEMPEDIDO */
                        //Este metodo pode estar dentro do anterior sem nescessidade de mais um loop
                        for (int i = 0; i < listaItensPedidoVenda.Count; i++)
                        {
                            BLLItemPedidoVenda bllItemPedidoVenda = new BLLItemPedidoVenda(conexao);
                            bllItemPedidoVenda.Incluir(listaItensPedidoVenda[i]);
                        }

                        /*Ler Lista de Itens Objeto e realizar a inclusão na tabela 
                         * TB_MOVIMENTOESTOQUE */
                        //Este metodo pode estar dentro do anterior sem nescessidade de mais um loop
                        for (int i = 0; i < listaMovimentoEstoque.Count; i++)
                        {
                            BLLMovimentoEstoque bllMovimentoEstoque = new BLLMovimentoEstoque(conexao);
                            bllMovimentoEstoque.Incluir(listaMovimentoEstoque[i]);
                        }



                        // 3 - Cadastro das Parcelas de Pagamento quando assim existirem

                        try
                        {

                            /*Gera parcelas no conta a receber somente se o CHECKBOX 'gerar parcelas no CR'
                                 * estiver marcado, caso contrário apenas grava na tb_pedidos, devendo o usuário
                                 * lançar a conta a receber manualmente 'utilzado quanto a forma de pagamento é complexa'
                                 * , também grava no campo FL_flagGerarCR na tb_pedido
                                 * sendo 1 quando gerou pelo pedido, e 0 ficando a critério manual*/

                            if (chkGerarCR.SelectedValue == "1")
                            {
                                if (idPedidoVendaGerado > 0)
                                {




                                    int quantidadeParcela = 0;
                                    quantidadeParcela = Convert.ToInt32(txtQuantidadeParcelaCadastro.Text);

                                    double valorParcela = 0;
                                    valorParcela = Convert.ToDouble(txtValorParcelaCadastro.Text.Trim().Replace(".", ","));

                                    DateTime dataAux = DateTime.Today;

                                    

                                    int parcela = 1;
                                    for (int i = 0; i < quantidadeParcela; i++)
                                    {


                                        //realiza inserção das parcelas no Contas a Receber
                                        ModeloContaReceber conta = new ModeloContaReceber();
                                        conta.IdPedido = idPedidoVendaGerado;
                                        conta.IdCliente = Convert.ToInt32(iCliente);
                                        conta.IdTipoPagamento = Convert.ToInt32(iTipoPagamento);

                                        conta.DataEmissao = Convert.ToInt32(iDataPedido);
                                        conta.CD_codOperador = idOperador;

                                        conta.NumeroParcela = parcela;
                                        conta.QuantidadeParcela = quantidadeParcela;

                                        //conta.Observacoes = "Inserido via Cadastro de Pedidos";
                                        conta.Observacoes = "";
                                        conta.ValorConta = Convert.ToDecimal(valorParcela);
                                        conta.ValorJuros = 0;
                                        conta.ValorMulta = 0;
                                        conta.ValorRecebido = 0;

                                        conta.ValorTotal = Convert.ToDecimal(valorParcela);

                                        //07032018 - Campos Novos - Maquina e Bandeira Cartão (ou financeira)
                                        conta.IdMaquinaCartao = 0;
                                        conta.IdBandeiraCartao = 0;
                                        //

                                        //20180726
                                        conta.NumeroContratoAutorizacao = numeroContratoAutorizacaoCartao;

                                        /*string dataPedido = iDataPedido.ToString();
                                        DateTime dtAtual = DateTime.ParseExact(dataPedido,
                                                                            "yyyyMMdd",
                                                                            CultureInfo.InvariantCulture,
                                                                            DateTimeStyles.None);*/

                                        string sugestao1Vencimento = idata1Vencimento.ToString();
                                        DateTime dt1Vencimento = DateTime.ParseExact(sugestao1Vencimento,
                                                                            "yyyyMMdd",
                                                                            CultureInfo.InvariantCulture,
                                                                            DateTimeStyles.None);


                                        /*na primeira ITERAÇÃO us a data 1venc sugerida
                                         * na segunda em dia fica sendo a data +30 */
                                        if (quantidadeParcela == 1)
                                        {
                                            dataAux = dt1Vencimento;

                                        }
                                        else
                                        {
                                            dataAux = dt1Vencimento.AddDays(30 * i);


                                            //atualiza data Auxiliar em +30 dias
                                            //dataAux = dataAux.AddDays(30);
                                        }

                                        txtData1Parcela.Text = dataAux.ToString("yyyy-MM-dd");
                                        conta.DataVencimento = Convert.ToInt32(txtData1Parcela.Text.ToString().Replace("-", ""));



                                        //se for Forma de Pagamento Dinheiro, já lança quitado
                                        if (conta.IdTipoPagamento == 1)
                                        {
                                            conta.SituacaoPagamento = "Quitado";
                                            conta.DataRecebimento = conta.DataEmissao;
                                        }
                                        //Inicio-20180726
                                        else if (conta.IdTipoPagamento == 2 )
                                        {
                                            conta.SituacaoPagamento = "Conferir";
                                            conta.DataRecebimento = conta.DataVencimento;
                                            conta.ValorRecebido = conta.ValorConta;
                                        }
                                        else if (conta.IdTipoPagamento == 3)
                                        {
                                            conta.SituacaoPagamento = "Conferir";
                                            conta.DataRecebimento = conta.DataVencimento;
                                            conta.ValorRecebido = conta.ValorConta;
                                        }
                                        else if (conta.IdTipoPagamento == 4)
                                        {
                                            conta.SituacaoPagamento = "Conferir";
                                            conta.DataRecebimento = conta.DataVencimento;
                                            conta.ValorRecebido = conta.ValorConta;
                                        }
                                        else if (conta.IdTipoPagamento == 5)
                                        {
                                            conta.SituacaoPagamento = "Conferir";
                                            conta.DataRecebimento = conta.DataVencimento;
                                            conta.ValorRecebido = conta.ValorConta;
                                        }
                                        //Fim-20180726
                                        else
                                        {
                                            conta.SituacaoPagamento = "Pendente";
                                            conta.DataRecebimento = 0;
                                        }

                                        
                                        conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                                        BLLContaReceber contasBLL = new BLLContaReceber(conexao);

                                        int idConta = 0;
                                        idConta = Convert.ToInt32(contasBLL.Incluir(conta));
                                        //contasBLL.Incluir(conta);

                                        /*Se incluiu a conta, também inclui um evento para esta*/
                                        if (idConta > 0)
                                        {
                                            ModeloLogEvento modeloLogEvento = new ModeloLogEvento();
                                            modeloLogEvento.IdTabela = 4; //Tabela de Contas a Receber
                                            modeloLogEvento.IdCampo = idConta;
                                            modeloLogEvento.Descricao = "Inclusão de Conta a Receber";
                                            modeloLogEvento.Valor = "Motivo= Inclusão via Tela de Contas a Receber";
                                            modeloLogEvento.Valor2 = "Automático";

                                            BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);
                                            bllLogEvento.Incluir(modeloLogEvento);


                                            // Gera tb o Evento Financeiro
                                            ModeloEventoFinanceiro modeloEventoFinanceiro = new ModeloEventoFinanceiro();
                                            modeloEventoFinanceiro.IdTabela = 6; //Tabela de Eventos Financeiros ( TB_eventoFinanceiro )
                                            modeloEventoFinanceiro.IdConta = idConta;
                                            modeloEventoFinanceiro.IdTipoConta = 1; //1-A Receber / 2-A Pagar
                                            modeloEventoFinanceiro.IdTipoPagamento = Convert.ToInt32(conta.IdTipoPagamento);
                                            modeloEventoFinanceiro.IdHistoricoFinanceiro = 0;
                                            modeloEventoFinanceiro.ValorEvento = Convert.ToDecimal(conta.ValorRecebido);
                                            modeloEventoFinanceiro.DataEvento = Convert.ToInt32(conta.DataRecebimento);
                                            modeloEventoFinanceiro.DE_Observacoes = "";
                                            modeloEventoFinanceiro.CD_operador = 0;
                                            modeloEventoFinanceiro.DT_dataCriacao = Convert.ToInt32(dataHoje);
                                            modeloEventoFinanceiro.HR_horaCriacao = Convert.ToInt32(horaHoje);
                                            modeloEventoFinanceiro.SituacaoPagamento = "Recebido";
                                            modeloEventoFinanceiro.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
                                            modeloEventoFinanceiro.HR_horaAtualizacao = Convert.ToInt32(horaHoje);
                                            modeloEventoFinanceiro.ST_situacaoRegistro = "A";

                                            BLLEventoFinanceiro bllEventoFinanceiro = new BLLEventoFinanceiro(conexao);
                                            bllEventoFinanceiro.Incluir(modeloEventoFinanceiro);

                                        }
                                        /*  */


                                        //incluir objeto carne também
                                        /*realiza inserção das parcelas no Contas a Receber
                                         * apenas se for tipo pagamento CARNE*/

                                        if (conta.IdTipoPagamento == 6)
                                        {
                                            ModeloCarne carne = new ModeloCarne();
                                            carne.IdPedido = idPedidoVendaGerado;
                                            carne.IdCliente = Convert.ToInt32(iCliente);
                                            carne.NumeroParcela = parcela;
                                            carne.QuantidadeParcela = quantidadeParcela;
                                            carne.Observacoes = "";
                                            carne.SituacaoPagamento = "Pendente";
                                            carne.ValorCompra = Convert.ToDecimal(iValorTotal); //valor do Pedido
                                            carne.ValorCompra = carne.ValorCompra / 100;
                                            carne.ValorParcela = Convert.ToDecimal(valorParcela);
                                            carne.ValorJuros = 0;
                                            carne.ValorMulta = 0;
                                            carne.ValorRecebido = 0;
                                            carne.ValorTotal = 0;
                                            carne.DataCompra = Convert.ToInt32(iDataPedido);
                                            carne.DataVencimento = Convert.ToInt32(txtData1Parcela.Text.ToString().Replace("-", ""));



                                            BLLCarne carneBLL = new BLLCarne(conexao);
                                            carneBLL.Incluir(carne);
                                        }
                                        //fimcarne

                                        parcela++;
                                        /*realiza a geração dos Carnês quando o tipo de pagamento assim for 
                                         * (passar essa funcionalidade para o momento da gravação no 
                                         * contas a receber)*/

                                    }

                                    //grava produtos na tabela de MovimentoEstoque
                                    ///o desconto vai ter que ser dado na linha do produto..e somado par ao campo de baixo. O valor de deconto na linha de 
                                    ///cada produto séra usado na tasbela de MovimentoEstoque, com o valor do produto no pedido e o valor vendido, senao o 
                                    ///relatorio de venda nao iria bater
                                    //
                                    //
                                    //TAMBEM GRAVAR na itemPedido para ter o relacionamento e historico





                                }


                            }


                            limpaCamposTela();
                            lblMensagem.Text = "Pedido Inserido com sucesso ! [ ID: " + idPedidoVendaGerado.ToString() + " ] ";
                            openModalMensagem();




                        }
                        catch (Exception ex)
                        {
                            //ex.ToString();
                            lblMensagem.Text = "Erro na gravação. Favor entrar em contato com o suporte. = " + sqlInsertPedido.ToString();

                            openModalMensagem();
                            this.gravarLog(ex.ToString());
                        }

                    }

                    

                    //this.gravarLog("Execução do sql: " + sqlInsertPedido.ToString());

                    //lblMensagem.Text = sqlInsertPedido.ToString();

                   

                    
                }
                else
                {
                    lblMensagem.Text = "Preencha todos os campos antes de salvar os dados ";
                    openModalMensagem();
                }
            }

           

        }
        
        public void gravarLog(String texto) {
            // Compose a string that consists of three lines.
            string lines = texto.ToString().Trim();

            // Write the string to a file.
            System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\temp\\logSoberana\\"+ util.obtemAmbiente().ToString() + "\\Soberana-Pedido"+dataHoje+" - "+horaHoje+".txt");
            file.WriteLine(lines);

            file.Close();
        }

        public void gravarAcesso(String texto)
        {
            string path = @"c:\temp\logSoberana\"+ util.obtemAmbiente().ToString() + "\\Acesso-"+dataHoje+"-"+horaHoje+".txt";

            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("#############################");
                    sw.WriteLine("Arquivo criado em "+dataHoje + " as "+horaHoje);
                    sw.WriteLine("#############################");
                }
            }

            // Compose a string that consists of three lines.
            string lines = texto.ToString().Trim();

            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(lines);
            }

        }

        public void limpaCamposTela() {

            txtNumeroPedidoInput.Text = "";
            txtDataPedidoInput.Text = "";
            txtValorPedidoInput.Text = "00.00";
            //txtValorDescontoInput.Text = "00.00";
            //txtValorDescontoInput.ReadOnly = true;
            txtValorTotalInput.Text = "00.00";

            txtQuantidadeItensInput.Text = "";
            txtObservacaoInput.Text = "";
            //ddlClienteInput.SelectedValue = "2690";
            //ddlVendedorInput.SelectedIndex = -1;
            ddlVendedorInput.SelectedValue = "1008";

            //07-06-2018
            txtValorAcrescimoInput.Text = "00.00";

            string ultimoPedido = "";

            ultimoPedido = recuperaUltimoPedido();

            txtNumeroPedidoInput.Text = ultimoPedido.ToString();
        }
        
        private void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtIdProduto");
                        TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtNomeProduto");
                        TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtValorUnitarioProduto");
                        TextBox box4 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtPorcentagemDescontoProduto");
                        TextBox box5 = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("txtValorDescontoProduto");
                        TextBox box6 = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("txtQuantidadeProduto");
                        TextBox box7 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("txtValorDescontoTotalProduto");
                        TextBox box8 = (TextBox)Gridview1.Rows[rowIndex].Cells[8].FindControl("txtValorTotalProduto");



                        drCurrentRow = dtCurrentTable.NewRow();
                            drCurrentRow["RowNumber"] = i + 1;

                            dtCurrentTable.Rows[i - 1]["Column1"] = box1.Text;
                            dtCurrentTable.Rows[i - 1]["Column2"] = box2.Text;
                            dtCurrentTable.Rows[i - 1]["Column3"] = box3.Text;
                            dtCurrentTable.Rows[i - 1]["Column4"] = box4.Text;
                            dtCurrentTable.Rows[i - 1]["Column5"] = box5.Text;
                            dtCurrentTable.Rows[i - 1]["Column6"] = box6.Text;
                            dtCurrentTable.Rows[i - 1]["Column7"] = box7.Text;
                            dtCurrentTable.Rows[i - 1]["Column8"] = box8.Text;

                        rowIndex++;

                    }

                    
                    
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        protected void ButtonAdd_Click1(object sender, EventArgs e)
        {

/*            string codigoProduto = "";

            codigoProduto = txtCodigoProdutoBusca.Text.ToString().Trim();

            if (!codigoProduto.Equals(""))
            {
                txtDescricaoProdutoBusca.Text = "buscando...";
            }
            else
            {
                txtDescricaoProdutoBusca.Text = "";
            }
            */

            AddNewRowToGrid();
        }

        private void SetPreviousData()
        {

            int quantidadeTotal = 0;
            decimal valorPedido = 0;
            decimal valorItem = 0;
            decimal valorItemQuantidade = 0;

            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtIdProduto");
                        TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtNomeProduto");
                        TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtValorUnitarioProduto");
                        TextBox box4 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtPorcentagemDescontoProduto");
                        TextBox box5 = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("txtValorDescontoProduto");
                        TextBox box6 = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("txtQuantidadeProduto");
                        TextBox box7 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("txtValorDescontoTotalProduto");
                        TextBox box8 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("txtValorTotalProduto");


                        box1.Text = dt.Rows[i]["Column1"].ToString();
                        box2.Text = dt.Rows[i]["Column2"].ToString();
                        box3.Text = dt.Rows[i]["Column3"].ToString();
                        box4.Text = dt.Rows[i]["Column4"].ToString();
                        box5.Text = dt.Rows[i]["Column5"].ToString();
                        box6.Text = dt.Rows[i]["Column6"].ToString();
                        box7.Text = dt.Rows[i]["Column7"].ToString();
                        box8.Text = dt.Rows[i]["Column8"].ToString();

                        if (!box6.Text.Equals(""))
                       {
                            valorItem = Convert.ToDecimal(box3.Text);
                            quantidadeTotal += Convert.ToInt32(box6.Text);
                            valorItemQuantidade = Convert.ToDecimal(box7.Text);
                            //valorItem = Convert.ToDecimal(box3.Text);
                            //valorItemQuantidade = valorItem * quantidadeTotal;
                            if (!box3.Text.Equals("")) valorPedido += valorItemQuantidade;
                           }



                            rowIndex++;
                    }
                }
            }

            /*txtQuantidadeItensInput.Text = quantidadeTotal.ToString();
            txtValorPedidoInput.Text = valorPedido.ToString();
            decimal valorDecontoInput = 0;
            decimal valorTotalInput = 0;
            valorTotalInput = valorPedido - valorDecontoInput;
            txtValorTotalInput.Text = valorTotalInput.ToString();*/
        }

        protected void atualizarGridFieldChange(object sender, EventArgs e) {

                TextBox txt = sender as TextBox;
            GridViewRow row = txt.NamingContainer as GridViewRow;
            int rowIndex = 0;
            try
            {
                rowIndex = row.RowIndex;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            

            //txtValorDescontoTotalProduto_TextChanged

            //codigo
            TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtIdProduto");
            String id = "";
            id = box1.Text.ToString().Trim();

            // procura dados do produto
            String sqlBuscaProduto = "select id, nm_nome,precoVenda from TB_produto ";
            if (!id.Equals(""))
            {
                sqlBuscaProduto = sqlBuscaProduto + " where id = " + id.ToString();

                //checa a quantidade disponivel

                int qtdDisponivelProduto = 0;
                int quantidadeTotal = 0;
                int quantidadeProduto = 0;
                int porcentagemDescontoItem = 0;

                decimal valorItemQuantidade = 0;
                decimal valorItem = 0;
                decimal valorDescontoItem = 0;
                decimal valorDescontoTotal = 0;

                


                SqlDataAdapter custAdapter = new SqlDataAdapter(sqlBuscaProduto, connectionString);
                DataSet customerOrders = new DataSet();
                custAdapter.Fill(customerOrders, "tb_produto");

                //zera quantidade gerar para recontar
                txtQuantidadeItensInput.Text = "0";
                txtValorTotalInput.Text = "0";
                txtValorPedidoInput.Text = "0";
                //txtValorDescontoInput.Text = "0";

                

                foreach (DataRow pRow in customerOrders.Tables["tb_produto"].Rows)
                {
                    //                Console.WriteLine(pRow["ID"]);
                    txtModalId.Text = pRow["ID"].ToString(); //id 

                    //atualiza tabela dinamica com dados do produto re
                    TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtNomeProduto");
                    box2.Text = pRow["nm_nome"].ToString();

                    TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtValorUnitarioProduto");
                    box3.Text = pRow["precoVenda"].ToString();

                    TextBox box4 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtPorcentagemDescontoProduto");
                    if (box4.Text.ToString().Trim().Equals("")) box4.Text = "0";
                    

                    TextBox box5 = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("txtValorDescontoProduto");
                    if (box5.Text.ToString().Trim().Equals("")) box5.Text = "0";

                    TextBox box6 = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("txtQuantidadeProduto");

                    if (box6.Text.ToString().Trim().Equals("")) box6.Text = "1";


                    TextBox box7 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("txtValorDescontoTotalProduto");

                    TextBox box8 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("txtValorTotalProduto");

                    if (!box6.Text.Equals(""))
                    {

                        /*quantidadeTotal = Convert.ToInt32(txtQuantidadeItensInput.Text);*/


                        

                        quantidadeProduto += Convert.ToInt32(box6.Text);
                        valorItem = Convert.ToDecimal(box3.Text);
                        valorItemQuantidade = valorItem * quantidadeProduto;
                        valorDescontoItem = 0;
                        valorDescontoTotal = 0;

                        porcentagemDescontoItem = Convert.ToInt32(box4.Text);


                        if (porcentagemDescontoItem > 0)
                        {
                            valorDescontoItem = (valorItem * porcentagemDescontoItem) / 100; //Preço do Item x Porcentagem de Desconto
                            valorDescontoTotal = valorDescontoItem * quantidadeProduto; //Valor de Desconto x Quanti. de Produtos
                            valorItemQuantidade = valorItemQuantidade - valorDescontoTotal; //Valor Total do Produto - Desconto Total
                        }

                        box5.Text = valorDescontoItem.ToString();
                        box7.Text = valorDescontoTotal.ToString();

                        //box7.Text = valorItemQuantidade.ToString();

                        /*valorPedido = Convert.ToDecimal(txtValorTotalInput.Text);
                        valorPedido += valorItemQuantidade;*/

                        if (!box3.Text.Equals("")) box8.Text = valorItemQuantidade.ToString();

                    }
                    else
                    {
                        box8.Text = pRow["precoVenda"].ToString();
                    }


                    //txtQuantidadeItensInput.Text = quantidadeTotal.ToString();
                    //txtValorPedidoInput.Text = valorPedido.ToString();

                    decimal valorDecontoInput = 0;
                    decimal valorTotalInput = 0;
                    valorTotalInput = valorPedido - valorDecontoInput;

                   

                    txtValorTotalInput.Text = valorTotalInput.ToString();
                    txtValorDescontoInput.Text = valorDescontoTotal.ToString();




                }



            }
            else
            {
                //produto não encontrado
            }

            this.atualizarResumoPedido();

        }

        protected void atualizarResumoPedido()
        {

            int quantidadeProdutosPedido = 0;
            int quantidadeItem = 0;
            decimal valorTotalProdutosPedido = 0;
            decimal valorTotalItemPedido = 0;
            decimal valorItemPedido = 0;
            decimal valorTotalDescontoPedido = 0;

            

            //zera quantidade gerar para recontar
            //txtQuantidadeItensInput.Text = "0";
            //txtValorTotalInput.Text = "0";
            //txtValorPedidoInput.Text = "0";
            //txtValorDescontoInput.Text = "0";

            int rowIndex = 0;
            foreach (GridViewRow row in Gridview1.Rows)
            {
                //                Console.WriteLine(pRow["ID"]);
                //txtModalId.Text = row.FindControl("ID").ToString();

                //atualiza tabela dinamica com dados do produto re
                TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtValorUnitarioProduto");
                TextBox box6 = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("txtQuantidadeProduto");

                TextBox box7 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("txtValorDescontoTotalProduto");

                quantidadeItem = Convert.ToInt32(box6.Text);
                quantidadeProdutosPedido += quantidadeItem;
                txtQuantidadeItensInput.Text = quantidadeProdutosPedido.ToString();

                valorItemPedido = Convert.ToDecimal(box3.Text);
                valorTotalDescontoPedido = Convert.ToDecimal(box7.Text);
                valorTotalItemPedido = Convert.ToDecimal(box3.Text) * quantidadeItem;
                //valorTotalProdutosPedido += valorTotalItemPedido;
                valorTotalProdutosPedido += valorTotalItemPedido;
                valorTotalProdutosPedido = valorTotalProdutosPedido - valorTotalDescontoPedido;

                txtValorPedidoInput.Text = valorTotalProdutosPedido.ToString();
                txtValorDescontoInput.Text = valorTotalDescontoPedido.ToString(); //


               

                TextBox box8 = (TextBox)Gridview1.Rows[rowIndex].Cells[8].FindControl("txtValorTotalProduto");
                rowIndex++;
            }

          

            validaDesconto();

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            this.atualizarGridFieldChange(sender, e);
        }

        public void buscaDadosProdutoPorCodigo(String codigo) {


        }

        public void validaDesconto() {

            decimal valorPedido = 0;
            decimal valorValorDesconto = 0;
            decimal valorValorTotal = 0;

            decimal valorSaldoPagarCadastro = 0;
            decimal valorEntrada = 0;
            valorEntrada = Convert.ToDecimal(txtValorEntradaCadastro.Text.Trim().Replace(".", ","));

            valorPedido = Convert.ToDecimal(txtValorPedidoInput.Text.Trim().Replace(".", ","));
            valorValorDesconto = Convert.ToDecimal(txtValorDescontoInput.Text.Trim().Replace(".", ","));

            /*Regra : Se o desconto for de até 10% do valor do Pedido, aplica o mesmo, 
             * senão, aplica apenas 10% que é o limite */
            decimal valor10Pedido = 0;
            //int limiteDesconto = 20;

            /*Desabilitado em 06/04/2018 devido a implantação de Descontos no Item, visto que, essa linha estava aplicando o desconto já realizado.
            valor10Pedido = valorPedido / 10; // obtem o valor correspondente a 10 %

            if(valorValorDesconto <= valor10Pedido)
            {
                valorValorTotal = valorPedido - valorValorDesconto;
            }else
            {
                //valorValorTotal = valorPedido - valor10Pedido;
                valorValorTotal = valorPedido - valorValorDesconto;
            }
            */
            valorValorTotal = valorPedido;

            //07-06-2018
            decimal valorAcrescimo = 0;
            valorAcrescimo = Convert.ToDecimal(txtValorAcrescimoInput.Text.Replace(".", ","));
            valorValorTotal = valorValorTotal + valorAcrescimo;
            txtValorTotalInput.Text = valorValorTotal.ToString();
            //


            //Se o valor da entrada for menor ou igual o valor da entrada, o valor de SALDO é igual o total - entrada
            if (valorEntrada <= valorValorTotal)
            {
                valorSaldoPagarCadastro = valorValorTotal - valorEntrada;
            }
            else
            {
                valorSaldoPagarCadastro = valorValorTotal;
            }

            txtValorSaldoPagar.Text = valorSaldoPagarCadastro.ToString();

            txtValorTotalInput.Text = valorValorTotal.ToString();
            txtValorDescontoInput.Text = valorValorDesconto.ToString();



            calcularParcelas();

           

            /* string valorPedido = txtValorPedidoInput.Text.Trim();
             decimal dValorPedido;

             Decimal.TryParse(valorPedido, out dValorPedido);

             string valorTotal = txtValorTotalInput.Text.Trim();
             decimal dValorTotal;

             Decimal.TryParse(valorTotal, out dValorTotal);


             string valorDesconto = txtValorDescontoInput.Text.Trim();
             decimal dValorDesconto;
             Decimal.TryParse(valorDesconto, out dValorDesconto);

             decimal dDecimaParteValorPedido;
             dDecimaParteValorPedido = dValorPedido / 10;

             if (dValorDesconto > dDecimaParteValorPedido)
             {
                 dValorDesconto = dDecimaParteValorPedido;
             }

             dValorTotal = dValorPedido - dValorDesconto;
             txtValorTotalInput.Text = dValorTotal.ToString();
             txtValorTotalInput.Text = string.Format("{0:0.##}", txtValorTotalInput.Text);*/






        }


        public void calcularParcelas()
        {

            decimal valorSaldoPagarCadastro = 0;
            decimal valorParcelaCadastro = 0;
            int quantiadeParcelaCadastro = 0;
            

            quantiadeParcelaCadastro = Convert.ToInt32(txtQuantidadeParcelaCadastro.Text.Trim().Replace(".", ","));

            valorSaldoPagarCadastro = Convert.ToDecimal(txtValorSaldoPagar.Text.Trim().Replace(".", ","));

            

            if (quantiadeParcelaCadastro > 0)
            {
                valorParcelaCadastro = valorSaldoPagarCadastro / quantiadeParcelaCadastro;
                txtValorParcelaCadastro.Text = valorParcelaCadastro.ToString();
            }
            else
            {
                txtValorParcelaCadastro.Text = txtValorSaldoPagar.Text;
            }
        }

        

        protected void txtValorTotalInput_TextChanged(object sender, EventArgs e)
        {
            //validaDesconto();
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            validaDesconto();
        }

        [WebMethod]
        public static string[] GetProdutos(string prefixo)
        {
            List<string> produtos = new List<string>();
            using (SqlConnection conn = new SqlConnection())
            {

                
                Util u = new Util();
                conn.ConnectionString  = u.conexao();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select id, codigo, nm_nome from TB_produto where nm_nome like @Texto + '%'";
                    cmd.Parameters.AddWithValue("@Texto", prefixo);
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            produtos.Add(string.Format("{0}-{1}", sdr["{"], sdr["id"]));
                        }
                    }
                    conn.Close();
                }
            }
            return produtos.ToArray();
        }

        protected void btnDetalhar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            openModalFillData(Convert.ToInt32(gvr.Cells[0].Text), "D", "N");
        }

        

        public void openModalMensagem()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalMensagem();", true);
        }

        public void openModalVisualizaImagem(int idSelecionado, string modo, string salvar)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalImagem();", true);
        }

        public void openModalFillData(int idSelecionado, string modo, string salvar)
        {

            if (modo.Equals("E"))
            {
                lblModo.Text = "Modo de edição";
            }
            else
            {
                lblModo.Text = "Modo de visualização";
            }

            if (salvar.Equals("S"))
            {
                btnSalvarModal.Visible = true;
                camposModalSomenteLeitura("N");
            }
            else
            {
                btnSalvarModal.Visible = false;
                camposModalSomenteLeitura("S");
            }

            this.recuperaDadosModal(idSelecionado);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }


        public void openModalListarProdutos(string entidade)
        {

            String queryString = " SELECT p.id as ID, P.[codigo] as Codigo ,D.NM_nome as Departamento ,C.NM_nome as Categoria " +
                             " ,C2.NM_nome as SubCategoria ,P.[NM_nome] as Produto " +
                             " ,P.[precoVenda] as Venda ,P.[precoMinimo] Minimo, P.[precoCompra] Compra" +
                             ", 0 as Quantidade,  P.[estoqueAtacado] as Deposito FROM [dbo].[TB_produto] P, [dbo].[TB_departamento] " +
                               " D, TB_categoria c, TB_categoria c2 " +
                               " where d.ID = p.id_departamento " +
                               " and p.[id_categoria] = c.ID" +
                               " and p.id_subcategoria = c2.ID order by  p.NM_nome asc ";

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);
            if (ds.Tables.Count > 0)
            {
                ProdutosGridView.DataSource = ds;

                ProdutosGridView.DataBind();

                //lblTotal.Text = ProdutosGridView.Rows.Count.ToString();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
                openModalMensagem();
            }
        
    


        //is.recuperaDadosEntidade(idSelecionado);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalListaProdutos();", true);
        }


        public void camposModalSomenteLeitura(String sn)
        {
           

        }

        public void recuperaDadosModal(int idPedido) {
            //Preencher Grid - Itens do Pedido
            /* List<ModeloItemPedidoVenda> listaItensPedidoVenda = new List<ModeloItemPedidoVenda>();
             BLLItemPedidoVenda bllItemPedido = new BLLItemPedidoVenda(conexao);

             listaItensPedidoVenda = bllItemPedido.RecuperaItensPedido(idPedido);*/

            String queryString = "select a.id , a.id_pedido, a.id_produto, (select b.nm_nome from tb_produto b where b.id = a.id_produto) as produto, a.VL_valorUnitario as ValorUnitario, a.VL_valorDescontoUnitario as ValorDescontoUnitario, a.VL_valorDescontoTotal as ValorDescontoTotal, a.VL_valorTotal as ValorTotal, a.QT_itens as Quantidade,  Convert(varchar(10),CONVERT(date,convert(varchar,a.DT_dataEntrega),106),103) as DataEntrega from [TB_itemPedido] a where a.id_pedido =" + idPedido.ToString();

            // Run the query and bind the resulting DataSet
            // to the GridView control.
            DataSet ds = GetData(queryString);

            if (ds.Tables.Count > 0)
            {
                ItensPedidoGridView.DataSource = ds;

                ItensPedidoGridView.DataBind();

                //lblTotal.Text = ItensPedidoGridView.Rows.Count.ToString();
            }
            else
            {
                lblMensagem.Text = "Não foi possível conectar a base de dados.";
            }


            conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
            BLLPedidoVenda bllPedidoVenda = new BLLPedidoVenda(conexao);
            ModeloPedidoVenda venda = new ModeloPedidoVenda();
            venda = bllPedidoVenda.CarregarModeloPedidoVenda(idPedido);

            txtObservacoesPedido.ReadOnly = true;
            txtObservacoesPedido.Text = venda.Observacoes.ToString();

            if (venda.SituacaoPedido == "Cancelado")
            {
                
                txtDetalhesPedido.Visible = true;
                txtDetalhesPedido.ReadOnly = true;
                string motivo = "";
                

                //Recupera Eventos
                conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                BLLLogEvento bllLogEvento = new BLLLogEvento(conexao);

                try
                {
                    //motivo = bllLogEvento.CarregarModeloLogEventoPedido(idPedido).Valor2;
                }catch(Exception ex)
                {
                    //Console.WriteLine(ex.ToString());
                }

                
                txtDetalhesPedido.Text = "Pedido Cancelado = " + motivo.ToString();

            }
            else
            {
                txtDetalhesPedido.Visible = false;
            }

            

        }

        

        protected void txtCodigoProdutoBusca_TextChanged(object sender, EventArgs e)
        {
           
        }

        protected void BtnListarProdutos_Click(object sender, EventArgs e)
        {
            openModalListarProdutos("produtos");
        }

        [WebMethod(EnableSession = true)]
        protected void showListaProdutosModal(object sender, EventArgs e)
        {
            openModalListarProdutos("produtos");
        }

        
        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            AddNewRowToGrid();

            //ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "Pop", "closeModalListaProdutos();", true);
            // txtCodigoProdutoBusca.Text = gvr.Cells[0].Text.ToString();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalListaProdutos();", true);
        }

        

        protected void txtValorDescontoInput_TextChanged(object sender, EventArgs e)
        {
            //validaDesconto();
        }

        protected void ddlTipoPagamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoPagamento.SelectedValue.Equals("1"))
            {
                txtData1Parcela.Enabled = false;
            }else
            {
                txtData1Parcela.Enabled = true;
            }
        }

        protected void btnGerarParcelas_Click(object sender, EventArgs e)
        {

        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            string cpfCliente = "";

            cpfCliente = txtCadCpfCliente.Text.ToString();

            if (cpfCliente != "")
            {
                //busca o cpf na base de dados e recupear o Codigo Interno + Nome do Cliente

                cpfCliente = cpfCliente.Replace(".", "").Replace("-", "").Replace("-", "").Replace("/", "");
                while(cpfCliente.Length < 14)
                {
                    cpfCliente = "0" + cpfCliente;
                }

                // procura dados do produto

                ModeloCliente modelo = new ModeloCliente();

                DALConexao conexao = new DALConexao(DadosDaConexao.StringDeConexao(util.obtemAmbiente()));
                //SqlDataAdapter da = new SqlDataAdapter("select id from tb_cliente where nu_cpfcnpj= @cpjcnpj", conexao.StringConexao);
                
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conexao.ObjetoConexao;
                cmd.CommandText = "select id, cd_codigo, nm_nomecompleto from tb_cliente where nu_cpfcnpj = @cpjcnpj";
                cmd.Parameters.AddWithValue("@cpjcnpj", cpfCliente.ToString());
                conexao.Conectar();

                SqlDataReader registro = cmd.ExecuteReader();
                if (registro.HasRows)
                {
                    registro.Read();
                    modelo.Id = Convert.ToInt32(registro["id"].ToString());
                    modelo.CD_codigo = registro["cd_codigo"].ToString();
                    modelo.NM_nomeCompleto = registro["nm_nomecompleto"].ToString();

                    string sqlTipoPagamentoCliente = "select ID as ID, NM_nome as NM_nome from tb_tipoPagamento where 1 = 1 ";
                    sqlTipoPagamentoCliente += " and id in( select ID_tipoPagamento from TB_autorizacaoCliente where id_cliente=" + modelo.Id + " and ST_flagAutorizado='S' )";
                    sqlTipoPagamentoCliente += " and st_situacaoregistro='A' order by NM_nome asc";

                    preencheCombo(ddlTipoPagamento, sqlTipoPagamentoCliente, "N");
                }

                conexao.Desconectar();



                txtCadCodigoInternoCliente.Text = modelo.Id.ToString();
                //txtCadCodigoInternoCliente.Text = modelo.CD_codigo.ToString();
                txtCadNomeCliente.Text = modelo.CD_codigo.ToString() + " - " + modelo.NM_nomeCompleto.ToString();
                
            }

                
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            txtCadCodigoInternoCliente.Text = "";
            txtCadCpfCliente.Text = "";
            txtCadNomeCliente.Text = "";
        }

        protected void PedidosGridView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

            //Get the button that raised the event
            ImageButton btn = (ImageButton)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            //openModalVisualizaImagem(Convert.ToInt32(gvr.Cells[1].Text), "D", "N");

            string url = "http://joao-pc/Img/Scan/pedidos/";
            url = url + gvr.Cells[1].Text + ".jpeg";

            gravarAcesso("----------------------------------------------------------");
            gravarAcesso("Inicio - Data:" +dataHoje + " - Hora: "+horaHoje);
            gravarAcesso("Clique - Abrir Imagem do Pedido " + gvr.Cells[1].Text);
            gravarAcesso(url.ToString());
            gravarAcesso("Fim - Data:" + dataHoje + " - Hora: " + horaHoje);
            gravarAcesso("----------------------------------------------------------");

            string _cId = "";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newWindow", "window.open('../../DataControlManager/Online complain/frmComplaintRevision.aspx?ID=" + url + "','_blank','status=1,toolbar=0,menubar=0,location=1,scrollbars=1,resizable=1,width=30,height=30');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "onclick", "javascript:window.open('"+url+"', '_blank', 'height=600px,width=600px,scrollbars=1'); ", true);

            //http://servidor:8090/Img/Scan/pedidos/18766.jpeg
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            //Get the button that raised the event
            Button btn = (Button)sender;

            //Get the row that contains this button
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            bool pedidoCancelavel = false;

            int idPedido = 0;

            //Obtem o ID do Pedido para realizar a verificação de eligibilidade para cancelamento
            idPedido = Convert.ToInt32(gvr.Cells[0].Text);

            if(idPedido > 0) {
                /*Verifica se o pedido pode ser cancelado
                 * 1.Checa a tabela de contas a receber
                 * 2.Checa a tabela de carnes
                 * 3.Checa a tabela de cupom fiscal emitido
                 * 4.Checa a movimentação de produtos no estoque
                 * 
                 * ** Registra na tabela TB_evento
                 * */

           pedidoCancelavel = verificaSeOPedidoPodeSerCancelado(idPedido);

            
            if (pedidoCancelavel)
            {
                
             
                txtJustificativaCancelamento.Visible = true;
                btnConfirmarCancelamento.Visible = true;
                btnConfirmarCancelamento.Enabled = true;
                txtModalIDConfirmacao.Text = idPedido.ToString();
                //Abre o Modal
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalConfirmacao();", true);
                }
            else
            {
                txtJustificativaCancelamento.Visible = false;
                btnConfirmarCancelamento.Visible = false;
                btnConfirmarCancelamento.Enabled = false;
            }

            

            }else
            {
                //id pedido não pode ser 0
            }


            
        }

        protected void PedidosGridView_DataBound(object sender, EventArgs e)
        {

        }

        protected void PedidosGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string StatusPedido = e.Row.Cells[8].Text.ToString();

                foreach (TableCell cell in e.Row.Cells)
                {
                    if (StatusPedido == "Pedido")
                    {
                        cell.BackColor = Color.Blue;
                    }
                    if (StatusPedido == "Verificar")
                    {
                        cell.BackColor = Color.Gray;
                        cell.ForeColor = Color.Black;
                    }
                    if (StatusPedido == "Quitado")
                    {
                        cell.BackColor = Color.Green;
                        cell.ForeColor = Color.White;

                        e.Row.Cells[12].Enabled = false;
                        e.Row.Cells[12].Text = "";
                        e.Row.Cells[13].Enabled = false;
                        e.Row.Cells[13].Text = "";
                    }
                    if (StatusPedido == "Cancelado")
                    {
                        //cell.BackColor = Color.DarkRed;
                        //cell.ForeColor = Color.White;

                        cell.BackColor = Color.Black;
                        cell.ForeColor = Color.Red;

                        e.Row.Cells[12].Enabled = false;
                        e.Row.Cells[12].Text = "";
                        e.Row.Cells[13].Enabled = false;
                        e.Row.Cells[13].Text = "";
                    }
                    else
                    {
                        cell.BackColor = Color.DarkGray;
                        cell.ForeColor = Color.White;
                    }

                }
            }
        }

        protected void txtDataPedidoInput_TextChanged(object sender, EventArgs e)
        {
            txtDataEntregaInput.Text = txtDataPedidoInput.Text;
        }

        protected void txtValorDescontoTotalProduto_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtValorAcrescimo_TextChanged(object sender, EventArgs e)
        {
            this.atualizarGridFieldChange(sender, e);
        }
    }
}
