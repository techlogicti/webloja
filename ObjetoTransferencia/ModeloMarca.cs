﻿namespace ObjetoTransferencia
{
    public class ModeloMarca
    {
        //Variável
        public int IdMarca { get; set; }
        public string Descricao_curta { get; set; }
        public string Descricao_longa { get; set; }
        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }


    }
}
