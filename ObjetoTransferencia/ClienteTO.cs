﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ClienteTO
    {
        public int Id { get; set; }
        public string CD_codigo { get; set; }
        public string NM_nomeCompleto { get; set; }
        public string NM_apelido { get; set; }
        public int DT_dataNascimento { get; set; }
        public int CD_estadoCivil { get; set; }
        public string NM_nomeConjuge { get; set; }
        public int CD_profissaoConjuge { get; set; }
        public string DE_nacionalidade { get; set; }
        public string DE_naturalidade { get; set; }
        public string NM_nomeMae { get; set; }
        public string NM_nomePai { get; set; }
        public string NU_cpfCnpj { get; set; }
        public string NU_numeroIdentidade { get; set; }
        public int DT_emissaoIdentidade { get; set; }
        public int CD_ufEmissorIdentidade { get; set; }
        public string DE_orgaoEmissorIdentidade { get; set; }
        public string NU_numeroCnh { get; set; }
        public int DT_emissaoCnh { get; set; }
        public int CD_ufEmissorCnh { get; set; }
        public int CD_categoriaCnh { get; set; }
        public int CD_pais { get; set; }
        public int CD_uf { get; set; }
        public int CD_cidade { get; set; }
        public string DE_bairro { get; set; }
        public string DE_logradouro { get; set; }
        public string DE_numero { get; set; }
        public string DE_complemento { get; set; }
        public string DE_pontoDeReferencia { get; set; }
        public string DE_numeroCep { get; set; }

        public string DE_empresa { get; set; }
        public string NU_telefoneComercial { get; set; }
        public string NM_responsavelComercial { get; set; }
        public int CD_tipoAtividade { get; set; }
        public int CD_profissao { get; set; }
        public int DT_dataAdmissao { get; set; }

        public int VL_ordenado { get; set; }
        public int VL_ultimaCompra { get; set; }
        public int DT_ultimaCompra { get; set; }
        public int VL_creditoLoja { get; set; }

        public string NU_telefoneFixo { get; set; }
        public string NU_celular1 { get; set; }
        public string NU_celular2 { get; set; }
        public string NU_celular3 { get; set; }
        public string NU_celular4 { get; set; }
        public string DE_email { get; set; }
        public string NU_telefoneRecado { get; set; }
        public string NM_nomeRecado { get; set; }
        public string DE_parentescoRecado { get; set; }

        public string DE_observacoes { get; set; }

        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }

    }
}
