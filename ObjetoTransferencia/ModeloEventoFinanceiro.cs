﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloEventoFinanceiro
    {
        public int Id { get; set; }
        public int IdTabela { get; set; }
        public int IdConta { get; set; }
        public int IdTipoConta { get; set; }
        public int IdTipoPagamento { get; set; }
        public int IdHistoricoFinanceiro { get; set; }
        public decimal ValorEvento { get; set; }
        public int DataEvento { get; set; }
        public string DE_Observacoes { get; set; }
        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public string SituacaoPagamento { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }

    }
}
