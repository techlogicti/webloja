﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    class Class1
    {
        public void Teste()
        {
            MarcaColecao marcaColecao = new MarcaColecao();

            ModeloMarca marca= new ModeloMarca();
            marca.IdMarca = 1;
            marca.Descricao_curta = "Arno";
            marcaColecao.Add(marca);

            ModeloMarca marca2 = new ModeloMarca();
            marca2.IdMarca = 2;
            marca2.Descricao_curta = "Faet";
            marcaColecao.Add(marca2);
        }
    }
}
