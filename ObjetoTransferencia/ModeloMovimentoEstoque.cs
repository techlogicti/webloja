﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloMovimentoEstoque
    {
        public int Id { get; set; }
        public int IdTabela { get; set; }
        public int IdDocumento { get; set; }
        public int IdProduto { get; set; }
        public int IdLocalOrigem { get; set; }
        public int IdLocalDestino { get; set; }
        public int Quantidade { get; set; }
        public string TipoMovimento { get; set; }
        public int DataMovimento { get; set; }
        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }
    }
}
