﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloContaReceber
    {
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public int IdCliente { get; set; }
        public int IdTipoPagamento { get; set; }
        public decimal ValorConta { get; set; }
        public decimal ValorJuros { get; set; }
        public decimal ValorMulta { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal ValorRecebido { get; set; }
        public int NumeroParcela { get; set; }
        public int QuantidadeParcela { get; set; }
        public int DataEmissao { get; set; }
        public int DataVencimento { get; set; }
        public int DataRecebimento { get; set; }
        public string Observacoes { get; set; }
        public string SituacaoPagamento { get; set; }

        public int CD_codOperador { get; set; }

        public int IdMaquinaCartao { get; set; }
        public int IdBandeiraCartao { get; set; }

        public string NumeroContratoAutorizacao { get; set; }
    }
}
