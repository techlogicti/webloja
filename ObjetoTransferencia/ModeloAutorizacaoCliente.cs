﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloAutorizacaoCliente
    {
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public string FlagAutorizado { get; set; }
        public int IdTipoPagamento { get; set; }
        
    }
}
