﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
   
    public class ModeloLogEvento
    {
        public int Id { get; set; }
        public int IdTabela { get; set; }
        public int IdCampo { get; set; }
        public string Descricao { get; set; }
        public string Valor { get; set; }
        public string Valor2 { get; set; }
        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string SituacaoRegistro { get; set; }
    }
}
