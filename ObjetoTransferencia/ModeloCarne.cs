﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloCarne
    {
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public int IdCliente { get; set; }
        public int DataCompra { get; set; }
        public decimal ValorCompra { get; set; }
        public int QuantidadeParcela { get; set; }
        public int NumeroParcela { get; set; }
        public decimal ValorParcela { get; set; }
        public int DataVencimento { get; set; }


        public decimal ValorJuros { get; set; }
        public decimal ValorMulta { get; set; }
        public decimal ValorTotal { get; set; }

        public decimal ValorRecebido { get; set; } //incluir na tabela
         
        public int DataEmissao { get; set; }
        
        public int DataRecebimento { get; set; }
        public string Observacoes { get; set; }
        public string SituacaoPagamento { get; set; }
    }
}
