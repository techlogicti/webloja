﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloOperador
    {
        public int Id { get; set; }
        public string CD_operador { get; set; }
        public string NM_nome { get; set; }
        public string CD_login { get; set; }
        public string DE_email { get; set; }
        public string DE_senha { get; set; }
        
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }

    }
}
