﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloProduto
    {
        public int id { get; set; }
        public string codigoEAN { get; set; }
        public int idEmpresa { get; set; }
        public int idGrupo { get; set; }
        public int idCategoria { get; set; }
        public int idSubCategoria { get; set; }
        public int idDepartamento { get; set; }
        public int idMarca { get; set; }
        public int idUnidadeMedida { get; set; }
        public int idTipoEstoque { get; set; }
        public string nmNome { get; set; }
        public string deDescricao { get; set; }
        public string codigoNCM { get; set; }
        public string cest { get; set; }

        public string precoCompra { get; set; }
        public string precoCompraImposto { get; set; }
        public string precoMinimo { get; set; }
        public string precoAtacado { get; set; }
        public string precoVenda { get; set; }
        /*public decimal precoCompra { get; set; }
        public decimal precoCompraImposto { get; set; }
        public decimal precoMinimo { get; set; }
        public decimal precoAtacado { get; set; }
        public decimal precoVenda { get; set; }*/
        public int estoqueMinimo { get; set; }
        public int estoqueMaximo { get; set; }
        public int estoqueAtual { get; set; }
        public int estoqueAtacado { get; set; }
        public int pis;
        public int cofins;
        public int icms;
        public string status { get; set; }

        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }

        public string DE_observacoes { get; set; }

    }
}
