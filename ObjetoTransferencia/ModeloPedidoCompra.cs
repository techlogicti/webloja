﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloPedidoCompra
    {
        public int IdPedido { get; set; }
        public int IdFuncionario { get; set; }
        public int IdFornecedor { get; set; }
        public string DE_PedidoFornecedor { get; set; }
        public decimal ValorPedido { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorImposto { get; set; }
        public decimal ValorFrete { get; set; }
        public decimal ValorTotal { get; set; }
        public int QuantidadeItens { get; set; }
        public int IdFormaPagamento { get; set; }
        public int DataPedido { get; set; }
        public int DataBase { get; set; }
        public int DataPrimeiroVencimento { get; set; }
        public int DataPrevisaoEntrega { get; set; }
        public string SituacaoPedido { get; set; }
        public string Observacoes { get; set; }

        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }
        public int DataEntrega { get; set; }
        public string SituacaoEntrega { get; set; }
    }
}
