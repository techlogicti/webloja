﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloItemPedidoVenda
    {

        public int IdItemPedido { get; set; }
        public int IdPedido { get; set; }
        public int IdProduto { get; set; }
        public decimal ValorUnitario { get; set; }
        public int QuantidadeItens { get; set; }
        public decimal ValorDescontoUnitario { get; set; }
        public decimal ValorDescontoTotal { get; set; }
        public decimal ValorTotal { get; set; }
        public string DE_observacoes { get; set; }
        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataEntrega { get; set; } //campo é por produto porque tem caso em que o cliente levo um item menor e o maior vai no caminhão
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }

    }
}
