﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloPedidoVenda
    {
        public int IdPedido { get; set; }
        public string NumeroPedido { get; set; }
        public int DataPedido { get; set; }
        public decimal ValorPedido { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorTotal { get; set; }
        public decimal ValorEntrada { get; set; }
        public decimal ValorSaldo { get; set; }
        public int QuantidadeItens { get; set; }
        public string Observacoes { get; set; }
        public int IdVendedor { get; set; }
        public int IdCliente { get; set; }
        public string SituacaoPedido { get; set; }
        public int DataEntrega { get; set; }
        public string SituacaoEntrega { get; set; }
        public int IdTipoPagamento { get; set; }

        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }

        public int flagGerarContaReceber { get; set; }
        public int flagGerarCarne { get; set; }
        public int IdLocalVenda { get; set; }

    }
}
