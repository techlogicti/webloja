﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloNotaFiscalEntrada
    {
        public int Id { get; set; }
        public int IdFornecedor { get; set; }
        public int IdTransportadora { get; set; }
        public int IdCfop { get; set; }
        public string NumeroNotaFiscal { get; set; }
        public string SerieNotaFiscal { get; set; }
        public string ModeloNotaFiscal { get; set; }
        public string ChaveAcesso { get; set; }
        public int DataEmissao { get; set; }
        public int DataEntrada { get; set; }
        public int IdFormaPagamento { get; set; }
        public int QuantidadeItens { get; set; }
        public decimal ValorBaseCalculoICMS { get; set; }
        public decimal ValorICMS { get; set; }
        public decimal ValorBaseCalculoICMSSubST { get; set; }
        public decimal ValorICMSSubST { get; set; }
        public decimal ValorFrete { get; set; }
        public decimal ValorSeguro { get; set; }
        public decimal ValorOutrasDespesas { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorIPI { get; set; }
        public decimal ValorTotalProdutos { get; set; }
        public decimal ValorTotalNota { get; set; }
        public string Observacoes { get; set; }
        public int IdFuncionarioRecebimento { get; set; }
        public int DataEntrega { get; set; }
        public string SituacaoPedido { get; set; }
        public string SituacaoEntrega { get; set; }

        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string SituacaoRegistro { get; set; }
        
    }
}
