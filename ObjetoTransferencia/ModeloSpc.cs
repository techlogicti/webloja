﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetoTransferencia
{
    public class ModeloSpc
    {
        //Variável
        public int Id { get; set; }
        public int Id_cliente { get; set; }
        public int DT_dataRegistro { get; set; }
        public string DE_Motivo { get; set; }
        public string DE_observacao { get; set; }
        public int CD_operador { get; set; }
        public int DT_dataCriacao { get; set; }
        public int HR_horaCriacao { get; set; }
        public int DT_dataAtualizacao { get; set; }
        public int HR_horaAtualizacao { get; set; }
        public string ST_situacaoRegistro { get; set; }

    }
}
