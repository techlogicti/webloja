﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SoberanaWindows
{
    public partial class ConsultaTabela : Form
    {


        string connectionString = "server=joao-pc\\SQLEXPRESS; Database=SoberanaLocal;User ID=soberana;Password=1q2w3e4r;Integrated Security=False";
        
        private BindingSource bindingSource1 = new BindingSource();
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();
        //private string sqlEstoque = "SELECT * FROM estoque ORDER BY categoria,  descricao ";
        private string sqlEstoque = "";
        

        public string tipoAcessoSistema = "";

        public ConsultaTabela(String tipoAcesso)
        {
            InitializeComponent();
            tipoAcessoSistema = tipoAcesso.ToString();
            carregaCombos();

            

        }

        public void carregaCombos() {

            
            cmbTabela.Items.Clear();
            cmbTabela.Items.Add("TB_categoria");
            cmbTabela.Items.Add("TB_cliente");
            cmbTabela.Items.Add("TB_carne");
            cmbTabela.Items.Add("Estoque");
            cmbTabela.Items.Add("TB_fornecedor");
            cmbTabela.Items.Add("TB_marca");
            cmbTabela.Items.Add("TB_pedido");
            cmbTabela.Items.Add("TB_produto");

            cmbTabela.SelectedIndex = 1;

            
        }

        
        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Exit();
        }


        public void comunicaBanco() {

            SqlConnection sqlConn = new SqlConnection(connectionString);

            //abre conexao
            sqlConn.Open();

            //realiza operações

            SqlCommand cmd = new SqlCommand(sqlEstoque, sqlConn);
            SqlDataReader dr = cmd.ExecuteReader();

           
            //fecha conexao
            sqlConn.Open();
        }

        private void GetData(string selectCommand)
        {
            try
            {
               

                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSource1.DataSource = table;

                // Resize the DataGridView columns to fit the newly loaded content.
                dataGridView1.AutoResizeColumns(
                    DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void GetDataFiltro(String tipoAcesso, string textoFiltro)
        {

            string sqlEstoqueFiltro = "";

            

            string tabelaSelecionada = "";
            tabelaSelecionada = cmbTabela.SelectedItem.ToString();


            if (tipoAcesso.Equals("basico"))
            {
                sqlEstoqueFiltro = "SELECT codigo, marca, descricao, categoria, subcategoria, precoVenda, precoMinimo, estoqueFinal FROM estoque where 1=1 ";
            }
            if (tipoAcesso.Equals("admin"))
            {
                sqlEstoqueFiltro = "SELECT * FROM "+ tabelaSelecionada.ToString()+" where 1=1 and de_descricao like '%"+ textoFiltro + "%'";
            }
         
            try
            {


                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(sqlEstoqueFiltro, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSource1.DataSource = table;

                // Resize the DataGridView columns to fit the newly loaded content.
                dataGridView1.AutoResizeColumns(
                    DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            GetDataFiltro(tipoAcessoSistema,"");

            string tabelaSelecionada = "";
            tabelaSelecionada = cmbTabela.SelectedItem.ToString();

            if (tipoAcessoSistema.Equals("basico"))
            {
                sqlEstoque = "SELECT codigo, marca, descricao, categoria, subcategoria, precoVenda, precoMinimo, estoqueFinal from estoque ";
            }
            if (tipoAcessoSistema.Equals("admin"))
            {
                sqlEstoque = "SELECT* FROM " + tabelaSelecionada + " ORDER BY dt_datacriacao desc, hr_horacriacao desc ";
            }

            dataGridView1.DataSource = bindingSource1;
            GetData(sqlEstoque);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void txtDescricao_TextChanged(object sender, EventArgs e)
        {
            string textoFiltro = "";


            textoFiltro = txtDescricao.Text.Trim().ToString();

            if (textoFiltro != "") {
                GetDataFiltro(tipoAcessoSistema,textoFiltro);
            }
        }
    }
}
