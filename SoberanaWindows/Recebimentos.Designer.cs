﻿namespace SoberanaWindows
{
    partial class Recebimentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbImpressoras = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cmbImpressoras
            // 
            this.cmbImpressoras.FormattingEnabled = true;
            this.cmbImpressoras.Location = new System.Drawing.Point(100, 31);
            this.cmbImpressoras.Name = "cmbImpressoras";
            this.cmbImpressoras.Size = new System.Drawing.Size(358, 21);
            this.cmbImpressoras.TabIndex = 0;
            // 
            // Recebimentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cmbImpressoras);
            this.Name = "Recebimentos";
            this.Text = "Recebimentos";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbImpressoras;
    }
}