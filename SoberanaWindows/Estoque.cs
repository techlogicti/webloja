﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SoberanaWindows
{
    public partial class Estoque : Form
    {


        string connectionString = "server=joao-pc\\SQLEXPRESS; Database=SoberanaLocal;User ID=soberana;Password=1q2w3e4r;Integrated Security=False";

        private BindingSource bindingSource1 = new BindingSource();
        private SqlDataAdapter dataAdapter = new SqlDataAdapter();
        //private string sqlEstoque = "SELECT * FROM estoque ORDER BY categoria,  descricao ";
        private string sqlEstoque = "";
        
        private string sqlMarca = "SELECT distinct marca FROM estoque ORDER BY marca asc";
        private string sqlCategoria = "SELECT distinct categoria FROM estoque ORDER BY categoria asc";

        public string tipoAcessoSistema = "";

        public Estoque(String tipoAcesso)
        {
            InitializeComponent();
            tipoAcessoSistema = tipoAcesso.ToString();
            carregaCombos();

            if (tipoAcesso.Equals("basico")) {
                sqlEstoque = "SELECT codigo, marca, descricao, categoria, subcategoria, precoVenda, precoMinimo, estoqueFinal from estoque ";
            }
            if (tipoAcesso.Equals("admin"))
            {
                sqlEstoque = "SELECT* FROM estoque ORDER BY categoria, descricao ";
            }

             dataGridView1.DataSource = bindingSource1;
            GetData(sqlEstoque);

        }

        public void carregaCombos() {

            cmbMarca.Items.Clear();
            cmbCategoria.Items.Clear();
            cmbSubCategoria.Items.Clear();

            cmbMarca.Items.Add("Todas");
            cmbMarca.SelectedIndex = 0;
            cmbCategoria.Items.Add("Todas");
            cmbCategoria.SelectedIndex = 0;
            cmbSubCategoria.Items.Add("Todas");
            cmbSubCategoria.SelectedIndex = 0;

            cmbSubCategoria.Enabled = false;
            cmbMarca.Enabled = false;

            //carregaMarcas();
            carregaCategorias();
            //carregaSubCategorias();
        }

        public void carregaMarcas()
        {

            SqlConnection sqlConn = new SqlConnection(connectionString);

            //abre conexao
            sqlConn.Open();

            //realiza operações

            SqlCommand cmd = new SqlCommand(sqlMarca, sqlConn);
            SqlDataReader dr = cmd.ExecuteReader();

            //percorre o SqlDataReader para obter os dados
            cmbMarca.Items.Add("Todas");
            while (dr.Read())
            {
                //Exibindo o código da categoria em dr[0]
                //e o nome da categoria em dr["CategoryName"]
                cmbMarca.Items.Add(dr[0] + " - " + dr["marca"]);
            }

            //fecha conexao
            sqlConn.Close();

        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Exit();
        }

        public void carregaCategorias()
        {

            SqlConnection sqlConn = new SqlConnection(connectionString);

            //abre conexao
            sqlConn.Open();

            //realiza operações

            SqlCommand cmd = new SqlCommand(sqlCategoria, sqlConn);
            SqlDataReader dr = cmd.ExecuteReader();

           
            cmbCategoria.Items.Add("Todas");
            while (dr.Read())
            {
                //cmbCategoria.Items.Add(dr[0] + " - " + dr["categoria"]);
                cmbCategoria.Items.Add(dr[0]);
            }

            //fecha conexao
            sqlConn.Close();

        }

        public void carregaSubCategorias(String categoriaSelecionada)
        {

            SqlConnection sqlConn = new SqlConnection(connectionString);

            //abre conexao
            sqlConn.Open();

            //realiza operações
            string sqlSubCategoria = "SELECT distinct subcategoria  FROM estoque ";
            if(!categoriaSelecionada.Equals("Todas")) sqlSubCategoria = sqlSubCategoria + " where categoria like '%" + categoriaSelecionada + "%' ";
            sqlSubCategoria = sqlSubCategoria + "  ORDER BY subcategoria asc ";


            SqlCommand cmd = new SqlCommand(sqlSubCategoria, sqlConn);
            SqlDataReader dr = cmd.ExecuteReader();


            //limpa combo
            cmbSubCategoria.Items.Clear();


            cmbSubCategoria.Items.Add("Todas");

            while (dr.Read())
            {
                cmbSubCategoria.Items.Add(dr[0] + " - " + dr["subcategoria"]);
            }

            //fecha conexao
            sqlConn.Close();

        }

        public void comunicaBanco() {

            SqlConnection sqlConn = new SqlConnection(connectionString);

            //abre conexao
            sqlConn.Open();

            //realiza operações

            SqlCommand cmd = new SqlCommand(sqlEstoque, sqlConn);
            SqlDataReader dr = cmd.ExecuteReader();

           
            //fecha conexao
            sqlConn.Open();
        }

        private void GetData(string selectCommand)
        {
            try
            {
               

                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSource1.DataSource = table;

                // Resize the DataGridView columns to fit the newly loaded content.
                dataGridView1.AutoResizeColumns(
                    DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }

        private void GetDataFiltro(String tipoAcesso)
        {

            string sqlEstoqueFiltro = "";

            if (tipoAcesso.Equals("basico"))
            {
                sqlEstoqueFiltro = "SELECT codigo, marca, descricao, categoria, subcategoria, precoVenda, precoMinimo, estoqueFinal FROM estoque where 1=1 ";
            }
            if (tipoAcesso.Equals("admin"))
            {
                sqlEstoqueFiltro = "SELECT * FROM estoque where 1=1 ";
            }


            
           
            

            string categoriaFiltro = "";
            string subCategoriaFiltro = "";
            //string marcaFiltro = "";
            string descricaoFiltro = "";
            

            categoriaFiltro = cmbCategoria.SelectedItem.ToString().Trim();

            //if(!cmbSubCategoria.SelectedValue.Equals("-1"))
            //subCategoriaFiltro = cmbSubCategoria.SelectedItem.ToString().Trim();
            //marcaFiltro = cmbMarca.SelectedItem.ToString().Trim();
            descricaoFiltro = txtDescricao.Text.ToString().Trim();


            if (!categoriaFiltro.Equals("Todas")) sqlEstoqueFiltro = sqlEstoqueFiltro + " and categoria like '%" + categoriaFiltro + "%' ";

            //if (!subCategoriaFiltro.Equals("Todas")) sqlEstoqueFiltro = sqlEstoqueFiltro + " and subcategoria like '%" + subCategoriaFiltro + "%' ";

            //if (!categoriaFiltro.Equals("Todas")) sqlEstoqueFiltro = sqlEstoqueFiltro + " and categoria like '%" + categoriaFiltro + "%' ";

            if(!descricaoFiltro.Equals("")) sqlEstoqueFiltro = sqlEstoqueFiltro + " and upper(descricao) like upper('%"+descricaoFiltro.ToString().Trim()+"%') ";

            if (chkSomenteNoEstoque.Checked) sqlEstoqueFiltro = sqlEstoqueFiltro + " and estoqueFinal > 0 ";


            sqlEstoqueFiltro = sqlEstoqueFiltro + " ORDER BY categoria,  descricao";
            try
            {


                // Create a new data adapter based on the specified query.
                dataAdapter = new SqlDataAdapter(sqlEstoqueFiltro, connectionString);

                // Create a command builder to generate SQL update, insert, and
                // delete commands based on selectCommand. These are used to
                // update the database.
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);

                // Populate a new data table and bind it to the BindingSource.
                DataTable table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                bindingSource1.DataSource = table;

                // Resize the DataGridView columns to fit the newly loaded content.
                dataGridView1.AutoResizeColumns(
                    DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }

        private void cmbCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            string categoriaSelecionada = "";

            categoriaSelecionada = cmbCategoria.SelectedItem.ToString().Trim();

            carregaSubCategorias(categoriaSelecionada);

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetDataFiltro(tipoAcessoSistema);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void txtDescricao_TextChanged(object sender, EventArgs e)
        {
            string texto = "";
            texto = txtDescricao.Text.Trim().ToString();

            if (texto != "") {
                GetDataFiltro(tipoAcessoSistema);
            }
        }
    }
}
