﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoberanaWindows
{
    public partial class Testes : Form
    {
        public Testes()
        {
            InitializeComponent();



            /* http://www.csharp-examples.net/string-format-datetime/ */
        }

        public void update() {
            DateTime a = DateTime.Now;
            DateTime b = DateTime.Now.AddHours(180);

            textBox1.Text = a.ToString();
            textBox2.Text = a.AddDays(30).ToString();
            textBox3.Text = DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;

            DateTime data = DateTime.Now.Date;
            textBox4.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now.Date);
            textBox5.Text = String.Format("{0:yyyy/MM/dd}", DateTime.Now.Date);
            textBox6.Text = String.Format("{0:yyyyMMdd}", DateTime.Now.Date.AddDays(30));
            textBox7.Text = String.Format("{0:yyyyMMdd}", DateTime.Now.Date.AddDays(60));

            

            textBox8.Text = String.Format("{0:HHmmss}", DateTime.Now);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            update();
        }
    }
}
