﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoberanaWindows
{
    public partial class Principal : Form
    {
        public Principal(String operador)
        {
            InitializeComponent();
            toolStripStatusLabel1.Text = "Operador: "+ operador;
            toolStripStatusLabel2.Text = "";
        }

        private void consultasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultaPedido cPedido = new ConsultaPedido();
        
            cPedido.MdiParent = this;
        
            cPedido.Show();
        
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void consultaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Estoque cEstoque = new Estoque("admin");

            cEstoque.MdiParent = this;

            cEstoque.Show();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void recebimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Recebimentos recebimentos = new Recebimentos();

            recebimentos.MdiParent = this;

            recebimentos.Show();
        }
    }
}
