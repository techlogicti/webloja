﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoberanaWindows
{
    public partial class Recebimentos : Form
    {
        public Recebimentos()
        {
            InitializeComponent();
            preencheImpressoras();
        }

        public void preencheImpressoras()
        {
            cmbImpressoras.Items.Clear();
            String pkInstalledPrinters;
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                cmbImpressoras.Items.Add(pkInstalledPrinters);
            }
        }
    }
}
