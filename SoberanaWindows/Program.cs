﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SoberanaWindows
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Login());
            //Application.Run(new ConsultaTabela("admin"));
            Application.Run(new Principal("admin"));
            //Application.Run(new Testes());
        }
    }
}
