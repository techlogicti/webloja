﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocios;
using ObjetoTransferencia;

namespace Apresentacao
{
    public partial class FrmMarcaSelecionar : Form
    {
        public FrmMarcaSelecionar()
        {
            InitializeComponent();

            //Não gerar linhas automaticamente
            dataGridViewPrincipal.AutoGenerateColumns = false;
        }

        private void buttonPesquisar_Click(object sender, EventArgs e)
        {
            MarcaNegocios marcaNegocios = new MarcaNegocios();

            MarcaColecao marcaColecao = new MarcaColecao();
            marcaColecao = marcaNegocios.ConsultarPorNome(textBoxPesquisa.Text.Trim());

            dataGridViewPrincipal.DataSource = null; //limpa os dados caso já existam no grid
            dataGridViewPrincipal.DataSource = marcaColecao;

            //Força atualização do grid e da tela (não é obrigatório)
            dataGridViewPrincipal.Update();
            dataGridViewPrincipal.Refresh();
        }
    }
}
