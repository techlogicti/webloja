﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALCategoria
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALCategoria(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloCategoria modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "insert into tb_categoria values(@departamento, @pai, @nome, @descricao, @cdOperador, @datacriacao, @horacriacao, @dataatualizacao, @horaatualizacao, @situacaoregistro ); select @@IDENTITY;";

            cmd.Parameters.AddWithValue("@pai", modelo.IdDepartamento);
            cmd.Parameters.AddWithValue("@departamento", modelo.IdCategoriaPai);
            cmd.Parameters.AddWithValue("@nome", modelo.Nome);
            cmd.Parameters.AddWithValue("@descricao", modelo.Descricao);
            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");

            conexao.Conectar();
            modelo.IdCategoria = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.IdCategoria);
        }

        public void Alterar(ModeloCategoria modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_categoria set idPai=@pai, idDepartamento=@departamento, nm_nome=@campo, de_descricao=@descricao where id= @id";
            cmd.Parameters.AddWithValue("@pai", modelo.IdCategoriaPai);
            cmd.Parameters.AddWithValue("@departamento", modelo.IdDepartamento);
            cmd.Parameters.AddWithValue("@nome", modelo.Nome);
            cmd.Parameters.AddWithValue("@descricao", modelo.Descricao);
            cmd.Parameters.AddWithValue("@id", modelo.IdCategoria);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int codigo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_categoria where id= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_categoria where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }




        public ModeloCategoria CarregarModeloCategoria(int codigo)
        {
            ModeloCategoria modelo = new ModeloCategoria();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_categoria where id= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                modelo.IdCategoria = Convert.ToInt32(registro["id"]);
                modelo.IdCategoriaPai = Convert.ToInt32(registro["IdPai"]);
                modelo.IdDepartamento = Convert.ToInt32(registro["IdDepartamento"]);
                modelo.Nome = registro["nm_nome"].ToString();
                modelo.Descricao = registro["de_descricao"].ToString();
            }

            conexao.Desconectar();

            return modelo;


        }

      
    }
}
