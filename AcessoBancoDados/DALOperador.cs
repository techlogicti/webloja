﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALOperador
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALOperador(DALConexao cx)
        {
            this.conexao = cx;
        }

        public ModeloOperador recuperaIdLogin(string login)
        {
            int id = 0;


            ModeloOperador modelo = new ModeloOperador();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_operador where cd_login= @login";
            cmd.Parameters.AddWithValue("@login", login);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                modelo.Id = Convert.ToInt32(registro["id"]);
                /*modelo.RazaoSocial = registro["id_itenspedido"].ToString();
                modelo.NomeFantasia = registro["id_produto"].ToString();*/
            }

            conexao.Desconectar();

            return modelo;

            
        }

        public int Incluir(ModeloOperador modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;

            string sqlInsert = "";
            /*
            sqlInsert = " insert into tb_cliente values(@cd_codigo, @nm_nomecompleto, @nm_apelido, @dt_datanascimento, @cd_estadocivil, @nm_conjuge,";
            sqlInsert += " @cd_profissaoconjuge,@de_nacionalidade, @de_naturalidade,@nm_nomemae,@nm_nomepai,@nu_cpfCnpj, @nu_numeroidentidade, ";
            sqlInsert += " @dt_emissaoidentidade, @cd_ufemissoridentidade, @de_orgaoemissoridentidade, @nu_numerocnh, @dt_dataemissaocnh, @cd_ufemissorcnh,";
            sqlInsert += " @cd_categoriacnh, @cd_pais, @cd_uf, @cd_cidade, @de_bairro, @de_logradouro, @de_numero, @de_complemento, @de_pontodereferencia,";
            sqlInsert += " @de_numerocep, @de_empresa, @nu_telefonecomercial, @nm_responsavelcomercial, @cd_tipoatividade, @cd_profissao, @dt_dataadmissao, ";
            sqlInsert += " @vl_ordenado, @vl_ultimacompra, @dt_ultimacompra, @vl_creditoLoja, @nu_telefonefixo, @nu_celular1, @nu_celular2, @nu_celular3, @nu_celular4 ";
            sqlInsert += " @de_email, @nu_telefonerecado, @nm_nomerecado, @de_parentescorecado, @de_observacoes, @cd_operador, @datacriacao, @horacriacao, ";
            sqlInsert += "@dataatualizacao, @horaatualizacao, @situacaoregistro, @de_profissaocliente, @de_profissaoconjuge ); select @@IDENTITY;";
            */
            cmd.CommandText = sqlInsert.ToString();
            cmd.Parameters.AddWithValue("@cdUf", "-");
            cmd.Parameters.AddWithValue("@cdOperador", 1);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");

            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.Id);
        }

    public void Alterar(ModeloOperador modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_operador set campo=@campo where id_= @codigo";
            /*cmd.Parameters.AddWithValue("@campo", modelo.NomeFantasia);
            cmd.Parameters.AddWithValue("@codigo", modelo.IdFornecedor);*/
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int codigo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_operador where id_= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_operador where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        public ModeloOperador CarregarModeloCliente(int codigo)
        {
            ModeloOperador modelo = new ModeloOperador();
            
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_operador where id_= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                /*modelo.IdFornecedor = Convert.ToInt32(registro["id_pedido"]);
                modelo.RazaoSocial = registro["id_itenspedido"].ToString();
                modelo.NomeFantasia = registro["id_produto"].ToString();*/
            }

            conexao.Desconectar();

            return modelo;


        }
    }
}
