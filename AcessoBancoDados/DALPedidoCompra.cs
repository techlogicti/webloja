﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALPedidoCompra
    {

        private DALConexao conexao;


        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALPedidoCompra(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloPedidoCompra modelo)
        {


            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;

            string sql = "";

            sql = "insert into tb_pedidoCompra values( ";
            sql += "@IdFuncionario, ";
            sql += "@IdFornecedor, ";
            sql += "@DE_PedidoFornecedor, ";
            sql += "@ValorPedido, ";
            sql += "@ValorDesconto, ";
            sql += "@ValorImposto, ";
            sql += "@ValorFrete, ";
            sql += "@ValorTotal, ";
            sql += "@QuantidadeItens, ";
            sql += "@IdFormaPagamento, ";
            sql += "@DataPedido, ";
            sql += "@DataBase, ";
            sql += "@DataPrevisaoEntrega, ";
            sql += "@DataPrimeiroVencimento, ";
            sql += "@SituacaoPedido, ";
            sql += "@Observacao, ";
            sql += "@cdOperador,";
            sql += "@dataCriacao,";
            sql += "@horaCriacao,";
            sql += "@situacaoPedido,";
            sql += "@dataAtualizacao,";
            sql += "@horaAtualizacao,";
            sql += "@situacaoRegistro,";
            sql += "@situacaoEntrega,";
            sql += "@dataEntrega";
            
            

            //sql += " ); select @@IDENTITY;";
            sql += " ); select SCOPE_IDENTITY();";

            //SCOPE_IDENTITY()

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@IdFuncionario", modelo.IdFuncionario);
            cmd.Parameters.AddWithValue("@IdFornecedor", modelo.IdFornecedor);
            cmd.Parameters.AddWithValue("@DE_PedidoFornecedor", modelo.DE_PedidoFornecedor);
            cmd.Parameters.AddWithValue("@ValorPedido", modelo.ValorPedido);
            cmd.Parameters.AddWithValue("@ValorDesconto", modelo.ValorDesconto);
            cmd.Parameters.AddWithValue("@ValorImposto", modelo.ValorImposto);
            cmd.Parameters.AddWithValue("@ValorFrete", modelo.ValorFrete);
            cmd.Parameters.AddWithValue("@ValorTotal", modelo.ValorTotal);
            cmd.Parameters.AddWithValue("@QuantidadeItens", modelo.QuantidadeItens);
            cmd.Parameters.AddWithValue("@IdFormaPagamento", modelo.IdFormaPagamento);
            cmd.Parameters.AddWithValue("@DataPedido", modelo.DataPedido);
            cmd.Parameters.AddWithValue("@DataBase", modelo.DataBase);
            cmd.Parameters.AddWithValue("@DataPrevisaoEntrega", modelo.DataPrevisaoEntrega);
            cmd.Parameters.AddWithValue("@DataPrimeiroVencimento", modelo.DataPrimeiroVencimento);
            cmd.Parameters.AddWithValue("@SituacaoPedido", modelo.SituacaoPedido);
            cmd.Parameters.AddWithValue("@Observacao", modelo.Observacoes);

            cmd.Parameters.AddWithValue("@CD_operador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@DT_dataCriacao", modelo.DT_dataCriacao);
            cmd.Parameters.AddWithValue("@HR_horaCriacao", modelo.HR_horaCriacao);
            cmd.Parameters.AddWithValue("@DT_dataAtualizacao", modelo.DT_dataAtualizacao);
            cmd.Parameters.AddWithValue("@HR_horaAtualizacao", modelo.HR_horaAtualizacao);
            cmd.Parameters.AddWithValue("@ST_situacaoRegistro", modelo.ST_situacaoRegistro);
            cmd.Parameters.AddWithValue("@SituacaoEntrega", modelo.SituacaoEntrega);
            cmd.Parameters.AddWithValue("@DataEntrega", modelo.DataEntrega);
            
            conexao.Conectar();

            int qtdLinhasAfetadas = 0;
            int idPedidoGerado = 0;


            //this.gravarLog("Execução do sql: " + sql.ToString());

            try
            {
                //qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
                idPedidoGerado = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }



            conexao.Desconectar();

            return idPedidoGerado;

        }



        public void Alterar(ModeloPedidoCompra modelo)
        {
            /* SqlCommand cmd = new SqlCommand();
             cmd.Connection = conexao.ObjetoConexao;
             cmd.CommandText = "update TB_contasReceber set campo=@campo where id= @codigo";
             /*cmd.Parameters.AddWithValue("@campo", modelo.NomeFantasia);
             cmd.Parameters.AddWithValue("@codigo", modelo.IdFornecedor);*/

            /*conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();*/
        }

        public void Excluir(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_pedidoCompra where id= @id";
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public int CancelarPedido(int id)
        {
            int qtdLinhasAfetadas = 0;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_pedidoCompra set st_situacaoPedido=@situacaoPedido, st_situacaoEntrega=@situacaoEntrega where idPedido= @id";
            cmd.Parameters.AddWithValue("@situacaoPedido", "C");
            cmd.Parameters.AddWithValue("@situacaoEntrega", "C");
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            //cmd.ExecuteNonQuery();
            qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());


            conexao.Desconectar();
            return qtdLinhasAfetadas;
        }



        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_pedidoCompra where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        public ModeloPedidoCompra CarregarModelo(int codigo)
        {
            ModeloPedidoCompra modelo = new ModeloPedidoCompra();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_pedidoCompra where id = @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                /*registro.Read();
                modelo.id = Convert.ToInt32(registro["id"]);
                modelo.codigoEAN = registro["codigo"].ToString();
                modelo.idCategoria = Convert.ToInt32(registro["id_categoria"]);
                modelo.idDepartamento = Convert.ToInt32(registro["id_departamento"]);
                modelo.idEmpresa = Convert.ToInt32(registro["id_empresa"]);
                modelo.idMarca = Convert.ToInt32(registro["id_marca"]);
                modelo.nmNome = registro["nm_nome"].ToString();
                modelo.deDescricao = registro["de_descricao"].ToString();
                modelo.precoCompra = Convert.ToDecimal(registro["precoCompra"]);
                modelo.precoCompraImposto = Convert.ToDecimal(registro["precoCompraImposto"]);
                modelo.precoMinimo = Convert.ToDecimal(registro["precoMinimo"]);
                modelo.precoAtacado = Convert.ToDecimal(registro["precoAtacado"]);
                modelo.precoVenda = Convert.ToDecimal(registro["precoVenda"]);
                modelo.estoqueAtacado = Convert.ToInt32(registro["estoqueAtacado"]);
                //modelo.estoqueFinal = Convert.ToInt32(registro["estoqueFinal"]);
                modelo.status = registro["status"].ToString();*/
            }

            conexao.Desconectar();

            return modelo;

        }

    }
}
