﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALNotaFiscalEntrada
    {

        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALNotaFiscalEntrada(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloNotaFiscalEntrada modelo)
        {
            String sql = "";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "";

            sql = "insert into tb_notaFiscalEntrada values( ";
            sql += "@ID_fornecedor, ";
            sql += "@ID_transportadora, ";
            sql += "@ID_cfop, ";
            sql += "@DE_numeroNotaFiscal, ";
            sql += "@DE_serie, ";
            sql += "@DE_modelo, ";
            sql += "@DE_chave, ";
            sql += "@DT_dataEmissao, ";
            sql += "@DT_dataEntrada, ";
            sql += "@TP_formaPagamento, ";
            sql += "@QT_itens, ";
            sql += "@VL_baseCalculoICMS, ";
            sql += "@VL_valorICMS, ";
            sql += "@VL_baseCalculoICMSSubstituicaoTributaria, ";
            sql += "@VL_valorICMSSubstituicaoTributaria, ";
            sql += "@VL_valorFrete, ";
            sql += "@VL_valorSeguro, ";
            sql += "@VL_valorOutrasDespesas, ";
            sql += "@VL_valorDesconto, ";
            sql += "@VL_valorIPI, ";
            sql += "@VL_valorTotalProdutos, ";
            sql += "@VL_valorTotalNota, ";
            sql += "@DE_observacao, ";
            sql += "@ID_funcionarioRecebimento, ";
            sql += "@CD_operador, ";
            sql += "@DT_dataCriacao, ";
            sql += "@HR_horaCriacao, ";
            sql += "@DT_dataEntrega, ";
            sql += "@ST_situacaoPedido, ";
            sql += "@DT_dataAtualizacao, ";
            sql += "@HR_horaAtualizacao, ";
            sql += "@ST_situacaoRegistro ";

            sql += " ); select @@IDENTITY;";

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@ID_fornecedor ", modelo.IdFornecedor);
            cmd.Parameters.AddWithValue("@ID_transportadora ", modelo.IdTransportadora);
            cmd.Parameters.AddWithValue("@ID_cfop ", modelo.IdCfop);
            cmd.Parameters.AddWithValue("@DE_numeroNotaFiscal ", modelo.NumeroNotaFiscal);
            cmd.Parameters.AddWithValue("@DE_serie ", modelo.SerieNotaFiscal);
            cmd.Parameters.AddWithValue("@DE_modelo ", modelo.ModeloNotaFiscal);
            cmd.Parameters.AddWithValue("@DE_chave ", modelo.ChaveAcesso);
            cmd.Parameters.AddWithValue("@DT_dataEmissao ", modelo.DataEmissao);
            cmd.Parameters.AddWithValue("@DT_dataEntrada ", modelo.DataEntrada);
            cmd.Parameters.AddWithValue("@TP_formaPagamento ", modelo.IdFormaPagamento);
            cmd.Parameters.AddWithValue("@QT_itens ", modelo.QuantidadeItens);
            cmd.Parameters.AddWithValue("@VL_baseCalculoICMS ", modelo.ValorBaseCalculoICMS);
            cmd.Parameters.AddWithValue("@VL_valorICMS ", modelo.ValorICMS);
            cmd.Parameters.AddWithValue("@VL_baseCalculoICMSSubstituicaoTributaria ", modelo.ValorBaseCalculoICMSSubST);
            cmd.Parameters.AddWithValue("@VL_valorICMSSubstituicaoTributaria ", modelo.ValorICMSSubST);
            cmd.Parameters.AddWithValue("@VL_valorFrete ", modelo.ValorFrete);
            cmd.Parameters.AddWithValue("@VL_valorSeguro ", modelo.ValorSeguro);
            cmd.Parameters.AddWithValue("@VL_valorOutrasDespesas ", modelo.ValorOutrasDespesas);
            cmd.Parameters.AddWithValue("@VL_valorDesconto ", modelo.ValorDesconto);
            cmd.Parameters.AddWithValue("@VL_valorIPI ", modelo.ValorIPI);
            cmd.Parameters.AddWithValue("@VL_valorTotalProdutos ", modelo.ValorTotalProdutos);
            cmd.Parameters.AddWithValue("@VL_valorTotalNota ", modelo.ValorTotalNota);
            cmd.Parameters.AddWithValue("@DE_observacao ", modelo.Observacoes);
            cmd.Parameters.AddWithValue("@ID_funcionarioRecebimento ", modelo.IdFuncionarioRecebimento);
            cmd.Parameters.AddWithValue("@CD_operador ", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@DT_dataCriacao ", modelo.DT_dataCriacao);
            cmd.Parameters.AddWithValue("@HR_horaCriacao ", modelo.HR_horaCriacao);
            cmd.Parameters.AddWithValue("@DT_dataEntrega ", modelo.DataEntrega);
            cmd.Parameters.AddWithValue("@ST_situacaoPedido ", modelo.SituacaoPedido);
            cmd.Parameters.AddWithValue("@DT_dataAtualizacao ", modelo.DT_dataAtualizacao);
            cmd.Parameters.AddWithValue("@HR_horaAtualizacao ", modelo.HR_horaAtualizacao);
            cmd.Parameters.AddWithValue("@ST_situacaoRegistro ", modelo.SituacaoRegistro);

            conexao.Conectar();

            try
            {
                modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.ToString());
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            
            conexao.Desconectar();

            return Convert.ToInt32(modelo.Id);
        }

        public void Alterar(ModeloProduto modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            //cmd.CommandText = "update tb_produto set campo=@campo where id= @codigo";
            /*cmd.Parameters.AddWithValue("@campo", modelo.NomeFantasia);
            cmd.Parameters.AddWithValue("@codigo", modelo.IdFornecedor);*/
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            //cmd.CommandText = "delete from tb_produto where id= @id";
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_notaFiscalEntrada where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        public ModeloNotaFiscalEntrada CarregarModeloNotaFiscalEntrada(int codigo)
        {
            ModeloNotaFiscalEntrada modelo = new ModeloNotaFiscalEntrada();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_notaFiscalEntrada where id = @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();

                modelo.Id = Convert.ToInt32(registro["id"]);
                modelo.IdFornecedor = Convert.ToInt32(registro["id_fornecedor"]);
                modelo.IdTransportadora = Convert.ToInt32(registro["id_transportadora"]);
                modelo.NumeroNotaFiscal = registro["de_numeroNotaFiscal"].ToString();
                modelo.SerieNotaFiscal = registro["de_serie"].ToString();
                modelo.ModeloNotaFiscal  = registro["de_modelo"].ToString();
                modelo.ChaveAcesso = registro["de_chave"].ToString();
                modelo.DataEmissao = Convert.ToInt32(registro["dt_dataEmissao"]);
                modelo.DataEntrada = Convert.ToInt32(registro["dt_dataEntrada"]);
                modelo.IdFormaPagamento = Convert.ToInt32(registro["tp_formaPagamento"]);
                modelo.QuantidadeItens = Convert.ToInt32(registro["qt_itens"]);
                modelo.ValorBaseCalculoICMS = Convert.ToDecimal(registro["vl_baseCalculoICMS"]);
                modelo.ValorICMS = Convert.ToDecimal(registro["vl_valorICMS"]);
                modelo.ValorBaseCalculoICMSSubST = Convert.ToDecimal(registro["vl_baseCalculoICMSSubstituicaoTributaria"]);
                modelo.ValorICMSSubST = Convert.ToDecimal(registro["vl_valorICMSSubstituicaoTributaria"]);
                modelo.ValorFrete = Convert.ToDecimal(registro["vl_valorFrete"]);
                modelo.ValorSeguro = Convert.ToDecimal(registro["vl_valorSeguro"]);
                modelo.ValorOutrasDespesas = Convert.ToDecimal(registro["vl_valorOutrasDespesas"]);
                modelo.ValorDesconto = Convert.ToDecimal(registro["vl_valorDesconto"]);
                modelo.ValorIPI = Convert.ToDecimal(registro["vl_valorIPI"]);
                modelo.ValorTotalProdutos = Convert.ToDecimal(registro["vl_valorTodalProdutos"]); //ajustar o toDal por toTal = D -> T
                modelo.ValorTotalNota = Convert.ToDecimal(registro["vl_valorTotalNota"]);
                modelo.Observacoes = registro["de_observacao"].ToString();
                modelo.IdFuncionarioRecebimento = Convert.ToInt32(registro["id_funcionarioRecebimento"]);
                modelo.DataEntrega = Convert.ToInt32(registro["dt_dataEntrega"]);
                modelo.SituacaoPedido = registro["st_situacaoPedido"].ToString();
                modelo.SituacaoEntrega = "0";

                modelo.CD_operador = Convert.ToInt32(registro["cd_operador"]);
                modelo.DT_dataCriacao = Convert.ToInt32(registro["DT_dataCriacao"]);
                modelo.HR_horaCriacao = Convert.ToInt32(registro["HR_horaCriacao"]);
                modelo.DT_dataAtualizacao = Convert.ToInt32(registro["DT_dataAtualizacao"]);
                modelo.HR_horaAtualizacao = Convert.ToInt32(registro["HR_horaAtualizacao"]);


                modelo.SituacaoRegistro = registro["st_situacaoRegistro"].ToString();



            }

            conexao.Desconectar();

            return modelo;

        }

        public ModeloNotaFiscalEntrada LocalizarBetter(string chaveNfe)
        {
            ModeloNotaFiscalEntrada modelo = new ModeloNotaFiscalEntrada();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from vw_betterNFe where ChaveNFE = @codigo";
            cmd.Parameters.AddWithValue("@codigo", chaveNfe);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();

                modelo.Id = Convert.ToInt32(registro["IdCompras"]);
                //modelo.DT_dataCriacao = Convert.ToInt32(registro["Datalancamento"].ToString().Replace("-","").Substring(0,6));
                modelo.DT_dataCriacao = 0;
                modelo.DataEmissao = 0;
                modelo.IdFornecedor = Convert.ToInt32(registro["CodigoFornecedor"]);
                modelo.NumeroNotaFiscal = registro["NumeroDocumento"].ToString();

                
                //modelo.IdTransportadora = Convert.ToInt32(registro["id_transportadora"]);
                modelo.IdTransportadora = 0;

                modelo.SerieNotaFiscal = registro["Serie"].ToString();
                modelo.ModeloNotaFiscal = registro["Modelo"].ToString();
                modelo.ChaveAcesso = registro["ChaveNFE"].ToString();


                modelo.DataEntrada = 0;
                modelo.IdFormaPagamento = 0;
                modelo.QuantidadeItens = 0;
                modelo.ValorBaseCalculoICMS = Convert.ToDecimal(0);
                modelo.ValorICMS = Convert.ToDecimal(0);
                modelo.ValorBaseCalculoICMSSubST = Convert.ToDecimal(0);
                modelo.ValorICMSSubST = Convert.ToDecimal(0);
                modelo.ValorFrete = Convert.ToDecimal(registro["valorFrete"]);
                modelo.ValorSeguro = Convert.ToDecimal(registro["valorSeguro"]);
                modelo.ValorOutrasDespesas = Convert.ToDecimal(registro["DespesasAcessorias"]);
                modelo.ValorDesconto = Convert.ToDecimal(0);
                modelo.ValorIPI = Convert.ToDecimal(0);
                modelo.ValorTotalProdutos = Convert.ToDecimal(registro["totaldacompra"]); //ajustar o toDal por toTal = D -> T
                modelo.ValorTotalNota = Convert.ToDecimal(registro["totaldacompra"]);
                modelo.Observacoes = "";
                modelo.IdFuncionarioRecebimento = 0;
                modelo.DataEntrega = 0;
                modelo.SituacaoPedido = "";
                modelo.SituacaoEntrega = "0";

                modelo.CD_operador = 1;
                modelo.DT_dataCriacao = Convert.ToInt32(dataHoje);
                modelo.HR_horaCriacao = Convert.ToInt32(horaHoje);
                modelo.DT_dataAtualizacao = Convert.ToInt32(dataHoje);
                modelo.HR_horaAtualizacao = Convert.ToInt32(horaHoje);


                modelo.SituacaoRegistro = "";



            }

            conexao.Desconectar();

            return modelo;

        }

    }
}
