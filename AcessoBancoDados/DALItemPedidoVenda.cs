﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALItemPedidoVenda
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALItemPedidoVenda(DALConexao cx)
        {
            this.conexao = cx;
        }

        public List<ModeloItemPedidoVenda> RecuperaItensPedido(int idPedido)
        {

            ModeloItemPedidoVenda modelo = new ModeloItemPedidoVenda();
            List<ModeloItemPedidoVenda> lista = new List<ModeloItemPedidoVenda>();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conexao.ObjetoConexao;
                cmd.CommandText = "select * from TB_itemPedido where id = @codigo";
                cmd.Parameters.AddWithValue("@codigo", idPedido);
                conexao.Conectar();

                SqlDataReader registro = cmd.ExecuteReader();
                while (registro.HasRows)
                {
                    registro.Read();
                    modelo.IdItemPedido = Convert.ToInt32(registro["id"]);
                    modelo.IdProduto = Convert.ToInt32(registro["id_produto"]);
                    modelo.ValorUnitario = Convert.ToInt32(registro["vl_valorUnitario"]);
                    modelo.QuantidadeItens = Convert.ToInt32(registro["QT_itens"]);
                /*modelo.idDepartamento = Convert.ToInt32(registro["id_departamento"]);
                modelo.idEmpresa = Convert.ToInt32(registro["id_empresa"]);
                modelo.idMarca = Convert.ToInt32(registro["id_marca"]);
                modelo.nmNome = registro["nm_nome"].ToString();
                modelo.deDescricao = registro["de_descricao"].ToString();
                modelo.precoCompra = Convert.ToDecimal(registro["precoCompra"]);
                modelo.precoCompraImposto = Convert.ToDecimal(registro["precoCompraImposto"]);
                modelo.precoMinimo = Convert.ToDecimal(registro["precoMinimo"]);
                modelo.precoAtacado = Convert.ToDecimal(registro["precoAtacado"]);
                modelo.precoVenda = Convert.ToDecimal(registro["precoVenda"]);
                modelo.estoqueAtacado = Convert.ToInt32(registro["estoqueAtacado"]);
                //modelo.estoqueFinal = Convert.ToInt32(registro["estoqueFinal"]);
                modelo.status = registro["status"].ToString();*/

                lista.Add(modelo);
            }

                conexao.Desconectar();

                return lista;
            
        }

        public int Incluir(ModeloItemPedidoVenda modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;

            string sql = "";

            sql = "insert into tb_itemPedido values( ";
            sql += "@idPedidoVenda, ";
            sql += "@idProduto, ";
            sql += "@valorUnitario, ";
            sql += "@quantidade, ";
            sql += "@valorDescontoUnitario, ";
            sql += "@valorDescontoTotal, ";
            sql += "@valorTotal, ";
            sql += "@observacao,";
            sql += "@cdOperador,";
            sql += "@dataCriacao,";
            sql += "@horaCriacao,";
            sql += "@dataEntrega,";
            sql += "@dataAtualizacao,";
            sql += "@horaAtualizacao,";
            sql += "@situacaoRegistro";

            //sql += " ); select @@IDENTITY;";
            sql += " ); select SCOPE_IDENTITY();";

            //SCOPE_IDENTITY()

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@idPedidoVenda", modelo.IdPedido);
            cmd.Parameters.AddWithValue("@idProduto", modelo.IdProduto);
            cmd.Parameters.AddWithValue("@valorUnitario", modelo.ValorUnitario);
            cmd.Parameters.AddWithValue("@quantidade", modelo.QuantidadeItens);
            cmd.Parameters.AddWithValue("@valorDescontoUnitario", modelo.ValorDescontoUnitario);
            cmd.Parameters.AddWithValue("@valorDescontoTotal", modelo.ValorDescontoTotal);
            cmd.Parameters.AddWithValue("@valorTotal", modelo.ValorTotal);
            cmd.Parameters.AddWithValue("@Observacao", modelo.DE_observacoes);
            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@dataCriacao", modelo.DT_dataCriacao);
            cmd.Parameters.AddWithValue("@horaCriacao", modelo.HR_horaCriacao);
            cmd.Parameters.AddWithValue("@dataEntrega", modelo.DT_dataEntrega);
            cmd.Parameters.AddWithValue("@dataAtualizacao", modelo.DT_dataAtualizacao);
            cmd.Parameters.AddWithValue("@horaAtualizacao", modelo.HR_horaAtualizacao);
            cmd.Parameters.AddWithValue("@situacaoRegistro", modelo.ST_situacaoRegistro);
            
            conexao.Conectar();

            int qtdLinhasAfetadas = 0;
            int idItemPedidoGerado = 0;


            //this.gravarLog("Execução do sql: " + sql.ToString());

            try
            {
                //qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
                idItemPedidoGerado = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }



            conexao.Desconectar();

            return idItemPedidoGerado;
        }
    }
}
