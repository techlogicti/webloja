﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALContaReceber
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALContaReceber(DALConexao cx)
        {
            this.conexao = cx;
        }


        public int ExcluirContasAReceber(int id)
        {
            int qtdLinhasAfetadas = 0;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete tb_contasReceber where id= @id";
            //cmd.Parameters.AddWithValue("@situacaoPedido", "Cancelado");
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            try
            {
                qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            
           
            conexao.Desconectar();
            return qtdLinhasAfetadas;
        }


        public int EstornarContasAReceber(int id)
        {
            /*metodo estorna O VALOR TODO do recebido na conta, logo, se teve um parcial, este tb sera estornado
             * ver a melhor maneira de contornar isso, possibilitando o estorno do PARCIAL */

            int qtdLinhasAfetadas = 0;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            //cmd.CommandText = "update tb_contasReceber set st_situacaoPagamento=@situacaoPagamento, dt_dataRecebimento=@dataRecebimento where id= @id";
            cmd.CommandText = "update tb_contasReceber set vl_valorRecebido=@valorRecebido, st_situacaoPagamento=@situacaoPagamento, dt_dataRecebimento=@dataRecebimento where id= @id";
            cmd.Parameters.AddWithValue("@situacaoPagamento", "Pendente"); //TODO.SOBERANA - ou Estornado de estornado, avaliar
            cmd.Parameters.AddWithValue("@dataRecebimento", "0");
            cmd.Parameters.AddWithValue("@valorRecebido", "0"); //24012018
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            //cmd.ExecuteNonQuery();
            try
            {
                qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            conexao.Desconectar();
            return qtdLinhasAfetadas;
        }

        

        public int CancelarContasAReceber(int idPedido) //Esse metodo é chamado via Cancelamento de pedido e o ID usado é ID do Pedido
        {
            int qtdLinhasAfetadas = 0;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_contasReceber set st_situacaoPagamento=@st_situacaoPagamento where id_Pedido= @id";
            cmd.Parameters.AddWithValue("@st_situacaoPagamento", "Cancelado");
            cmd.Parameters.AddWithValue("@id", idPedido);
            conexao.Conectar();
            //cmd.ExecuteNonQuery();
           
            try
            {
                qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            conexao.Desconectar();
            return qtdLinhasAfetadas;
        }

        public int baixarConta(ModeloContaReceber modelo,string tipoRecebimento)
        {
            int qtdLinhasAfetadas = 0;

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_contasReceber set DT_dataAtualizacao= @dataatualizacao" +
             ",HR_horaAtualizacao=@horaatualizacao " +
             ", CD_operador= @cdOperador" +
             ", DT_dataRecebimento = @dataRecebimento" +
             ", VL_valorJuros = @valorJuros" +
             ", VL_valorMulta = @valorMulta" +
             ", VL_valorTotal = @valorTotal" +
             ", VL_valorRecebido = @valorRecebido" +
             //", ST_situacaoPagamento = @situacaoPagamento" +
             " where ID = @codigo ";

            cmd.Parameters.AddWithValue("@codigo", modelo.Id);
            cmd.Parameters.AddWithValue("@dataRecebimento", modelo.DataRecebimento);
            cmd.Parameters.AddWithValue("@valorJuros", modelo.ValorJuros);
            cmd.Parameters.AddWithValue("@valorMulta", modelo.ValorMulta);
            cmd.Parameters.AddWithValue("@valorTotal", modelo.ValorTotal);
            cmd.Parameters.AddWithValue("@valorRecebido", modelo.ValorRecebido);

            //cmd.Parameters.AddWithValue("@situacaoPagamento", modelo.SituacaoPagamento);

            cmd.Parameters.AddWithValue("@cdOperador", 1);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");
            
            conexao.Conectar();

            try
            {
                qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            //cmd.ExecuteNonQuery();
            //i = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return qtdLinhasAfetadas;
        }

        public int Incluir(ModeloContaReceber modelo)
        {
            String sql = "";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "";

            sql = "insert into TB_contasReceber values( ";
            sql += "@idPedido, ";
            sql += "@idCliente, ";
            sql += "@idTipoPagamento, ";
            sql += "@ValorConta, ";
            sql += "@NumeroParcela, ";
            sql += "@QuantidadeParcela, ";
            sql += "@DataEmissao, ";
            sql += "@DataVencimento, ";
            sql += "@ValorJuros, ";
            sql += "@ValorMulta, ";
            sql += "@ValorTotal,";
            sql += "@DataRecebimento,";
            sql += "@Observacao,";
            sql += "@cdOperador,";
            sql += "@dataCriacao,";
            sql += "@horaCriacao,";
            sql += "@situacaoPagamento,";
            sql += "@dataAtualizacao,";
            sql += "@horaAtualizacao,";
            sql += "@situacaoRegistro,";
            sql += "@ValorRecebido,";
            sql += "@MaquinaCartao,";
            sql += "@BandeiraCartao,";
            sql += "@NumeroContratoAutorizacao";

            sql += " ); select @@IDENTITY;";

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@idPedido", modelo.IdPedido);
            cmd.Parameters.AddWithValue("@idCliente", modelo.IdCliente);
            cmd.Parameters.AddWithValue("@idTipoPagamento", modelo.IdTipoPagamento);
            cmd.Parameters.AddWithValue("@ValorConta", modelo.ValorConta);
            cmd.Parameters.AddWithValue("@NumeroParcela", modelo.NumeroParcela);
            cmd.Parameters.AddWithValue("@QuantidadeParcela", modelo.QuantidadeParcela);
            cmd.Parameters.AddWithValue("@DataEmissao", modelo.DataEmissao);
            cmd.Parameters.AddWithValue("@ValorJuros", modelo.ValorJuros);
            cmd.Parameters.AddWithValue("@ValorMulta", modelo.ValorMulta);
            cmd.Parameters.AddWithValue("@ValorTotal", modelo.ValorTotal);
            cmd.Parameters.AddWithValue("@DataRecebimento", modelo.DataRecebimento);
            cmd.Parameters.AddWithValue("@DataVencimento", modelo.DataVencimento);
            cmd.Parameters.AddWithValue("@Observacao", modelo.Observacoes);
            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_codOperador);
            cmd.Parameters.AddWithValue("@dataCriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaCriacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoPagamento", modelo.SituacaoPagamento);
            cmd.Parameters.AddWithValue("@dataAtualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaAtualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoRegistro", "A");
            cmd.Parameters.AddWithValue("@ValorRecebido", modelo.ValorTotal);
            cmd.Parameters.AddWithValue("@MaquinaCartao", modelo.IdMaquinaCartao);
            cmd.Parameters.AddWithValue("@BandeiraCartao", modelo.IdBandeiraCartao);
            //20180726
            cmd.Parameters.AddWithValue("@NumeroContratoAutorizacao", modelo.NumeroContratoAutorizacao);

            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.Id);


        }

        public void Alterar(ModeloContaReceber modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update TB_contasReceber set campo=@campo where id= @codigo";
            /*cmd.Parameters.AddWithValue("@campo", modelo.NomeFantasia);
            cmd.Parameters.AddWithValue("@codigo", modelo.IdFornecedor);*/
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from TB_contasReceber where id= @id";
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void ExcluirPorIDPedido(int idPedido)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from TB_contasReceber where id_pedido= @id";
            cmd.Parameters.AddWithValue("@id", idPedido);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from TB_contasReceber where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        public string obtemSaldoAPagarDaPrestacao(string idConta, string parcela)
        {
            string saldo = "";
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            //cmd.CommandText = "select (ValorParcela-ValorRecebido) as ValorSaldo from vw_contasAReceber where Id=@idconta and NumeroParcela=@parcela";
            cmd.CommandText = "select ValorParcela from vw_contasAReceber where Id=14 and NumeroParcela=3";
            cmd.Parameters.AddWithValue("@idconta", idConta);
            cmd.Parameters.AddWithValue("@parcela", parcela);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                Console.WriteLine("1");
                try
                {
                    
                    //saldo = registro["ValorSaldo"].ToString();
                    saldo = Convert.ToInt32(registro["ValorParcela"]).ToString();
                }
                catch (SqlException odbcEx)
                {
                    Console.WriteLine(odbcEx.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                
                Console.WriteLine("2");
            }
            Console.WriteLine("3");
            Console.WriteLine(saldo.ToString());
            conexao.Desconectar();
            return saldo;
        }

        public ModeloContaReceber CarregarModelo(int codigo)
        {
            ModeloContaReceber modelo = new ModeloContaReceber();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from TB_contasReceber where id = @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                /*registro.Read();
                modelo.id = Convert.ToInt32(registro["id"]);
                modelo.codigoEAN = registro["codigo"].ToString();
                modelo.idCategoria = Convert.ToInt32(registro["id_categoria"]);
                modelo.idDepartamento = Convert.ToInt32(registro["id_departamento"]);
                modelo.idEmpresa = Convert.ToInt32(registro["id_empresa"]);
                modelo.idMarca = Convert.ToInt32(registro["id_marca"]);
                modelo.nmNome = registro["nm_nome"].ToString();
                modelo.deDescricao = registro["de_descricao"].ToString();
                modelo.precoCompra = Convert.ToDecimal(registro["precoCompra"]);
                modelo.precoCompraImposto = Convert.ToDecimal(registro["precoCompraImposto"]);
                modelo.precoMinimo = Convert.ToDecimal(registro["precoMinimo"]);
                modelo.precoAtacado = Convert.ToDecimal(registro["precoAtacado"]);
                modelo.precoVenda = Convert.ToDecimal(registro["precoVenda"]);
                modelo.estoqueAtacado = Convert.ToInt32(registro["estoqueAtacado"]);
                //modelo.estoqueFinal = Convert.ToInt32(registro["estoqueFinal"]);
                modelo.status = registro["status"].ToString();*/
            }

            conexao.Desconectar();

            return modelo;

        }


    }
}
