﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALPedidoVenda
    {

        private DALConexao conexao;
        

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALPedidoVenda(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloPedidoVenda modelo)
        {
            

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;

            string sql = "";

            sql = "insert into TB_PEDIDO values( ";
            sql += "@numeroPedido, ";
            sql += "@dataPedido, ";
            sql += "@valorPedido, ";
            sql += "@valorDesconto, ";
            sql += "@valorTotal, ";
            sql += "@quantidade, ";
            sql += "@Observacao,";
            sql += "@idVendedor, ";
            sql += "@idCliente, ";
            sql += "@cdOperador,";
            sql += "@dataCriacao,";
            sql += "@horaCriacao,";
            sql += "@dataEntrega,";
            sql += "@situacaoPedido,";
            sql += "@dataAtualizacao,";
            sql += "@horaAtualizacao,";
            sql += "@situacaoRegistro,";
            sql += "@idTipoPagamento,";
            sql += "@situacaoEntrega,";
            sql += "@valorEntrada, ";
            sql += "@valorSaldoPagar, ";
            sql += "@flagGerarContaReceber,";
            //sql += "@flagGerarCarne, "; Campo ainda não está na tabela
            sql += "@idLocalVenda ";
            

            //sql += " ); select @@IDENTITY;";
            sql += " ); select SCOPE_IDENTITY();";

            //SCOPE_IDENTITY()

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@numeroPedido", modelo.NumeroPedido);
            cmd.Parameters.AddWithValue("@dataPedido", modelo.DataPedido);
            cmd.Parameters.AddWithValue("@valorPedido", modelo.ValorPedido);
            cmd.Parameters.AddWithValue("@valorDesconto", modelo.ValorDesconto);
            cmd.Parameters.AddWithValue("@valorTotal", modelo.ValorTotal);
            cmd.Parameters.AddWithValue("@quantidade", modelo.QuantidadeItens);
            cmd.Parameters.AddWithValue("@Observacao", modelo.Observacoes);
            cmd.Parameters.AddWithValue("@idVendedor", modelo.IdVendedor);
            cmd.Parameters.AddWithValue("@idCliente", modelo.IdCliente);
            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@dataCriacao", modelo.DT_dataCriacao);
            cmd.Parameters.AddWithValue("@horaCriacao", modelo.HR_horaCriacao);
            cmd.Parameters.AddWithValue("@dataEntrega", modelo.DataEntrega);
            cmd.Parameters.AddWithValue("@situacaoPedido", modelo.SituacaoPedido);
            cmd.Parameters.AddWithValue("@dataAtualizacao", modelo.DT_dataAtualizacao);
            cmd.Parameters.AddWithValue("@horaAtualizacao", modelo.HR_horaAtualizacao);
            cmd.Parameters.AddWithValue("@situacaoRegistro", modelo.ST_situacaoRegistro);
            cmd.Parameters.AddWithValue("@idTipoPagamento", modelo.IdTipoPagamento);
            cmd.Parameters.AddWithValue("@situacaoEntrega", modelo.SituacaoEntrega);
            cmd.Parameters.AddWithValue("@valorEntrada", modelo.ValorEntrada);
            cmd.Parameters.AddWithValue("@valorSaldoPagar", modelo.ValorSaldo);
            cmd.Parameters.AddWithValue("@flagGerarContaReceber", modelo.flagGerarContaReceber);
            //cmd.Parameters.AddWithValue("@flagGerarCarne", modelo.flagGerarCarne); Campo ainda não está na tabela
            cmd.Parameters.AddWithValue("@idLocalVenda",modelo.IdLocalVenda);

            conexao.Conectar();

            int qtdLinhasAfetadas = 0;
            int idPedidoGerado = 0;


            //this.gravarLog("Execução do sql: " + sql.ToString());

            try
            {
                //qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
                idPedidoGerado = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            
            
            conexao.Desconectar();

            return idPedidoGerado;
            
        }

       

        public void Alterar(ModeloPedidoVenda modelo)
        {
            /* SqlCommand cmd = new SqlCommand();
             cmd.Connection = conexao.ObjetoConexao;
             cmd.CommandText = "update TB_contasReceber set campo=@campo where id= @codigo";
             /*cmd.Parameters.AddWithValue("@campo", modelo.NomeFantasia);
             cmd.Parameters.AddWithValue("@codigo", modelo.IdFornecedor);*/

            /*conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();*/
        }

        public void Excluir(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_pedido where id= @id";
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public int CancelarPedido(int id)
        {
            int qtdLinhasAfetadas = 0;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_pedido set st_situacaoPedido=@situacaoPedido where idPedido= @id";
            cmd.Parameters.AddWithValue("@situacaoPedido", "Cancelado");
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            //cmd.ExecuteNonQuery();
            qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());

           
            conexao.Desconectar();
            return qtdLinhasAfetadas;
        }

        

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_pedido where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        public ModeloPedidoVenda CarregarModelo(int codigo)
        {
            ModeloPedidoVenda modelo = new ModeloPedidoVenda();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_pedido where idPedido = @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                try
                {

                
                registro.Read();
                modelo.IdPedido = Convert.ToInt32(registro["idPedido"]);
                modelo.IdCliente = Convert.ToInt32(registro["id_cliente"]);
                modelo.Observacoes = registro["de_observacao"].ToString();
                modelo.DataPedido = Convert.ToInt32(registro["DT_dataPedido"].ToString());

                /*modelo.ValorTotal = Convert.ToInt32(registro["VL_valorTotal"].ToString());
                modelo.ValorPedido = Convert.ToInt32(registro["VL_valorPedido"].ToString());*/

                modelo.ValorTotal = 0;
                    modelo.ValorPedido = 0;

                modelo.NumeroPedido = registro["NU_numeroPedido"].ToString();
                /*modelo.codigoEAN = registro["codigo"].ToString();
                modelo.idCategoria = Convert.ToInt32(registro["id_categoria"]);
                modelo.idDepartamento = Convert.ToInt32(registro["id_departamento"]);
                modelo.idEmpresa = Convert.ToInt32(registro["id_empresa"]);
                modelo.idMarca = Convert.ToInt32(registro["id_marca"]);
                modelo.nmNome = registro["nm_nome"].ToString();
                modelo.deDescricao = registro["de_descricao"].ToString();
                modelo.precoCompra = Convert.ToDecimal(registro["precoCompra"]);
                modelo.precoCompraImposto = Convert.ToDecimal(registro["precoCompraImposto"]);
                modelo.precoMinimo = Convert.ToDecimal(registro["precoMinimo"]);
                modelo.precoAtacado = Convert.ToDecimal(registro["precoAtacado"]);
                modelo.precoVenda = Convert.ToDecimal(registro["precoVenda"]);
                modelo.estoqueAtacado = Convert.ToInt32(registro["estoqueAtacado"]);
                modelo.estoqueFinal = Convert.ToInt32(registro["estoqueFinal"]);
                modelo.status = registro["status"].ToString();*/
                modelo.SituacaoPedido = registro["ST_situacaoPedido"].ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            conexao.Desconectar();

            return modelo;

        }

    }
}
