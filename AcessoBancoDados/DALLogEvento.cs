﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALLogEvento
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALLogEvento(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloLogEvento modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "insert into tb_logEvento values(@idTabela, @idCampo, @descricao, @valor1, @valor2, @cdOperador, @datacriacao, @horacriacao, @dataatualizacao, @horaatualizacao, @situacaoregistro ); select @@IDENTITY;";

            cmd.Parameters.AddWithValue("@idTabela", modelo.IdTabela);
            cmd.Parameters.AddWithValue("@idCampo", modelo.IdCampo);
            cmd.Parameters.AddWithValue("@descricao", modelo.Descricao);
            cmd.Parameters.AddWithValue("@valor1", modelo.Valor);
            cmd.Parameters.AddWithValue("@valor2", modelo.Valor2);
            cmd.Parameters.AddWithValue("@cdOperador", 1);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");

            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.Id);
        }

        public void Alterar(ModeloFornecedor modelo)
        {
        }

        public void Excluir(int codigo)
        { }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_logEvento where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }
        public ModeloLogEvento CarregarModeloLogEventoPedido(int idPedido)
        {
            ModeloLogEvento modelo = new ModeloLogEvento();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select de_valor2 from tb_logEvento where ID_tabela=1 and id_campo='@idPedido' and de_valor='Cancelamento de Pedido'";
            cmd.Parameters.AddWithValue("@idPedido", idPedido);

            try
            {
                conexao.Conectar();

                SqlDataReader registro = cmd.ExecuteReader();
                if (registro.HasRows)
                {
                    registro.Read();
                    modelo.Valor2 = registro["de_valor2"].ToString();
                    /*modelo.IdFornecedor = Convert.ToInt32(registro["idFornecedor"]);
                    modelo.RazaoSocial = registro["de_razaoSocial"].ToString();
                    modelo.NomeFantasia = registro["de_nomeFantasia"].ToString();*/
                }

                conexao.Desconectar();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            

            return modelo;
        }

        public ModeloLogEvento CarregarModeloLogEvento(int codigo)
        {
            ModeloLogEvento modelo = new ModeloLogEvento();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_logEvento where id= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                /*modelo.IdFornecedor = Convert.ToInt32(registro["idFornecedor"]);
                modelo.RazaoSocial = registro["de_razaoSocial"].ToString();
                modelo.NomeFantasia = registro["de_nomeFantasia"].ToString();*/
            }

            conexao.Desconectar();

            return modelo;


        }

    }
}
