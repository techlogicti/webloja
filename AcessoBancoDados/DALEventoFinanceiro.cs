﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALEventoFinanceiro
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALEventoFinanceiro(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int EstornarEventoFinanceiro(int idConta, int idEventoFinanceiro)
        {
            /*metodo estorna O VALOR TODO do recebido na conta, logo, se teve um parcial, este tb sera estornado
             * ver a melhor maneira de contornar isso, possibilitando o estorno do PARCIAL */

            int qtdLinhasAfetadas = 0;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_eventofinanceiro where id= @id";
            cmd.Parameters.AddWithValue("@id", idEventoFinanceiro);
            conexao.Conectar();
            //cmd.ExecuteNonQuery();
            try
            {
                //qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            /*
            ModeloEventoFinanceiro modelo = new ModeloEventoFinanceiro();
            modelo.IdConta = idConta;
            atualizaValorRecebidoTabelaContasReceberAposRecebimento(modelo);

            */
            conexao.Desconectar();
            return qtdLinhasAfetadas;
        }

        public int Incluir(ModeloEventoFinanceiro modelo)
        {
            String sql = "";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "";

            sql = "insert into tb_eventoFinanceiro values( ";
            sql += "@idTabela, ";
            sql += "@idConta, ";
            sql += "@idTipoConta, ";
            sql += "@idTipoPagamento, ";
            sql += "@idHistoricoFinanceiro, ";
            sql += "@ValorEvento,";
            sql += "@DataEvento,";
            sql += "@Observacao,";
            sql += "@cdOperador,";
            sql += "@dataCriacao,";
            sql += "@horaCriacao,";
            sql += "@situacaoPagamento,";
            sql += "@dataAtualizacao,";
            sql += "@horaAtualizacao,";
            sql += "@situacaoRegistro";

            sql += " ); select @@IDENTITY;";

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@idTabela", modelo.IdTabela);
            cmd.Parameters.AddWithValue("@idConta", modelo.IdConta);
            cmd.Parameters.AddWithValue("@idTipoConta", modelo.IdTipoConta);
            cmd.Parameters.AddWithValue("@idTipoPagamento", modelo.IdTipoPagamento);
            cmd.Parameters.AddWithValue("@idHistoricoFinanceiro", modelo.IdHistoricoFinanceiro);
            cmd.Parameters.AddWithValue("@ValorEvento", modelo.ValorEvento);
            cmd.Parameters.AddWithValue("@DataEvento", modelo.DataEvento);
            cmd.Parameters.AddWithValue("@Observacao", modelo.DE_Observacoes);
            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@dataCriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaCriacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoPagamento", modelo.SituacaoPagamento);
            cmd.Parameters.AddWithValue("@dataAtualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaAtualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoRegistro", "A");

            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            //Atualiza tabela tb_contasReceber campos valor recebido e data recebimento
            if (modelo.Id > 0) atualizaValorRecebidoTabelaContasReceberAposRecebimento(modelo);

            /*Se o tipo de pagamento for cartão, ou seja, cliente pagar uma prestação 
             * originalmente em dinheiro, com um cartão de débito ou crédito, a mesma
             * embora fique quitada, gera uma nova conta a receber agora como forma
             * de pagamento cartão, uma vez que o cliente quitou a conta e a empresa
             * ainda não recebeu.*/

            if((modelo.IdTipoPagamento == 2) //c.débito
                || (modelo.IdTipoPagamento == 3) //c.crédito
                || (modelo.IdTipoPagamento == 4) //c.cdc
                || (modelo.IdTipoPagamento == 5)) //financeira
            {

            }
            else
            {
                //
            }

            return Convert.ToInt32(modelo.Id);


        }

        public void atualizaValorRecebidoTabelaContasReceberAposRecebimento(ModeloEventoFinanceiro modelo)
        {
            String sql = "";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "";

            sql += "update tb_contasreceber ";
            sql += "set ";
            sql += "VL_ValorRecebido = (select sum(vl_valorevento) from tb_eventoFinanceiro where ID_conta = @idconta), ";
            sql += "dt_dataRecebimento = (select max(dt_dataevento) from tb_eventoFinanceiro where ID_conta = @idconta) ";
            sql += "where id = @idconta; ";

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@idConta", modelo.IdConta);
            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            
        }
    }
}
