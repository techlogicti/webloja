﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALAutorizacaoCliente
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALAutorizacaoCliente(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloAutorizacaoCliente modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "insert into tb_autorizacaoCliente values(@idcliente, @idtipopagamento,@flagautorizado, @cdOperador, @datacriacao, @horacriacao, @dataatualizacao, @horaatualizacao, @situacaoregistro ); select @@IDENTITY;";

            cmd.Parameters.AddWithValue("@idcliente", modelo.IdCliente);
            cmd.Parameters.AddWithValue("@idtipopagamento", modelo.IdTipoPagamento);
            cmd.Parameters.AddWithValue("@flagautorizado", modelo.FlagAutorizado);
            cmd.Parameters.AddWithValue("@cdOperador", 1);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");

            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.Id);
        }

        public void Alterar(ModeloAutorizacaoCliente modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_autorizacaoCliente set ST_flagAutorizado=@flagautorizado where id_cliente=@idcliente and id_tipopagamento=@idtipopagamento";
            cmd.Parameters.AddWithValue("@idcliente", modelo.IdCliente);
            cmd.Parameters.AddWithValue("@idtipopagamento", modelo.IdTipoPagamento);
            cmd.Parameters.AddWithValue("@flagautorizado", modelo.FlagAutorizado);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int codigo)
        {
            /*SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_autorizacaoCliente where id_cliente=@idcliente and id_tipopagamento=@idtipopagamento";
            cmd.Parameters.AddWithValue("@idcliente", modelo.IdCliente);
            cmd.Parameters.AddWithValue("@idtipopagamento", modelo.IdTipoPagamento);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();*/
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_autorizacaoCliente where id_pedido = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        public ModeloAutorizacaoCliente CarregarModeloAutorizacaoCliente(int codigo)
        {
            ModeloAutorizacaoCliente modelo = new ModeloAutorizacaoCliente();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_autorizacaoCliente where id_cliente= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                modelo.Id = Convert.ToInt32(registro["id"]);
                modelo.IdCliente = Convert.ToInt32(registro["id_cliente"]);
                modelo.IdTipoPagamento = Convert.ToInt32(registro["id_tipopagamento"]);
                modelo.FlagAutorizado = registro["st_flagAutorizacao"].ToString();
            }

            conexao.Desconectar();

            return modelo;


        }
    }
}
