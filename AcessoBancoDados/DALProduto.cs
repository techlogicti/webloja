﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALProduto
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALProduto(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int ObtemSaldoAtualProdutoPorId(int id)
        {
            int saldo = 0;


            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from vw_produtosEstoque where id = @codigo";
            cmd.Parameters.AddWithValue("@codigo", id);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                /*registro.Read();*/
                saldo = Convert.ToInt32(registro["saldo"]);
            }


            return saldo;
        }

        public int Incluir(ModeloProduto modelo)
        {
            String sql = "";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "";

            sql = "insert into tb_produto values( ";
            sql += "@codigoEAN, ";
            sql += "@idEmpresa, ";
            sql += "@idGrupo, ";
            sql += "@idDepartamento, ";
            sql += "@idCategoria, ";
            sql += "@idSubCategoria, ";
            sql += "@idMarca, ";
            sql += "@idTipoEstoque, ";
            sql += "@idUnidadeMedida, ";
            sql += "@nomeProduto,";
            sql += "@referenciaProduto,";
            sql += "@codigoNCM,";
            sql += "@precoCompra,";
            sql += "@precoCompraImposto,";
            sql += "@precoVenda,";
            sql += "@precoMinimo,";
            sql += "@precoAtacado,";
            sql += "@estoqueAtual,";
            sql += "@estoqueMinimo,";
            sql += "@estoqueMaximo,";
            sql += "@estoqueAtacado,";
            sql += "@cest,";
            sql += "@pis,";
            sql += "@cofins,";
            sql += "@status,";
            sql += "@cdOperador,";
            sql += "@dataCriacao,";
            sql += "@horaCriacao,";
            sql += "@dataAtualizacao,";
            sql += "@horaAtualizacao,";
            sql += "@situacaoRegistro,";
            sql += "@observacoes";

            sql +=" ); select @@IDENTITY;";

            cmd.CommandText = sql.ToString();

           
            cmd.Parameters.AddWithValue("@codigoEAN", modelo.codigoEAN);

            
            cmd.Parameters.AddWithValue("@idTipoEstoque", modelo.idTipoEstoque);
            cmd.Parameters.AddWithValue("@idUnidadeMedida", modelo.idUnidadeMedida);
            cmd.Parameters.AddWithValue("@idEmpresa", modelo.idEmpresa);
            cmd.Parameters.AddWithValue("@idGrupo", modelo.idGrupo);
            cmd.Parameters.AddWithValue("@idDepartamento", modelo.idDepartamento);
            cmd.Parameters.AddWithValue("@idCategoria", modelo.idCategoria);
            cmd.Parameters.AddWithValue("@idSubCategoria", modelo.idSubCategoria);
            cmd.Parameters.AddWithValue("@idMarca", modelo.idMarca);

            cmd.Parameters.AddWithValue("@nomeProduto", modelo.nmNome);
            cmd.Parameters.AddWithValue("@referenciaProduto", modelo.deDescricao);
            cmd.Parameters.AddWithValue("@codigoNCM", modelo.codigoNCM);

            cmd.Parameters.AddWithValue("@precoCompra", modelo.precoCompra);
            cmd.Parameters.AddWithValue("@precoCompraImposto", modelo.precoCompraImposto);
            cmd.Parameters.AddWithValue("@precoVenda", modelo.precoVenda);
            cmd.Parameters.AddWithValue("@precoMinimo", modelo.precoMinimo);
            cmd.Parameters.AddWithValue("@precoAtacado", modelo.precoAtacado);

            cmd.Parameters.AddWithValue("@estoqueMinimo", modelo.estoqueMinimo);
            cmd.Parameters.AddWithValue("@estoqueMaximo", modelo.estoqueMaximo);
            cmd.Parameters.AddWithValue("@estoqueAtual", modelo.estoqueAtual);
            cmd.Parameters.AddWithValue("@estoqueAtacado", modelo.estoqueAtacado);

            cmd.Parameters.AddWithValue("@cest", modelo.cest);
            cmd.Parameters.AddWithValue("@pis", modelo.pis);
            cmd.Parameters.AddWithValue("@cofins", modelo.cofins);

            cmd.Parameters.AddWithValue("@status", modelo.status);

            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@dataCriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaCriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataAtualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaAtualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoRegistro", "A");
            cmd.Parameters.AddWithValue("@observacoes", modelo.DE_observacoes);
            

            conexao.Conectar();
            modelo.id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.id);

            
        }


        public int Alterar(ModeloProduto modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;

            int t = 0;

            string sql = "update tb_produto set ";

            sql += "codigo         = @codigoEAN, ";
            sql += "id_Empresa      = @idEmpresa, ";
            sql += "id_Grupo        = @idGrupo, ";
            sql += "id_Departamento = @idDepartamento, ";
            sql += "id_Categoria    = @idCategoria, ";
            sql += "id_SubCategoria = @idSubCategoria, ";
            sql += "id_Marca        = @idMarca, ";
            sql += "id_TipoEstoque  = @idTipoEstoque, ";
            sql += "id_UnidadeMedida= @idUnidadeMedida, ";
            sql += "nm_nome        = @nomeProduto,";
            sql += "de_descricao   = @referenciaProduto,";
            sql += "codigoNCM      = @codigoNCM,";
            sql += "precoCompra    = @precoCompra,";
            sql += "precoCompraImposto = @precoCompraImposto,";
            sql += "precoVenda     = @precoVenda,";
            sql += "precoMinimo    = @precoMinimo,";
            sql += "precoAtacado   = @precoAtacado,";
            sql += "estoqueAtual   = @estoqueAtual,";
            sql += "estoqueMinimo  = @estoqueMinimo,";
            sql += "estoqueMaximo  = @estoqueMaximo,";
            sql += "estoqueAtacado = @estoqueAtacado,";
            sql += "cest           = @cest,";
            sql += "pis            = @pis,";
            sql += "cofins         = @cofins,";
            sql += "status=@status,";
            sql += "CD_operador=@cdOperador,";
            sql += "DT_dataCriacao=@dataCriacao,";
            sql += "HR_horaCriacao=@horaCriacao,";
            sql += "DT_dataAtualizacao=@dataAtualizacao,";
            sql += "HR_horaAtualizacao=@horaAtualizacao,";
            sql += "ST_situacaoRegistro=@situacaoRegistro,";
            sql += "de_observacoes=@observacoes";
            

            sql += " where id= @id ";

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@id", modelo.id);

            cmd.Parameters.AddWithValue("@codigoEAN", modelo.codigoEAN);
            cmd.Parameters.AddWithValue("@idEmpresa", modelo.idEmpresa);
            cmd.Parameters.AddWithValue("@idGrupo", modelo.idGrupo);
            cmd.Parameters.AddWithValue("@idDepartamento", modelo.idDepartamento);
            cmd.Parameters.AddWithValue("@idCategoria", modelo.idCategoria);
            cmd.Parameters.AddWithValue("@idSubCategoria", modelo.idSubCategoria);
            cmd.Parameters.AddWithValue("@idMarca", modelo.idMarca);
            cmd.Parameters.AddWithValue("@idTipoEstoque", modelo.idTipoEstoque);
            cmd.Parameters.AddWithValue("@idUnidadeMedida", modelo.idUnidadeMedida);
            cmd.Parameters.AddWithValue("@nomeProduto", modelo.nmNome);
            cmd.Parameters.AddWithValue("@referenciaProduto", modelo.deDescricao);
            cmd.Parameters.AddWithValue("@codigoNCM", modelo.codigoNCM);
            cmd.Parameters.AddWithValue("@precoCompra", modelo.precoCompra);
            cmd.Parameters.AddWithValue("@precoCompraImposto", modelo.precoCompraImposto);
            cmd.Parameters.AddWithValue("@precoVenda", modelo.precoVenda);
            cmd.Parameters.AddWithValue("@precoMinimo", modelo.precoMinimo);
            cmd.Parameters.AddWithValue("@precoAtacado", modelo.precoAtacado);
            cmd.Parameters.AddWithValue("@estoqueAtual", modelo.estoqueAtual);
            cmd.Parameters.AddWithValue("@estoqueMinimo", modelo.estoqueMinimo);
            cmd.Parameters.AddWithValue("@estoqueMaximo", modelo.estoqueMaximo);
            cmd.Parameters.AddWithValue("@estoqueAtacado", modelo.estoqueAtacado);
            cmd.Parameters.AddWithValue("@cest", modelo.cest);
            cmd.Parameters.AddWithValue("@pis", modelo.pis);
            cmd.Parameters.AddWithValue("@cofins", modelo.cofins);
            cmd.Parameters.AddWithValue("@status", modelo.status);

            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@dataCriacao", modelo.DT_dataCriacao);
            cmd.Parameters.AddWithValue("@horaCriacao", modelo.HR_horaCriacao);
            cmd.Parameters.AddWithValue("@dataAtualizacao", modelo.DT_dataAtualizacao);
            cmd.Parameters.AddWithValue("@horaAtualizacao", modelo.HR_horaAtualizacao);
            cmd.Parameters.AddWithValue("@situacaoRegistro", modelo.ST_situacaoRegistro);

            cmd.Parameters.AddWithValue("@observacoes", modelo.DE_observacoes);
            

           /* conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();*/

            conexao.Conectar();
            t = Convert.ToInt32(cmd.ExecuteNonQuery());
            conexao.Desconectar();

            return t;

        }

        public void Excluir(int id)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_produto where id= @id";
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_produto where id = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        

        public ModeloProduto CarregarModeloProduto(int codigo)
        {
            ModeloProduto modelo = new ModeloProduto();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_produto where id = @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();

                modelo.idEmpresa = Convert.ToInt32(registro["id_empresa"]);

                modelo.id = Convert.ToInt32(registro["id"]);
                modelo.codigoEAN = registro["codigo"].ToString();
                modelo.nmNome = registro["nm_nome"].ToString();
                modelo.deDescricao = registro["de_descricao"].ToString();



                modelo.idMarca = Convert.ToInt32(registro["id_marca"]);
                modelo.idDepartamento = Convert.ToInt32(registro["id_departamento"]);
                modelo.idCategoria = Convert.ToInt32(registro["id_categoria"]);
                modelo.idSubCategoria = Convert.ToInt32(registro["id_subcategoria"]);

                modelo.codigoNCM = registro["codigoNCM"].ToString();
                modelo.idGrupo = Convert.ToInt32(registro["id_grupo"]);
                modelo.idUnidadeMedida = Convert.ToInt32(registro["id_unidadeMedida"]);
                modelo.idTipoEstoque = Convert.ToInt32(registro["id_tipoEstoque"]);

                modelo.precoCompra = registro["precoCompra"].ToString();
                modelo.precoCompraImposto = registro["precoCompraImposto"].ToString();
                modelo.precoMinimo = registro["precoMinimo"].ToString();
                modelo.precoAtacado = registro["precoAtacado"].ToString();
                modelo.precoVenda = registro["precoVenda"].ToString();
                modelo.estoqueAtacado = Convert.ToInt32(registro["estoqueAtacado"]);

                modelo.cest = registro["cest"].ToString();
                modelo.pis = Convert.ToInt32(registro["pis"].ToString());
                modelo.cofins = Convert.ToInt32(registro["cofins"].ToString());

                //modelo.estoqueFinal = Convert.ToInt32(registro["estoqueFinal"]);
                modelo.status = registro["status"].ToString();


                modelo.DE_observacoes = registro["DE_observacoes"].ToString();
            }

            conexao.Desconectar();

            return modelo;

        }

        }
}



