﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjetoTransferencia;
using System.Data.SqlClient;
using System.Data;

namespace AcessoBancoDados
{
   public class DALFornecedor
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALFornecedor(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloFornecedor modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "insert into TB_fornecedor values(@cpfcnpj, @tipopessoa, @razaosocial, @nomefantasia, @cdUf, @cdOperador, @datacriacao, @horacriacao, @dataatualizacao, @horaatualizacao, @situacaoregistro ); select @@IDENTITY;";

            cmd.Parameters.AddWithValue("@cpfcnpj", modelo.CpfCnpj);
            cmd.Parameters.AddWithValue("@tipopessoa", modelo.TipoPessoa);
            cmd.Parameters.AddWithValue("@razaosocial", modelo.RazaoSocial);
            cmd.Parameters.AddWithValue("@nomefantasia", modelo.NomeFantasia);
            cmd.Parameters.AddWithValue("@cdUf", "-");
            cmd.Parameters.AddWithValue("@cdOperador", 1);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");
            
            conexao.Conectar();
            modelo.IdFornecedor = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.IdFornecedor);
        }

        public void Alterar(ModeloFornecedor modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_fornecedor set campo=@campo where id_= @codigo";
            cmd.Parameters.AddWithValue("@campo", modelo.NomeFantasia);
            cmd.Parameters.AddWithValue("@codigo", modelo.IdFornecedor);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int codigo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_itenspedido where id_= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_fornecedor where id_pedido = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }


        

        public ModeloFornecedor CarregarModeloFornecedor(int codigo)
        {
            ModeloFornecedor modelo = new ModeloFornecedor();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_fornecedor where idFornecedor= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                modelo.IdFornecedor = Convert.ToInt32(registro["idFornecedor"]);
                modelo.RazaoSocial = registro["de_razaoSocial"].ToString();
                modelo.NomeFantasia = registro["de_nomeFantasia"].ToString();
            }

            conexao.Desconectar();

            return modelo;

        
        }
    }
}
