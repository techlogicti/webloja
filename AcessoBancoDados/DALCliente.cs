﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALCliente
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALCliente(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloCliente modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;

            string sqlInsertCliente = "";
            
            sqlInsertCliente = " insert into tb_cliente values(@cd_codigo, @nm_nomecompleto, @nm_apelido, @dt_datanascimento, @cd_estadocivil, @nm_conjuge,";
            sqlInsertCliente += " @cd_profissaoconjuge,@de_nacionalidade, @de_naturalidade,@nm_nomemae,@nm_nomepai,@nu_cpfCnpj, @nu_numeroidentidade, ";
            sqlInsertCliente += " @dt_emissaoidentidade, @cd_ufemissoridentidade, @de_orgaoemissoridentidade, @nu_numerocnh, @dt_dataemissaocnh, @cd_ufemissorcnh,";
            sqlInsertCliente += " @cd_categoriacnh, @cd_pais, @cd_uf, @cd_cidade, @de_bairro, @de_logradouro, @de_numero, @de_complemento, @de_pontodereferencia,";
            sqlInsertCliente += " @de_numerocep, @de_empresa, @nu_telefonecomercial, @nm_responsavelcomercial, @cd_tipoatividade, @cd_profissao, @dt_dataadmissao, ";
            sqlInsertCliente += " @vl_ordenado, @vl_ultimacompra, @dt_ultimacompra, @vl_creditoLoja, @nu_telefonefixo, @nu_celular1, @nu_celular2, @nu_celular3, @nu_celular4 ";
            sqlInsertCliente += " @de_email, @nu_telefonerecado, @nm_nomerecado, @de_parentescorecado, @de_observacoes, @cd_operador, @datacriacao, @horacriacao, ";
            sqlInsertCliente += "@dataatualizacao, @horaatualizacao, @situacaoregistro, @de_profissaocliente, @de_profissaoconjuge ); select @@IDENTITY;";

            cmd.CommandText  = sqlInsertCliente.ToString();

            
            

            /*sqlInsertCliente += v_codFinancial.ToString() + " , ";
            sqlInsertCliente += "'" + v_nomeCliente.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_apelido.ToString() + "'" + " , ";
            if (v_dataNascimento.Equals("")) { v_dataNascimento = "0"; }
            sqlInsertCliente += v_dataNascimento.ToString() + " , ";
            sqlInsertCliente += v_estadoCivil.ToString() + " , ";
            sqlInsertCliente += "'" + v_nomeConjuge.ToString() + "'" + " , ";
            //sqlInsertCliente += v_profissaoConjuge.ToString() + " , ";
            sqlInsertCliente += "0 , ";
            sqlInsertCliente += "'" + v_nacionalidade.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_naturalidade.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_nomeMae.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_nomePai.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_cpf.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_numeroIdentidade.ToString() + "'" + " , ";
            sqlInsertCliente += v_dataEmissaoIdentidade.ToString() + " , ";
            sqlInsertCliente += v_ufEmissorIdentidade.ToString() + " , ";
            sqlInsertCliente += "'" + v_orgaoEmissorIdentidade.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_numeroCNH.ToString() + "'" + " , ";
            sqlInsertCliente += v_dataEmissaoCNH.ToString() + " , ";
            sqlInsertCliente += v_ufEmissorCNH.ToString() + " , ";
            sqlInsertCliente += v_categoriaCNH.ToString() + " , ";
            sqlInsertCliente += v_pais.ToString() + " , ";
            sqlInsertCliente += v_uf.ToString() + " , ";
            sqlInsertCliente += v_cidade.ToString() + " , ";
            sqlInsertCliente += "'" + v_bairro.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_endereco.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_numero.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_complemento.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_referencia.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_cep.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_empresa.ToString() + "'" + " , ";
            sqlInsertCliente += v_telefoneComercial.ToString() + " , ";
            sqlInsertCliente += "'" + v_nomeResponsavel.ToString() + "'" + " , ";
            sqlInsertCliente += v_tipoAtividade.ToString() + " , ";
            //sqlInsertCliente += v_profissao.ToString() + " , ";
            sqlInsertCliente += "0 , ";
            sqlInsertCliente += v_dataAdmissao.ToString() + " , ";
            sqlInsertCliente += v_salario.ToString() + " , ";
            sqlInsertCliente += "0 , ";
            sqlInsertCliente += "0 , ";
            sqlInsertCliente += "0 , ";
            sqlInsertCliente += v_telefoneFixo.ToString() + " , ";
            sqlInsertCliente += v_celular1.ToString() + " , ";
            sqlInsertCliente += v_celular2.ToString() + " , ";
            sqlInsertCliente += v_celular3.ToString() + " , ";
            sqlInsertCliente += v_celular4.ToString() + " , ";
            sqlInsertCliente += "'" + v_email.ToString() + "'" + " , ";
            sqlInsertCliente += v_telRecado.ToString() + " , ";
            sqlInsertCliente += "'" + v_nomeRecado.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_parentescoRecado.ToString() + "'" + " , ";
            sqlInsertCliente += "'" + v_outros.ToString() + "'" + " , ";
            sqlInsertCliente += "1,";
            sqlInsertCliente += dataHoje + " , "; ;
            sqlInsertCliente += horaHoje + " , "; ;
            sqlInsertCliente += dataHoje + " , "; ;
            sqlInsertCliente += horaHoje + " , "; ;
            sqlInsertCliente += "'A' " + ", ";
            sqlInsertCliente += "'" + v_profissao + "' , "; ;
            sqlInsertCliente += "'" + v_profissaoConjuge + "'  "; ;

            sqlInsertCliente += " ); ";*/
            
            

            /*cmd.Parameters.AddWithValue("@cpfcnpj", modelo.CpfCnpj);
            cmd.Parameters.AddWithValue("@tipopessoa", modelo.TipoPessoa);
            cmd.Parameters.AddWithValue("@razaosocial", modelo.RazaoSocial);
            cmd.Parameters.AddWithValue("@nomefantasia", modelo.NomeFantasia);*/
            cmd.Parameters.AddWithValue("@cdUf", "-");
            cmd.Parameters.AddWithValue("@cdOperador", 1);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");

            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.Id);
        }

        public void Alterar(ModeloCliente modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_itenspedido set campo=@campo where id_= @codigo";
            /*cmd.Parameters.AddWithValue("@campo", modelo.NomeFantasia);
            cmd.Parameters.AddWithValue("@codigo", modelo.IdFornecedor);*/
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int codigo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_itenspedido where id_= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_itenspedido where id_pedido = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }

        public ModeloCliente CarregarModeloCliente(int codigo)
        {
            ModeloCliente modelo = new ModeloCliente();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_cliente where id= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();

                modelo.NU_cpfCnpj = registro["nu_cpfcnpj"].ToString();
                modelo.NM_nomeCompleto = registro["nm_nomecompleto"].ToString();

                /*modelo.IdFornecedor = Convert.ToInt32(registro["id_pedido"]);
                modelo.RazaoSocial = registro["id_itenspedido"].ToString();
                modelo.NomeFantasia = registro["id_produto"].ToString();*/
            }

            conexao.Desconectar();

            return modelo;


        }
    }
}
