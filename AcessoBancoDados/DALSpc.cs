﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALSpc
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALSpc(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloSpc modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "insert into TB_spc values(@idCliente, @dataRegistro, @motivo, @observacao, @cdOperador, @datacriacao, @horacriacao, @dataatualizacao, @horaatualizacao, @situacaoregistro ); select @@IDENTITY;";

            cmd.Parameters.AddWithValue("@idCliente", modelo.Id_cliente);
            cmd.Parameters.AddWithValue("@dataRegistro", modelo.DT_dataRegistro);
            cmd.Parameters.AddWithValue("@motivo", modelo.DE_Motivo);
            cmd.Parameters.AddWithValue("@observacao", modelo.DE_observacao);
            cmd.Parameters.AddWithValue("@cdUf", "-");
            cmd.Parameters.AddWithValue("@cdOperador", 1);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");

            conexao.Conectar();
            modelo.Id = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.Id);
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_spc where id_cliente = @valor", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }




        public ModeloSpc CarregarModeloSpc(int codigo)
        {
            ModeloSpc modelo = new ModeloSpc();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_spc where id_cliente= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                modelo.Id = Convert.ToInt32(registro["id"]);
                modelo.Id_cliente = Convert.ToInt32(registro["id_cliente"]);
                modelo.DT_dataRegistro = Convert.ToInt32(registro["DT_dataRegistro"]);
                modelo.DE_Motivo = registro["de_motivo"].ToString();
                modelo.DE_observacao = registro["de_observacao"].ToString();
            }

            conexao.Desconectar();

            return modelo;


        }
    }
}
