﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALMarca
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALMarca(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int Incluir(ModeloMarca modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "insert into tb_marca values(@nome, @descricao, @cdOperador, @datacriacao, @horacriacao, @dataatualizacao, @horaatualizacao, @situacaoregistro ); select @@IDENTITY;";

            cmd.Parameters.AddWithValue("@nome", modelo.Descricao_curta);
            cmd.Parameters.AddWithValue("@descricao", modelo.Descricao_longa);
            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@datacriacao", dataHoje);
            cmd.Parameters.AddWithValue("@horacriacao", horaHoje);
            cmd.Parameters.AddWithValue("@dataatualizacao", dataHoje);
            cmd.Parameters.AddWithValue("@horaatualizacao", horaHoje);
            cmd.Parameters.AddWithValue("@situacaoregistro", "A");

            conexao.Conectar();
            modelo.IdMarca = Convert.ToInt32(cmd.ExecuteScalar());
            conexao.Desconectar();

            return Convert.ToInt32(modelo.IdMarca);
        }

        public void Alterar(ModeloMarca modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "update tb_marca set nm_nome=@campo, de_descricao=@descricao where idmarca= @codigo";
            cmd.Parameters.AddWithValue("@nome", modelo.Descricao_curta);
            cmd.Parameters.AddWithValue("@descricao", modelo.Descricao_longa);
            cmd.Parameters.AddWithValue("@codigo", modelo.IdMarca);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public void Excluir(int codigo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "delete from tb_marca where id_= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();
            cmd.ExecuteNonQuery();
            conexao.Desconectar();
        }

        public DataTable Localizar(String valor)
        {
            DataTable tabela = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter("select * from tb_marca where idmarca = @codigo", conexao.StringConexao);
            da.Fill(tabela);
            return tabela;
        }




        public ModeloMarca CarregarModeloMarca(int codigo)
        {
            ModeloMarca modelo = new ModeloMarca();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from tb_marca where idmarca= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();
            if (registro.HasRows)
            {
                registro.Read();
                modelo.IdMarca = Convert.ToInt32(registro["idFornecedor"]);
                modelo.Descricao_curta = registro["nm_nome"].ToString();
                modelo.Descricao_longa = registro["de_descricao"].ToString();
            }

            conexao.Desconectar();

            return modelo;


        }
    }
}
