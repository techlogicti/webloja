﻿using ObjetoTransferencia;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcessoBancoDados
{
    public class DALMovimentoEstoque
    {
        private DALConexao conexao;

        string dataHoje = String.Format("{0:yyyyMMdd}", DateTime.Now.Date);
        string horaHoje = String.Format("{0:HHmmss}", DateTime.Now);

        public DALMovimentoEstoque(DALConexao cx)
        {
            this.conexao = cx;
        }

        public int RetornaItensPedidoCancelado(ModeloMovimentoEstoque modelo)
        {
            int retorno = 0;

            return retorno;
        }

        public List<ModeloMovimentoEstoque> RecuperaItensMovimento(int id)
        {

            ModeloMovimentoEstoque modelo = new ModeloMovimentoEstoque();
            List<ModeloMovimentoEstoque> lista = new List<ModeloMovimentoEstoque>();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;
            cmd.CommandText = "select * from TB_movimentoEstoque where id = @id";
            cmd.Parameters.AddWithValue("@id", id);
            conexao.Conectar();

            SqlDataReader registro = cmd.ExecuteReader();

            while (registro.HasRows)
            {
                registro.Read();
                modelo.Id = Convert.ToInt32(registro["id"]);
                modelo.IdDocumento = Convert.ToInt32(registro["id_documento"]);
                modelo.IdProduto = Convert.ToInt32(registro["id_produto"]);
                modelo.IdTabela = Convert.ToInt32(registro["id_tabela"]);
                modelo.IdLocalDestino = Convert.ToInt32(registro["id_localDestino"]);
                modelo.IdLocalOrigem = Convert.ToInt32(registro["id_localOrigem"]);
                modelo.Quantidade = Convert.ToInt32(registro["QT_quantidade"]);
                

                lista.Add(modelo);
            }

            conexao.Desconectar();

            return lista;

        }

        public int Incluir(ModeloMovimentoEstoque modelo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao.ObjetoConexao;

            string sql = "";

            sql = "insert into tb_movimentoEstoque values( ";
            sql += "@idTabela, ";
            sql += "@idDocumento, ";
            sql += "@idProduto, ";
            sql += "@idLocalOrigem, ";
            sql += "@idLocalDestino, ";
            sql += "@quantidade, ";
            sql += "@tipoMovimento, ";
            sql += "@dataMovimento, ";
            sql += "@cdOperador,";
            sql += "@dataCriacao,";
            sql += "@horaCriacao,";
            sql += "@dataAtualizacao,";
            sql += "@horaAtualizacao,";
            sql += "@situacaoRegistro";

            //sql += " ); select @@IDENTITY;";
            sql += " ); select SCOPE_IDENTITY();";

            //SCOPE_IDENTITY()

            cmd.CommandText = sql.ToString();

            cmd.Parameters.AddWithValue("@idTabela", modelo.IdTabela);
            cmd.Parameters.AddWithValue("@idDocumento", modelo.IdDocumento);
            cmd.Parameters.AddWithValue("@idProduto", modelo.IdProduto);
            cmd.Parameters.AddWithValue("@idLocalOrigem", modelo.IdLocalOrigem);
            cmd.Parameters.AddWithValue("@idLocalDestino", modelo.IdLocalDestino);
            cmd.Parameters.AddWithValue("@quantidade", modelo.Quantidade);
            cmd.Parameters.AddWithValue("@tipoMovimento", modelo.TipoMovimento);
            cmd.Parameters.AddWithValue("@dataMovimento", modelo.DataMovimento);
            cmd.Parameters.AddWithValue("@cdOperador", modelo.CD_operador);
            cmd.Parameters.AddWithValue("@dataCriacao", modelo.DT_dataCriacao);
            cmd.Parameters.AddWithValue("@horaCriacao", modelo.HR_horaCriacao);
            cmd.Parameters.AddWithValue("@dataAtualizacao", modelo.DT_dataAtualizacao);
            cmd.Parameters.AddWithValue("@horaAtualizacao", modelo.HR_horaAtualizacao);
            cmd.Parameters.AddWithValue("@situacaoRegistro", modelo.ST_situacaoRegistro);

            conexao.Conectar();

            int qtdLinhasAfetadas = 0;
            int idMovimentoGerado = 0;


            //this.gravarLog("Execução do sql: " + sql.ToString());

            try
            {
                //qtdLinhasAfetadas = Convert.ToInt32(cmd.ExecuteNonQuery());
                idMovimentoGerado = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }



            conexao.Desconectar();

            return idMovimentoGerado;
        }
    }
}
